
// DlgProxy.cpp: archivo de implementación
//

#include "stdafx.h"
#include "MFCApplication1.h"
#include "DlgProxy.h"
#include "MFCApplication1Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCApplication1DlgAutoProxy

IMPLEMENT_DYNCREATE(CMFCApplication1DlgAutoProxy, CCmdTarget)

CMFCApplication1DlgAutoProxy::CMFCApplication1DlgAutoProxy()
{
	EnableAutomation();
	
	// Para que la aplicación se ejecute durante el tiempo de que un objeto de 
	//	automatización está activo, el constructor llama a AfxOleLockApp.
	AfxOleLockApp();

	// Obtener acceso al cuadro de diálogo a través del puntero
	//  de la ventana principal de la aplicación.  Establecer el puntero interno del servidor proxy
	//  para que apunte al cuadro de diálogo y volver a establecer el puntero
	//  del cuadro de diálogo a este servidor proxy.
	ASSERT_VALID(AfxGetApp()->m_pMainWnd);
	if (AfxGetApp()->m_pMainWnd)
	{
		ASSERT_KINDOF(CMFCApplication1Dlg, AfxGetApp()->m_pMainWnd);
		if (AfxGetApp()->m_pMainWnd->IsKindOf(RUNTIME_CLASS(CMFCApplication1Dlg)))
		{
			m_pDialog = reinterpret_cast<CMFCApplication1Dlg*>(AfxGetApp()->m_pMainWnd);
			m_pDialog->m_pAutoProxy = this;
		}
	}
}

CMFCApplication1DlgAutoProxy::~CMFCApplication1DlgAutoProxy()
{
	// Para terminar la aplicación cuando terminen todos los objetos creados con
	// 	automatización, el destructor llama a AfxOleUnlockApp.
	//  Entre otras cosas, esta operación destruirá el cuadro de diálogo principal
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CMFCApplication1DlgAutoProxy::OnFinalRelease()
{
	// Cuando se libera la última referencia para un objeto de automatización,
	// se llama a OnFinalRelease.  La clase base eliminará automáticamente
	// el objeto.  Se requiere limpieza adicional para el
	// objeto antes de llamar a la clase base.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CMFCApplication1DlgAutoProxy, CCmdTarget)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CMFCApplication1DlgAutoProxy, CCmdTarget)
END_DISPATCH_MAP()

// Nota: se agrega compatibilidad para IID_IMFCApplication1 para admitir enlaces con seguridad de tipos
//  desde VBA. IID debe coincidir con el id. GUID adjuntado a 
//  la interfaz dispinterface del archivo .IDL.

// {3C9C71E3-B8C9-4B9F-B97E-7D9CF6FBF907}
static const IID IID_IMFCApplication1 =
{ 0x3C9C71E3, 0xB8C9, 0x4B9F, { 0xB9, 0x7E, 0x7D, 0x9C, 0xF6, 0xFB, 0xF9, 0x7 } };

BEGIN_INTERFACE_MAP(CMFCApplication1DlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CMFCApplication1DlgAutoProxy, IID_IMFCApplication1, Dispatch)
END_INTERFACE_MAP()

// La macro IMPLEMENT_OLECREATE2 se define en el archivo StdAfx.h de este proyecto
// {EF4A32E5-FFDC-4129-95B2-C41197224E00}
IMPLEMENT_OLECREATE2(CMFCApplication1DlgAutoProxy, "MFCApplication1.Application", 0xef4a32e5, 0xffdc, 0x4129, 0x95, 0xb2, 0xc4, 0x11, 0x97, 0x22, 0x4e, 0)


// Controladores de mensaje de CMFCApplication1DlgAutoProxy
