
// MFCApplication1Dlg.h: archivo de encabezado
//

#pragma once

class CMFCApplication1DlgAutoProxy;


// Cuadro de di�logo de CMFCApplication1Dlg
class CMFCApplication1Dlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMFCApplication1Dlg);
	friend class CMFCApplication1DlgAutoProxy;

// Construcci�n
public:
	CMFCApplication1Dlg(CWnd* pParent = NULL);	// Constructor est�ndar
	virtual ~CMFCApplication1Dlg();

// Datos del cuadro de di�logo
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCAPPLICATION1_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	CMFCApplication1DlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;

	BOOL CanExit();

	

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButprova1();
	void thinningIteration(cv::Mat & img, int iter);
	void ZhangSuen(const cv::Mat & src, cv::Mat & dst);
	afx_msg void OnBnClickedButton2();
};
