
// DlgProxy.h: archivo de encabezado
//

#pragma once

class CMFCApplication1Dlg;


// Destino del comando de CMFCApplication1DlgAutoProxy

class CMFCApplication1DlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CMFCApplication1DlgAutoProxy)

	CMFCApplication1DlgAutoProxy();           // Constructor protegido utilizado por la creaci�n din�mica

// Atributos
public:
	CMFCApplication1Dlg* m_pDialog;

// Operaciones
public:

// Reemplazos
	public:
	virtual void OnFinalRelease();

// Implementaci�n
protected:
	virtual ~CMFCApplication1DlgAutoProxy();

	// Funciones de asignaci�n de mensajes generadas

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CMFCApplication1DlgAutoProxy)

	// Funciones de asignaci�n de env�o OLE generadas

	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

