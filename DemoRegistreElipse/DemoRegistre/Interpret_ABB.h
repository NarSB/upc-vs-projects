#ifndef INTERPRET
#define INTERPRET

#include "math.h"
//using namespace std;

namespace Interpret
{	
	CString incremental_joints(double joint1, double joint2, double joint3, double joint4, double joint5, double joint6, int code);
	CString incremental_cartesian(double x, double y, double z, double alfa, double beta, double gamma, int code);
	CString incremental_tool(double x, double y, double z, double alfa, double beta, double gamma, int code);
	CString absolute_joints(double joint1, double joint2, double joint3, double joint4, double joint5, double joint6, int code);
	CString absolute_joints_mil(double joint1, double joint2, double joint3, double joint4, double joint5, double joint6, int code);
	CString absolute_joints_zone(double joint1, double joint2, double joint3, double joint4, double joint5, double joint6, int zone, int code);
	CString absolute_cartesian(double x, double y, double z, double alfa, double beta, double gamma, int code);
	CString absolute_cart_quat_zone(double x, double y, double z, double qw, double qx, double qy, double qz,int zone, int code);
	CString absolute_cart_quat(double x, double y, double z, double qw, double qx, double qy, double qz, int code);
	CString initial_position(int code);
	CString vertical_position(int code);
	CString reset(int code);	
	CString open(int code);
	CString close(int code);
	CString tool(double x, double y, double z, int code);
	CString tool(double x, double y, double z, double alfa, double beta, double gamma, int code);
	CString close_connexion(int code);
	void capture_data(CString msg, double *x, double *y, double *z,
		double *alfa, double *beta, double *gamma,
		double *joint1,	double *joint2, double *joint3, double *joint4, double *joint5, double *joint6,
		int *error, CString *err, int *code,double *qw,double *qx,double *qy,double *qz);
	void capture_data(CString msg, double *pose, double *joints, int *error, CString *err, int *code);
	void capture_data(CString msg, double *x, double *y, double *z,
		double *alfa, double *beta, double *gamma, double *joint1,
		double *joint2, double *joint3, double *joint4,
		double *joint5, double *joint6, int *error, CString *err, int *code);
	//FUNCIONS ESPECIFIQUES ABB
	CString ABB_SetVelocity(int vel, int code);
	CString ABB_SetZone(int vel, int code);
}
#endif