#include "stdafx.h"
#include "Interpret_ABB.h"

/**	\brief Increases the robot's task space configration.
 *	\param x Increase in coordinate x (in mm).
 *	\param y Increase in coordinate y (in mm).
 *	\param z Increase in coordinate z (in mm).
 *	\param alfa Increase in angle Z (in degrees).
 *	\param beta Increase in angle Y' (in degrees).
 *	\param gamma Increase in angle Z'' (in degrees).
 *	\return a CString to be sent to the robot's controller.
 *
 *	Increases the robot's configration. Increase in position is given un cartesian
 *	coordinates and increase in orientation in the ZY'Z'' Euler parameters.
 */
CString Interpret::incremental_cartesian(double x, double y, double z, double alfa, double beta, double gamma, int code)
{
	CString Dades;
	long d[6];
	long checkSum = 1;

	d[0] = long(x*1000);
	d[1] = long(y*1000);
	d[2] = long(z*1000);
	d[3] = long(alfa*1000);
	d[4] = long(beta*1000);
	d[5] = long(gamma*1000);

	for (int i=0;i<6;i++)
		checkSum += d[i];

	checkSum *= 211;			
	Dades.Format ("1 %d %d %d %d %d %d %d %d\r\n",d[0],d[1],d[2],d[3],d[4],d[5],checkSum, code);

	return (Dades);
}

/**	\brief Moves the robot to the cartesian configuration (,,,,,).
 *	\return a CString to be sent to the robot's controller.
 *
 *	Moves the robot to the cartesian configuration (,,,,,).
 */
CString Interpret::initial_position(int code)
{
	CString Dades;
	long checkSum =422;
	Dades.Format ("2 0 0 0 0 0 0 %d %d\r\n",checkSum, code);
	return (Dades);
}

/**	\brief Moves the robot to the ready configuration.
 *	\return a CString to be sent to the robot's controller.
 *
 *	Moves the robot to the ready configuration.
 */
CString Interpret::vertical_position(int code)
{
	CString Dades;
	long checkSum = 633;			
	Dades.Format ("3 0 0 0 0 0 0 %d %d\r\n",checkSum, code);
	return (Dades);
}

/**	\brief Changes the tool position.
 *	\param x Position in coordinate x (in mm).
 *	\param y Position in coordinate y (in mm).
 *	\param z Position in coordinate z (in mm).
 *	\return a CString to be sent to the robot's controller.
 *
 *	Increases the robot's configration. Increase in position is given un cartesian
 *	coordinates.
 */
CString Interpret::tool(double x, double y, double z, int code)
{
	CString Dades;
	double checkSum = 4;
	checkSum = checkSum + x + y + z;
	checkSum *= 211;
	
	Dades.Format ("4 %lf %lf %lf 0 0 0 %lf %d\r\n",x,y,z,checkSum, code);

	return (Dades);
}

/**	\brief Move to a joint space configuration.
 *	\param joint1 value of joint 1 (in degrees).
 *	\param joint2 value of joint 2 (in degrees).
 *	\param joint3 value of joint 3 (in degrees).
 *	\param joint4 value of joint 4 (in degrees).
 *	\param joint5 value of joint 5 (in degrees).
 *	\param joint6 value of joint 6 (in degrees).
 *	\return a CString to be sent to the robot's controller.
 */
CString Interpret::absolute_joints(double joint1, double joint2, double joint3, double joint4, double joint5, double joint6, int code)
{
	CString Dades;
	double checkSum = 5;
	checkSum = checkSum + joint1 + joint2 + joint3 + joint4 + joint5 + joint6;
	checkSum *= 211;			
	
	Dades.Format ("5 %.3lf %.3lf %.3lf %.3lf %.3lf %.3lf %lf %d\r\n",joint1,joint2,joint3,joint4,joint5,joint6,checkSum, code);

	//long d[6];
	//long checkSum = 5;

	//d[0] = (long)joint1;
	//d[1] = (long)joint2;
	//d[2] = (long)joint3;
	//d[3] = (long)joint4;
	//d[4] = (long)joint5;
	//d[5] = (long)joint6;

	//for (int i=0;i<6;i++)
	//	checkSum += d[i];

	//checkSum *= 211;			
	
//	Dades.Format ("5 %d %d %d %d %d %d %d %d\r\n",d[0],d[1],d[2],d[3],d[4],d[5],checkSum, code);
	return(Dades);
}


/**	\brief Move to a joint space configuration.
 *	\param joint1 value of joint 1 (in degrees).
 *	\param joint2 value of joint 2 (in degrees).
 *	\param joint3 value of joint 3 (in degrees).
 *	\param joint4 value of joint 4 (in degrees).
 *	\param joint5 value of joint 5 (in degrees).
 *	\param joint6 value of joint 6 (in degrees). 
 *	\param zone value of zone:  0,1,5,10,15,20,30,40,50,60,80,100,150,200
 *	\return a CString to be sent to the robot's controller.
 */
CString Interpret::absolute_joints_zone(double joint1, double joint2, double joint3, double joint4, double joint5, double joint6, int zone, int code)
{
	CString Dades;
	double checkSum = 17;
	checkSum = checkSum + joint1 + joint2 + joint3 + joint4 + joint5 + joint6;
	checkSum *= 211;			
	
	Dades.Format ("17 %.3lf %.3lf %.3lf %.3lf %.3lf %.3lf %d %lf %d\r\n",joint1,joint2,joint3,joint4,joint5,joint6,zone,checkSum, code);

	return(Dades);
}

CString Interpret::absolute_joints_mil(double joint1, double joint2, double joint3, double joint4, double joint5, double joint6, int code)
{
	CString Dades;
	double checkSum = 15;
	checkSum = checkSum + (int)(10000*joint1) + (int)(10000*joint2) + (int)(10000*joint3) + (int)(10000*joint4) + (int)(10000*joint5) + (int)(10000*joint6);
	checkSum *= 211;			
	
	Dades.Format ("15 %d %d %d %d %d %d %d %d\r\n",(int)(10000*joint1),(int)(10000*joint2),(int)(10000*joint3),(int)(10000*joint4),(int)(10000*joint5),(int)(10000*joint6),(long)checkSum, code);

	//long d[6];
	//long checkSum = 5;

	//d[0] = (long)joint1;
	//d[1] = (long)joint2;
	//d[2] = (long)joint3;
	//d[3] = (long)joint4;
	//d[4] = (long)joint5;
	//d[5] = (long)joint6;

	//for (int i=0;i<6;i++)
	//	checkSum += d[i];

	//checkSum *= 211;			
	
//	Dades.Format ("5 %d %d %d %d %d %d %d %d\r\n",d[0],d[1],d[2],d[3],d[4],d[5],checkSum, code);
	return(Dades);
}

/**	\brief Makes a reset in the robots position.
 *	\return a CString to be sent to the robot's controller.
 *
 *	Moves the robot to the ready configuration.
 */
CString Interpret::reset(int code)
{
	CString Dades;
	long checkSum =1266;
	Dades.Format ("6 0 0 0 0 0 0 %d %d\r\n",checkSum, code);
	return (Dades);
}


/**	\brief Move to a task space configuration.
 *	\param x value of x (in mm).
 *	\param y value of y (in mm).
 *	\param z value of z (in mm).
 *	\param alfa value of angle Z (in degrees).
 *	\param beta value of angle Y' (in degrees).
 *	\param gamma value of angle Z'' (in degrees).
 *	\return a CString to be sent to the robot's controller.
 */
CString Interpret::absolute_cartesian(double x, double y, double z, double alfa, double beta, double gamma, int code)
{
	CString Dades;
	long d[6];
	long checkSum = 7;

	d[0] = long(x*100);
	d[1] = long(y*100);
	d[2] = long(z*100);
	d[3] = long(alfa*100);
	d[4] = long(beta*100);
	d[5] = long(gamma*100);

	for (int i=0;i<6;i++)
		checkSum += d[i];

	checkSum *= 211;			
	Dades.Format ("7 %d %d %d %d %d %d %d %d\r\n",d[0],d[1],d[2],d[3],d[4],d[5],checkSum, code);

	return (Dades);
}

/**	\brief Move to a task space configuration.
 *	\param x value of x (in mm).
 *	\param y value of y (in mm).
 *	\param z value of z (in mm).
 *	\param qw value of qw .
 *	\param qx value of qx .
 *	\param qy value of qy .
 *  \param qz value of qz .
 *	\return a CString to be sent to the robot's controller.
 */

CString Interpret::absolute_cart_quat(double x, double y, double z, double qw, double qx, double qy, double qz, int code)
{
	CString Dades;
	long d[7];
	long checkSum = 16;

	d[0] = long(x*100);
	d[1] = long(y*100);
	d[2] = long(z*100);
	d[3] = long(qw*10000);
	d[4] = long(qx*10000);
	d[5] = long(qy*10000);
	d[6] = long(qz*10000);

	for (int i=0;i<7;i++)
		checkSum += d[i];

	checkSum *= 211;			
	Dades.Format ("16 %d %d %d %d %d %d %d %d %d\r\n",d[0],d[1],d[2],d[3],d[4],d[5],d[6],checkSum, code);

	return (Dades);
}

CString Interpret::absolute_cart_quat_zone(double x, double y, double z, double qw, double qx, double qy, double qz,int zone, int code)
{
	CString Dades;
	long d[7];
	long checkSum = 18;

	d[0] = long(x*100);
	d[1] = long(y*100);
	d[2] = long(z*100);
	d[3] = long(qw*10000);
	d[4] = long(qx*10000);
	d[5] = long(qy*10000);
	d[6] = long(qz*10000);

	for (int i=0;i<7;i++)
		checkSum += d[i];

	checkSum *= 211;			
	Dades.Format ("18 %d %d %d %d %d %d %d %d %d %d\r\n",d[0],d[1],d[2],d[3],d[4],d[5],d[6],zone,checkSum, code);

	return (Dades);
}

/**	\brief Orders the robot to close connection with computer.
 *	\return a CString to be sent to the robot's controller.
 */

CString Interpret::open(int code)
{
	CString Dades;
	long checkSum =1688;
	Dades.Format ("8 0 0 0 0 0 0 %d %d\r\n",checkSum, code);
	return (Dades);
}

CString Interpret::close(int code)
{
	CString Dades;
	long checkSum =1899;
	Dades.Format ("9 0 0 0 0 0 0 %d %d\r\n",checkSum, code);
	return (Dades);
}

CString Interpret::incremental_joints( double joint1, double joint2, double joint3, double joint4, double joint5, double joint6, int code)
{


	CString Dades;
	double checkSum = 10;
	checkSum = checkSum + joint1 + joint2 + joint3 + joint4 + joint5 + joint6;
	checkSum *= 211;			
	
	Dades.Format ("10 %.3lf %.3lf %.3lf %.3lf %.3lf %.3lf %lf %d\r\n",joint1,joint2,joint3,joint4,joint5,joint6,checkSum, code);
	return(Dades);
}

/**	\brief Increases the robot pos (position and orientation) in the cartesian tool's 
 *   reference frame.
 *	\param x Increase in coordinate x (in mm).
 *	\param y Increase in coordinate y (in mm).
 *	\param z Increase in coordinate z (in mm).
 *	\param alfa Increase in angle Z (in degrees).
 *	\param beta Increase in angle Y' (in degrees).
 *	\param gamma Increase in angle Z'' (in degrees).
 *	\return a CString to be sent to the robot's controller.
 *
 *	Increases the robot's pos. The increase must be given int the robot's tool reference
 *	frame.
 */
CString Interpret::incremental_tool(double x, double y, double z, double alfa, double beta, double gamma, int code)
{
	CString Dades;
	//double checkSum = 11;
	long checkSum = 11;
	long d[6];
	//x=x*1000;
	//y=y*1000;
	//z=z*1000;
	//alfa=alfa*1000;
	//beta=beta*1000;
	//gamma=gamma*1000;
	//checkSum = checkSum + x + y + z + alfa + beta + gamma;
	//checkSum *= 211;
	
	d[0]=(long)(x*1000);
	d[1]=(long)(y*1000);
	d[2]=(long)(z*1000);
	d[3]=(long)(alfa*1000);
	d[4]=(long)(beta*1000);
	d[5]=(long)(gamma*1000);

	for(int i=0; i<6; i++)
		checkSum+=d[i];
	checkSum *= 211;
	
	
	//Dades.Format("11 %0.3lf %0.3lf %0.3lf %0.3lf %0.3lf %0.3lf %0.3lf %d\r\n",x,y,z,alfa,beta,gamma,checkSum, code);
	Dades.Format("11 %d %d %d %d %d %d %d %d\r\n",d[0],d[1],d[2],d[3],d[4],d[5],checkSum,code);

	return(Dades);
}

/**	\brief Changes the tool's pos (position and orientation).
 *	\param x Position in coordinate x (in mm).
 *	\param y Position in coordinate y (in mm).
 *	\param z Position in coordinate z (in mm).
 *	\param alfa Orientation in angle Z (in degrees).
 *	\param beta Orientation in angle Y' (in degrees).
 *	\param gamma Orientation in angle Z'' (in degrees).
 *	\return a CString to be sent to the robot's controller.
 *
 *	Increases the robot's configration. Increase in position is given un cartesian
 *	coordinates and increase in orientation in the ZY'Z'' Euler parameters.
 */
CString Interpret::tool(double x, double y, double z, double alfa, double beta, double gamma, int code)
{
	CString Dades;
	double checkSum = 12;
	checkSum = checkSum + x + y + z + alfa + beta + gamma;
	checkSum *= 211;
	
	Dades.Format("12 %lf %lf %lf %lf %lf %lf %lf %d\r\n",x,y,z,alfa,beta,gamma,checkSum, code);
	return(Dades);
}

CString Interpret::close_connexion(int code)
{
	CString Dades;
	long checkSum =-211;
	Dades.Format ("-1 0 0 0 0 0 0 %d %d\r\n",checkSum, code);
	return (Dades);
}

void Interpret::capture_data(CString msg, double *x, double *y, double *z,
							double *alfa, double *beta, double *gamma, 
							double *joint1,	double *joint2, double *joint3, double *joint4, double *joint5, double *joint6, 
							int *error, CString *err, int *code,double *qw,double *qx,double *qy,double *qz)
{
	int codigo;
	double checkSum, aux_checkSum=0;
	
	sscanf_s(msg,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %d %lf %d %lf %lf %lf %lf",
		x,y,z,alfa,beta,gamma,
		joint1,joint2, joint3, joint4, joint5,joint6, 
		&codigo, &checkSum, code, 
		qw,qx,qy,qz);
		
	aux_checkSum = *x + *y + *z + *alfa + *beta + *gamma + (double)((int)(*joint1)) + (double)((int)(*joint2)) + (double)((int)(*joint3)) + (double)((int)(*joint4)) + (double)((int)(*joint5)) + (double)((int)(*joint6));
	aux_checkSum+=codigo;
	aux_checkSum/=211;

	if(fabs(checkSum - aux_checkSum)>1000)//0.1)
	{
		*error=17;
		err->Format("Bad CheckSum recived.");
	}
	else 
	{
		switch ((int)codigo)
		{
		case 0:
			*error=0;
			err->Format("Correct communication.");
			break;
		case 1:
			*error=1;
			err->Format("Joint 1 is limiting.");
			break;
		case 2:
			*error=2;
			err->Format("Joint 2 is limiting.");
			break;
		case 4:
			*error=3;
			err->Format("Joint 3 is limiting.");
			break;
		case 8:
			*error=4;
			err->Format("Joint 4 is limiting.");
			break;
		case 16:
			*error=5;
			err->Format("Joint 5 is limiting.");
			break;
		case 32:
			*error=6;
			err->Format("Joint 6 is limiting.");
			break;
		case 64:
			*error=7;
			err->Format("Bad String received.");
			break;
		case 128:
			*error=8;
			err->Format("Bad CheckSum sent.");
			break;
		case 256:
			*error=9;
			err->Format("Joint 4 next to 180 degrees.");
			break;
		case 512:
			*error=10;
			err->Format("Joint 5 next to 0 degrees.");
			break;
		case 1024:
			*error=11;
			err->Format("Joint 6 next to 180 degrees.");
			break;
		case 2048:
			*error=12;
			err->Format("Incremental movement larger than 5 degrees for some joint.");
			break;
		case 4096:
			*error=13;
			err->Format("Collision detected.");
			break;
		case 8192:
			*error=14;
			err->Format("Location is too close in.");
			break;
		case 16384:
			*error=15;
			err->Format("Location is too close out.");
			break;
		case 32768:
			*error=16;
			err->Format("Motor rather than joint is limiting.");
			break;
		case 65536:
			*error=17;
			err->Format("Position out of robot limits.");
			break;
		default:
			*error=-1;
			err->Format("");
			break;
		}
	}
}

void Interpret::capture_data(CString msg, double *x, double *y, double *z,
							double *alfa, double *beta, double *gamma, double *joint1,
							double *joint2, double *joint3, double *joint4,
							double *joint5, double *joint6, int *error, CString *err, int *code)
{
	int codigo;
	double checkSum, aux_checkSum=0;
	
	sscanf_s(msg,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %d %lf %d",x,y,z,alfa,beta,gamma,joint1,joint2, joint3, joint4, joint5,joint6, &codigo, &checkSum, code);
		
	aux_checkSum = *x + *y + *z + *alfa + *beta + *gamma + *joint1 + *joint2 + *joint3 + *joint4 + *joint5 + *joint6;
	aux_checkSum+=codigo;
	aux_checkSum/=211;

	if(fabs(checkSum - aux_checkSum)>1000)//0.1)
	{
		*error=17;
		err->Format("Bad CheckSum recived.");
	}
	else 
	{
		switch ((int)codigo)
		{
		case 0:
			*error=0;
			err->Format("Correct communication.");
			break;
		case 1:
			*error=1;
			err->Format("Joint 1 is limiting.");
			break;
		case 2:
			*error=2;
			err->Format("Joint 2 is limiting.");
			break;
		case 4:
			*error=3;
			err->Format("Joint 3 is limiting.");
			break;
		case 8:
			*error=4;
			err->Format("Joint 4 is limiting.");
			break;
		case 16:
			*error=5;
			err->Format("Joint 5 is limiting.");
			break;
		case 32:
			*error=6;
			err->Format("Joint 6 is limiting.");
			break;
		case 64:
			*error=7;
			err->Format("Bad String received.");
			break;
		case 128:
			*error=8;
			err->Format("Bad CheckSum sent.");
			break;
		case 256:
			*error=9;
			err->Format("Joint 4 next to 180 degrees.");
			break;
		case 512:
			*error=10;
			err->Format("Joint 5 next to 0 degrees.");
			break;
		case 1024:
			*error=11;
			err->Format("Joint 6 next to 180 degrees.");
			break;
		case 2048:
			*error=12;
			err->Format("Incremental movement larger than 5 degrees for some joint.");
			break;
		case 4096:
			*error=13;
			err->Format("Collision detected.");
			break;
		case 8192:
			*error=14;
			err->Format("Location is too close in.");
			break;
		case 16384:
			*error=15;
			err->Format("Location is too close out.");
			break;
		case 32768:
			*error=16;
			err->Format("Motor rather than joint is limiting.");
			break;
		case 65536:
			*error=17;
			err->Format("Position out of robot limits.");
			break;
		default:
			*error=-1;
			err->Format("");
			break;
		}
	}
}

void Interpret::capture_data(CString msg, double *pose, double *joints, int *error, CString *err, int *code)
{
	int codigo;
	double aux_checkSum, checkSum;
	
	sscanf_s(msg,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %d %lf %d",&pose[0],&pose[1],&pose[2],&pose[3],&pose[4],&pose[5],&joints[0],&joints[1],&joints[2],&joints[3],&joints[4],&joints[5], &codigo, &checkSum, code);
	
	aux_checkSum=0;
	for(int i=0;i<6;i++)
		aux_checkSum = aux_checkSum + pose[i] + joints[i];
	aux_checkSum+=codigo;
	
	aux_checkSum/=211;

	if(fabs(checkSum - aux_checkSum)>1000)//0.1)
	{
		*error=17;
		err->Format("Bad CheckSum recived.");
	}
	else 
	{
		switch ((int)codigo)
		{
		case 0:
			*error=0;
			err->Format("Correct communication.");
			break;
		case 1:
			*error=1;
			err->Format("Joint 1 is limiting.");
			break;
		case 2:
			*error=2;
			err->Format("Joint 2 is limiting.");
			break;
		case 4:
			*error=3;
			err->Format("Joint 3 is limiting.");
			break;
		case 8:
			*error=4;
			err->Format("Joint 4 is limiting.");
			break;
		case 16:
			*error=5;
			err->Format("Joint 5 is limiting.");
			break;
		case 32:
			*error=6;
			err->Format("Joint 6 is limiting.");
			break;
		case 64:
			*error=7;
			err->Format("Bad String received.");
			break;
		case 128:
			*error=8;
			err->Format("Bad CheckSum sent.");
			break;
		case 256:
			*error=9;
			err->Format("Joint 4 next to 180 degrees.");
			break;
		case 512:
			*error=10;
			err->Format("Joint 5 next to 0 degrees.");
			break;
		case 1024:
			*error=11;
			err->Format("Joint 6 next to 180 degrees.");
			break;
		case 2048:
			*error=12;
			err->Format("Incremental movement larger than 5 degrees for some joint.");
			break;
		case 4096:
			*error=13;
			err->Format("Collision detected.");
			break;
		case 8192:
			*error=14;
			err->Format("Location is too close in.");
			break;
		case 16384:
			*error=15;
			err->Format("Location is too close out.");
			break;
		case 32768:
			*error=16;
			err->Format("Motor rather than joint is limiting.");
			break;
		case 65536:
			*error=17;
			err->Format("Position out of robot limits.");
			break;
		default:
			*error=-1;
			err->Format("");
			break;
		}
	}
}

//FUNCIONS ESPECIFIQUES ABB
CString Interpret::ABB_SetVelocity(int vel, int code)
{
	CString Dades;
	double checkSum = 13;
	checkSum = checkSum + vel;
	checkSum *= 211;
	Dades.Format("13 %d 0 0 0 0 0 %lf %d\r\n", vel, checkSum, code);
	return(Dades);
}

CString Interpret::ABB_SetZone(int zone, int code)
{
	CString Dades;
	double checkSum = 14;
	checkSum = checkSum + zone;
	checkSum *= 211;
	Dades.Format("14 %d 0 0 0 0 0 %lf %d\r\n", zone, checkSum, code);
	return(Dades);
}