#include "stdafx.h"
#include "OurSocket.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/** \brief Main Constructor.
 *	\param pDlg Pointer to the dialog where the socket is related.
 *	\param identificator A value to identify witch socket has called a Callbacks. It is returned in everey callback as a parameter.
 *
 *  This constructor specifies the default callbacks as:\n
 *      OnReceive = true\n
 *      OnSend = false\n
 *      OnOutOfBandData = false\n
 *      OnClose = true\n
 *      OnConnect = true\n
 *      OnAccept = true      
 */
COurSocket::COurSocket(CDialog* pDlg, int identificator)
{
	mainDlg = pDlg;
	id=identificator;

	Readcallback = true;
	Writecallback = true;
	Oobcallback = false;
	Acceptcallback = true;
	Connectcallback = true;
	Closecallback = true;

}

/** \brief Constructor.
 *	\param identificator A value to identify witch socket has called a Callbacks. It is returned in everey callback as a parameter.
 *
 *  This constructor specifies the default callbacks as:\n
 *      OnReceive = true\n
 *      OnSend = false\n
 *      OnOutOfBandData = false\n
 *      OnClose = true\n
 *      OnConnect = true\n
 *      OnAccept = true      
 */
COurSocket::COurSocket(int identificator)
{
	id=identificator;

	Readcallback = true;
	Writecallback = true;
	Oobcallback = false;
	Acceptcallback = true;
	Connectcallback = true;
	Closecallback = true;
}

COurSocket::~COurSocket()
{
}

/** \brief Creates the socket over the COurSocket constructed.\n
 *		   It is assigned the default IP and the system selects an empty and available PORT.
 *  \return 0			- The socket has been created correctly.\n
 *			1			- The socket was already created.\n
 *			Otherwise	- There has been an error with the code returned.
 *
 *  This constructor specifies the default callbacks as:\n
 *      OnReceive = true\n
 *      OnSend = false\n
 *      OnOutOfBandData = false\n
 *      OnClose = true\n
 *      OnConnect = true\n
 *      OnAccept = true      
 */
int COurSocket::Create()
{
	if(	m_hSocket == INVALID_SOCKET)
	{
		if((CAsyncSocket::Create())==0)
			return(GetLastError());
		DefineCallbacks();
	}
	return(1);
}

/** \brief Creates the socket over the COurSocket constructed.
 *		   It is assigned the default IP.
 *	\param LocalPort The PORT where the socket must be created.
 *  \return 0			- The socket has been created correctly.\n
 *			1			- The socket was already created.\n
 *			Otherwise	- There has been an error with the code returned.
 *
 *  This constructor specifies the default callbacks as:\n
 *      OnReceive = true\n
 *      OnSend = false\n
 *      OnOutOfBandData = false\n
 *      OnClose = true\n
 *      OnConnect = true\n
 *      OnAccept = true     
 */
int COurSocket::Create(int LocalPort)
{
	if(	m_hSocket == INVALID_SOCKET)
	{
		if((CAsyncSocket::Create(LocalPort,SOCK_STREAM,0,NULL))==0)
			return(GetLastError());
		DefineCallbacks();
	}
	return(1);
}

/** \brief Creates the socket over the COurSocket constructed.
 *	\param LocalPort The PORT where the socket must be created.
 *	\param LocalIP A string where it is specified the IP of the computer where the socket must be created (ex: "147.83.26.128").
 *  \return 0			- The socket has been created correctly.\n
 *			1			- The socket was already created.\n
 *			Otherwise	- There has been an error with the code returned.
 *
 *  This constructor specifies the default callbacks as:\n
 *      OnReceive = true\n
 *      OnSend = false\n
 *      OnOutOfBandData = false\n
 *      OnClose = true\n
 *      OnConnect = true\n
 *      OnAccept = true     
 */
int COurSocket::Create(int LocalPort, CString LocalIP)
{
	if(	m_hSocket == INVALID_SOCKET)
	{
		if((CAsyncSocket::Create(LocalPort,SOCK_STREAM,0,LocalIP))==0)
			return(GetLastError());
		DefineCallbacks();
	}
	return(1);
}


/** \brief Connects a created socket to the socket located in the direction specified by PORT and IP.
 *	\param HostPort The PORT where the local socket must be connected.
 *	\param HostIP A string where it is specified the IP of the computer where the local socket must be connected (ex: "147.83.26.128").
 *  \return 0			- The socket has been connected correctly.\n
 *			1			- The specified socket was not a valid socket (possibly not already created).\n
 *			Otherwise	- There has been an error with the code returned.
 */
int COurSocket::Connect(int HostPort, CString HostIP)
{
	int error;
	if(m_hSocket!=INVALID_SOCKET)
	{
		if((CAsyncSocket::Connect(HostIP,HostPort))==0)
		{
			if(error=(GetLastError()==WSAEWOULDBLOCK))
				return(0);
			else
				return(error);
		}
		else
			return(0);
	}
	else
		return(1);
}

/** \brief Creates a Client socket and connects it with the local and host configuration specified.
 *	\param LocalPort The PORT where the socket must be created.
 *	\param LocalIP A string where it is specified the IP of the computer where the socket must be created (ex: "147.83.26.128").
 *	\param HostPort The PORT where the local socket must be connected.
 *	\param HostIP A string where it is specified the IP of the computer where the local socket must be connected (ex: "147.83.26.128").
 *  \return 0			- The socket has been created and connected correctly.\n
 *			1			- The socket was already created.\n
 *			Otherwise	- There has been an error in the creation or the connection process with the code returned.
 */
int COurSocket::CreateAndConnect(int LocalPort, CString LocalIP, int HostPort, CString HostIP)
{
	if(	m_hSocket == INVALID_SOCKET)
	{
		if((CAsyncSocket::Create(LocalPort,SOCK_STREAM,0,LocalIP))==0)
			return(GetLastError());
		else
		{
			DefineCallbacks();
			return(Connect(HostPort,HostIP));
		}
	}
	return(1);
}
	
/** \brief Creates a Client socket and connects it with the host configuration specified.
 *	\param HostPort The PORT where the local socket must be connected.
 *	\param HostIP A string where it is specified the IP of the computer where the local socket must be connected (ex: "147.83.26.128").
 *  \return 0			- The socket has been created and connected correctly.\n
 *			1			- The socket was already created.\n
 *			Otherwise	- There has been an error in the creation or the connection process with the code returned.
 *
 *	It is assigned the default IP and the system selects an empty and available PORT, for the configuration of the local socket.
 */
int COurSocket::CreateAndConnect(int HostPort, CString HostIP)
{
	if(	m_hSocket == INVALID_SOCKET)
	{
		if((CAsyncSocket::Create())==0)
			return(GetLastError());
		else
		{
			DefineCallbacks();
			return(Connect(HostPort, HostIP));
		}
	}
	return(1);
}

/** \brief Prepares a created socket for listening to incoming connections. 
 *  \return 0			- The socket has been set to listening state correctly.\n
 *			1			- The socket was not a valid socket (possibly not already created).\n
 *			Otherwise	- There has been an error in the listening process with the code returned.
 *
 *	Every time that an incomming connection is received, it is generated a OnAccept callback.\n
 *  In this case there must be called the function Accept with another socket as a parameter. It shouldn't be already created, because the Accept function will create it identical to the server socket that has received the incoming connection.
 */
int COurSocket::Listen()
{
	if(m_hSocket!=INVALID_SOCKET)
	{
		if((CAsyncSocket::Listen())==0)
			return(GetLastError());
		return(0);
	}
	else
		return(1);
}

/** \brief Creates a Server socket, with the local configuration specified, and sets it in the listening status.
 *	\param LocalPort The PORT where the socket must be created.
 *	\param LocalIP A string where it is specified the IP of the computer where the socket must be created (ex: "147.83.26.128").
 *  \return 0			- The socket has been created and set correctly to the listening status.\n
 *			1			- The socket was already created.\n
 *			Otherwise	- There has been an error in the creation or the listening process with the code returned.
 */
int COurSocket::CreateAndListen(int LocalPort, CString LocalIP)
{
	if(	m_hSocket == INVALID_SOCKET)
	{
		if((CAsyncSocket::Create(LocalPort,SOCK_STREAM,0,LocalIP))==0)
			return(GetLastError());
		else
		{
			DefineCallbacks();
			return(Listen());
		}
	}
	return(1);
}

/** \brief Creates a Server socket and sets it in the listening status.
 *  \return 0			- The socket has been created and set correctly to the listening status.\n
 *			1			- The socket was already created.\n
 *			Otherwise	- There has been an error in the creation or the listening process with the code returned.
 *
 *	It is assigned the default IP and the system selects an empty and available PORT, for the configuration of the local socket.
 */
int COurSocket::CreateAndListen()
{
	if(	m_hSocket == INVALID_SOCKET)
	{
		if((CAsyncSocket::Create())==0)
			return(GetLastError());
		else
		{
			DefineCallbacks();
			return(Listen());
		}
	}
	return(1);
}

/** \brief Closes a valid socket. Otherwise this function doesn't do anything.
 */
void COurSocket::Close()
{
	if(	m_hSocket != INVALID_SOCKET)
		CAsyncSocket::Close();
}

/** \brief This method is called by the framework to notify this socket that the socket to which it was connected was closed by its process.
 *	\param nErrorCode	0			- The function executed successfully.\n
 *						Otherwise	- There has been an error with that error code.
 *
 * If Something has to be done after the host socket is closed, this method should call a similar method placed in the dialog like for example:\n
 *  \code ((CServerAndClientDlg*)mainDlg)->OnClose(id, nErrorCode);\endcode
 * In this case from the socket it is called a function of the dialog called OnClose with two parameters:\n
 *   - the error code generated.\n
 *   - the identificator of the socket that has received the notification of the closing of the host socket.\n
 * In that dialog method there must be implemented a code similar to this:\n
 * \code
 * switch(identificator)
 	{
 	case LISTENER:
 		Listensock->Close();
 		m_server_receive.Format("The client has closed connection,");
 		break;
 	case CLIENT:
 		Clientsock->Close();
 		m_client_receive.Format("The server has closed its connection.");
 		break;
 	}\endcode
 */
void COurSocket::OnClose(int nErrorCode) 
{
	((CDemoRegistreDlg*)mainDlg)->OnClosed(id, nErrorCode);
	CAsyncSocket::OnClose(nErrorCode);
}

/** \brief This method is called by the framework to notify the socket that its connection attempt is complete.
 *	\param nErrorCode	0			- The function executed successfully.\n
 *						Otherwise	- There has been an error with that error code.
 *
 * If Something has to be done after the local socket is connected (for example depending if the connection process has been successful or not), this method should call a similar method placed in the dialog like for example:\n
 *  \code ((CServerAndClientDlg*)mainDlg)->OnConnect(id, nErrorCode);\endcode
 * In this case from the socket it is called a function of the dialog called OnConnect with two parameters:\n
 *   - the error code generated.\n
 *   - the identificator of the socket that has received the notification of the ending of the connection process.\n
 * In that dialog method there must be implemented a code similar to this:\n
 * 	\code
 * switch(identificator)
    {
	case CLIENT:
		if(nErrorCode==0)
 		{
 			m_client_receive.Format("The connetion with the server has been established.");
 		}
		else
 		m_client_receive.Format("Conection refused. Error code %d.",nErrorCode);
		break;
	}\endcode
 	
 */
void COurSocket::OnConnect(int nErrorCode) 
{
	((CDemoRegistreDlg*)mainDlg)->OnConnected(id,nErrorCode);
	CAsyncSocket::OnConnect(nErrorCode);
}

/** \brief This method is called by the framework to notify the socket that there is data in the buffer that can be retrieved by calling the Receive method. 
 *	\param nErrorCode	0			- The function executed successfully.\n
 *						Otherwise	- There has been an error with that error code.
 *
 * This method should call a similar method placed in the dialog like for example:
 *  \code ((CServerAndClientDlg*)mainDlg)->OnReceive(id, nErrorCode);\endcode
 * In this case from the socket it is called a function of the dialog called OnReceive with two parameters:\n
 *   - the error code generated.\n
 *   - the identificator of the socket that has received the notification of the presence of data in the buffer.\n
 * In that dialog method there must be implemented a code similar to this:\n
 * \code
 * switch(identificator)
	{
	case CLIENT:
		Clientsock->Receive(&m_client_receive);
		break;
	case LISTENER:
		Listensock->Receive(&m_server_receive);
		break;
	}\endcode
 */
void COurSocket::OnReceive(int nErrorCode) 
{
	((CDemoRegistreDlg*)mainDlg)->OnRecieve(id, nErrorCode);
	CAsyncSocket::OnReceive(nErrorCode);
}

/** \brief This method is called by the framework to notify a listening socket that it can accept pending connection requests. 
 *	\param nErrorCode	0			- The function executed successfully.\n
 *						Otherwise	- There has been an error with that error code.
 *
 * This method should call a similar method placed in the dialog like for example:
 *  \code ((CServerAndClientDlg*)mainDlg)->OnAccept(id, nErrorCode);\endcode
 * In this case from the socket it is called a function of the dialog called OnAccept with two parameters:\n
 *   - the error code generated.\n
 *   - the identificator of the socket that has received the notification of the incomming connection.\n
 * In that dialog method there must be implemented a code similar to this:\n
 * \code
   switch(identificator)
    {
    case SERVER:
		Listensock=new COurSocket(this,LISTENER);
		if(Serversock->Accept(Listensock))==0)
			m_server_receive.Format("The connetion with the client has been established.");
		else
			m_server_receive.Format("The server received a connection petition but was nunable to accept it.");
		break;
	}\endcode
 */
void COurSocket::OnAccept(int nErrorCode)
{
	//((ABBnCAMClients*)mainDlg)->OnAccept(id, nErrorCode);
	CAsyncSocket::OnAccept(nErrorCode);
}

/** \brief This method is called by the framework to notify the receiving socket that the sending socket has out-of-band data to send. Out-of-band data is a logically independent channel that is associated with each pair of connected sockets of type SOCK_STREAM. The channel is generally used to send urgent data.
 *	\param nErrorCode	0			- The function executed successfully.\n
 *						Otherwise	- There has been an error with that error code.
 *
 * This method should call a similar method placed in the dialog like for example:
 *  \code ((CServerAndClientDlg*)mainDlg)->OnOutOfBandData(id, nErrorCode);\endcode
 * In this case from the socket it is called a function of the dialog called OnOutOfBandData with two parameters:\n
 *   - the error code generated.\n
 *   - the identificator of the socket that has received the notification of the incomming OutOfBandData.
 */
void COurSocket::OnOutOfBandData(int nErrorCode)
{
	CAsyncSocket::OnOutOfBandData(nErrorCode);
}

/** \brief This method is called by the framework to notify the socket that it can send data by calling the Send method.
 *	\param nErrorCode	0			- The function executed successfully.\n
 *						Otherwise	- There has been an error with that error code.
 *
 * This method should call a similar method placed in the dialog like for example:
 *  \code ((CServerAndClientDlg*)mainDlg)->OnSend(id, nErrorCode);\endcode
 * In this case from the socket it is called a function of the dialog called OnSend with two parameters:\n
 *   - the error code generated.\n
 *   - the identificator of the socket that has received the notification of the possibility to send data.
 */
void COurSocket::OnSend(int nErrorCode)
{
	CAsyncSocket::OnSend(nErrorCode);
}

/** \brief This method Receives a string from the incomming buffer. This should be called only after receiving an OnReceive notification.
 *	\param str	Pointer to a CString where the data will be stored. 
 *
 *  \return   0         - The string was correctly received.\n
 *			  1		    - The input string is larger than 500 bytes and CAsyncSocket::Receive() should be used to receive the string, with an input buffer larger than 500 bytes.\n
 *            2         - The connection was previously closed.\n
 *			Otherwise	- There has been an error in the reading process with the code returned.
 */
int COurSocket::Receive(CString * str)
{
	char buff[500];
	int longitud=CAsyncSocket::Receive(buff, 499, 0);
	if(longitud == SOCKET_ERROR)
	{
		return(GetLastError());
	}
	else if((longitud==0)&&(str->GetLength()!=0))
	{
		return(2);
	}
	else if(longitud>499)
	{
		buff[499]=0;
		*str=buff;
		return(1);
	}
	else
	{
		buff[longitud]=0;
		*str=buff;
		return(0);
	}
}

/** \brief This method sends a CString to the remote socket.
 *	\param str	Pointer to the CString to be sent. 
 *
 *  \return    0         - The string was correctly sent.
 *			 Otherwise	 - There has been an error in the sending process with the code returned.
 */
int COurSocket::Send(CString *str)
{
	int code=CAsyncSocket::Send(*str, str->GetLength() + 1, 0);
	if(code == SOCKET_ERROR)
	{
		return(GetLastError());
	}
	else
	{
		return(0);
	}

}

/** \brief Specifies a pointer to the dialog related with the socket created.
 *	\param pDlg Pointer to a Dialog. 
 *
 *  When a socket is created from a Dialog it can be specified by:
 * \code 
    COurSocket socket;
    socket.SetParent(this);\endcode
 */
void COurSocket::SetParent(CDialog* pDlg)
{
	mainDlg = pDlg;
}

/** \brief Specifies an integer that can be used as an identificator of the socket.
 *	\param identificator Integer to identify the socket. 
 *
 */
void COurSocket::SetIdentificator(int identificator)
{
	id=identificator;
}

/** \brief Specifies whether the OnReceive callback is active or not .
 *	\param set Boolean specifying:\n
                 - true		Active callback function.
				 - false	Inactive callback function.
 */
void COurSocket::SetOnReceive(bool set)
{
	Readcallback=set;
	if(	m_hSocket != INVALID_SOCKET)
		DefineCallbacks();
	
}

/** \brief Specifies whether the OnSend callback is active or not .
 *	\param set Boolean specifying:\n
                 - true		Active callback function.
				 - false	Inactive callback function.
 */
void COurSocket::SetOnSend(bool set)
{
	Writecallback=set;
	if(	m_hSocket != INVALID_SOCKET)
		DefineCallbacks();
}

/** \brief Specifies whether the OnOutOfBandData callback is active or not .
 *	\param set Boolean specifying:\n
                 - true		Active callback function.
				 - false	Inactive callback function.
 */
void COurSocket::SetOnOutOfBandData(bool set)
{
	Oobcallback=set;
	if(	m_hSocket != INVALID_SOCKET)
		DefineCallbacks();
}

/** \brief Specifies whether the OnAccept callback is active or not .
 *	\param set Boolean specifying:\n
                 - true		Active callback function.
				 - false	Inactive callback function.
 */
void COurSocket::SetOnAccept(bool set)
{
	Acceptcallback=set;
	if(	m_hSocket != INVALID_SOCKET)
		DefineCallbacks();
}

/** \brief Specifies whether the OnConnect callback is active or not .
 *	\param set Boolean specifying:\n
                 - true		Active callback function.
				 - false	Inactive callback function.
 */
void COurSocket::SetOnConnect(bool set)
{
	Connectcallback=set;
	if(	m_hSocket != INVALID_SOCKET)
		DefineCallbacks();
}

/** \brief Specifies whether the OnClose callback is active or not .
 *	\param set Boolean specifying:\n
                 - true		Active callback function.
				 - false	Inactive callback function.
 */
void COurSocket::SetOnClose(bool set)
{
	Closecallback=set;
	if(	m_hSocket != INVALID_SOCKET)
		DefineCallbacks();
}

/** \brief Sets to the socket the callbacks that must be active or not depending on the value of the parameters.
 *	\param accept	The value of the callback OnAccept.
 *	\param close	The value of the callback OnClose.
 *	\param connect	The value of the callback OnConnect.
 *	\param oob		The value of the callback OnOutOfBandData.
 *	\param read		The value of the callback OnReceive.
 *	\param write	The value of the callback OnSend.
 */
void COurSocket::SetCallBacks(bool accept, bool close, bool connect, bool oob, bool read, bool write)
{
	Readcallback=read;
	Writecallback=write;
	Oobcallback=oob;
	Acceptcallback=accept;
	Connectcallback=connect;
	Closecallback=close;
	
	if(	m_hSocket != INVALID_SOCKET)
		DefineCallbacks();
}

/** \brief Private method for sets to the socket the callbacks that must be active or not depending on the value of the variables Readcallback, Writecallback, Oobcallback, Acceptcallback, Connectcallback, Closecallback.
 */
void COurSocket::DefineCallbacks()
{
	long aux=0;
	if(Readcallback)
		aux=(aux|FD_READ);
	if(Writecallback)
		aux=(aux|FD_WRITE);
	if(Oobcallback)
		aux=(aux|FD_OOB);
	if(Acceptcallback)
		aux=(aux|FD_ACCEPT);
	if(Connectcallback)
		aux=(aux|FD_CONNECT);
	if(Closecallback)
		aux=(aux|FD_CLOSE);

	CAsyncSocket::AsyncSelect(aux);
}

/** \brief This method Accepts an incomming connection. 
 *	\param Listensock	Pointer to a COurSocket object.
 *
 *  - The connection will finally be established between the incomming petition and the socket that is passed as a parameter. 
 *  - The Listensock socket must be only defined but not yet created, because the Accept method will create it identical to the socket that is serving the connection petition.
 *  \return   0         - The connection has been stablished correctly.\n
 *			Otherwise	- There has been an error in the connection process with the code returned.
 */
int COurSocket::Accept(COurSocket *Listensock)
{
	SOCKADDR lpSockAddr;
	int lpSockAddrLen=sizeof(SOCKADDR);
	
	if((CAsyncSocket::Accept(*Listensock, &lpSockAddr, &lpSockAddrLen))==0)
		return(GetLastError());
	return(0);
}

