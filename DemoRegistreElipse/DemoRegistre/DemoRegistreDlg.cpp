
// DemoRegistreDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "DemoRegistre.h"
#include "DemoRegistreDlg.h"
#include "afxdialogex.h"
#include <iostream>
#include <iterator>
#include <windows.h>
#include <time.h>
# include <conio.h>
#include <Ws2tcpip.h>
# include "WinSock2.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;

inline int modval(int a, int b) { return (a % b + b) % b; }

//Thread Pintar Sync

UINT ThreadPintar(LPVOID pParam)
{
	CDemoRegistreDlg* pObject = (CDemoRegistreDlg*)pParam;

	_LARGE_INTEGER StartingTime, EndingTime, ElapsedMiliseconds;
	LARGE_INTEGER Frequency;

	namedWindow("Kalman Registration", WINDOW_AUTOSIZE);// Create a window for display.
	
	QueryPerformanceFrequency(&Frequency);

	while (pObject->bPintant)
	{
		//1. Get Timer
		QueryPerformanceCounter(&StartingTime);
		pObject->printRegister();
		QueryPerformanceCounter(&EndingTime);
		ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
		ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
		ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;

		while (ElapsedMiliseconds.QuadPart <= FRAMERATEPINTAR)
		{
			QueryPerformanceCounter(&EndingTime);
			ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
			ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
			ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
		}

	}
	cv::destroyWindow("Kalman Registration");
	//waitKey(10);
	return 0;
}


//Thread Kalman Filter
UINT ThreadKalmanFilter(LPVOID pParam)
{
	CDemoRegistreDlg* pObject = (CDemoRegistreDlg*)pParam;
	int indThread = -1;

	pObject->indKFMutex.lock();
	for (int i = 0; i < 6; i++)
	{
		if (pObject->indKF[i])
		{
			indThread = i;
			pObject->indKF[i] = false;
			break;
		}
	}
	pObject->indKFMutex.unlock();
	
	if (indThread == -1)
	{
		TRACE("ERRROOOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRRRRRRRRR\n");
		return 0;
	}
	cv::Mat predMatrix, stimateMatrix;
	float DeltaT = 1 / pObject->predRate;

	///Inicialitzar el KF
	cv::KalmanFilter KF(KFSTATEDIM, 1, 0, CV_32F);
	cv::Mat stateKF = cv::Mat(KFSTATEDIM, 1, CV_32F); //(p,v,a) com a estat.
	cv::Mat processNoise(KFSTATEDIM, 1, CV_32F);
	float *myData = (float *)stateKF.data;
	size_t elem_step = stateKF.step / sizeof(float);
	float novaData;
	cv::Mat novaDataMatrix = cv::Mat(1, 1, CV_32F);
	float *myNovaData = (float*)novaDataMatrix.data;

	cv::setIdentity(KF.transitionMatrix);
	KF.measurementMatrix = cv::Mat::zeros(1, KFSTATEDIM, CV_32F);
	KF.measurementMatrix.at<float>(0) = 1.0f;
	float *transMatrixData = (float*)KF.transitionMatrix.data;
	setIdentity(KF.processNoiseCov, Scalar::all(1e-6));
	setIdentity(KF.measurementNoiseCov, Scalar::all(1.0));


/*	//Zona de transitorietat

	for (int i = 0; i < KFSTATEDIM; i++)
	{
		while (true) 
		{
			pObject->measureMutex.lock();
			if (pObject->dadaNova > -1)
			{
				pObject->dadaNova++;

				if (pObject->dadaNova == 6)
					pObject->dadaNova = -1;

				myData[i] = pObject->measurement[indThread];
				pObject->measureMutex.unlock();
				break;
			}
			else
			{
				pObject->measureMutex.unlock();
			}
		}
	}*/
	//INICI
	pObject->iniICPMutex.lock();
	KF.statePost.at<float>(0) = pObject->IniICP[indThread];
	pObject->iniICPMutex.unlock();
	KF.statePost.at<float>(1) = 0.0f;
	//KF.statePost.at<float>(2) = 0.0f;

	//Zona estacionaria
	double ticks = (double)cv::getTickCount() - 70.0;
	double preticks = 0;
	float dT;
	double freqTick = cv::getTickFrequency();

	bool iniPred = true;
	bool lectura = false;


	while (true)
	{
		pObject->finalKF.lock();
		if (pObject->bFinalKF)
		{
			pObject->finalKF.unlock();
			break;
		}	
		pObject->finalKF.unlock();

		preticks = ticks;
		ticks = (double)cv::getTickCount();
		dT = (float)((ticks - preticks) / freqTick);

		transMatrixData[0 * KFSTATEDIM + 1] = dT;
		//transMatrixData[1 * KFSTATEDIM + 2] = dT;
		//transMatrixData[0 * KFSTATEDIM + 2] = dT*dT / 2;

		///Kalman Filter Predicci�
		predMatrix = KF.predict();


		//Llegir una nova Dada ////Potser fer-ho continuu de tal forma que si no hi ha dada nova nom�s faci predicci� i sin� les dues coses (depenen de la dada)
		pObject->measureMutex.lock();
		if (pObject->dadaNova[indThread] > -1)
		{
			pObject->dadaNova[indThread] = -1;
			myNovaData[0] = pObject->measurement[indThread];
			pObject->measureMutex.unlock();
			lectura = true;
		}
		else
		{
			pObject->measureMutex.unlock();
		 	lectura = false;
		}

		//Correcci� KF
		if ((iniPred) &&(lectura))
		{
			KF.statePost.at<float>(0) = myNovaData[0];
			KF.statePost.at<float>(1) = 0.0f;
			//KF.statePost.at<float>(2) = 0.0f;
			iniPred = false;
			pObject->currentStateMutex.lock();
			pObject->currentState[indThread] = KF.statePost.at<float>(0);
			pObject->currentStateMutex.unlock();
		}
		else if ((iniPred) && (!lectura))
		{
			pObject->currentStateMutex.lock();
			pObject->currentState[indThread] = predMatrix.at<float>(0);
			pObject->currentStateMutex.unlock();
		}
		else if ((!iniPred) && (lectura))
		{
			stimateMatrix = KF.correct(novaDataMatrix);
			pObject->currentStateMutex.lock();
			pObject->currentState[indThread] = stimateMatrix.at<float>(0);
			pObject->currentStateMutex.unlock();
		}
		else
		{
			pObject->currentStateMutex.lock();
			pObject->currentState[indThread] = predMatrix.at<float>(0);
			pObject->currentStateMutex.unlock();
		}


	}
	
	return 0;
}

//Thread ICP
UINT ThreadICP(LPVOID pParam)
{
	CDemoRegistreDlg* pObject = (CDemoRegistreDlg*)pParam;


	bool iniPred = true;
	bool lectura = false;
	std::vector<cv::Point3f> Punts3D(0);
	std::vector<cv::Point2f> Punts2D(0);
	Eigen::Vector3f pos, eulerXYZ1, eulerXYZ2, eulerAnt, eulerAntRad;
	int nPuntsIn, nPuntsOut, nDelta;
	float distEuler1, distEuler2;
	float errorNormaICP;

	Eigen::Matrix4f Tin, Tout;

	pObject->punts3DMutex.lock();
	std::copy(pObject->punts3D.begin(), pObject->punts3D.end(), std::back_inserter(Punts3D));
	pObject->punts3DMutex.unlock();

	pObject->iniICPMutex.lock();
	eulerAnt = Eigen::Vector3f(pObject->IniICP[3], pObject->IniICP[4], pObject->IniICP[5]);
	eulerAntRad = eulerAnt* M_PI / 180.0f;
	pObject->EulerXYZandPos2Matriu(&eulerAntRad, &(Eigen::Vector3f(pObject->IniICP[0], pObject->IniICP[1], pObject->IniICP[2])), &Tin);
	pObject->iniICPMutex.unlock();

	nPuntsIn = pObject->nPuntsIn;//ceil(Punts2D.size() / 4.0f);
	nDelta = pObject->nDelta;// ceil(Punts2D.size() / 20.0f);

	bool lockvalue;
	int transitori = 0;
	bool transEnd = false;


	while (!pObject->bFinalICP)
	{
		/*pObject->finalICP.lock();
		if (pObject->bFinalICP)
		{
			pObject->finalICP.unlock();
			break;
		}
		pObject->finalICP.unlock();*/

		Punts2D.clear();
		
		while (true)
		{
			lockvalue = pObject->punts2DMutex.try_lock();
			if (lockvalue)
			{
				if (pObject->bPunts2D)
				{
					pObject->bPunts2D = false;
					break;
				}
				pObject->punts2DMutex.unlock();
				if (pObject->bFinalICP)
					return 0;
			}
			if (pObject->bFinalICP)
				return 0;
			
		}
		std::copy(pObject->punts2D.begin(), pObject->punts2D.end(), std::back_inserter(Punts2D));
		pObject->punts2DMutex.unlock();


		errorNormaICP = pObject->ICPCurvatura2D3DIter(&Punts2D, &Punts3D, &Tin, &Tout, nPuntsIn, nDelta, MAXITERLM, TOLERANCIA, &nPuntsOut);
		if ((errorNormaICP < 1.2f) && (errorNormaICP > 0.0f))
		{
			pObject->Matriu2EulerXYZandPos(Tout, &eulerXYZ1, &eulerXYZ2, &pos);
			eulerXYZ1 = eulerXYZ1 * 180.0f / M_PI;
			eulerXYZ2 = eulerXYZ2 * 180.0f / M_PI;
			//Mirar quina de les dues representacions �s m�s propera a l'anterior.
			distEuler1 = (eulerAnt - eulerXYZ1).norm();
			distEuler2 = (eulerAnt - eulerXYZ2).norm();

			pObject->measureMutex.lock();
			pObject->measurement[0] = pos(0);
			pObject->measurement[1] = pos(1);
			pObject->measurement[2] = pos(2);

			if (distEuler1 < distEuler2)
			{
				pObject->measurement[3] = eulerXYZ1(0);
				pObject->measurement[4] = eulerXYZ1(1);
				pObject->measurement[5] = eulerXYZ1(2);
				eulerAnt = eulerXYZ1;
			}
			else
			{
				pObject->measurement[3] = eulerXYZ2(0);
				pObject->measurement[4] = eulerXYZ2(1);
				pObject->measurement[5] = eulerXYZ2(2);
				eulerAnt = eulerXYZ2;
			}


			pObject->dadaNova[0] = 0;
			pObject->dadaNova[1] = 0;
			pObject->dadaNova[2] = 0;
			pObject->dadaNova[3] = 0;
			pObject->dadaNova[4] = 0;
			pObject->dadaNova[5] = 0;
			pObject->measureMutex.unlock();



			/*	///TO DO: BORRAR DES D'AQU�
				pObject->currentStateMutex.lock();
				pObject->currentState[0] = pos(0);
				pObject->currentState[1] = pos(1);
				pObject->currentState[2] = pos(2);
				pObject->currentState[3] = eulerXYZ1(0);
				pObject->currentState[4] = eulerXYZ1(1);
				pObject->currentState[5] = eulerXYZ1(2);
				pObject->currentStateMutex.unlock();
				///FINS AQU� BORRAR*/

				//TO DO: COM ACTUALITZAR nPuntsIn, nPuntsOut, nDelta
			nPuntsIn = nPuntsOut;

			//Actualitzaci� de Transformaci�
			if (transEnd) {
				pObject->currentStateMutex.lock();
				eulerAntRad = Eigen::Vector3f(pObject->currentState[3], pObject->currentState[4], pObject->currentState[5])* M_PI / 180.0f;
				pObject->EulerXYZandPos2Matriu(&eulerAntRad, &(Eigen::Vector3f(pObject->currentState[0], pObject->currentState[1], pObject->currentState[2])), &Tin);
				pObject->currentStateMutex.unlock();
			}
			else
			{
				transitori++;
				if (transitori > 10)
					transEnd = true;
				Tin = Tout;
			}
		}
	}
	//pObject->finalKF.unlock();

	return 0;
}


//Thread Camera

UINT ThreadRecording(LPVOID pParam)
{
	CDemoRegistreDlg* pObject = (CDemoRegistreDlg*)pParam;

	_LARGE_INTEGER StartingTime, EndingTime, ElapsedMiliseconds;
	LARGE_INTEGER Frequency;
	CString fpsText;
	std::vector<cv::Point2i> auxPointsContorn = std::vector<cv::Point2i>();
	std::vector<cv::Point2f> auxPointsFloat = std::vector<cv::Point2f>();

	pObject->iTotalFramesRecorded = 0;
	int index;
	float x0, y0, a_2, b_2, angE;
	float x1, y1;
	x0 = 0.0f;
	y0 = 0.0f;
	
	//double dbgfps;
	//is_GetFramesPerSecond(pObject->ueCamera->m_hCam, &dbgfps);

	//Re-Start Continuous acquisition and restart the time stamp
	//is_CaptureVideo(pObject->ueCamera->m_hCam, IS_DONT_WAIT);
	//LPVOID pParam;
	//pObject->iTimeStamp = 0;	//Reset time stamp;
	//pObject->iFrameRate = 25;
	int iFrameRate = 1000 / pObject->VideoFrameRate; //milliseconds per frame
													 //Prepare video writers for recording from uEyeCamera to mjpg file using OpenCV3.1
													 //CString VideoFile;
													 //VideoFile = "Record.avi";
													 //Video Size initialization  
													 //pObject->sVideoSize = pObject->ueCamera->GetImageSize();	//Get Video Size directly from the uEyeCamera information
	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&StartingTime);
	//Vec2f* mp;
	//pObject->iVideoImage = cvCreateImage(pObject->sVideoSize, 8, 3);	//Create image header and allocate the image data
	//pObject->vwVideoWriter = cvCreateVideoWriter("Record.avi", CV_FOURCC_PROMPT, pObject->iFrameRate, pObject->sVideoSize, true);

	int nPuntsBons;
	int a1, a2;
	for (int i = 0; i < 10; i++)
	{
		pObject->valMigx[i] = 0;
		pObject->valMigy[i] = 0;
	}

	while (pObject->bRecording)
	{
		//1. Get Timer
		//pObject->iVideoImage = pObject->ueCamera->iImage;	//to Mat (openCV)
		//cvCopy(pObject->ueCamera->iImage, pObject->VideoImage);
		//cvSetImageROI(iVideoImage[LAP_VIDEO], cvRect((iVideoImage[LAP_VIDEO]->width - iVideoImage[LAP_VIDEO]->height) / 2, 0, iVideoImage[LAP_VIDEO]->height, iVideoImage[LAP_VIDEO]->height));
		//cvResize(iInterfaceImage[UE_LAPAROSCOP], iVideoImage[LAP_VIDEO], CV_INTER_NN);
		//cvResetImageROI(iVideoImage[LAP_VIDEO]);
		//cvWriteFrame(pObject->VideoWriter, pObject->ueCamera->iImage);// pObject->VideoImage);
		//pObject->lastImage = cv::cvarrToMat(pObject->ueCamera->iImage);
		pObject->ueCamera->GetImage((pObject->lastImage.data));
		//TRACE("IMATGE NOVA: %d\n", pObject->iTotalFramesRecorded);
		
		if (pObject->bSegmenting)
		{
			pObject->iTotalFramesRecorded++;
			
			
			pObject->meanSegmentats[1].x = pObject->meanSegmentats[0].x;
			pObject->meanSegmentats[1].y = pObject->meanSegmentats[0].y;
			QueryPerformanceCounter(&StartingTime);
			pObject->SegmentacioImatge(&pObject->lastImage, &(pObject->segImage));
			/*if (pObject->iTotalFramesRecorded < 10)
			{*/
				pObject->BuscarEllipse(pObject->lastImage, a1, a2);
				pObject->meanSegmentats[0].x = (float)a1;
				pObject->meanSegmentats[0].y = (float)a2;
			//}
			
			pObject->trobarPuntsElipse((pObject->segImage), auxPointsContorn, PUNTSCONTORNIMATGE2D);
			auxPointsFloat.clear();
			nPuntsBons = 0;
			for (int i = 0; i < auxPointsContorn.size(); i++)
			{
				if ((auxPointsContorn[i].x == 0) || (auxPointsContorn[i].y == 0))
					nPuntsBons++;
				auxPointsFloat.push_back((cv::Point2f)auxPointsContorn[i]);
			}
			if (nPuntsBons < PUNTSCONTORNIMATGE2D / 3)
			{
				
				pObject->BuscarEllipse(pObject->lastImage,a1,a2);
				pObject->meanSegmentats[0].x = (float)a1;
				pObject->meanSegmentats[0].y = (float)a2;
				pObject->trobarPuntsElipse((pObject->segImage), auxPointsContorn, PUNTSCONTORNIMATGE2D);
			}
			x1 = x0;
			y1 = y0;
			pObject->ParametresEllipseProbabilitat(auxPointsFloat, x0, y0, a_2, b_2, angE);
			pObject->puntsSegmentats = cv::Mat(1, auxPointsContorn.size(), CV_32FC2);
			//pObject->puntsSegmentatsDes = cv::Mat(1, auxPointsContorn.size(), CV_32FC2);
			pObject->puntsSegmentatsvect.clear();
			std::copy(auxPointsContorn.begin(), auxPointsContorn.end(), std::back_inserter(pObject->puntsSegmentatsvect));
			pObject->PosarImatgeSegmentacio(&pObject->lastImage);//&(pObject->lastImage)); 
			//pObject->punts2DMutex.lock();
			//pObject->punts2D = std::vector<cv::Point2f>(pObject->puntsSegmentatsvect.size());
			/*mp = &pObject->puntsSegmentats.at<Vec2f>(0);
			for (int i = 0; i < auxPointsContorn.size(); i++)
			{
				mp[i].val[0] = (float)pObject->puntsSegmentatsvect[i].y;
				mp[i].val[1] = (float)pObject->puntsSegmentatsvect[i].x;
			}

			cv::undistortPoints(pObject->puntsSegmentats, pObject->puntsSegmentatsDes, pObject->cameraMatrix, pObject->distCoef);
			mp = &pObject->puntsSegmentatsDes.at<Vec2f>(0);*/
			
			//pObject->desdistorcionarPunts(&pObject->puntsSegmentatsvect, &pObject->punts2D, pObject->puntsSegmentatsvect.size());
			//pObject->bPunts2D = true;
			//pObject->punts2DMutex.unlock();
			/*for (int i = 0; i < auxPointsContorn.size(); i++)
			{
				pObject->puntsSegmentatsDesvect[i].x = mp[i].val[1];
				pObject->puntsSegmentatsDesvect[i].y = mp[i].val[0];
			}*/
			//pObject->puntsSegmentats = auxPointsContorn;
			QueryPerformanceCounter(&EndingTime);
			ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
			
			// We now have the elapsed number of ticks, along with the number of ticks-per-second. We use these values
			// to convert to the number of elapsed milliseconds.  To guard against loss-of-precision, we convert
			// to milliseconds *before* dividing by ticks-per-second.
			ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
			ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
			fpsText.Format("%d", ElapsedMiliseconds);
			pObject->m_strFps.SetWindowTextA(fpsText);
			fpsText.Format("%.2f  %.2f", pObject->meanSegmentats[0].x - pObject->meanSegmentats[1].x, x1 - x0);
			pObject->m_xvalCElipse.SetWindowTextA(fpsText);
			fpsText.Format("%.2f  %.2f", cos(22.0f*M_PI / 180.0f)*(pObject->meanSegmentats[0].y - pObject->meanSegmentats[1].y),cos(22.0f*M_PI/180.0f)*(y1 - y0));
			pObject->m_yvalCElipse.SetWindowTextA(fpsText);

			pObject->centreMutex.lock();
			pObject->ex0 = pObject->ex;
			pObject->ey0 = pObject->ey;
			pObject->ex = x0;
			pObject->ey = y0;
			if ((pObject->meanSegmentats[0].y > 0.0f) && (pObject->meanSegmentats[0].x > 0.0f))
			{
				for (int i = 0; i < 9; i++)
				{
					pObject->valMigx[i + 1] = pObject->valMigx[i];
					pObject->valMigy[i + 1] = pObject->valMigy[i];
				}
				pObject->valMigx[0] = pObject->meanSegmentats[0].x;
				pObject->valMigy[0] = pObject->meanSegmentats[0].y;
			}
			pObject->centreMutex.unlock();

			QueryPerformanceCounter(&EndingTime);

		}

	}
	if (pObject->mfcImg)
	{
		pObject->mfcImg->ReleaseDC();
		delete pObject->mfcImg; pObject->mfcImg = nullptr;
	}

	return 0;
}



//Thread Socket Albert

UINT ThreadSocket(LPVOID pParam)
{
	CDemoRegistreDlg* pObject = (CDemoRegistreDlg*)pParam;

	pObject->clientConnectat = false;
	/* Set the size of the in-out parameter */
	int claddrlength = sizeof(pObject->ClntAddr);
	/* Wait for a client to connect */
	if ((pObject->clntdes = accept(pObject->servdes, (struct sockaddr *) &pObject->ClntAddr, &claddrlength)) < 0) {
		printf("accept() failed");
		exit(-1);
	}

	pObject->clientConnectat = true;
	pObject->HandleClientStandAlone();

	return 0;
}

// Cuadro de di�logo de CDemoRegistreDlg



CDemoRegistreDlg::CDemoRegistreDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DEMOREGISTRE_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDemoRegistreDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTCAMERA, m_butCamera);
	DDX_Control(pDX, IDC_BUTSEGMEN, m_butSegmentation);
	DDX_Control(pDX, IDC_DISPLAY2, m_picSegmentation);
	DDX_Control(pDX, IDC_FPS, m_strFps);
	DDX_Control(pDX, IDC_BUTTracking, m_butTracking);
	DDX_Control(pDX, IDC_EDIT1, m_xvalCElipse);
	DDX_Control(pDX, IDC_EDIT2, m_yvalCElipse);
}

BEGIN_MESSAGE_MAP(CDemoRegistreDlg, CDialogEx)
	ON_WM_PAINT()
	ON_MESSAGE(IS_UEYE_MESSAGE, OnUEyeMessage)
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTCAMERA, &CDemoRegistreDlg::OnBnClickedButcamera)
	ON_BN_CLICKED(IDC_BUTSEGMEN, &CDemoRegistreDlg::OnBnClickedButsegmen)
	ON_BN_CLICKED(IDC_BUTPROVA, &CDemoRegistreDlg::OnBnClickedButprova)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_Prova2, &CDemoRegistreDlg::OnBnClickedProva2)
	ON_BN_CLICKED(IDCANCEL, &CDemoRegistreDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_Prova3, &CDemoRegistreDlg::OnBnClickedProva3)
	ON_BN_CLICKED(IDC_BUTTracking, &CDemoRegistreDlg::OnBnClickedButtracking)
	ON_BN_CLICKED(IDC_BUTPROVA4, &CDemoRegistreDlg::OnBnClickedButprova4)
END_MESSAGE_MAP()


// Controladores de mensaje de CDemoRegistreDlg

BOOL CDemoRegistreDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Establecer el icono para este cuadro de di�logo.  El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// TODO: agregar aqu� inicializaci�n adicional


	//App Control variables
	bRecording = false;
	ctrlVideoImageReady = 0;
	//Initialitation camera
	ueCamera = new uEyeCam();
	// get handle to display window
	ueCamera->m_hWndDisplay = GetDlgItem(IDC_DISPLAY)->m_hWnd;
	//Set configuration camera file [.ini]
	CString CamConfigFile("C:\\Users\\UPC-ESAII\\Google Drive\\Files\\ESAII\\ModelsOrgans\\Fotos Cor\\configuracioCameraCaptura_10fps.ini");
	//CString CamConfigFile("C:\\Users\\UPC-ESAII\\Google Drive\\Files\\ESAII\\ModelsOrgans\\Fotos Cor\\configuracioCameraCaptura_30fps.ini");
	//Open and setup uEye camera
	if (!ueCamera->OpenCamera(ueCamera->m_hWndDisplay, 0, CamConfigFile))
	{
		AfxMessageBox(_T("Error: Camera not initialized"), IDOK);
	}
	lastImage = cv::Mat(ueCamera->GetImageSize(), CV_8UC1);
	m_butCamera.SetWindowTextA(_T("Start Camera"));

	//Camera parameters
	CString filename = "CameraCalibracio.txt";
	CStdioFile file;
	CString filestr;
	file.Open(filename, CFile::modeRead);
	file.SeekToBegin();
	file.ReadString(filestr);
	sscanf_s(filestr, "%g  %g  %g  %g  %g  %lf  %lf  %lf  %lf  %lf", &fc[0], &fc[1], &cc[0], &cc[1], &alpha_c, &kc[0], &kc[1], &kc[2], &kc[3], &kc[4]);
	float intCamera[3][3] = { { fc[0], alpha_c, cc[0] },{ 0.0f, fc[1], cc[1] },{ 0.0f, 0.0f, 1.0f } };

	cameraMatrix = cv::Mat(3, 3, CV_32F, intCamera);
	distCoef = cv::Mat(1, 5, CV_64F, kc);
	TRACE("%f %f %f %f %f %f %f %f %f\n", cameraMatrix.at<float>(0, 0), cameraMatrix.at<float>(0, 1), cameraMatrix.at<float>(0, 2), cameraMatrix.at<float>(1, 0), cameraMatrix.at<float>(1, 1), cameraMatrix.at<float>(1, 2), cameraMatrix.at<float>(2, 0), cameraMatrix.at<float>(2, 1), cameraMatrix.at<float>(2, 2));
	file.Close();

	sms = "";
	//m_strRobotIP = "192.168.1.30";
	m_strRobotIP = "10.4.173.14";
	bRobConnected = FALSE;
	SetTimer(0, 500, NULL);//timer de connexi�

	RobotClientSock = new COurSocket(this, ROB_CLIENT);
	RobotClientSock->Create();

	//Variables segmentacio
	bSegmenting = false;
	m_butSegmentation.EnableWindow(FALSE);
	m_butSegmentation.SetWindowTextA(_T("Start Segmentation"));
	gradMorf = getStructuringElement(MORPH_RECT, Size(3, 3), Point(-1, -1));
	segerode = getStructuringElement(MORPH_CROSS, Size(3, 3), Point(-1, -1));
	segdilate = getStructuringElement(MORPH_RECT, Size(5, 5), Point(-1, -1));
	squareKernel = getStructuringElement(MORPH_RECT,Size(3,3), Point(-1, -1));
	datacross[0][0] = 1; datacross[0][1] = 0; datacross[0][2] = 1; datacross[1][0] = 0; datacross[1][1] = 1; datacross[1][2] = 0; datacross[2][0] = 1; datacross[2][1] = 0; datacross[2][2] = 1;
	b1[0][0] = 0; b1[0][1] = 0; b1[0][2] = 0; b1[1][0] = 1; b1[1][1] = 1; b1[1][2] = 1; b1[2][0] = 0; b1[2][1] = 0; b1[2][2] = 0;
	b2[0][0] = 0; b2[0][1] = 0; b2[0][2] = 1; b2[1][0] = 0; b2[1][1] = 1; b2[1][2] = 0; b2[2][0] = 1; b2[2][1] = 0; b2[2][2] = 0;
	b3[0][0] = 0; b3[0][1] = 1; b3[0][2] = 0; b3[1][0] = 0; b3[1][1] = 1; b3[1][2] = 0; b3[2][0] = 0; b3[2][1] = 1; b3[2][2] = 0;
	b4[0][0] = 1; b4[0][1] = 0; b4[0][2] = 0; b4[1][0] = 0; b4[1][1] = 1; b4[1][2] = 0; b4[2][0] = 0; b4[2][1] = 0; b4[2][2] = 1;
	crossKernel = cv::Mat(3,3,CV_8U,datacross);
	b1Kernel = cv::Mat(3, 3, CV_8U, b1);
	b2Kernel = cv::Mat(3, 3, CV_8U, b2);
	b3Kernel = cv::Mat(3, 3, CV_8U, b3);
	b4Kernel = cv::Mat(3, 3, CV_8U, b4);

	RECT r;
	m_picSegmentation.GetClientRect(&r);
	winSegSize = cv::Size(r.right, r.bottom);
	if (winSegSize.width % 4 != 0)
	{
		winSegSize.width = 4 * (winSegSize.width / 4);
	}
	cvImgTmp = cv::Mat(winSegSize, CV_8UC3);
	bitInfo.bmiHeader.biBitCount = 24;
	bitInfo.bmiHeader.biWidth = winSegSize.width;
	bitInfo.bmiHeader.biHeight = winSegSize.height;
	bitInfo.bmiHeader.biPlanes = 1;
	bitInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfo.bmiHeader.biCompression = BI_RGB;
	bitInfo.bmiHeader.biClrImportant = 0;
	bitInfo.bmiHeader.biClrUsed = 0;
	bitInfo.bmiHeader.biSizeImage = 0;
	bitInfo.bmiHeader.biXPelsPerMeter = 0;
	bitInfo.bmiHeader.biYPelsPerMeter = 0;

	//Read Punts 3D
	CString OrganFilename = "Ronyo3D.txt";
	//CString OrganFilename = "Cor3D.txt";
	CStdioFile organfile;
	CString fileorganstr;
	int sizeOrgan;

	organfile.Open(OrganFilename, CFile::modeRead);
	organfile.SeekToBegin();
	organfile.ReadString(fileorganstr);
	sscanf_s(fileorganstr, "%d", &sizeOrgan);
	punts3DMutex.lock();
	punts3D = std::vector<cv::Point3f>(sizeOrgan);
	for (int i = 0; i < sizeOrgan; i++) 
	{   
		organfile.ReadString(fileorganstr);
		sscanf_s(fileorganstr, "%g %g %g", &punts3D[i].x, &punts3D[i].y, &punts3D[i].z);
	}
	punts3DMutex.unlock();
	organfile.Close();

	//Kalman Filter
	predRate = 20.0f;

	//Timer Pintar
	bPintant = false;
	SetTimer(0, 50, NULL);
	pintarSize = ueCamera->GetImageSize();

	//Initizlization pos and angles
	iniICPMutex.lock();
	IniICP[0] = -20.0f;
	IniICP[1] = 40.0f;
	IniICP[2] = 270.0f;
	IniICP[3] = 90.0f;
	IniICP[4] = 0.0f;
	IniICP[5] = 90.0f;
	iniICPMutex.unlock();

	currentStateMutex.lock();
	currentState[0] = -20.0f;
	currentState[1] = 40.0f;
	currentState[2] = 270.0f;
	currentState[3] = 90.0f;
	currentState[4] = 0.0f;
	currentState[5] = 90.0f;
	currentStateMutex.unlock();
	btracking = false;
	m_butTracking.EnableWindow(FALSE);
	m_butTracking.SetWindowTextA(_T("Start Tracking"));

	PuntsPintar3D = Eigen::Matrix4Xf(4, sizeOrgan);
	punts3DMutex.lock();
	for (int i = 0; i < punts3D.size(); i++)
	{
		PuntsPintar3D(0, i) = punts3D[i].x;
		PuntsPintar3D(1, i) = punts3D[i].y;
		PuntsPintar3D(2, i) = punts3D[i].z;
		PuntsPintar3D(3, i) = 1.0f;
	}
	punts3DMutex.unlock();

	//Sockets Demo
	clientConnectat = false;
	ex = 1.0f;
	ey = 0.2f;
	ex0 = 0.0f;
	ey0 = 0.0f;
	/* Create socket for incoming connections */
	if ((servdes = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		printf("socket() failed");
		exit(-1);
	}

	/* Construct local address structure */
	memset(&ServAddr, 0, sizeof(ServAddr));       /* Zero out structure */
	ServAddr.sin_family = AF_INET;                /* Internet address family */
	ServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
	ServAddr.sin_port = htons(LOCALPORT);         /* Local port */
	 /* Bind to the local address */
	if (::bind(servdes, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		printf("bind() failed");
		exit(-1);
	}

	/* Mark the socket so it will listen for incoming connections */
	if (listen(servdes, 1) < 0) {
		printf("listen() failed");
		exit(-1);
	}
	thSocket = AfxBeginThread(ThreadSocket, this, THREAD_PRIORITY_HIGHEST, 0, 0, NULL);	//Initialize thread Socket


	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono.  Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CDemoRegistreDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CDemoRegistreDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

///////////////////////////////////////////////////////////////////////////////
//
// METHOD CIdsSimpleLiveDlg::OnUEyeMessage() 
//
// DESCRIPTION: - handles the messages from the uEye camera
//				- messages must be enabled using is_EnableMessage()
//
///////////////////////////////////////////////////////////////////////////////
LRESULT CDemoRegistreDlg::OnUEyeMessage(WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
	case IS_DEVICE_REMOVED:
		Beep(400, 50);
		break;
	case IS_DEVICE_RECONNECTED:
		Beep(400, 50);
		break;
	case IS_FRAME:
	{
		/*if (ctrlVideoImageReady == 0)
		{
			ctrlVideoImageReady = 1;
			ueCamera->CaptureFrame();
			ueCamera->GetImage(ueCamera->iImage);
			ctrlVideoImageReady = 0;
		}*/


		/*// lock buffer for processing
		int nRet = is_LockSeqBuf(ueCamera->m_hCam, ueCamera->m_lMemoryId, ueCamera->m_pcImageMemory);
		if (nRet != IS_SUCCESS)
		{
		AfxMessageBox(_T("Error lock uEyeCam buffer"), IDOK);
		exit(-1);
		}

		//iTimeStamp++;
		UEYEIMAGEINFO ImageInfo;
		if (is_GetImageInfo(ueCamera->m_hCam, ueCamera->m_lMemoryId, &ImageInfo, sizeof(ImageInfo)) == IS_SUCCESS)
		{
		UINT64 u64TimestampDevice = ImageInfo.u64TimestampDevice;
		//iTimeStamp = u64TimestampDevice;
		iTimeStamp++;
		UpdateData(false);
		}*/

		//INI DEBUG: MIRAR SI TRASPASSO EL RENDERITZAT DE LA IMG AL THREAD.
		if (ueCamera->m_pcImageMemory != NULL)
		{
			is_RenderBitmap(ueCamera->m_hCam, ueCamera->m_lMemoryId, ueCamera->m_hWndDisplay, ueCamera->m_nRenderMode);
		}
		//END DEBUG: MIRAR SI TRASPASSO EL RENDERITZAT DE LA IMG AL THREAD.

		/*DWORD ImageBuffers = ImageInfo.dwImageBuffers;
		DWORD ImageBuffersInUse = ImageInfo.dwImageBuffersInUse;

		// unlock buffer
		nRet = is_UnlockSeqBuf(ueCamera->m_hCam, ueCamera->m_lMemoryId, ueCamera->m_pcImageMemory);
		if (nRet != IS_SUCCESS)
		{
		AfxMessageBox(_T("Error unlock uEyeCam buffer"),IDOK);
		exit(-1);
		}

		*/
		break;
	}
	}
	return 0;
}

void CDemoRegistreDlg::desdistorcionarPunts(std::vector<cv::Point2i> *pixelsIn, std::vector<cv::Point2f> *pixelsDes, int nPix)
{
	Eigen::VectorXf rnorm(nPix), aux(nPix), uns(nPix);
	Eigen::Matrix2Xf pixels(2,nPix);
	Eigen::Matrix2Xf pixelsDesdistorcionats(2,nPix);
	float delta_x[2];
	uns.setOnes();
	for (int i = 0; i < nPix; i++)
	{
		pixels(0, i) = ((float)(*pixelsIn)[i].x - cc[0])/fc[0];
		pixels(1, i) = ((float)(*pixelsIn)[i].y - cc[1])/fc[1];
		pixelsDesdistorcionats(0, i) = pixels(0, i);
		pixelsDesdistorcionats(1, i) = pixels(1, i);
	}
	for (int nit = 0; nit<40; nit++)
	{
		rnorm = (pixelsDesdistorcionats).row(1).cwiseProduct((pixelsDesdistorcionats).row(1)) + (pixelsDesdistorcionats).row(0).cwiseProduct((pixelsDesdistorcionats).row(0));
		aux = uns + kc[0] * rnorm + (float)kc[1] * rnorm.cwiseProduct(rnorm) + (float)kc[4] * rnorm.cwiseProduct(rnorm.cwiseProduct(rnorm));
		for (int i = 0; i<nPix; i++)
		{
			delta_x[0] = 2 * (float)kc[2] * (pixelsDesdistorcionats)(0, i)*(pixelsDesdistorcionats)(1, i) + (float)kc[3] * (rnorm(i) + 2 * (pixelsDesdistorcionats)(0, i)*(pixelsDesdistorcionats)(0, i));
			delta_x[1] = (float)kc[2] * (rnorm(i) + 2 * (pixelsDesdistorcionats)(1, i)*(pixelsDesdistorcionats)(1, i)) + 2 * (float)kc[3] * (pixelsDesdistorcionats)(0, i)*(pixelsDesdistorcionats)(1, i);
			for (int j = 0; j<2; j++)
				(pixelsDesdistorcionats)(j, i) = (pixels(j, i) - delta_x[j]) / aux(i);
		}
	}
	*pixelsDes = std::vector<cv::Point2f>(nPix);
	//M'interessa sense ser pixels. En el pla z = 1
	for (int i = 0; i < nPix; i++)
	{
		(*pixelsDes)[i].x = pixelsDesdistorcionats(0, i);// *fc[0] + cc[0];
		(*pixelsDes)[i].y = pixelsDesdistorcionats(1, i);// *fc[1] + cc[1];
	}
}

bool CDemoRegistreDlg::InitializeVideoRecording()
{
	//Setup Video Size (Activate desired size or define a new one)
	VideoSize = ueCamera->GetImageSize();   //Resolution from ueCamera

											//VideoSize = cvSize(1920,1080);          //Resolution for 1080p (Full HD)
											//VideoSize = cvSize(1440,1080);          //Resolution for 1080i (HDV)

											//Setup Video frame rate (FPS) (Activate desired FPS or define a new one)
											//VideoFrameRate = ueCamera->GetFPS();    //FPS from ueCamera
	VideoFrameRate = 30.0;
	//VideoFrameRate = 60.0;

	//Video Image initialization (image that will be used to display and record from camera
	VideoImage = cvCreateImage(VideoSize, 8, 1);	//Create original image header and allocate the image data. Function parameters (CvSize size, int depth, int channels)
	//VideoWriter = cvCreateVideoWriter("record.avi", -1, VideoFrameRate, VideoSize);
	//CV_FOURCC('M', 'J', 'P', 'G')
	//Initialize and setup camera acquisition
	//Initialize camera acquisition
	bool ret = ueCamera->InitAcquisition(GetSafeHwnd());
	return ret;
}

void CDemoRegistreDlg::FinalizeVideoRecording()
{
	//Finalize camera acquisition
	ueCamera->FinalizeAcquisition();

	//Finalize and close videos and images
	cvReleaseImage(&VideoImage);
	cvReleaseImage(&VideoImageUI);
	//cvReleaseVideoWriter(&VideoWriter);
}

void CDemoRegistreDlg::OnConnected(int id, int nErrorCode)
{
	switch (id)
	{
	case ROB_CLIENT:
		if (nErrorCode == 0)
		{
			UpdateData(true);
			bRobConnected = TRUE;

			sms = Interpret::reset(STATE_WAITING);
			TRACE(sms);
			RobotClientSock->CAsyncSocket::Send(sms, sms.GetLength(), 0);
			//m_strMissatge = "Connexi� amb el robot OK";
			//m_butConnect.SetWindowText(CString("Disconnect"));
			UpdateData(false);
		}
		else
			bRobConnected = FALSE;
		break;
	}
}

void CDemoRegistreDlg::OnClosed(int id, int nErrorCode)
{
	int res;
	switch (id)
	{
	case ROB_CLIENT:
		RobotClientSock->Close();
		delete RobotClientSock;
		RobotClientSock = new COurSocket(this, ROB_CLIENT);
		res = RobotClientSock->Create();
		bRobConnected = FALSE;
		//m_strMissatge = "Robot Desconnectat!";
		//m_butConnect.SetWindowText(CString("Connectar"));
		UpdateData(false);
		break;
	}
}

void CDemoRegistreDlg::OnRecieve(int id, int nErrorCode)
{
	UpdateData(true);
	CString sent;
	//CString sms;
	//int res;
	//double x,y,z,alfa,beta, gamma,joint0,joint1,joint2,joint3,joint4,joint5;
	int errorcode;
	CString errmssg;
	TRACE("REBO DEL ROBOT\n");
	//Sleep(1);
	int code;
	switch (id)
	{
	case ROB_CLIENT:
		RobotClientSock->Receive(&sent);

		//m_srtStatusMssg.Insert(m_srtStatusMssg.GetLength(),_T("\r\n"));
		//SetDlgItemText(IDC_EDIT38,sent);
		TRACE(sent + "\n");
		Interpret::capture_data(sent, &last_pos.x, &last_pos.y, &last_pos.z, &last_pos.alpha, &last_pos.beta, &last_pos.gamma, &last_pos.joint0, &last_pos.joint1, &last_pos.joint2, &last_pos.joint3, &last_pos.joint4, &last_pos.joint5, &errorcode, &errmssg, &code, &last_pos.qw, &last_pos.qx, &last_pos.qy, &last_pos.qz);
		//		m_strRobotStr.Format("x:%lf y:%lf z:%lf a:%lf b:%lf c:%lf | j1:%lf j2:%lf j3:%lf j4:%lf j5:%lf j6:%lf | qw:%lf qx:%lf qy:%lf qz:%lf |", last_pos.x, last_pos.y, last_pos.z, last_pos.alpha, last_pos.beta, last_pos.gamma, last_pos.joint0, last_pos.joint1, last_pos.joint2, last_pos.joint3, last_pos.joint4, last_pos.joint5,last_pos.qw,last_pos.qx,last_pos.qy,last_pos.qz);
		//m_srtStatusMssg.Insert(m_srtStatusMssg.GetLength(),sent);
		TRACE("Code missatge rebut: %d\n", code);
		if (sent.Find("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0") >= 0)
		{
			sms = Interpret::reset(STATE_WAITING);
			RobotClientSock->Send(&sms);
		}

		if ((errorcode != 0) && (errorcode != 7))
		{
			TRACE("ERROR1: " + errmssg + "\n");
		}
		//	euler2quaternion(last_pos.alpha, last_pos.beta, last_pos.gamma,&last_pos.qw,&last_pos.qx,&last_pos.qy,&last_pos.qz);
		if (errorcode == 7)
		{
			TRACE("ERROR2: " + errmssg + "\n");
			TRACE(sms);
			RobotClientSock->Send(&sms);
		}
		else
		{

			if (code == STATE_WAITING)
			{
				if (bRobConnected)
				{
					/*sms = Interpret::reset(STATE_WAITING);
					RobotClientSock->Send(&sms);*/
				}
			}
		}
		break;
	}
	//ReadyRob= true;

	UpdateData(false);
}



BOOL CDemoRegistreDlg::DestroyWindow()
{
	// TODO: Agregue aqu� su c�digo especializado o llame a la clase base

	//Robot destruction
	if (bRobConnected)
	{
		sms = Interpret::close_connexion(0);
		RobotClientSock->Send(&sms);
		//OnClosed(ROB_CLIENT, 0);
	}
	delete RobotClientSock;

	//Close and Delete uEyeCam

	if (bSegmenting)
	{
		OnBnClickedButsegmen();
		bSegmenting = false;
		Sleep(100);
	}

	if (bRecording)
	{
		OnBnClickedButcamera();
		ueCamera->FinalizeAcquisition();
		bRecording = false;
		//Whait until thread is dead
		Sleep(100);
		//Finalize and close video
		cvReleaseImage(&VideoImage);
		//cvReleaseVideoWriter(&VideoWriter);
	}
	ueCamera->FinalizeAcquisition();
	ueCamera->ExitCamera();
	delete(ueCamera);
	if(bPintant)
		acabarPintar();
	if (bFinalICP)
	{
		bFinalICP = false;
		bFinalKF = false;
		Sleep(500);
	}
	if (bFinalKF)
	{
		bFinalKF = false;
		Sleep(500);
	}

	return CDialogEx::DestroyWindow();
}


void CDemoRegistreDlg::OnBnClickedButcamera()
{
	if (bRecording)
	{
		bRecording = false;
		if (bSegmenting)
		{ 
			bSegmenting = false;
		}
		if (btracking)
		{
			OnBnClickedButtracking();
		}
		FinalizeVideoRecording();
		Sleep(100);
		m_butCamera.SetWindowTextA(_T("Start Camera"));
		m_butSegmentation.EnableWindow(FALSE);
		m_butSegmentation.SetWindowTextA(_T("Start Segmentation"));
		m_butTracking.EnableWindow(FALSE);
		m_butTracking.SetWindowTextA(_T("Start Tracking"));
	}
	else
	{
		bRecording = true;
		InitializeVideoRecording();
		Sleep(100);
		m_butCamera.SetWindowTextA(_T("Stop Camera"));
		m_butSegmentation.EnableWindow(TRUE);
		m_butSegmentation.SetWindowTextA(_T("Start Segmentation"));
		thRecording = AfxBeginThread(ThreadRecording, this, THREAD_PRIORITY_HIGHEST, 0, 0, NULL);	//Initialize thread uEyeCamera
	}
}

void CDemoRegistreDlg::Imadjust(cv::Mat * entrada, cv::Mat * sortida, int lin, int hin, int lout, int hout)
{
	(*sortida) = entrada->clone();
	float scale = float(hout - lout) / float(hin - lin);
	uint8_t *myData = entrada->data;
	uint8_t *myOut = sortida->data;
	int width = entrada->cols;
	int height = entrada->rows;
	int index;
	for (int r = 0; r < height; ++r)
	{
		for (int c = 0; c < width; ++c)
		{
			index = r * sortida->cols + c;
			uint8_t val = myData[index];
			if (val < lin)
				myOut[index] = lout;
			else if (val > hin)
				myOut[index] = hout;
			else
			{
				myOut[index] = int((val -lin) * scale + 0.5f + lout);
			}
		}
	}
}

void CDemoRegistreDlg::Imadjust(const Mat1b& src, Mat1b& dst, int tol = 1, Vec2i in = Vec2i(0, 255), Vec2i out = Vec2i(0, 255))
{
	// src : input CV_8UC1 image
	// dst : output CV_8UC1 imge
	// tol : tolerance, from 0 to 100.
	// in  : src image bounds
	// out : dst image buonds

	dst = src.clone();

	tol = max(0, min(100, tol));

	if (tol > 0)
	{
		// Compute in and out limits

		// Histogram
		vector<int> hist(256, 0);
		for (int r = 0; r < src.rows; ++r) {
			for (int c = 0; c < src.cols; ++c) {
				hist[src(r, c)]++;
			}
		}

		// Cumulative histogram
		vector<int> cum = hist;
		for (int i = 1; i < hist.size(); ++i) {
			cum[i] = cum[i - 1] + hist[i];
		}

		// Compute bounds
		int total = src.rows * src.cols;
		int low_bound = total * tol / 100;
		int upp_bound = total * (100 - tol) / 100;
		in[0] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), low_bound));
		in[1] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), upp_bound));

	}

	// Stretching
	float scale = float(out[1] - out[0]) / float(in[1] - in[0]);
	for (int r = 0; r < dst.rows; ++r)
	{
		for (int c = 0; c < dst.cols; ++c)
		{
			int vs = max(src(r, c) - in[0], 0);
			int vd = min(int(vs * scale + 0.5f) + out[0], out[1]);
			dst(r, c) = saturate_cast<uchar>(vd);
		}
	}
}

//CannyPF from the paper CannyLines: A parameter-free line segment detector (Lu et al ICIP2015)
void CDemoRegistreDlg::cannyPF(cv::Mat &image, int gaussianSize, float VMGradient, cv::Mat &edgeMap)
{
	int i, j, m, n;
	int grayLevels = 255;
	float gNoise = 1.3333;  //1.3333 
	float thGradientLow = gNoise;

	int cols = image.cols;
	int rows = image.rows;
	int cols_1 = cols - 1;
	int rows_1 = rows - 1;

	//meaningful Length
	int thMeaningfulLength = int(2.0*log((float)rows*cols) / log(8.0) + 0.5);
	float thAngle = 2 * atan(2.0 / float(thMeaningfulLength));

	/*//get gray image
	cv::Mat grayImage;
	int aa = image.channels();

	if (image.channels() == 1)
		grayImage = image;
	else
		cv::cvtColor(image, grayImage, CV_BGR2GRAY);*/

	//gaussian filter
	cv::Mat filteredImage;
	cv::GaussianBlur(image, filteredImage, cv::Size(gaussianSize, gaussianSize), 1.0);
	//grayImage.release();

	//get gradient map and orientation map
	cv::Mat gradientMap = cv::Mat::zeros(filteredImage.rows, filteredImage.cols, CV_32FC1);
	cv::Mat dx(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));
	cv::Mat dy(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));

	int apertureSize = 3;
	cv::Sobel(filteredImage, dx, CV_16S, 1, 0, apertureSize, 1, 0, cv::BORDER_REPLICATE);
	cv::Sobel(filteredImage, dy, CV_16S, 0, 1, apertureSize, 1, 0, cv::BORDER_REPLICATE);

	//calculate gradient and orientation
	int totalNum = 0;
	int times = 8;
	std::vector<int> histogram(times*grayLevels, 0);
	int inir,finr,inic,finc;
	if (numSegm == 0)
	{
		inir = rows / 5;
		finr = 4 * rows / 5;
		inic = cols / 5; 
		finc = 4 * cols / 5;
	}
	else
	{
		inir = max((int)meanSegmentats[0].y - 100 - pintarSize.height / 4,0);
		finr = min((int)meanSegmentats[0].y + 100 - pintarSize.height/4, pintarSize.height/2);
		inic = max((int)meanSegmentats[0].x - 150 - pintarSize.width / 4, 0);
		finc = min((int)meanSegmentats[0].x + 150 - pintarSize.width / 4, pintarSize.width/2);
	}
	for (i = inir; i<finr; ++i)//for (i = 0; i<rows; ++i)
	{
		float *ptrG = gradientMap.ptr<float>(i);
		short *ptrX = dx.ptr<short>(i);
		short *ptrY = dy.ptr<short>(i);
		for (j = inic; j<finc; ++j)//for (j = 0; j<cols; ++j)
		{
			float gx = ptrX[j];
			float gy = ptrY[j];

			ptrG[j] = sqrt(gx*gx + gy*gy);// abs(gx) + abs(gy);
			if (ptrG[j]>thGradientLow)
			{
				histogram[int(ptrG[j] + 0.5)]++;
				totalNum++;
			}
			else
				ptrG[j] = 0.0;
		}
	}

	//gradient statistic
	float N2 = 0;
	for (i = 0; i<histogram.size(); ++i)
	{
		if (histogram[i])
			N2 += (float)histogram[i] * (histogram[i] - 1);
	}
	float pMax = 1.0 / exp((log(N2) / thMeaningfulLength));
	float pMin = 1.0 / exp((log(N2) / sqrt((float)cols*rows)));

	std::vector<float> greaterThan(times*grayLevels, 0);
	int count = 0;
	for (i = times*grayLevels - 1; i >= 0; --i)
	{
		count += histogram[i];
		float probabilityGreater = float(count) / float(totalNum);
		greaterThan[i] = probabilityGreater;
	}
	count = 0;

	//get two gradient thresholds
	int thGradientHigh = 0;
	for (i = times*grayLevels - 1; i >= 0; --i)
	{
		if (greaterThan[i]>pMax)
		{
			thGradientHigh = i;
			break;
		}
	}
	for (i = times*grayLevels - 1; i >= 0; --i)
	{
		if (greaterThan[i]>pMin)
		{
			thGradientLow = i;
			break;
		}
	}
	if (thGradientLow<gNoise) thGradientLow = gNoise;

	//cout<<thGradientLow<<"  "<<thGradientHigh<<endl;

	//convert probabilistic meaningful to visual meaningful
	thGradientHigh = sqrt(thGradientHigh*VMGradient);

	//canny
	cv::Canny(filteredImage, edgeMap, thGradientLow, thGradientHigh, apertureSize,true);
}

//CannyPF from the paper CannyLines: A parameter-free line segment detector (Lu et al ICIP2015)
void CDemoRegistreDlg::cannyPF(cv::Mat &image, int gaussianSize, float VMGradient, cv::Mat &edgeMap, cv::Mat &gradMod, cv::Mat &gradAngle)
{
	int i, j, m, n;
	int grayLevels = 255;
	float gNoise = 1.3333;  //1.3333 
	float thGradientLow = gNoise;

	int cols = image.cols;
	int rows = image.rows;
	int cols_1 = cols - 1;
	int rows_1 = rows - 1;

	//meaningful Length
	int thMeaningfulLength = int(2.0*log((float)rows*cols) / log(8.0) + 0.5);
	float thAngle = 2 * atan(2.0 / float(thMeaningfulLength));

	/*//get gray image
	cv::Mat grayImage;
	int aa = image.channels();

	if (image.channels() == 1)
	grayImage = image;
	else
	cv::cvtColor(image, grayImage, CV_BGR2GRAY);*/

	//gaussian filter
	cv::Mat filteredImage;
	cv::GaussianBlur(image, filteredImage, cv::Size(gaussianSize, gaussianSize), 1.0);
	//grayImage.release();

	//get gradient map and orientation map
	gradMod = cv::Mat::zeros(filteredImage.rows, filteredImage.cols, CV_32FC1);
	gradAngle = cv::Mat::zeros(filteredImage.rows, filteredImage.cols, CV_32FC1);
	cv::Mat dx(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));
	cv::Mat dy(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));

	int apertureSize = 3;
	cv::Sobel(filteredImage, dx, CV_16S, 1, 0, apertureSize, 1, 0, cv::BORDER_REPLICATE);
	cv::Sobel(filteredImage, dy, CV_16S, 0, 1, apertureSize, 1, 0, cv::BORDER_REPLICATE);

	//calculate gradient and orientation
	int totalNum = 0;
	int times = 8;
	std::vector<int> histogram(times*grayLevels, 0);
	for (i = rows / 10; i<9 * rows / 10; ++i)//for (i = 0; i<rows; ++i)
	{
		float *ptrG = gradMod.ptr<float>(i);
		float *ptrA = gradAngle.ptr<float>(i);
		short *ptrX = dx.ptr<short>(i);
		short *ptrY = dy.ptr<short>(i);
		for (j = cols / 10; j<9 * cols / 10; ++j)//for (j = 0; j<cols; ++j)
		{
			float gx = ptrX[j];
			float gy = ptrY[j];

			ptrG[j] = sqrt(gx*gx + gy*gy);// abs(gx) + abs(gy);
			ptrA[j] = atan2(gy, gx);
			if (ptrG[j]>thGradientLow)
			{
				histogram[int(ptrG[j] + 0.5)]++;
				totalNum++;
			}
			else
				ptrG[j] = 0.0;
		}
	}

	//gradient statistic
	float N2 = 0;
	for (i = 0; i<histogram.size(); ++i)
	{
		if (histogram[i])
			N2 += (float)histogram[i] * (histogram[i] - 1);
	}
	float pMax = 1.0 / exp((log(N2) / thMeaningfulLength));
	float pMin = 1.0 / exp((log(N2) / sqrt((float)cols*rows)));

	std::vector<float> greaterThan(times*grayLevels, 0);
	int count = 0;
	for (i = times*grayLevels - 1; i >= 0; --i)
	{
		count += histogram[i];
		float probabilityGreater = float(count) / float(totalNum);
		greaterThan[i] = probabilityGreater;
	}
	count = 0;

	//get two gradient thresholds
	int thGradientHigh = 0;
	for (i = times*grayLevels - 1; i >= 0; --i)
	{
		if (greaterThan[i]>pMax)
		{
			thGradientHigh = i;
			break;
		}
	}
	for (i = times*grayLevels - 1; i >= 0; --i)
	{
		if (greaterThan[i]>pMin)
		{
			thGradientLow = i;
			break;
		}
	}
	if (thGradientLow<gNoise) thGradientLow = gNoise;

	//cout<<thGradientLow<<"  "<<thGradientHigh<<endl;

	//convert probabilistic meaningful to visual meaningful
	thGradientHigh = sqrt(thGradientHigh*VMGradient);

	//canny
	cv::Canny(filteredImage, edgeMap, thGradientLow, thGradientHigh, apertureSize, true);
}

void CDemoRegistreDlg::SegmentacioImatge(cv::Mat * entrada, cv::Mat * sortida)
{

	_LARGE_INTEGER StartingTime, EndingTime, ElapsedMiliseconds;
	LARGE_INTEGER Frequency;
	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&StartingTime);
	

	cv::Mat auxiliar,auxiliar2;
	cv::Mat centroids, stats;
	//cv::Mat Gb1, Gb2, Gb3, Gb4;
	//cv::Mat Gd1, Ge1, Gd2, Ge2, Gd3, Ge3, Gd4, Ge4;

	//CString path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeCanny2\\";
	CString nameImage;


	auxiliar = entrada->clone();
	auxiliar = Mat(auxiliar, cv::Rect(auxiliar.cols / 4, auxiliar.rows / 4, auxiliar.cols / 2, auxiliar.rows / 2));
	cv::medianBlur( auxiliar, auxiliar2, 5); //Median Filter of window 3x3

	cv::bilateralFilter(auxiliar2, auxiliar, 5, 150, 150);
	//Imadjust(&auxiliar2, &auxiliar, 25, 140, 0, 255); //Adjust a part of the histogram

	auxiliar = imCompositeFilter(auxiliar);

	/*path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeAdjust\\";
	nameImage.Format("Imatge_%d.png", globalImage);
	nameImage = path + nameImage;
	imwrite(nameImage.GetString(), auxiliar);*/
	/*Gb1 = imMorphGrad(auxiliar, b1Kernel);
	Gb2 = imMorphGrad(auxiliar, b2Kernel);
	Gb3 = imMorphGrad(auxiliar, b3Kernel);
	Gb4 = imMorphGrad(auxiliar, b4Kernel);*/
	/*Gd1 = imMorphDilate(auxiliar, b1Kernel);
	Gd2 = imMorphDilate(auxiliar, b2Kernel);
	Gd3 = imMorphDilate(auxiliar, b3Kernel);
	Gd4 = imMorphDilate(auxiliar, b4Kernel);
	Ge1 = imMorphErode(auxiliar, b1Kernel);
	Ge2 = imMorphErode(auxiliar, b2Kernel);
	Ge3 = imMorphErode(auxiliar, b3Kernel);
	Ge4 = imMorphErode(auxiliar, b4Kernel);
	Gb1 = cv::min(Gd1, Ge1);
	Gb2 = cv::min(Gd2, Ge2);
	Gb3 = cv::min(Gd3, Ge3);
	Gb4 = cv::min(Gd4, Ge4);*/
	//auxiliar = Gd1 + Ge1 + Gb1 + Gd2 + Ge2 + Gb2 + Gd3 + Ge3 + Gb3 + Gd4 + Ge4 + Gb4;
	/*auxiliar = Gd1 + Ge1 + Gb1 + Gd2 + Ge2 + Gb2;// +Gd2 / 2 + Ge2 / 2 + Gb2 / 2 + Gd4 / 2 + Ge4 / 2 + Gb4 / 2;
	auxiliar2 = Gd3 + Ge3 + Gb3 + Gd4 + Ge4 + Gb4;//+ Gd2 / 2 + Ge2 / 2 + Gb2 / 2 - Gd4 / 2 - Ge4 / 2 - Gb4 / 2;

	namedWindow("Display window1", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window1", auxiliar);
	waitKey(10);

	namedWindow("Display window2", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window2", auxiliar2);
	waitKey(10);*/


	//Morfologic gradient
	/*cv::dilate(auxiliar, auxiliar2, gradMorf);
	cv::erode(auxiliar, auxiliar, gradMorf);
	auxiliar = auxiliar2 - auxiliar;*/

	//Laplacian(auxiliar, auxiliar, CV_8U, 3);// , scale, delta, BORDER_DEFAULT);
	//blur(auxiliar, auxiliar, Size(3, 3));
	//Canny(auxiliar, auxiliar2, 1100, 600, 5, true);
	cannyPF(auxiliar, 5, 110.0, auxiliar2);
	/*path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeCanny\\";
	nameImage.Format("Imatge_%d.png", globalImage);
	nameImage = path + nameImage;
	imwrite(nameImage.GetString(), auxiliar2);*/
	/*auxiliar.convertTo(auxiliar, CV_16SC1, 32767.0/255);
	auxiliar2.convertTo(auxiliar2, CV_16SC1, 32767.0 / 255);

	namedWindow("Display window3", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window3", auxiliar);
	waitKey(10);

	namedWindow("Display window4", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window4", auxiliar2);
	waitKey(10);

	Canny(auxiliar, auxiliar2, auxiliar2, 13000, 8000, true);
	namedWindow("Display window5", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window5", auxiliar2);
	waitKey(10);

	auxiliar2.convertTo(auxiliar2, CV_8U);
	*/
	

	//Filter small blobs
	//cv::filterSpeckles(auxiliar2, 0, 20, 0);

	//Dilate+Erosion to connect the contorn
	//cv::dilate(auxiliar2, auxiliar, segdilate);
	//cv::erode(auxiliar, auxiliar2, segerode);
	
	cv::morphologyEx(auxiliar2, auxiliar2, MORPH_CLOSE, squareKernel, cv::Point(-1, -1), 2);
	cv::dilate(auxiliar2, auxiliar, segdilate);
	cv::erode(auxiliar, auxiliar2, segerode);

	/*path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeClose\\";
	nameImage.Format("Imatge_%d.png", globalImage);
	nameImage = path + nameImage;
	imwrite(nameImage.GetString(), auxiliar2);*/

	//binarize
	//cv::threshold(auxiliar2, auxiliar2, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

	//Filter small blobs
	cv::filterSpeckles(auxiliar2, 0, 100, 0);
	/*path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeSpeckles\\";
	nameImage.Format("Imatge_%d.png", globalImage);
	nameImage = path + nameImage;
	imwrite(nameImage.GetString(), auxiliar2);*/
	//Dilate+Erosion to connect the contorn
	//cv::dilate(auxiliar2, auxiliar, segdilate);
	//cv::erode(auxiliar, auxiliar2, segerode);

	ZhangSuen(auxiliar2, auxiliar2);

	//Etiquetem els conjunts de pixels
	int nLabels = cv::connectedComponentsWithStats(auxiliar2, auxiliar, stats, centroids);
	
	//Busquem el de m�nima d�stacia al centre
	double centreImgh, centreImgw;
	if (numSegm > 0)
	{
		centreImgh = meanSegmentats[0].y - auxiliar.rows / 2;
		centreImgw = meanSegmentats[0].x - auxiliar.cols / 2;
	}
	else
	{
		centreImgh = auxiliar.rows / 2.0;
		centreImgw = auxiliar.cols / 2.0;
	}
	/*double distMin = winSegSize.height + winSegSize.width + 1000.0;
	double distAux;
	int indexMin = 0;
	for (int i = 1; i < nLabels; ++i)
	{
		distAux = sqrt(pow(centreImgh - centroids.at<double>(i, 1), 2) + pow(centreImgw - centroids.at<double>(i, 0), 2));
		if (distAux < distMin)
		{
			distMin = distAux;
			indexMin = i;
		}
	}
	compare(auxiliar, indexMin, auxiliar2, CMP_EQ);
	(*sortida) = (auxiliar2).clone();*/
	cv::Mat auxiliar3 = auxiliar2.clone();
	double distMin = max(centreImgh,centreImgw)/5.0;
	double distAux;
	auxiliar3.setTo(Scalar::all(0));
	for (int i = 1; i < nLabels; ++i)
	{
		distAux = sqrt(pow(centreImgh - centroids.at<double>(i, 1), 2) + pow(centreImgw - centroids.at<double>(i, 0), 2));
		if (distAux < distMin)
		{
			compare(auxiliar, i, auxiliar2, CMP_EQ);
			auxiliar3 += auxiliar2;
		}
	}
	(*sortida) = cv::Mat::zeros(cv::Size(entrada->cols,entrada->rows), entrada->type());
	auxiliar3.copyTo((*sortida)(cv::Rect(entrada->cols / 4, entrada->rows / 4, auxiliar.cols, auxiliar.rows)));
	//(*sortida) = (auxiliar3).clone();
	 

	QueryPerformanceCounter(&EndingTime);
	ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
	// We now have the elapsed number of ticks, along with the number of ticks-per-second. We use these values
	// to convert to the number of elapsed milliseconds.  To guard against loss-of-precision, we convert
	// to milliseconds *before* dividing by ticks-per-second.
	ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
	ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
	TRACE("TEMPS TARDAAAAAAAAAAAAAAAAAAAAAAAAAAT %d\n", ElapsedMiliseconds.QuadPart);
}

void CDemoRegistreDlg::SegmentacioImatge(cv::Mat &entrada, cv::Mat &sortida, cv::Mat &gradMod, cv::Mat &gradAngle)
{

	_LARGE_INTEGER StartingTime, EndingTime, ElapsedMiliseconds;
	LARGE_INTEGER Frequency;
	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&StartingTime);


	cv::Mat auxiliar, auxiliar2;
	cv::Mat centroids, stats;
	cv::Mat Gb1, Gb2, Gb3, Gb4;
	cv::Mat Gd1, Ge1, Gd2, Ge2, Gd3, Ge3, Gd4, Ge4;

	CString path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeCanny2\\";
	CString nameImage;


	auxiliar = entrada.clone();
	cv::medianBlur(auxiliar, auxiliar2, 5); //Median Filter of window 3x3
											//Imadjust(&auxiliar2, &auxiliar, 25, 140, 0, 255); //Adjust a part of the histogram

	auxiliar = imCompositeFilter(auxiliar);

	path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeAdjust\\";
	nameImage.Format("Imatge_%d.png", globalImage);
	nameImage = path + nameImage;
	imwrite(nameImage.GetString(), auxiliar);
	/*Gb1 = imMorphGrad(auxiliar, b1Kernel);
	Gb2 = imMorphGrad(auxiliar, b2Kernel);
	Gb3 = imMorphGrad(auxiliar, b3Kernel);
	Gb4 = imMorphGrad(auxiliar, b4Kernel);*/
	/*Gd1 = imMorphDilate(auxiliar, b1Kernel);
	Gd2 = imMorphDilate(auxiliar, b2Kernel);
	Gd3 = imMorphDilate(auxiliar, b3Kernel);
	Gd4 = imMorphDilate(auxiliar, b4Kernel);
	Ge1 = imMorphErode(auxiliar, b1Kernel);
	Ge2 = imMorphErode(auxiliar, b2Kernel);
	Ge3 = imMorphErode(auxiliar, b3Kernel);
	Ge4 = imMorphErode(auxiliar, b4Kernel);
	Gb1 = cv::min(Gd1, Ge1);
	Gb2 = cv::min(Gd2, Ge2);
	Gb3 = cv::min(Gd3, Ge3);
	Gb4 = cv::min(Gd4, Ge4);*/
	//auxiliar = Gd1 + Ge1 + Gb1 + Gd2 + Ge2 + Gb2 + Gd3 + Ge3 + Gb3 + Gd4 + Ge4 + Gb4;
	/*auxiliar = Gd1 + Ge1 + Gb1 + Gd2 + Ge2 + Gb2;// +Gd2 / 2 + Ge2 / 2 + Gb2 / 2 + Gd4 / 2 + Ge4 / 2 + Gb4 / 2;
	auxiliar2 = Gd3 + Ge3 + Gb3 + Gd4 + Ge4 + Gb4;//+ Gd2 / 2 + Ge2 / 2 + Gb2 / 2 - Gd4 / 2 - Ge4 / 2 - Gb4 / 2;

	namedWindow("Display window1", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window1", auxiliar);
	waitKey(10);

	namedWindow("Display window2", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window2", auxiliar2);
	waitKey(10);*/


	//Morfologic gradient
	/*cv::dilate(auxiliar, auxiliar2, gradMorf);
	cv::erode(auxiliar, auxiliar, gradMorf);
	auxiliar = auxiliar2 - auxiliar;*/

	//Laplacian(auxiliar, auxiliar, CV_8U, 3);// , scale, delta, BORDER_DEFAULT);
	//blur(auxiliar, auxiliar, Size(3, 3));
	//Canny(auxiliar, auxiliar2, 1100, 100, 5, true);
	cannyPF(auxiliar, 5, 110.0, auxiliar2,gradMod, gradAngle);
	path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeCanny\\";
	nameImage.Format("Imatge_%d.png", globalImage);
	nameImage = path + nameImage;
	imwrite(nameImage.GetString(), auxiliar2);
	/*auxiliar.convertTo(auxiliar, CV_16SC1, 32767.0/255);
	auxiliar2.convertTo(auxiliar2, CV_16SC1, 32767.0 / 255);

	namedWindow("Display window3", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window3", auxiliar);
	waitKey(10);

	namedWindow("Display window4", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window4", auxiliar2);
	waitKey(10);

	Canny(auxiliar, auxiliar2, auxiliar2, 13000, 8000, true);
	namedWindow("Display window5", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window5", auxiliar2);
	waitKey(10);

	auxiliar2.convertTo(auxiliar2, CV_8U);
	*/


	//Filter small blobs
	//cv::filterSpeckles(auxiliar2, 0, 20, 0);

	//Dilate+Erosion to connect the contorn
	//cv::dilate(auxiliar2, auxiliar, segdilate);
	//cv::erode(auxiliar, auxiliar2, segerode);

	cv::morphologyEx(auxiliar2, auxiliar2, MORPH_CLOSE, squareKernel, cv::Point(-1, -1), 2);
	cv::dilate(auxiliar2, auxiliar, segdilate);
	cv::erode(auxiliar, auxiliar2, segerode);

	path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeClose\\";
	nameImage.Format("Imatge_%d.png", globalImage);
	nameImage = path + nameImage;
	imwrite(nameImage.GetString(), auxiliar2);

	//binarize
	//cv::threshold(auxiliar2, auxiliar2, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

	//Filter small blobs
	cv::filterSpeckles(auxiliar2, 0, 100, 0);
	path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeSpeckles\\";
	nameImage.Format("Imatge_%d.png", globalImage);
	nameImage = path + nameImage;
	imwrite(nameImage.GetString(), auxiliar2);
	//Dilate+Erosion to connect the contorn
	//cv::dilate(auxiliar2, auxiliar, segdilate);
	//cv::erode(auxiliar, auxiliar2, segerode);

	//Etiquetem els conjunts de pixels
	int nLabels = cv::connectedComponentsWithStats(auxiliar2, auxiliar, stats, centroids);

	//Busquem el de m�nima d�stacia al centre
	double centreImgh = auxiliar.rows / 2.0;
	double centreImgw = auxiliar.cols / 2.0;
	/*double distMin = winSegSize.height + winSegSize.width + 1000.0;
	double distAux;
	int indexMin = 0;
	for (int i = 1; i < nLabels; ++i)
	{
	distAux = sqrt(pow(centreImgh - centroids.at<double>(i, 1), 2) + pow(centreImgw - centroids.at<double>(i, 0), 2));
	if (distAux < distMin)
	{
	distMin = distAux;
	indexMin = i;
	}
	}
	compare(auxiliar, indexMin, auxiliar2, CMP_EQ);
	(*sortida) = (auxiliar2).clone();*/
	cv::Mat auxiliar3 = auxiliar2.clone();
	double distMin = max(centreImgh, centreImgw) / 3.0;
	double distAux;
	auxiliar3.setTo(Scalar::all(0));
	for (int i = 1; i < nLabels; ++i)
	{
		distAux = sqrt(pow(centreImgh - centroids.at<double>(i, 1), 2) + pow(centreImgw - centroids.at<double>(i, 0), 2));
		if (distAux < distMin)
		{
			compare(auxiliar, i, auxiliar2, CMP_EQ);
			auxiliar3 += auxiliar2;
		}
	}
	sortida = (auxiliar3).clone();


	QueryPerformanceCounter(&EndingTime);
	ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
	// We now have the elapsed number of ticks, along with the number of ticks-per-second. We use these values
	// to convert to the number of elapsed milliseconds.  To guard against loss-of-precision, we convert
	// to milliseconds *before* dividing by ticks-per-second.
	ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
	ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
	TRACE("TEMPS TARDAAAAAAAAAAAAAAAAAAAAAAAAAAT %d\n", ElapsedMiliseconds.QuadPart);
}

void CDemoRegistreDlg::PosarImatgeSegmentacio(cv::Mat * entrada)
{
	cv::Mat auxiliar;
	cv::cvtColor(*entrada, auxiliar, cv::COLOR_GRAY2BGR);
	
	////PER BORRAR
	uint8_t *imData = auxiliar.data;
	int nrows = auxiliar.rows;
	int ncols = auxiliar.cols;
	int deltax, deltay;

	for (int i = 0; i < puntsSegmentatsvect.size(); ++i)
	{
		for (int a = -2; a < 3; a++)
		{
			for (int b = -2; b < 3; b++)
			{
				if ((puntsSegmentatsvect[i].x == 0) || (puntsSegmentatsvect[i].y == 0) || (puntsSegmentatsvect[i].x + b>= auxiliar.cols - 2) || (puntsSegmentatsvect[i].y + a>= auxiliar.rows - 2) || (puntsSegmentatsvect[i].x + b < 2) || (puntsSegmentatsvect[i].y + a < 2))
				{
					continue;
				}

				imData[((puntsSegmentatsvect[i].y + a) * auxiliar.cols + puntsSegmentatsvect[i].x + b) * 3] = 0;
				imData[((puntsSegmentatsvect[i].y + a) * auxiliar.cols + puntsSegmentatsvect[i].x + b) * 3 + 1] = 255;
				imData[((puntsSegmentatsvect[i].y + a) * auxiliar.cols + puntsSegmentatsvect[i].x + b) * 3 + 2] = 255;
			}
		}		
	}

	for (int a = -2; a < 3; a++)
	{
		for (int b = -2; b < 3; b++)
		{
			if ((meanSegmentats[0].x == 0) || (meanSegmentats[0].y == 0) || (meanSegmentats[0].x + b >= auxiliar.cols - 2) || (meanSegmentats[0].y + a >= auxiliar.rows - 2) || (meanSegmentats[0].x + b < 2) || (meanSegmentats[0].y + a < 2))
			{
				continue;
			}
			if ((isnan(meanSegmentats[0].x)) || (isnan(meanSegmentats[0].y)))
			{
				continue;
			}
			imData[(((int)meanSegmentats[0].y + a) * auxiliar.cols + (int)meanSegmentats[0].x + b) * 3] = 255;
			imData[(((int)meanSegmentats[0].y + a) * auxiliar.cols + (int)meanSegmentats[0].x + b) * 3 + 1] = 0;
			imData[(((int)meanSegmentats[0].y + a) * auxiliar.cols + (int)meanSegmentats[0].x + b) * 3 + 2] = 0;
		}
	}
	////


	//If size does not match
	if (auxiliar.size() != winSegSize)
	{
		cv::resize(auxiliar, cvImgTmp, winSegSize);
	}
	else 
	{
		cvImgTmp = auxiliar.clone();
	}

	
	//Rotate image
	cv::flip(cvImgTmp, cvImgTmp, 0);
	

	//Create MFC image
	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}

	mfcImg = new CImage();
	mfcImg->Create(winSegSize.width, winSegSize.height, 24);


	//Add header and OpenCV image to mfcImg
	StretchDIBits(mfcImg->GetDC(), 0, 0,
		winSegSize.width, winSegSize.height, 0, 0,
		winSegSize.width, winSegSize.height,
		cvImgTmp.data, &bitInfo, DIB_RGB_COLORS, SRCCOPY
	);

	//Display mfcImg in MFC window
	mfcImg->BitBlt(::GetDC(m_picSegmentation.m_hWnd), 0, 0);
}

void CDemoRegistreDlg::OnBnClickedButsegmen()
{
	if (bSegmenting)
	{
		bSegmenting = false;
		if (btracking)
		{
			OnBnClickedButtracking();
		}
		m_butSegmentation.SetWindowTextA(_T("Start Segmentation"));
		m_butTracking.EnableWindow(FALSE);
		m_butSegmentation.SetWindowTextA(_T("Start Segmentation"));

		//mean Segmentaci�
		numSegm = 0;
		for (int i = 0; i<NUMMEANPUNTSCONTORN; i++)
			ultimsSegmentats[i].clear();
	}
	else
	{
		//mean Segmentacio
		numSegm = 0;
		ultimsSegmentats.clear();
		ultimsSegmentats.resize(NUMMEANPUNTSCONTORN);
		for (int i = 0; i < NUMMEANPUNTSCONTORN; i++)
		{
			ultimsSegmentats[i].resize(PUNTSCONTORNIMATGE2D);
		}
		meanSegmentats.clear();
		meanSegmentats.resize(PUNTSCONTORNIMATGE2D);
		for (int i = 0; i < PUNTSCONTORNIMATGE2D; i++)
		{
			meanSegmentats[i].x = 0.0f;
			meanSegmentats[i].y = 0.0f;
		}

		bSegmenting = true;
		m_butTracking.EnableWindow(TRUE);
		m_butTracking.SetWindowTextA(_T("Start Tracking"));
		m_butSegmentation.SetWindowTextA(_T("Stop Segmentation"));
	}
}

inline float FuncioSigne(float x)
{
	if (std::abs(x)<TOLERANCIA)
		return 0.0;
	else if (x>0)
		return 1.0;
	else
		return -1.0;
}

void CDemoRegistreDlg::EulerXYZtoMatrix(float angx, float angy, float angz, Eigen::Matrix3f * Rot)
{
	(*Rot) << (std::cosf(angy)*std::cosf(angz)), (-std::cosf(angy)*std::sinf(angz)), std::sinf(angy), (std::cosf(angx)*std::sinf(angz) + std::cosf(angz)*std::sinf(angx)*std::sinf(angy)), (std::cosf(angx)*std::cosf(angz) - std::sinf(angx)*std::sinf(angy)*std::sinf(angz)), (-std::cosf(angy)*std::sinf(angx)), (std::sinf(angx)*std::sinf(angz) - std::cosf(angx)*std::cosf(angz)*std::sinf(angy)), (std::cosf(angz)*std::sinf(angx) + std::cosf(angx)*std::sinf(angy)*std::sinf(angz)), (std::cosf(angx)*std::cosf(angy));

}

void CDemoRegistreDlg::QuaternionToMatrix(float q0, float qx, float qy, float qz, Eigen::Matrix3f * resultat)
{
	Eigen::Vector4f Q;
	Q(0) = q0;
	Q(1) = qx;
	Q(2) = qy;
	Q(3) = qz;
	Q = Q / Q.norm();
	(*resultat)(0, 0) = (Q(0)*Q(0) + Q(1)*Q(1) - Q(2)*Q(2) - Q(3)*Q(3));
	(*resultat)(0, 1) = (2 * (Q(1)*Q(2) - Q(0)*Q(3)));
	(*resultat)(0, 2) = (2 * (Q(1)*Q(3) + Q(0)*Q(2)));
	(*resultat)(1, 0) = (2 * (Q(1)*Q(2) + Q(0)*Q(3)));
	(*resultat)(1, 1) = (Q(0)*Q(0) - Q(1)*Q(1) + Q(2)*Q(2) - Q(3)*Q(3));
	(*resultat)(1, 2) = (2 * (Q(2)*Q(3) - Q(0)*Q(1)));
	(*resultat)(2, 0) = (2 * (Q(1)*Q(3) - Q(0)*Q(2)));
	(*resultat)(2, 1) = (2 * (Q(2)*Q(3) + Q(0)*Q(1)));
	(*resultat)(2, 2) = (Q(0)*Q(0) - Q(1)*Q(1) - Q(2)*Q(2) + Q(3)*Q(3));
}

void CDemoRegistreDlg::EulerXYZFromMatrix(float angx[2], float angy[2], float angz[2], Eigen::Matrix3f R)
{
	if (R.coeffRef(0, 2)>1 - TOLERANCIA)
	{
		angx[0] = 0.0;
		angy[0] = M_PI / 2.0;
		angz[0] = std::atan2(R.coeffRef(1, 0), R.coeffRef(1, 1));
		if (angz[0]>2.0*M_PI)
			angz[0] = angx[0] - 2.0*M_PI;

		angx[1] = angx[0];
		angy[1] = angy[0];
		angz[2] = angz[0];
	}
	else if (R.coeffRef(0, 2) == -1 + TOLERANCIA)
	{
		angx[0] = 0.0;
		angy[0] = -M_PI / 2.0;
		angz[0] = std::atan2(R.coeffRef(1, 0), R.coeffRef(1, 1));
		if (angz[0]>2.0*M_PI)
			angz[0] = angx[0] - 2.0*M_PI;
		angx[1] = angx[0];
		angy[1] = angy[0];
		angz[1] = angz[0];
	}
	else
	{
		angy[0] = std::asin(R.coeffRef(0, 2));
		angy[1] = M_PI - angy[0];
		angx[0] = std::atan2(-R.coeffRef(1, 2) / std::cos(angy[0]), R.coeffRef(2, 2) / std::cos(angy[0]));
		angx[1] = std::atan2(-R.coeffRef(1, 2) / std::cos(angy[1]), R.coeffRef(2, 2) / std::cos(angy[1]));
		angz[0] = std::atan2(-R.coeffRef(0, 1) / std::cos(angy[0]), R.coeffRef(0, 0) / std::cos(angy[0]));
		angz[1] = std::atan2(-R.coeffRef(0, 1) / std::cos(angy[1]), R.coeffRef(0, 0) / std::cos(angy[1]));
	}
}

void CDemoRegistreDlg::QuaternionFromMatrix(float * q0, float * qx, float * qy, float * qz, Eigen::Matrix3f R)
{
	Eigen::Matrix4f A;

	//TROS PER CALCULAR LA MATRIU DE ROTACIO M�S PR�XIMA A R
	Eigen::Matrix3f Ri = R.transpose()*R;
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigsol(Ri);

	Eigen::Matrix3f invEigValue, auxMatrix;
	invEigValue.setZero();
	for (int i = 0; i < 3; i++)
	{
		auxMatrix = eigsol.eigenvectors().col(i).real()*eigsol.eigenvectors().col(i).transpose().real() / std::sqrt(eigsol.eigenvalues()[i]);
		invEigValue += auxMatrix;
	}
	R *= invEigValue;
	//FINS AQU� C�LCUL DE MATRIU ROTACI� M�S PR�XIMA


	A << 1, 1, 1, 1, 1, -1, -1, 1, -1, 1, -1, 1, -1, -1, 1, 1;
	//A.transposeInPlace();
	A = A / 4;

	Eigen::Vector4f aux, sQ;
	aux << R(0, 0), R(1, 1), R(2, 2), 1;

	Eigen::Vector4f QQ = A*aux;
	for (int i = 0; i < 4; i++)
		QQ(i) = std::sqrt(QQ(i));

	if (QQ(0) + TOLERANCIA > QQ.maxCoeff())
		sQ << 1.0, FuncioSigne(R(2, 1) - R(1, 2)), FuncioSigne(R(0, 2) - R(2, 0)), FuncioSigne(R(1, 0) - R(0, 1));
	else if (QQ(1) + TOLERANCIA > QQ.maxCoeff())
		sQ << FuncioSigne(R(2, 1) - R(1, 2)), 1.0, FuncioSigne(R(0, 1) + R(1, 0)), FuncioSigne(R(2, 0) + R(0, 2));
	else if (QQ(2) + TOLERANCIA > QQ.maxCoeff())
		sQ << FuncioSigne(R(0, 2) - R(2, 0)), FuncioSigne(R(1, 0) + R(0, 1)), 1.0, FuncioSigne(R(2, 1) + R(1, 2));
	else if (QQ(3) + TOLERANCIA > QQ.maxCoeff())
		sQ << FuncioSigne(R(1, 0) - R(0, 1)), FuncioSigne(R(0, 2) + R(2, 0)), FuncioSigne(R(2, 1) + R(1, 2)), 1.0;
	else
	{
		sQ << 0.0, 0.0, 0.0, 0.0;
		TRACE("Error de dades al calcular Quaterni�\n");
	}

	*q0 = sQ(0)*QQ(0);
	*qx = sQ(1)*QQ(1);
	*qy = sQ(2)*QQ(2);
	*qz = sQ(3)*QQ(3);
}

inline int evaluarMat(uint8_t * matrix, int r, int c, int ncols)
{
	if ((r < 0)||(c<0)||(c>= ncols))
		return -1;
	return matrix[r * ncols + c];
}

inline float normDistancia(float x1, float y1, float x2, float y2)
{
	return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
}

inline float normCvPoint2f(cv::Point2f P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint2i(cv::Point2i P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint3f(cv::Point3f P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline float normCvPoint3i(cv::Point3i P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline int float2int(float x)
{
	return (int)(x + TOLERANCIA);
}

cv::Point2f CDemoRegistreDlg::getMeanPunts2D(Eigen::Matrix2Xf * punts)
{
	cv::Point2f meanVal(0.0f, 0.0f);
	Eigen::Vector2f meanVec = punts->rowwise().mean();
	meanVal.x = meanVec(0);
	meanVal.y = meanVec(1);
	return meanVal;
}

cv::Point2f CDemoRegistreDlg::PuntsContornProjectats(Eigen::Matrix2Xf * punts, int nPunts, std::vector <cv::Point2f> * puntsContorn, std::vector<int> * indPuntsContorn)
{
	*puntsContorn = std::vector<cv::Point2f>(nPunts);
	*indPuntsContorn = std::vector<int>(nPunts);
	std::vector<float> radis(nPunts);
	float radiAux, angAux;
	int indEx;

	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	cv::Point2f valCentrat;

	Eigen::Vector2f meanVec = punts->rowwise().mean();
	meanVal.x = meanVec(0);
	meanVal.y = meanVec(1);
	std::vector<std::vector<cv::Point2f>> puntsCirc(nPunts);
	std::vector < std::vector<int>> indPuntsCirc(nPunts);
	cv::Point2f PuntLocal;
	/*for (int i = 0; i < punts->size(); ++i)
	{
		meanVal += (*punts)[i];
	}
	meanVal.x = meanVal.x / punts->size();
	meanVal.y = meanVal.y / punts->size();*/
	for (int i = 0; i < punts->cols(); ++i)
	{
		valCentrat.x = (*punts)(0, i) - meanVal.x;
		valCentrat.y = (*punts)(1, i) - meanVal.y;
		//valCentrat = (*punts)[i] - meanVal;
		radiAux = normCvPoint2f(valCentrat);
		angAux = atan2(valCentrat.y, valCentrat.x);
		indEx = floor((angAux + M_PI) / 2 / M_PI * nPunts);
		if (indEx >= nPunts)
			indEx = 0;
		PuntLocal.x = (*punts)(0, i);
		PuntLocal.y = (*punts)(1, i);
		puntsCirc[indEx].push_back(PuntLocal);
		indPuntsCirc[indEx].push_back(i);
		if (radiAux > radis[indEx])
		{
			radis[indEx] = radiAux;
			//(*puntsContorn)[indEx] = (*punts)[i];
			(*puntsContorn)[indEx].x = (*punts)(0,i);
			(*puntsContorn)[indEx].y = (*punts)(1,i);
			(*indPuntsContorn)[indEx] = i;
		}
	}
	radis.clear();
	int esborrats = 0;
	float maxvalx = -100000.0f, minvalx = 100000.0f;

	for (int i = 0; i < nPunts - esborrats; ++i)
	{
		if (max(abs((*puntsContorn)[i].x), abs((*puntsContorn)[i].y)) == 0)
		{
			esborrats++;
			puntsContorn->erase(puntsContorn->begin() + i, puntsContorn->begin() + i + 1);
			indPuntsContorn->erase(indPuntsContorn->begin() + i, indPuntsContorn->begin() + i + 1);
			i--;
		}
		else if ((*puntsContorn)[i].x < minvalx)
		{
			minvalx = (*puntsContorn)[i].x;
		}
		else if ((*puntsContorn)[i].x > maxvalx)
		{
			maxvalx = (*puntsContorn)[i].x;
		}
	}

	////
	std::vector<int> indFalta(0);
	std::vector<float> distFalta = std::vector<float>();
	float norA;
	float distMinX = (maxvalx - minvalx)*10.0f / (float)(puntsContorn->size());
	for (int i = 0; i < puntsContorn->size(); i++)
	{
		norA = sqrt(((*puntsContorn)[i].x - (*puntsContorn)[modval(i + 1, puntsContorn->size())].x)*((*puntsContorn)[i].x - (*puntsContorn)[modval(i + 1, puntsContorn->size())].x) + ((*puntsContorn)[i].y - (*puntsContorn)[modval(i + 1, puntsContorn->size())].y)*((*puntsContorn)[i].y - (*puntsContorn)[modval(i + 1, puntsContorn->size())].y));
		if (norA > distMinX)
		{
			indFalta.push_back(i);
			distFalta.push_back(norA);
		}
	}

	float delta, normAux, normAct;
	float recor;
	float vecnorm;
	std::vector<cv::Point2f> puntsFalten;
	std::vector<int> indPuntsFalten;
	cv::Point2f paux, pini, pfin;
	cv::Point2f direc;
	int jind, kind;
	int afegits = 0;
	int indExInt;
	for (int i = 0; i < indFalta.size(); i++)
	{
		indEx = indFalta[i];
		norA = distFalta[i];
		delta = norA / distMinX;
		pini.x = (*puntsContorn)[indEx + afegits].x;
		pini.y = (*puntsContorn)[indEx + afegits].y;
		pfin.x = (*puntsContorn)[modval(indEx + 1 + afegits, puntsContorn->size())].x;
		pfin.y = (*puntsContorn)[modval(indEx + 1 + afegits, puntsContorn->size())].y;
		puntsFalten.clear();
		indPuntsFalten.clear();
		indExInt = indEx;
		while (delta > 1.0f)
		{
			if (delta < 2.0f)
			{
				recor = norA / 2.0f;
			}
			else
			{
				recor = distMinX;
			}
			direc.x = pfin.x - pini.x;
			direc.y = pfin.y - pini.y;
			vecnorm = sqrt(direc.x * direc.x + direc.y * direc.y);
			direc.x /= vecnorm;
			direc.y /= vecnorm;
			paux.x = pini.x + recor*direc.x;
			paux.y = pini.y + recor*direc.y;
			normAux = 100 * norA;
			jind = 1000;
			kind = 1000;
			for (int j = -2; j < 3; j++)
			{
				for (int k = 0; k < puntsCirc[modval(indExInt + j, puntsCirc.size())].size(); k++)
				{
					normAct = sqrt((puntsCirc[modval(indExInt + j, puntsCirc.size())][k].x - paux.x)*(puntsCirc[modval(indExInt + j, puntsCirc.size())][k].x - paux.x) + (puntsCirc[modval(indExInt + j, puntsCirc.size())][k].y - paux.y)*(puntsCirc[modval(indExInt + j, puntsCirc.size())][k].y - paux.y));
					if (normAct < normAux)
					{
						normAux = normAct;
						jind = j;
						kind = k;
					}
				}
			}
			if ((jind == 1000) || (kind == 1000))
				break;
			if ((pini.x != puntsCirc[modval(indExInt + jind, puntsCirc.size())][kind].x) || (pini.y != puntsCirc[modval(indExInt + jind, puntsCirc.size())][kind].y))
			{
				pini.x = puntsCirc[modval(indExInt + jind, puntsCirc.size())][kind].x;
				pini.y = puntsCirc[modval(indExInt + jind, puntsCirc.size())][kind].y;
			}
			else
			{
				break;
			}
			norA = sqrt((pfin.x - pini.x)*(pfin.x - pini.x) + (pfin.y - pini.y)*(pfin.y - pini.y));
			delta = norA / distMinX;
			cv::Point2f ptf = cv::Point2f(pini.x, pini.y);
			puntsFalten.push_back(ptf);
			indPuntsFalten.push_back(indPuntsCirc[modval(indExInt + jind, puntsCirc.size())][kind]);
			indExInt = indExInt + jind;
		}
		puntsContorn->insert(puntsContorn->begin() + indEx + afegits + 1, puntsFalten.begin(), puntsFalten.end());
		indPuntsContorn->insert(indPuntsContorn->begin() + indEx + afegits + 1, indPuntsFalten.begin(), indPuntsFalten.end());
		afegits = afegits + puntsFalten.size();
	}
	////

	return meanVal;
}

void CDemoRegistreDlg::PuntsMaxCurvatura(std::vector<cv::Point2f> Punts, int nPunts, std::vector<cv::Point2f> * PuntsOut, std::vector<int> * indPuntsOut, int * nPuntsImp, std::vector<int> * indPuntsOrdenat, std::vector<int> * indPuntsOrdImp, std::vector<int> *permutImp)
{
	int nPuntsIn = Punts.size();
	(*PuntsOut) = std::vector<cv::Point2f>(nPunts);
	(*indPuntsOut) = std::vector<int>(nPunts);
	std::vector<float> indDistancia(nPunts);
	std::vector<float> curv(nPuntsIn);
	indPuntsOrdenat->clear();
	indPuntsOrdImp->clear();
	cv::Point2f v1, v2;
	std::vector<int> totsIndexos;

	if (nPunts >= Punts.size())
	{
		*indPuntsOrdenat = std::vector<int>(nPunts);
		*indPuntsOrdImp = std::vector<int>(nPunts);
		*permutImp = std::vector<int>(nPunts);
		for (int i = 0; i < Punts.size(); i++)
		{
			(*indPuntsOut)[i] = i;
			*nPuntsImp = 0;
			(*indPuntsOrdenat)[i] = i; 
			(*indPuntsOrdImp)[i] = i;
			(*PuntsOut)[i] = Punts[i];
			(*permutImp)[i] = i;
		}
	}
	else
	{
		for (int i = 0; i < nPuntsIn; i++)
		{
			if (i < nPunts)
			{
				indDistancia[i] = 10000.0f;
			}
			totsIndexos.push_back(i);
			v1 = Punts[modval(i - 2, nPuntsIn)] - Punts[i];
			v1 = v1 / normCvPoint2f(v1);
			v2 = Punts[modval(i + 2, nPuntsIn)] - Punts[i];
			v2 = v2 / normCvPoint2f(v2);
			curv[i] = normCvPoint2f(v1 - v2);
		}

		float maximvalue = 10000.0f;
		float deltaPicProper = 0.3f;
		float maxvalue = indDistancia[0];
		float maxVal = 1.93f;
		float maxValImp = 1.7f;
		std::vector <int> segonPics;
		std::vector <int> altresPics;
		std::vector <int> puntsPicsPropers;
		int npics = 0;
		*nPuntsImp = 0;
		std::vector<int> nind;
		int jpos, lpos, finalsegonPic, finalPicProp;

		for (int i = 0; i < nPuntsIn; i++)
		{
			if (curv[i] < std::min({ curv[modval(i - 2, nPuntsIn)], curv[modval(i - 1, nPuntsIn)], curv[modval(i + 1, nPuntsIn)], curv[modval(i + 2, nPuntsIn)] }))
			{
				if (curv[i] < maxVal)
				{
					if (curv[i] < maximvalue)
					{
						for (int j = 0; j < nPunts; j++)
						{
							if (curv[i] < indDistancia[j])
							{
								indDistancia.insert(indDistancia.begin() + j, curv[i]);
								indDistancia.pop_back();
								indPuntsOut->insert(indPuntsOut->begin() + j, i);
								indPuntsOut->pop_back();
								PuntsOut->insert(PuntsOut->begin() + j, Punts[i]);
								PuntsOut->pop_back();
								nind.clear();
								if ((curv[modval(i - 2, nPuntsIn)] < curv[modval(i - 3, nPuntsIn)]) && (curv[modval(i - 2, nPuntsIn)] < curv[modval(i - 1, nPuntsIn)]))
									nind.push_back(-2);
								if ((curv[modval(i + 2, nPuntsIn)] < curv[modval(i + 1, nPuntsIn)]) && (curv[modval(i + 2, nPuntsIn)] < curv[modval(i + 3, nPuntsIn)]))
									nind.push_back(2);
								if (nind.size() > 0)
								{
									if (nind.size() > 1)
									{
										nind.clear();
										if (curv[modval(i - 2, nPuntsIn)] < curv[modval(i + 2, nPuntsIn)])
											nind.push_back(-2);
										else
											nind.push_back(2);
									}

									lpos = 0;
									finalsegonPic = 0;
									if (puntsPicsPropers.size() > 0)
									{
										for (int l = 0; l < puntsPicsPropers.size(); l++)
										{
											if (puntsPicsPropers[l] == modval(i + nind[0], nPuntsIn))
												finalsegonPic = 1;
											break;
										}

									}
									if (finalsegonPic == 0)
									{
										for (int l = 0; l < segonPics.size(); ++l)
										{
											if (curv[modval(i + nind[0], nPuntsIn)] < curv[segonPics[l]])
												break;
											else if (modval(i + nind[0], nPuntsIn) == segonPics[l])
											{
												lpos = -1;
												break;
											}
											else
												lpos = l;
										}
										if (lpos > 0)
										{
											if (curv[modval(i + nind[0], nPuntsIn)] != curv[segonPics[lpos]])
											{
												segonPics.insert(segonPics.begin() + lpos, modval(i + nind[0], nPuntsIn));
											}
										}
										else if (lpos < 0)
										{

										}
										else
										{
											segonPics.insert(segonPics.begin() + lpos, modval(i + nind[0], nPuntsIn));
										}
									}
								}
								npics = npics + 1;
								maximvalue = indDistancia[indDistancia.size() - 1];
								break;
							}
						}
					}
					if (curv[i] < maxValImp)
					{
						*nPuntsImp = *nPuntsImp + 1;
					}
				}
				else
				{
					jpos = 0;
					for (int j = 0; j < altresPics.size(); ++j)
					{
						if (curv[i] < curv[altresPics[j]])
						{
							break;
						}
						else
						{
							jpos = j;
						}
					}
					altresPics.insert(altresPics.begin() + jpos, i);
				}
			}
			else if ((curv[i] < curv[modval(i - 1, nPuntsIn)] - deltaPicProper) && (curv[i] < curv[modval(i + 1, nPuntsIn)] - deltaPicProper))
			{
				jpos = 0;
				finalPicProp = 0;
				if (segonPics.size() > 0)
				{
					for (int j = 0; j < segonPics.size(); j++)
					{

						if (segonPics[j] == i)
						{
							finalPicProp = 1;
							break;
						}
					}
					if (finalPicProp == 0)
					{
						for (int j = 0; j < puntsPicsPropers.size(); j++)
						{
							if (curv[i] < curv[puntsPicsPropers[j]])
							{
								break;
							}
							else if ( i == puntsPicsPropers[j])
							{
								jpos = -1;
							}
							else
							{
								jpos = j;
							}
						}
						if (jpos > -1)
						{
							puntsPicsPropers.insert(puntsPicsPropers.begin() + jpos, i);
						}
					}
				}
			}
		}

		int	naltres = 0;
		int n2pics = 0;
		int nPicProper = 0;
		int nfalta;
		if (npics < nPunts)
		{
			nfalta = nPunts - npics;
			if (nfalta > puntsPicsPropers.size() + altresPics.size() + segonPics.size())
			{
				nPicProper = puntsPicsPropers.size();
				naltres = altresPics.size();
				n2pics = segonPics.size();
			}
			else
			{
				if (nfalta > puntsPicsPropers.size() + segonPics.size())
				{
					nPicProper = puntsPicsPropers.size();
					n2pics = segonPics.size();
					naltres = nfalta - nPicProper - n2pics;
				}
				else
				{
					if (puntsPicsPropers.size() > segonPics.size())
					{
						if (segonPics.size() * 2 <= nfalta)
						{
							n2pics = segonPics.size();
							nPicProper = nfalta - n2pics;
						}
						else
						{
							nPicProper = floor(nfalta / 2);
							n2pics = nfalta - nPicProper;
						}
					}
					else
					{
						if (puntsPicsPropers.size() * 2 <= nfalta)
						{
							nPicProper = puntsPicsPropers.size();
							n2pics = nfalta - nPicProper;
						}
						else
						{
							nPicProper = floor(nfalta / 2);
							n2pics = nfalta - nPicProper;
						}
					}
				}
			}

			if (nPicProper == 0)
			{
				if ((naltres == 0) && (n2pics != 0))
				{
					for (int j = 0; j < n2pics; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[segonPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, segonPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[segonPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
				else if ((n2pics == 0) && (naltres != 0))
				{
					for (int j = 0; j < naltres; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[altresPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, altresPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[altresPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
				else if ((n2pics != 0) && (naltres != 0))
				{
					for (int j = 0; j < n2pics; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[segonPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, segonPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[segonPics[j]]);
					}
					for (int j = 0; j < naltres; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + n2pics + j, Punts[altresPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + n2pics + j, altresPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + n2pics + j, curv[altresPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
			}
			else
			{
				if ((naltres == 0) && (n2pics != 0))
				{
					for (int j = 0; j < n2pics; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[segonPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, segonPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[segonPics[j]]);
					}
					for (int j = 0; j < nPicProper; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + n2pics + j, Punts[puntsPicsPropers[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + n2pics + j, puntsPicsPropers[j]);
						indDistancia.insert(indDistancia.begin() + npics + n2pics + j, curv[puntsPicsPropers[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
				else if ((n2pics == 0) && (naltres != 0))
				{
					for (int j = 0; j < nPicProper; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[puntsPicsPropers[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, puntsPicsPropers[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[puntsPicsPropers[j]]);
					}
					for (int j = 0; j < naltres; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + nPicProper + j, Punts[altresPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + nPicProper + j, altresPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + nPicProper + j, curv[altresPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
				else if ((n2pics != 0) && (naltres != 0))
				{
					for (int j = 0; j < naltres; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[altresPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, altresPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[altresPics[j]]);
					}
					for (int j = 0; j < nPicProper; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + naltres + j, Punts[puntsPicsPropers[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + naltres + j, puntsPicsPropers[j]);
						indDistancia.insert(indDistancia.begin() + npics + naltres + j, curv[puntsPicsPropers[j]]);
					}
					for (int j = 0; j < n2pics; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + naltres + nPicProper + j, Punts[segonPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + naltres + nPicProper + j, segonPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + naltres + nPicProper + j, curv[segonPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
			}
		}
		int nPuntsN;
		std::vector<int> auxArray;
		if (nPunts > npics + naltres + n2pics + nPicProper)
		{
			nPuntsN = nPunts - (npics + naltres + n2pics + nPicProper);
			std::vector<int> auxIndPuntsOut = (*indPuntsOut);
			std::sort(auxIndPuntsOut.begin(), auxIndPuntsOut.end());
			std::set_difference(totsIndexos.begin(), totsIndexos.end(), auxIndPuntsOut.begin(), auxIndPuntsOut.end(), std::inserter(auxArray, auxArray.begin()));
			for (int j = 0; j < nPuntsN; ++j)
			{
				PuntsOut->insert(PuntsOut->begin() + npics + naltres + n2pics + nPicProper + j, Punts[auxArray[floor(auxArray.size() * j / (float)nPuntsN)]]);
				indPuntsOut->insert(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper + j, auxArray[floor(auxArray.size() * j / (float)nPuntsN)]);
				indDistancia.insert(indDistancia.begin() + npics + naltres + n2pics + nPicProper + j, curv[auxArray[floor(auxArray.size() * j / (float)nPuntsN)]]);
			}
			PuntsOut->erase(PuntsOut->begin() + nPunts, PuntsOut->begin() + PuntsOut->size());
			indPuntsOut->erase(indPuntsOut->begin() + nPunts, indPuntsOut->begin() + indPuntsOut->size());
			indDistancia.erase(indDistancia.begin() + nPunts, indDistancia.begin() + indDistancia.size());
		}

		std::vector<int> aux(nPuntsIn);
		std::vector<int> arrayNPunts(nPunts);
		
		
		for (int i = 0; i < nPunts; i++)
		{
			aux[(*indPuntsOut)[i]] = 1;
			arrayNPunts[i] = i;
		}
		for (int i = 0; i < nPuntsIn; i++)
		{
			if (aux[i] > 0)
			{
				indPuntsOrdenat->push_back(i);
			}
		}
		int posT;
		vector<int>::iterator indx;
		std::vector<int> auxPerm(0);
		for (int i = 0; i < *nPuntsImp; i++)
		{
			indPuntsOrdImp->push_back((*indPuntsOut)[i]);
			indx = lower_bound(indPuntsOrdenat->begin(), indPuntsOrdenat->end(), (*indPuntsOrdImp)[i]);
			posT = indx - indPuntsOrdenat->begin();
			auxPerm.push_back(posT);

		}
		std::vector<int> auxIndPuntsOut = (*indPuntsOrdImp);
		std::sort(auxIndPuntsOut.begin(), auxIndPuntsOut.end());
		std::set_difference(indPuntsOrdenat->begin(), indPuntsOrdenat->end(), auxIndPuntsOut.begin(), auxIndPuntsOut.end(), std::inserter(*indPuntsOrdImp, indPuntsOrdImp->begin() + (*nPuntsImp)));
		auxIndPuntsOut = auxPerm;
		std::sort(auxIndPuntsOut.begin(), auxIndPuntsOut.end());
		std::set_difference(arrayNPunts.begin(), arrayNPunts.end(), auxIndPuntsOut.begin(), auxIndPuntsOut.end(), std::inserter(auxPerm, auxPerm.begin() + (*nPuntsImp)));
		*permutImp = std::vector<int>(auxPerm.size());
		for (int i = 0; i < auxPerm.size(); i++)
		{
			(*permutImp)[auxPerm[i]] = i;
		}
	}
}

void CDemoRegistreDlg::PuntsMaxCurvatura(std::vector<cv::Point2f> Punts, int nPunts, std::vector<cv::Point2f> * PuntsOut, std::vector<int> * indPuntsOut, int * nPuntsImp, std::vector<int> * indPuntsOrdenat, std::vector<int> * indPuntsOrdImp, std::vector<int> *permutImp, std::vector<float>* sigma)
{
	int nPuntsIn = Punts.size();
	(*PuntsOut) = std::vector<cv::Point2f>(nPunts);
	(*indPuntsOut) = std::vector<int>(nPunts);
	std::vector<float> indDistancia(nPunts);
	std::vector<float> curv(nPuntsIn);
	indPuntsOrdenat->clear();
	indPuntsOrdImp->clear();
	cv::Point2f v1, v2;
	std::vector<int> totsIndexos;

	if (nPunts >= Punts.size())
	{
		*indPuntsOrdenat = std::vector<int>(nPunts);
		*indPuntsOrdImp = std::vector<int>(nPunts);
		*permutImp = std::vector<int>(nPunts);
		for (int i = 0; i < Punts.size(); i++)
		{
			(*indPuntsOut)[i] = i;
			*nPuntsImp = 0;
			(*indPuntsOrdenat)[i] = i;
			(*indPuntsOrdImp)[i] = i;
			(*PuntsOut)[i] = Punts[i];
			(*permutImp)[i] = i;
		}
	}
	else
	{
		for (int i = 0; i < nPuntsIn; i++)
		{
			if (i < nPunts)
			{
				indDistancia[i] = 10000.0f;
			}
			totsIndexos.push_back(i);
			v1 = Punts[modval(i - 2, nPuntsIn)] - Punts[i];
			v1 = v1 / normCvPoint2f(v1);
			v2 = Punts[modval(i + 2, nPuntsIn)] - Punts[i];
			v2 = v2 / normCvPoint2f(v2);
			curv[i] = normCvPoint2f(v1 - v2);
		}

		float maximvalue = 10000.0f;
		float deltaPicProper = 0.3f;
		float maxvalue = indDistancia[0];
		float maxVal = 1.93f;
		float maxValImp = 1.7f;
		std::vector <int> segonPics;
		std::vector <int> altresPics;
		std::vector <int> puntsPicsPropers;
		int npics = 0;
		*nPuntsImp = 0;
		std::vector<int> nind;
		int jpos, lpos, finalsegonPic, finalPicProp;

		for (int i = 0; i < nPuntsIn; i++)
		{
			if (curv[i] < std::min({ curv[modval(i - 2, nPuntsIn)], curv[modval(i - 1, nPuntsIn)], curv[modval(i + 1, nPuntsIn)], curv[modval(i + 2, nPuntsIn)] }))
			{
				if (curv[i] < maxVal)
				{
					if (curv[i] < maximvalue)
					{
						for (int j = 0; j < nPunts; j++)
						{
							if (curv[i] < indDistancia[j])
							{
								indDistancia.insert(indDistancia.begin() + j, curv[i]);
								indDistancia.pop_back();
								indPuntsOut->insert(indPuntsOut->begin() + j, i);
								indPuntsOut->pop_back();
								PuntsOut->insert(PuntsOut->begin() + j, Punts[i]);
								PuntsOut->pop_back();
								nind.clear();
								if ((curv[modval(i - 2, nPuntsIn)] < curv[modval(i - 3, nPuntsIn)]) && (curv[modval(i - 2, nPuntsIn)] < curv[modval(i - 1, nPuntsIn)]))
									nind.push_back(-2);
								if ((curv[modval(i + 2, nPuntsIn)] < curv[modval(i + 1, nPuntsIn)]) && (curv[modval(i + 2, nPuntsIn)] < curv[modval(i + 3, nPuntsIn)]))
									nind.push_back(2);
								if (nind.size() > 0)
								{
									if (nind.size() > 1)
									{
										nind.clear();
										if (curv[modval(i - 2, nPuntsIn)] < curv[modval(i + 2, nPuntsIn)])
											nind.push_back(-2);
										else
											nind.push_back(2);
									}

									lpos = 0;
									finalsegonPic = 0;
									if (puntsPicsPropers.size() > 0)
									{
										for (int l = 0; l < puntsPicsPropers.size(); l++)
										{
											if (puntsPicsPropers[l] == modval(i + nind[0], nPuntsIn))
												finalsegonPic = 1;
											break;
										}

									}
									if (finalsegonPic == 0)
									{
										for (int l = 0; l < segonPics.size(); ++l)
										{
											if (curv[modval(i + nind[0], nPuntsIn)] < curv[segonPics[l]])
												break;
											else if (modval(i + nind[0], nPuntsIn) == segonPics[l])
											{
												lpos = -1;
												break;
											}
											else
												lpos = l;
										}
										if (lpos > 0)
										{
											if (curv[modval(i + nind[0], nPuntsIn)] != curv[segonPics[lpos]])
											{
												segonPics.insert(segonPics.begin() + lpos, modval(i + nind[0], nPuntsIn));
											}
										}
										else if (lpos < 0)
										{

										}
										else
										{
											segonPics.insert(segonPics.begin() + lpos, modval(i + nind[0], nPuntsIn));
										}
									}
								}
								npics = npics + 1;
								maximvalue = indDistancia[indDistancia.size() - 1];
								break;
							}
						}
					}
					if (curv[i] < maxValImp)
					{
						*nPuntsImp = *nPuntsImp + 1;
					}
				}
				else
				{
					jpos = 0;
					for (int j = 0; j < altresPics.size(); ++j)
					{
						if (curv[i] < curv[altresPics[j]])
						{
							break;
						}
						else
						{
							jpos = j;
						}
					}
					altresPics.insert(altresPics.begin() + jpos, i);
				}
			}
			else if ((curv[i] < curv[modval(i - 1, nPuntsIn)] - deltaPicProper) && (curv[i] < curv[modval(i + 1, nPuntsIn)] - deltaPicProper))
			{
				jpos = 0;
				finalPicProp = 0;
				if (segonPics.size() > 0)
				{
					for (int j = 0; j < segonPics.size(); j++)
					{

						if (segonPics[j] == i)
						{
							finalPicProp = 1;
							break;
						}
					}
					if (finalPicProp == 0)
					{
						for (int j = 0; j < puntsPicsPropers.size(); j++)
						{
							if (curv[i] < curv[puntsPicsPropers[j]])
							{
								break;
							}
							else if (i == puntsPicsPropers[j])
							{
								jpos = -1;
							}
							else
							{
								jpos = j;
							}
						}
						if (jpos > -1)
						{
							puntsPicsPropers.insert(puntsPicsPropers.begin() + jpos, i);
						}
					}
				}
			}
		}

		int	naltres = 0;
		int n2pics = 0;
		int nPicProper = 0;
		int nfalta;
		if (npics < nPunts)
		{
			nfalta = nPunts - npics;
			if (nfalta > puntsPicsPropers.size() + altresPics.size() + segonPics.size())
			{
				nPicProper = puntsPicsPropers.size();
				naltres = altresPics.size();
				n2pics = segonPics.size();
			}
			else
			{
				if (nfalta > puntsPicsPropers.size() + segonPics.size())
				{
					nPicProper = puntsPicsPropers.size();
					n2pics = segonPics.size();
					naltres = nfalta - nPicProper - n2pics;
				}
				else
				{
					if (puntsPicsPropers.size() > segonPics.size())
					{
						if (segonPics.size() * 2 <= nfalta)
						{
							n2pics = segonPics.size();
							nPicProper = nfalta - n2pics;
						}
						else
						{
							nPicProper = floor(nfalta / 2);
							n2pics = nfalta - nPicProper;
						}
					}
					else
					{
						if (puntsPicsPropers.size() * 2 <= nfalta)
						{
							nPicProper = puntsPicsPropers.size();
							n2pics = nfalta - nPicProper;
						}
						else
						{
							nPicProper = floor(nfalta / 2);
							n2pics = nfalta - nPicProper;
						}
					}
				}
			}

			if (nPicProper == 0)
			{
				if ((naltres == 0) && (n2pics != 0))
				{
					for (int j = 0; j < n2pics; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[segonPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, segonPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[segonPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
				else if ((n2pics == 0) && (naltres != 0))
				{
					for (int j = 0; j < naltres; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[altresPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, altresPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[altresPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
				else if ((n2pics != 0) && (naltres != 0))
				{
					for (int j = 0; j < n2pics; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[segonPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, segonPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[segonPics[j]]);
					}
					for (int j = 0; j < naltres; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + n2pics + j, Punts[altresPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + n2pics + j, altresPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + n2pics + j, curv[altresPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
			}
			else
			{
				if ((naltres == 0) && (n2pics != 0))
				{
					for (int j = 0; j < n2pics; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[segonPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, segonPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[segonPics[j]]);
					}
					for (int j = 0; j < nPicProper; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + n2pics + j, Punts[puntsPicsPropers[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + n2pics + j, puntsPicsPropers[j]);
						indDistancia.insert(indDistancia.begin() + npics + n2pics + j, curv[puntsPicsPropers[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
				else if ((n2pics == 0) && (naltres != 0))
				{
					for (int j = 0; j < nPicProper; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[puntsPicsPropers[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, puntsPicsPropers[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[puntsPicsPropers[j]]);
					}
					for (int j = 0; j < naltres; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + nPicProper + j, Punts[altresPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + nPicProper + j, altresPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + nPicProper + j, curv[altresPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
				else if ((n2pics != 0) && (naltres != 0))
				{
					for (int j = 0; j < naltres; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + j, Punts[altresPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + j, altresPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + j, curv[altresPics[j]]);
					}
					for (int j = 0; j < nPicProper; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + naltres + j, Punts[puntsPicsPropers[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + naltres + j, puntsPicsPropers[j]);
						indDistancia.insert(indDistancia.begin() + npics + naltres + j, curv[puntsPicsPropers[j]]);
					}
					for (int j = 0; j < n2pics; ++j)
					{
						PuntsOut->insert(PuntsOut->begin() + npics + naltres + nPicProper + j, Punts[segonPics[j]]);
						indPuntsOut->insert(indPuntsOut->begin() + npics + naltres + nPicProper + j, segonPics[j]);
						indDistancia.insert(indDistancia.begin() + npics + naltres + nPicProper + j, curv[segonPics[j]]);
					}
					PuntsOut->erase(PuntsOut->begin() + npics + naltres + n2pics + nPicProper, PuntsOut->begin() + PuntsOut->size());
					indPuntsOut->erase(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper, indPuntsOut->begin() + indPuntsOut->size());
					indDistancia.erase(indDistancia.begin() + npics + naltres + n2pics + nPicProper, indDistancia.begin() + indDistancia.size());
				}
			}
		}
		int nPuntsN;
		std::vector<int> auxArray;
		if (nPunts > npics + naltres + n2pics + nPicProper)
		{
			nPuntsN = nPunts - (npics + naltres + n2pics + nPicProper);
			std::vector<int> auxIndPuntsOut = (*indPuntsOut);
			std::sort(auxIndPuntsOut.begin(), auxIndPuntsOut.end());
			std::set_difference(totsIndexos.begin(), totsIndexos.end(), auxIndPuntsOut.begin(), auxIndPuntsOut.end(), std::inserter(auxArray, auxArray.begin()));
			for (int j = 0; j < nPuntsN; ++j)
			{
				PuntsOut->insert(PuntsOut->begin() + npics + naltres + n2pics + nPicProper + j, Punts[auxArray[floor(auxArray.size() * j / (float)nPuntsN)]]);
				indPuntsOut->insert(indPuntsOut->begin() + npics + naltres + n2pics + nPicProper + j, auxArray[floor(auxArray.size() * j / (float)nPuntsN)]);
				indDistancia.insert(indDistancia.begin() + npics + naltres + n2pics + nPicProper + j, curv[auxArray[floor(auxArray.size() * j / (float)nPuntsN)]]);
			}
			PuntsOut->erase(PuntsOut->begin() + nPunts, PuntsOut->begin() + PuntsOut->size());
			indPuntsOut->erase(indPuntsOut->begin() + nPunts, indPuntsOut->begin() + indPuntsOut->size());
			indDistancia.erase(indDistancia.begin() + nPunts, indDistancia.begin() + indDistancia.size());
		}

		std::vector<int> aux(nPuntsIn);
		std::vector<int> arrayNPunts(nPunts);


		for (int i = 0; i < nPunts; i++)
		{
			aux[(*indPuntsOut)[i]] = 1;
			arrayNPunts[i] = i;
		}
		for (int i = 0; i < nPuntsIn; i++)
		{
			if (aux[i] > 0)
			{
				indPuntsOrdenat->push_back(i);
			}
		}
		int posT;
		vector<int>::iterator indx;
		std::vector<int> auxPerm(0);
		for (int i = 0; i < *nPuntsImp; i++)
		{
			indPuntsOrdImp->push_back((*indPuntsOut)[i]);
			indx = lower_bound(indPuntsOrdenat->begin(), indPuntsOrdenat->end(), (*indPuntsOrdImp)[i]);
			posT = indx - indPuntsOrdenat->begin();
			auxPerm.push_back(posT);

		}
		std::vector<int> auxIndPuntsOut = (*indPuntsOrdImp);
		std::sort(auxIndPuntsOut.begin(), auxIndPuntsOut.end());
		std::set_difference(indPuntsOrdenat->begin(), indPuntsOrdenat->end(), auxIndPuntsOut.begin(), auxIndPuntsOut.end(), std::inserter(*indPuntsOrdImp, indPuntsOrdImp->begin() + (*nPuntsImp)));
		auxIndPuntsOut = auxPerm;
		std::sort(auxIndPuntsOut.begin(), auxIndPuntsOut.end());
		std::set_difference(arrayNPunts.begin(), arrayNPunts.end(), auxIndPuntsOut.begin(), auxIndPuntsOut.end(), std::inserter(auxPerm, auxPerm.begin() + (*nPuntsImp)));
		*permutImp = std::vector<int>(auxPerm.size());
		for (int i = 0; i < auxPerm.size(); i++)
		{
			(*permutImp)[auxPerm[i]] = i;
		}
	}
}

void CDemoRegistreDlg::trobarPuntsElipse(cv::Mat &imatge, std::vector<cv::Point2i> &punts, int nPunts)
{
	cv::Mat imatgeAux = imatge.clone();
	uint8_t *imData = imatgeAux.data;
	cv::Mat mascara = cv::Mat::zeros(imatge.rows, imatge.cols, CV_8UC1);
	uint8_t *masData = mascara.data;
	std::vector<cv::Point2i> points = std::vector<cv::Point2i>();
	std::vector<cv::Point2i> pointsaux = std::vector<cv::Point2i>();
	int rows = imatgeAux.rows;
	int cols = imatgeAux.cols;
	int val, vmas;
	int rowMid = rows / 2;
	int colMid = cols / 2;
	int nSeg;
	int nMaxSeg = 0;

	//Buscarem en la direccions (1,0)
	if (numSegm < 2)
	{
		meanSegmentats[0].y = (float)rowMid;
		meanSegmentats[0].x = (float)colMid;
		for (int i = cols / 3; i < 2 * cols / 3; ++i)
		{
			val = imData[rowMid * cols + i];
			vmas = masData[rowMid * cols + i];
			if ((val > 1) && (vmas == 0))
			{
				while (1)
				{
					val = imData[rowMid * cols + i];
					vmas = masData[rowMid * cols + i];
					if ((val > 0) && (vmas == 0) && (i < cols - 3))
					{
						i++;
					}
					else
					{
						break;
					}
				}
				val = imData[rowMid * cols + i];
				vmas = masData[rowMid * cols + i];
				if ((val == 0) && (vmas == 0))
				{
					nSeg = creixementRegio(&imatgeAux, &mascara, i, rowMid, 1, &pointsaux);
					/*if (nSeg > nMaxSeg)
					{
					nMaxSeg = nSeg;
					points.clear();
					std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
					}*/
					if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
					{
						std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
					}
				}
			}
		}
		if (numSegm < 2)
			numSegm++;

	}
	else
	{
		rowMid = (int)meanSegmentats[0].y;
		for (int i = (int)meanSegmentats[0].x; i < cols; ++i)
		{
			val = imData[rowMid * cols + i];
			vmas = masData[rowMid * cols + i];
			if ((val > 1) && (vmas == 0))
			{
				nSeg = creixementRegio(&imatgeAux, &mascara, i, rowMid, 1, &pointsaux);
				/*if (nSeg > nMaxSeg)
				{
				nMaxSeg = nSeg;
				points.clear();
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				}*/
				if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
				{
					std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				}
			}
		}
		for (int i = (int)meanSegmentats[0].x; i > -1; --i)
		{
			val = imData[rowMid * cols + i];
			vmas = masData[rowMid * cols + i];
			if ((val > 1) && (vmas == 0))
			{
				nSeg = creixementRegio(&imatgeAux, &mascara, i, rowMid, 1, &pointsaux);
				/*if (nSeg > nMaxSeg)
				{
				nMaxSeg = nSeg;
				points.clear();
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				}*/
				if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
				{
					std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				}
			}
		}
	}
	//for (int i = rows/3; i < 2.0*rows/3; ++i)
	//{
	//	val = imData[i * cols + colMid];
	//	vmas = masData[i * cols + colMid];
	//	if ((val > 1) && (vmas == 0))
	//	{
	//		while (1)
	//		{
	//			val = imData[i * cols + colMid];
	//			vmas = masData[i * cols + colMid];
	//			if ((val > 0) && (vmas == 0) && (i < rows - 3))
	//			{
	//				i++;
	//			}
	//			else
	//			{
	//				break;
	//			}
	//		}
	//		val = imData[i * cols + colMid];
	//		vmas = masData[i * cols + colMid];
	//		if ((val == 0) && (vmas == 0))
	//		{
	//			nSeg = creixementRegio(&imatgeAux, &mascara, colMid, i, 1, &pointsaux);
	//			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
	//			{
	//				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
	//			}
	//		}
	//	}
	//}
	//for (int i = cols / 3; i < 2.0f * cols / 3; ++i)
	//{
	//	int j = i*rows / cols;
	//	val = imData[j * cols + i];
	//	vmas = masData[j * cols + i];
	//	if ((val > 1) && (vmas == 0))
	//	{
	//		while (1)
	//		{
	//			val = imData[j * cols + i];
	//			vmas = masData[j * cols + i];
	//			if ((val > 0) && (vmas == 0) && (i < cols - 3) && (j < rows - 3))
	//			{
	//				i++;
	//			}
	//			else
	//			{
	//				break;
	//			}
	//		}
	//		val = imData[j * cols + i];
	//		vmas = masData[j * cols + i];
	//		if ((val == 0) && (vmas == 0))
	//		{
	//			nSeg = creixementRegio(&imatgeAux, &mascara, i, j, 1, &pointsaux);
	//			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
	//			{
	//				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
	//			}
	//		}
	//	}
	//}
	//for (int i = cols / 3; i < 2.0f * cols / 3; ++i)
	//{
	//	int j = rows - i*rows / cols;
	//	val = imData[j * cols + i];
	//	vmas = masData[j * cols + i];
	//	if ((val > 1) && (vmas == 0))
	//	{
	//		while (1)
	//		{
	//			val = imData[j * cols + i];
	//			vmas = masData[j * cols + i];
	//			if ((val > 0) && (vmas == 0) && (i < cols - 3) && (j < rows - 3))
	//			{
	//				i++;
	//			}
	//			else
	//			{
	//				break;
	//			}
	//		}
	//		val = imData[j * cols + i];
	//		vmas = masData[j * cols + i];
	//		if ((val == 0) && (vmas == 0))
	//		{
	//			nSeg = creixementRegio(&imatgeAux, &mascara, i, j, 1, &pointsaux);
	//			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
	//			{
	//				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
	//			}
	//		}
	//	}
	//}

	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	punts = std::vector<cv::Point2i>(nPunts);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPunts);
	std::vector<float> rmaxP(nPunts);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPunts; i++)
	{
		rmaxP[i] = 100.0f*(winSegSize.height + winSegSize.width);
	}

	/*for (int i = 0; i < points.size(); ++i)
	{
	meanVal += (cv::Point2f)points[i];
	}
	meanVal.x = meanVal.x / points.size();
	meanVal.y = meanVal.y / points.size();*/
	if (numSegm < 1)
	{
		meanVal.x = colMid;
		meanVal.y = rowMid;
	}
	else
	{
		meanVal = meanSegmentats[0];
	}


	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPunts);
		if (indEx >= nPunts)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] > raux)
		{
			rmaxP[indEx] = raux;
			(punts)[indEx].x = points[i].x;
			(punts)[indEx].y = points[i].y;
		}
	}
	cv::Point2f centerElipse(0.0f, 0.0f);
	int esborrats = 0;
	for (int i = 0; i < punts.size(); i++)
	{
		if ((punts[i].x == 0) || (punts[i].y == 0))
		{
			esborrats++;
		}
		else
		{
			centerElipse.x = centerElipse.x + punts[i].x;
			centerElipse.y = centerElipse.y + punts[i].y;
		}
	}
	centerElipse /= (float)(punts.size() - esborrats);

	//Zona mean contorn
	if (numSegm < 1)
	{
		if (numSegm == 0)
		{
			meanSegmentats[0] = centerElipse;
		}
		numSegm++;

	}
	else
	{
		if (normCvPoint2f(meanSegmentats[0] - centerElipse) < DISTANCIACENTREELIPSE)
		{
			meanSegmentats[0] = centerElipse;
		}
	}
}


void CDemoRegistreDlg::trobarPuntsContorn(cv::Mat * imatge, std::vector<cv::Point2i> * punts, int nPunts)
{
	cv::Mat imatgeAux = imatge->clone();
	uint8_t *imData = imatgeAux.data;
	cv::Mat mascara = cv::Mat::zeros(imatge->rows, imatge->cols, CV_8UC1);
	uint8_t *masData = mascara.data;
	std::vector<cv::Point2i> points = std::vector<cv::Point2i>();
	std::vector<cv::Point2i> pointsaux = std::vector<cv::Point2i>();
	int rows = imatgeAux.rows;
	int cols = imatgeAux.cols;
	int val, vmas;
	int rowMid = rows / 2;
	int colMid = cols / 2;
	int nSeg;
	int nMaxSeg = 0;

	//Buscarem en 4 direccions (1,0), (1,1) (0,1) (1,-1)
	for (int i = 0; i < cols; ++i)
	{
		val = imData[rowMid * cols + i];
		vmas = masData[rowMid * cols + i];
		if ((val > 1) && (vmas == 0))
		{
			while (1)
			{
				val = imData[rowMid * cols + i];
				vmas = masData[rowMid * cols + i];
				if ((val > 0) && (vmas == 0) && (i < cols - 3))
				{
					i++;
				}
				else
				{
					break;
				}
			}
			val = imData[rowMid * cols + i];
			vmas = masData[rowMid * cols + i];
			if ((val == 0) && (vmas == 0))
			{
				nSeg = creixementRegio(&imatgeAux, &mascara, i, rowMid, 1, &pointsaux);
				/*if (nSeg > nMaxSeg)
				{
					nMaxSeg = nSeg;
					points.clear();
					std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				}*/
				if (nSeg > MINTAMANYZONACONTORN)
				{
					std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				}
			}
		}
	}
	for (int i = 0; i < rows; ++i)
	{
		val = imData[i * cols + colMid];
		vmas = masData[i * cols + colMid];
		if ((val > 1) && (vmas == 0))
		{
			while (1)
			{
				val = imData[i * cols + colMid];
				vmas = masData[i * cols + colMid];
				if ((val > 0) && (vmas == 0) && (i < rows - 3))
				{
					i++;
				}
				else
				{
					break;
				}
			}
			val = imData[i * cols + colMid];
			vmas = masData[i * cols + colMid];
			if ((val == 0) && (vmas == 0))
			{
				nSeg = creixementRegio(&imatgeAux, &mascara, colMid, i, 1, &pointsaux);
				if (nSeg > MINTAMANYZONACONTORN)
				{
					std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				}
			}
		}
	}
	for (int i = cols/3; i < 2*cols/3; ++i)
	{
		int j = i*rows/cols;
		val = imData[j * cols + i];
		vmas = masData[j * cols + i];
		if ((val > 1) && (vmas == 0))
		{
			while (1)
			{
				val = imData[j * cols + i];
				vmas = masData[j * cols + i];
				if ((val > 0) && (vmas == 0) && (i < cols - 3) &&(j < rows -3))
				{
					i++;
				}
				else
				{
					break;
				}
			}
			val = imData[j * cols + i];
			vmas = masData[j * cols + i];
			if ((val == 0) && (vmas == 0))
			{
				nSeg = creixementRegio(&imatgeAux, &mascara, i, j, 1, &pointsaux);
				if (nSeg > MINTAMANYZONACONTORN)
				{
					std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				}
			}
		}
	}
	for (int i = 0; i < cols; ++i)
	{
		int j = rows - i*rows / cols;
		val = imData[j * cols + i];
		vmas = masData[j * cols + i];
		if ((val > 1) && (vmas == 0))
		{
			while (1)
			{
				val = imData[j * cols + i];
				vmas = masData[j * cols + i];
				if ((val > 0) && (vmas == 0) && (i < cols - 3) && (j < rows - 3))
				{
					i++;
				}
				else
				{
					break;
				}
			}
			val = imData[j * cols + i];
			vmas = masData[j * cols + i];
			if ((val == 0) && (vmas == 0))
			{
				nSeg = creixementRegio(&imatgeAux, &mascara, i, j, 1, &pointsaux);
				if (nSeg > MINTAMANYZONACONTORN)
				{
					std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				}
			}
		}
	}

	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	*punts = std::vector<cv::Point2i>(nPunts);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPunts);
	std::vector<float> rmaxP(100);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;
	
	/*for (int i = 0; i < points.size(); ++i)
	{
		meanVal += (cv::Point2f)points[i];
	}
	meanVal.x = meanVal.x / points.size();
	meanVal.y = meanVal.y / points.size();*/
	if (numSegm < NUMMEANPUNTSCONTORN)
	{
		int xmin = 100000, xmax = -1, ymin = 10000, ymax = -1;
		for (int i = 0; i < points.size(); i++)
		{
			if (points[i].x < xmin)
			{
				xmin = points[i].x;
			}
			if (points[i].x > xmax)
			{
				xmax = points[i].x;
			}
			if (points[i].y < ymin)
			{
				ymin = points[i].y;
			}
			if (points[i].y > ymax)
			{
				ymax = points[i].y;
			}
		}
		meanVal.x = (xmax + xmin) / 2.0;
		meanVal.y = (ymax + ymin) / 2.0;
	}
	else
	{
		for (int i = 0; i < meanSegmentats.size(); ++i)
		{
			meanVal += meanSegmentats[i];
		}
		meanVal.x = meanVal.x / meanSegmentats.size();
		meanVal.y = meanVal.y / meanSegmentats.size();
	}
	

	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPunts);
		if (indEx >= nPunts)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] < raux)
		{
			rmaxP[indEx] = raux;
			(*punts)[indEx].x = points[i].x;
			(*punts)[indEx].y = points[i].y;
		}
	}

	//Zona mean contorn
	if (numSegm < NUMMEANPUNTSCONTORN)
	{
		if (numSegm == 0)
		{
			for (int i = 0; i < PUNTSCONTORNIMATGE2D; i++)
			{
				meanSegmentats[i].x = 0.0f;
				meanSegmentats[i].y = 0.0f;
			}
		}
		primerSegm = 0;
		ultimsSegmentats[numSegm].clear();
		std::copy(punts->begin(), punts->end(), std::back_inserter(ultimsSegmentats[numSegm]));
		numSegm++;
		for (int i = 0; i < PUNTSCONTORNIMATGE2D; i++)
		{
			meanSegmentats[i].x += (*punts)[i].x / (float)NUMMEANPUNTSCONTORN;
			meanSegmentats[i].y += (*punts)[i].y / (float)NUMMEANPUNTSCONTORN;
		}
	}
	else
	{
		for (int i = 0; i < PUNTSCONTORNIMATGE2D; i++)
		{
			if (((meanSegmentats[i].x - DISTANCIAMEANCONTORN < (*punts)[i].x) && (meanSegmentats[i].x + DISTANCIAMEANCONTORN > (*punts)[i].x)) && ((meanSegmentats[i].y - DISTANCIAMEANCONTORN < (*punts)[i].y) && (meanSegmentats[i].y + DISTANCIAMEANCONTORN > (*punts)[i].y)))
			{
				meanSegmentats[i].x = meanSegmentats[i].x + (*punts)[i].x / (float)NUMMEANPUNTSCONTORN - (ultimsSegmentats[primerSegm])[i].x / (float)NUMMEANPUNTSCONTORN;
				meanSegmentats[i].y = meanSegmentats[i].y + (*punts)[i].y / (float)NUMMEANPUNTSCONTORN - (ultimsSegmentats[primerSegm])[i].y / (float)NUMMEANPUNTSCONTORN;
				ultimsSegmentats[primerSegm][i].x = (*punts)[i].x;
				ultimsSegmentats[primerSegm][i].y = (*punts)[i].y;
			}
			else
			{
				meanSegmentats[i].x = meanSegmentats[i].x + (*punts)[i].x / (float)NUMMEANPUNTSCONTORN - (ultimsSegmentats[primerSegm])[i].x / (float)NUMMEANPUNTSCONTORN;
				meanSegmentats[i].y = meanSegmentats[i].y + (*punts)[i].y / (float)NUMMEANPUNTSCONTORN - (ultimsSegmentats[primerSegm])[i].y / (float)NUMMEANPUNTSCONTORN;
				ultimsSegmentats[primerSegm][i].x = (*punts)[i].x;
				ultimsSegmentats[primerSegm][i].y = (*punts)[i].y;
				(*punts)[i].x = 0;
				(*punts)[i].y = 0;
			}
		}
		primerSegm++;
		if (primerSegm >= NUMMEANPUNTSCONTORN)
		{
			primerSegm = 0;
		}
	}

	//Completar contorn
	std::vector<std::vector<int>> indNuls = std::vector<std::vector<int>>();
	std::vector<int> vauxiliar;
	std::vector<int> indFalta = std::vector<int>();
	std::vector<float> distFalta = std::vector<float>();
	float norA;
	int segmentNull = -1;
	int foratFinal = 0;
	for (int i = 0; i < punts->size() - foratFinal; ++i)
	{
		if (segmentNull == -1)
		{
			vauxiliar.clear();
			segmentNull = 0;
		}
		if (max((*punts)[i].x, (*punts)[i].y) == 0)
		{
			if (i == 0)
			{
				int iniForat = punts->size() - 1;
				while (max((*punts)[iniForat].x, (*punts)[iniForat].y) == 0)
				{
					iniForat--;
					if (iniForat < punts->size() / 2)
						break;
				}
				for (int j = iniForat + 1; j < punts->size(); j++)
				{
					segmentNull++;
					foratFinal++;
					vauxiliar.push_back(j);
					if (foratFinal >= punts->size() - 2)
						break;
				}
				segmentNull++;
				vauxiliar.push_back(i);
				continue;
			}
			else
			{ 
				segmentNull++;
				vauxiliar.push_back(i);
				continue;
			}
		}
		else
		{
			if (segmentNull > 0)
			{
				indNuls.push_back(vauxiliar);
				segmentNull = -1;
			}
		}
		norA = sqrt(((*punts)[i].x - (*punts)[modval(i + 1, punts->size())].x)*((*punts)[i].x - (*punts)[modval(i + 1, punts->size())].x) + ((*punts)[i].y - (*punts)[modval(i + 1, punts->size())].y)*((*punts)[i].y - (*punts)[modval(i + 1, punts->size())].y));
		if (norA > TOLDIST)
		{
			indFalta.push_back(i);
			distFalta.push_back(norA);
		}
	}
	int sizeIni = indNuls.size();
	int npuntsMaxOmplir = ceil(ANGMAXOMPLIRCONTORN / 360.0f*punts->size());
	int xiniForat, yiniForat, xfinForat, yfinForat;
	float deltaxForat, deltayForat;
	for(int i = 0; i < indNuls.size();i++)
	{
		vauxiliar.clear();
		vauxiliar = indNuls[i];
		if (vauxiliar.size()>npuntsMaxOmplir)
		{
			
			////FALTA FER EL CAS FORAT GRAN ( TO DO )
			//// MIRAR QU� FER: POTSER ESBORRARLOS	 
			////
		}
		else
		{
			xiniForat = (*punts)[modval(vauxiliar[0] - 1, punts->size())].x;
			yiniForat = (*punts)[modval(vauxiliar[0] - 1, punts->size())].y;
			xfinForat = (*punts)[modval(vauxiliar[vauxiliar.size() - 1] + 1, punts->size())].x;
			yfinForat = (*punts)[modval(vauxiliar[vauxiliar.size() - 1] + 1, punts->size())].y;
			deltaxForat = (xfinForat - xiniForat) / (float)(vauxiliar.size() + 1);
			deltayForat = (yfinForat - yiniForat) / (float)(vauxiliar.size() + 1);
			for (int j = 0; j < vauxiliar.size(); j++)
			{
				(*punts)[vauxiliar[j]].x = floor(xiniForat + deltaxForat * (j + 1));
				(*punts)[vauxiliar[j]].y = floor(yiniForat + deltayForat * (j + 1));
			}
			
		}
	}
	float delta, normAux, normAct;
	float recor;
	float vecnorm;
	std::vector<cv::Point2i> puntsFalten, puntsFalten2;
	cv::Point2i paux, pini, pfin;
	cv::Point2f direc;
	int jind, kind;
	int afegits = 0;
	int canvi = 0;
	for (int i = 0; i < indFalta.size(); i++)
	{
		indEx = indFalta[i];
		norA = distFalta[i];
		delta = norA / TOLDISTOMPLIR;
		pini.x = (*punts)[indEx + afegits].x;
		pini.y = (*punts)[indEx + afegits].y;
		pfin.x = (*punts)[modval(indEx + 1 + afegits, punts->size())].x;
		pfin.y = (*punts)[modval(indEx + 1 + afegits, punts->size())].y;
		puntsFalten.clear();
		puntsFalten2.clear();
		canvi = 0;
		while (delta > 1.0f)
		{
			if (delta < 2.0f)
			{
				recor = norA / 2.0f;
			}
			else
			{
				recor = TOLDISTOMPLIR;
			}
			direc.x = (float)(pfin.x - pini.x);
			direc.y = (float)(pfin.y - pini.y);
			vecnorm = sqrt(direc.x * direc.x + direc.y * direc.y);
			direc.x /= vecnorm;
			direc.y /= vecnorm;
			paux.x = pini.x + floor(recor*direc.x);
			paux.y = pini.y + floor(recor*direc.y);
			normAux = 100 * norA;
			jind = -1000;
			kind = -1000;
			for (int j = -2; j < 3; j++)
			{
				for (int k = 0; k < puntsCirc[modval(indEx + j,puntsCirc.size())].size(); k++)
				{
					normAct = sqrt((puntsCirc[modval(indEx + j, puntsCirc.size())][k].x - paux.x)*(puntsCirc[modval(indEx + j, puntsCirc.size())][k].x - paux.x) + (puntsCirc[modval(indEx + j, puntsCirc.size())][k].y - paux.y)*(puntsCirc[modval(indEx + j, puntsCirc.size())][k].y - paux.y));
					if (normAct < normAux)
					{
						normAux = normAct;
						jind = j;
						kind = k;
					}
				}
			}
			if ((jind == -1000) || (kind == -1000))
			{
				canvi = 1;
				break;
			}
			if ((pini.x != puntsCirc[modval(indEx + jind, puntsCirc.size())][kind].x) || (pini.y != puntsCirc[modval(indEx + jind, puntsCirc.size())][kind].y))
			{
				pini.x = puntsCirc[modval(indEx + jind, puntsCirc.size())][kind].x;
				pini.y = puntsCirc[modval(indEx + jind, puntsCirc.size())][kind].y;
			}
			else
			{
				canvi = 1;
				break;
			}
			norA = sqrt((pfin.x - pini.x)*(pfin.x - pini.x) + (pfin.y - pini.y)*(pfin.y - pini.y));
			delta = norA / TOLDISTOMPLIR;
			cv::Point2i ptf = cv::Point2i(pini.x, pini.y);
			puntsFalten.push_back(ptf);
		}
		if (canvi == 1)
		{
			indEx = indFalta[i];
			norA = distFalta[i];
			delta = norA / TOLDISTOMPLIR;
			pfin.x = (*punts)[indEx + afegits].x;
			pfin.y = (*punts)[indEx + afegits].y;
			pini.x = (*punts)[modval(indEx + 1 + afegits, punts->size())].x;
			pini.y = (*punts)[modval(indEx + 1 + afegits, punts->size())].y;
			puntsFalten2.clear();
			while (delta > 1.0f)
			{
				if (delta < 2.0f)
				{
					recor = norA / 2.0f;
				}
				else
				{
					recor = TOLDISTOMPLIR;
				}
				direc.x = (float)(pfin.x - pini.x);
				direc.y = (float)(pfin.y - pini.y);
				vecnorm = sqrt(direc.x * direc.x + direc.y * direc.y);
				direc.x /= vecnorm;
				direc.y /= vecnorm;
				paux.x = pini.x + floor(recor*direc.x);
				paux.y = pini.y + floor(recor*direc.y);
				normAux = 100 * norA;
				jind = -1000;
				kind = -1000;
				for (int j = -2; j < 3; j++)
				{
					for (int k = 0; k < puntsCirc[modval(indEx + j, puntsCirc.size())].size(); k++)
					{
						normAct = sqrt((puntsCirc[modval(indEx + j, puntsCirc.size())][k].x - paux.x)*(puntsCirc[modval(indEx + j, puntsCirc.size())][k].x - paux.x) + (puntsCirc[modval(indEx + j, puntsCirc.size())][k].y - paux.y)*(puntsCirc[modval(indEx + j, puntsCirc.size())][k].y - paux.y));
						if (normAct < normAux)
						{
							normAux = normAct;
							jind = j;
							kind = k;
						}
					}
				}
				if ((jind == -1000) || (kind == -1000))
				{
					break;
					canvi = 2;
				}
				if ((pini.x != puntsCirc[modval(indEx + jind, puntsCirc.size())][kind].x) || (pini.y != puntsCirc[modval(indEx + jind, puntsCirc.size())][kind].y))
				{
					pini.x = puntsCirc[modval(indEx + jind, puntsCirc.size())][kind].x;
					pini.y = puntsCirc[modval(indEx + jind, puntsCirc.size())][kind].y;
				}
				else
				{
					canvi = 2;
					break;
				}
				norA = sqrt((pfin.x - pini.x)*(pfin.x - pini.x) + (pfin.y - pini.y)*(pfin.y - pini.y));
				delta = norA / TOLDISTOMPLIR;
				cv::Point2i ptf = cv::Point2i(pini.x, pini.y);
				puntsFalten2.insert(puntsFalten2.begin(),ptf);
			}
		}
		if (canvi == 0)
		{
			punts->insert(punts->begin() + indEx + afegits + 1, puntsFalten.begin(), puntsFalten.end());
			afegits = afegits + puntsFalten.size();
		}
		else if (canvi == 1)
		{
			punts->insert(punts->begin() + indEx + afegits + 1, puntsFalten2.begin(), puntsFalten2.end());
			afegits = afegits + puntsFalten2.size();
		}
		else
		{
			punts->insert(punts->begin() + indEx + afegits + 1, puntsFalten.begin(), puntsFalten.end());
			afegits = afegits + puntsFalten.size();
			punts->insert(punts->begin() + indEx + afegits + 1, puntsFalten2.begin(), puntsFalten2.end());
			afegits = afegits + puntsFalten2.size();
		}
	}
}

int CDemoRegistreDlg::BuscarEllipse(cv::Mat &image, int &x, int &y)
{
	cv::Mat auxiliar = image.clone();
	auxiliar = Mat(auxiliar, cv::Rect(auxiliar.cols / 4, auxiliar.rows / 4, auxiliar.cols / 2, auxiliar.rows / 2));
	cv::Mat auxiliar2;

	

	double otsuvalue = cv::threshold(auxiliar, auxiliar2, 0,255,CV_THRESH_OTSU);
	cv::threshold(auxiliar, auxiliar2, (int)otsuvalue + 10, 255, CV_THRESH_BINARY);
	u_char * auxData = auxiliar2.data;
	/*CString nameImage;
	CString path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistreElipse\\DemoRegistre\\ImagesText\\ImatgesSegDemoDiaAnterior\\";

	nameImage.Format("Imatge_prova.png");
	nameImage = path + nameImage;
	imwrite(nameImage.GetString(), auxiliar2);*/

	//path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistreElipse\\DemoRegistre\\ImagesText\\ImatgesSegDemoDiaAnterior\\";
	//if (ind < 10)
	//{
	//	nameImage.Format("Imatge_solT_00%d.png", ind+1);
	//}
	//else if (ind < 100)
	//{
	//	nameImage.Format("Imatge_solT_0%d.png", ind+1);
	//}
	//else
	//{
	//	nameImage.Format("Imatge_solT_%d.png", ind+1);
	//}
	//nameImage = path + nameImage;
	//imwrite(nameImage.GetString(), auxiliar2);

	int maxValue = 0;
	int jBona = -1;
	int valueAux, valueAux2;
	int jAux,jini;
	bool bsc, bscn;
	bool finalImage;

	std::vector<std::vector<cv::Point3i>> PosX(0);
	std::vector<bool> possible(0);

	for (int i = 0; i < auxiliar2.rows; i = i + 5)
	{
		valueAux = 0;
		jAux = 0;
		while (jAux < auxiliar2.cols)
		{
			valueAux2 = 0;
			if (evaluarMat(auxData, i, jAux, auxiliar2.cols) > 0)
			{
				bscn = true;
				bsc = false;
				finalImage = false;

				while (bscn)
				{
					jAux = jAux + 5;
					if (jAux >= auxiliar2.cols)
					{
						bscn = false;
						bsc = false;
					}
					if (evaluarMat(auxData, i, jAux, auxiliar2.cols) == 0)
					{
						jini = jAux;
						bscn = false;
						bsc = true;
					}
				}
				if (bsc)
				{
					while (bsc)
					{
						jAux = jAux + 5;
						if (jAux >= auxiliar2.cols)
						{
							valueAux2 = jAux - jini;
							bsc = false;
							finalImage = true;
						}
						else if (evaluarMat(auxData, i, jAux, auxiliar2.cols) > 0)
						{
							bsc = false;
							valueAux2 = jAux - jini;
						}
					}
					if (valueAux2 > 15)
					{
						if (PosX.size() == 0)
						{
							std::vector<cv::Point3i> auxv(1);
							auxv[0].x = i;
							auxv[0].y = jini;
							auxv[0].z = jAux;
							PosX.push_back(auxv);
							if (finalImage)
							{
								possible.push_back(false);
							}
							else
							{
								possible.push_back(true);
							}
						}
						else
						{
							bool afegit = false;
							for (int jj = 0; jj < PosX.size(); jj++)
							{
								if ((i - PosX[jj][PosX[jj].size() - 1].x < 15) && (abs(PosX[jj][PosX[jj].size() - 1].y - jini) < 10))
								{
									PosX[jj].push_back(cv::Point3i(i, jini, jAux));
									if (finalImage)
									{
										possible[jj] = false;
									}
									afegit = true;
								}
								else if ((i - PosX[jj][PosX[jj].size() - 1].x < 15) && (abs(PosX[jj][PosX[jj].size() - 1].z - jAux) < 10))
								{
									PosX[jj].push_back(cv::Point3i(i, jini, jAux));
									if (finalImage)
									{
										possible[jj] = false;
									}
									afegit = true;
								}
							}
							if (!afegit)
							{
								std::vector<cv::Point3i> auxv(1);
								auxv[0].x = i;
								auxv[0].y = jini;
								auxv[0].z = jAux;
								PosX.push_back(auxv);
								if (finalImage)
								{
									possible.push_back(false);
								}
								else
								{
									possible.push_back(true);
								}
							}
						}
					}
					//jAux += 5;
				}
			}
			else
			{
				jAux += 5;
				bscn = false;
				bsc = false;
			}
		}
	}
	for (int i = 0; i < PosX.size(); i++)
	{
		if (PosX[i].size() < 6)
		{
			possible[i] = false;
			continue;
		}
		if ((auxiliar2.rows - PosX[i][PosX[i].size() - 1].x) < 10)
		{
			possible[i] = false;
			continue;
		}
		if (PosX[i][0].x < 10)
		{
			possible[i] = false;
			continue;
		}
		if (evaluarMat(auxData, PosX[i][0].x - 5, PosX[i][0].y, auxiliar2.cols) == 0)
		{
			bool finalE = false;
			bool blanc = false;
			int jaux = PosX[i][0].y - 5;
			while (!finalE)
			{
				if (evaluarMat(auxData, PosX[i][0].x - 5, jaux, auxiliar2.cols) == 0)
				{
					if (PosX[i][0].y - jaux > 20)
					{
						finalE = true;
						blanc = false;
					}
					jaux -= 5;
				}
				else
				{
					finalE = true;
					blanc = true;
				}
			}
			if (!blanc)
			{
				possible[i] = false;
			}
		}
		if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, PosX[i][PosX[i].size() - 1].y, auxiliar2.cols) == 0)
		{
			bool finalE = false;
			bool blanc = false;
			int jaux = PosX[i][PosX[i].size() - 1].y - 5;
			while (!finalE)
			{
				if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, jaux, auxiliar2.cols) == 0)
				{
					if (PosX[i][PosX[i].size() - 1].y - jaux > 20)
					{
						finalE = true;
						blanc = false;
					}
					jaux -= 5;
				}
				else
				{
					finalE = true;
					blanc = true;
				}
			}
			if (!blanc)
			{
				possible[i] = false;
			}
		}
		if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, PosX[i][PosX[i].size() - 1].z, auxiliar2.cols) == 0)
		{
			bool finalE = false;
			bool blanc = false;
			int jaux = PosX[i][PosX[i].size() - 1].z + 5;
			while (!finalE)
			{
				if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, jaux, auxiliar2.cols) == 0)
				{
					if (jaux - PosX[i][PosX[i].size() - 1].z > 20)
					{
						finalE = true;
						blanc = false;
					}
					jaux += 5;
				}
				else
				{
					finalE = true;
					blanc = true;
				}
			}
			if (!blanc)
			{
				possible[i] = false;
			}
		}
	}
	std::vector<int> iPoss(0);
	int maxA = 0;
	int posM = -1;
	int maxAux;
	for(int i = 0; i < possible.size(); i++)
	{
		if (possible[i])
		{
			iPoss.push_back(i);
		}
	}
	if (iPoss.size() == 0)
	{
		return -1;
	}
	if (iPoss.size() == 1)
	{
		y = (PosX[iPoss[0]][0].x + PosX[iPoss[0]][PosX[iPoss[0]].size() - 1].x) / 2 + image.rows / 4;
		x = (PosX[iPoss[0]][PosX[iPoss[0]].size() / 2].y + PosX[iPoss[0]][PosX[iPoss[0]].size() / 2].z) / 2 + image.cols / 4;
		return 0;
	}
	else
	{
		for (int i = 0; i < iPoss.size(); i++)
		{
			maxAux = 0;
			for (int j = 0; j < PosX[iPoss[i]].size(); j++)
			{
				maxAux = maxAux + PosX[iPoss[i]][j].z - PosX[iPoss[i]][j].y;
			}
			if (maxAux > maxA)
			{
				maxA = maxAux;
				posM = i;
			}
		}
		y = (PosX[iPoss[posM]][0].x + PosX[iPoss[posM]][PosX[iPoss[posM]].size() - 1].x) / 2 + image.rows / 4;
		x = (PosX[iPoss[posM]][PosX[iPoss[posM]].size() / 2].y + PosX[iPoss[posM]][PosX[iPoss[posM]].size() / 2].z) / 2 + image.cols / 4;
		return 1;
	}
}

int CDemoRegistreDlg::ParametresEllipseProbabilitat(std::vector <cv::Point2f> &punts, float &x0, float &y0, float &a2, float &b2, float &ang)
{
	int numTirades = 200;
	srand(time(NULL));
	int millor;
	int millorAux;
	float distancia, distanciaMillor = 100000000.0f, distanciaTotal;
	float distBona = 0.0002f;
	int tirada;
	Eigen::VectorXf paramBons(5);

	std::vector <cv::Point2f> puntsBons;
	std::vector <int> samples;
	puntsBons.clear();
	Eigen::Matrix3f conicaAux;

	for (int i = 0; i < punts.size(); i++)
	{
		if ((punts[i].x == 0) || (punts[i].y == 0))
			continue;
		puntsBons.push_back(punts[i]);
	}

	if (puntsBons.size() < PUNTSCONTORNIMATGE2D / 4)
		return -1;

	bool trobat = false;
	int sizeMatrix = 10;
	Eigen::MatrixXf A(sizeMatrix, 5);
	Eigen::VectorXf B(sizeMatrix);
	Eigen::VectorXf param;
	float deltaAux, JAux, IAux;
	millor = 0;
	for (int i = 0; i < numTirades; i++)
	{
		samples.clear();
		while (samples.size() < sizeMatrix)
		{
			tirada = rand() % puntsBons.size();
			trobat = false;
			for (int j = 0; j < samples.size(); j++)
			{
				if (samples[j] == tirada)
				{
					trobat = true;
					break;
				}
			}
			if (!trobat)
			{
				samples.push_back(tirada);
			}
		}
		for (int j = 0; j < samples.size(); j++)
		{
			A(j, 0) = puntsBons[samples[j]].x * puntsBons[samples[j]].x;
			A(j, 1) = puntsBons[samples[j]].x * puntsBons[samples[j]].y;
			A(j, 2) = puntsBons[samples[j]].y * puntsBons[samples[j]].y;
			A(j, 3) = puntsBons[samples[j]].x;
			A(j, 4) = puntsBons[samples[j]].y;
			B(j) = -1.0f;
		}
		param = (A.transpose()*A).inverse()*A.transpose()*B;
		millorAux = 0;
		distanciaTotal = 0.0f;

		
		conicaAux(0, 0) = param(0) / param(0);
		conicaAux(0, 1) = param(1) / 2.0f / param(0);
		conicaAux(1, 0) = param(1) / 2.0f / param(0);
		conicaAux(1, 1) = param(2) / param(0);
		conicaAux(0, 2) = param(3) / 2.0f / param(0);
		conicaAux(2, 0) = param(3) / 2.0f / param(0);
		conicaAux(1, 2) = param(4) / 2.0f / param(0);
		conicaAux(2, 1) = param(4) / 2.0f / param(0);
		conicaAux(2, 2) = 1.0f / param(0);
		deltaAux = conicaAux.determinant();
		JAux = conicaAux(0, 0)*conicaAux(1, 1) - conicaAux(0, 1)*conicaAux(1, 0);
		IAux = conicaAux(0, 0) + conicaAux(1, 1);
		if ((fabs(deltaAux) > 0.00000001f) && (JAux > 0.0f) && (deltaAux / IAux < 0.0f))
		{
			for (int j = 0; j < puntsBons.size(); j++)
			{
				distancia = fabs(puntsBons[j].x* puntsBons[j].x*param(0) + puntsBons[j].x * puntsBons[j].y*param(1) + puntsBons[j].y*puntsBons[j].y*param(2) + puntsBons[j].x*param(3) + puntsBons[j].y*param(4) + 1.0f);
				if (distancia < distBona)
				{
					millorAux++;
					distanciaTotal += distancia;
				}
			}
			if (millorAux > millor)
			{
				millor = millorAux;
				distanciaMillor = distanciaTotal;
				for (int j = 0; j < 5; j++)
					paramBons(j) = param(j);
			}
			else if ((millorAux == millor) && (distanciaTotal < distanciaMillor))
			{
				distanciaMillor = distanciaTotal;
				for (int j = 0; j < 5; j++)
					paramBons(j) = param(j);
			}
		}
	}
	if (millor < 10)
		return 0;
	Eigen::Matrix3f conica;
	conica(0, 0) = paramBons(0) / paramBons(0);
	conica(0, 1) = paramBons(1) / 2.0f / paramBons(0);
	conica(1, 0) = paramBons(1) / 2.0f / paramBons(0);
	conica(1, 1) = paramBons(2) / paramBons(0);
	conica(0, 2) = paramBons(3) / 2.0f / paramBons(0);
	conica(2, 0) = paramBons(3) / 2.0f / paramBons(0);
	conica(1, 2) = paramBons(4) / 2.0f / paramBons(0);
	conica(2, 1) = paramBons(4) / 2.0f / paramBons(0);
	conica(2, 2) = 1.0f / paramBons(0);
	float delta = conica.determinant();
	float J = conica(0, 0)*conica(1, 1) - conica(0, 1)*conica(1, 0);
	float I = conica(0, 0) + conica(1, 1);
	float termeEixos;
	if (fabs(delta)> 0.00000000001f)
	{
		if (J > 0.0f)
		{
			if (delta / I < 0.0f)
			{
				x0 = -(conica(1, 1)*conica(0, 2) - conica(0, 1)*conica(1, 2)) / J;
				y0 = -(conica(0, 0)*conica(1, 2) - conica(0, 1)*conica(0, 2)) / J;
				termeEixos = -2.0f*(conica(0, 0)*conica(1, 2)*conica(1, 2) + conica(1, 1)*conica(0, 2)*conica(0, 2) + conica(2,2)*conica(0, 1)*conica(0, 1) - 2.0f*conica(0, 1)*conica(0, 2)*conica(1, 2) - conica(0, 0)*conica(1, 1)*conica(2,2)) / J;
				a2 = sqrtf(termeEixos / (sqrtf((conica(0, 0) - conica(1, 1))*(conica(0, 0) - conica(1, 1)) + 4.0f*conica(0, 1)*conica(0, 1)) - conica(0, 0) - conica(1, 1)));
				b2 = sqrtf(termeEixos / (-sqrtf((conica(0, 0) - conica(1, 1))*(conica(0, 0) - conica(1, 1)) + 4.0f*conica(0, 1)*conica(0, 1)) - conica(0, 0) - conica(1, 1)));
				
				if ((fabs(conica(0, 1)) < 0.000000001f)&&(conica(0,0)<conica(1,1)))
				{
					ang = 0.0f;
				}
				else if (fabs(conica(0, 1)) < 0.000000001f)
				{
					ang = M_PI / 2.0f;
				}
				else if (conica(0, 0) < conica(1, 1))
				{
					ang = 1.0f / 2.0f * atanf(1/((conica(0, 0) - conica(1, 1)) / 2.0f / conica(0, 1)));
				}
				else
				{
					ang = M_PI/2.0f + 1.0f / 2.0f * atanf(1/((conica(0, 0) - conica(1, 1)) / 2.0f / conica(0, 1)));
				}

				return 1;
			}
			return -2;
		}
		return -3;
	}
	return -4;
}

std::vector<cv::Point2f> CDemoRegistreDlg::calculNormals(std::vector<cv::Point2f> Punts)
{
	int lp = Punts.size();
	std::vector<cv::Point2f> Normals(lp);
	cv::Point2f v1, v2;
	for (int i = 0; i < lp; i++)
	{
		v1 = Punts[modval(i - 1, lp)] - Punts[i];
		v2 = Punts[modval(i + 1, lp)] - Punts[i];
		v1 /= normCvPoint2f(v1);
		v2 /= normCvPoint2f(v2);
		Normals[i] = (v2 + v1);
		Normals[i] /= normCvPoint2f(Normals[i]);
	}
	return Normals;
}

int CDemoRegistreDlg::creixementRegio(cv::Mat * imatge, cv::Mat * mascara, int x, int y, int lbl, std::vector <cv::Point2i> * points)
{
	std::vector<cv::Point2i> cua;
	cv::Point2i pt = cv::Point2i(x, y);
	points->clear();
	points->push_back(pt);
	cua.push_back(pt);
	uint8_t *imData = imatge->data;
	uint8_t *masData = mascara->data;
	int DD[2][8] = {{-1, -1, -1, 0, 1, 1, 1, 0}, {-1, 0, 1, 1, 1, 0, -1, -1}};
	int nPunts = 1;
	bool trobat = false;
	masData[y * mascara->cols + x] = lbl;
	while (cua.size() > 0)
	{
		pt = cua.back();
		cua.pop_back();
		for (int i = 0; i < 8; ++i)
		{
			if ((pt.x + DD[0][i] > -1) && (pt.y + DD[1][i] > -1) && (pt.x + DD[0][i] < mascara->cols) && (pt.y + DD[1][i] < mascara->rows))
			{
				if ((evaluarMat(imData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge->cols) > 0) && (evaluarMat(masData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge->cols) == 0))
				{
					cv::Point2i ptr = cv::Point2i(pt.x + DD[0][i], pt.y + DD[1][i]);
					cua.push_back(ptr);
					masData[(pt.y + DD[1][i]) * imatge->cols + (pt.x + DD[0][i])] = lbl;
					points->push_back(ptr);
					nPunts++;
				}
			}
		}
	}
	return nPunts;
}

void CDemoRegistreDlg::getSkeleton(cv::Mat &entrada, cv::Mat & sortida)
{
	cv::Mat auxiliar = entrada.clone();
	cv::Mat skel(auxiliar.size(), CV_8UC1, cv::Scalar(0));
	cv::Mat temp;
	cv::Mat eroded;

	cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));
	//int i = 0;

	bool done;
	do
	{
		/*if (i > 1)
		{
			break;
		}
		i++;*/
		cv::erode(auxiliar, eroded, element);
		cv::dilate(eroded, temp, element); // temp = open(img)
		cv::subtract(auxiliar, temp, temp);
		cv::bitwise_or(skel, temp, skel);
		eroded.copyTo(auxiliar);

		done = (cv::countNonZero(auxiliar) == 0);
	} while (!done);
	cv::morphologyEx(auxiliar, auxiliar, MORPH_CLOSE, squareKernel);
	sortida = skel.clone();
}


void CDemoRegistreDlg::thinningIteration(cv::Mat& img, int iter)
{
	CV_Assert(img.channels() == 1);
	CV_Assert(img.depth() != sizeof(uchar));
	CV_Assert(img.rows > 3 && img.cols > 3);

	cv::Mat marker = cv::Mat::zeros(img.size(), CV_8UC1);

	int nRows = img.rows;
	int nCols = img.cols;

	if (img.isContinuous()) {
		nCols *= nRows;
		nRows = 1;
	}

	int x, y;
	uchar *pAbove;
	uchar *pCurr;
	uchar *pBelow;
	uchar *nw, *no, *ne;    // north (pAbove)
	uchar *we, *me, *ea;
	uchar *sw, *so, *se;    // south (pBelow)

	uchar *pDst;

	// initialize row pointers
	pAbove = NULL;
	pCurr = img.ptr<uchar>(0);
	pBelow = img.ptr<uchar>(1);

	for (y = 1; y < img.rows - 1; ++y) {
		// shift the rows up by one
		pAbove = pCurr;
		pCurr = pBelow;
		pBelow = img.ptr<uchar>(y + 1);

		pDst = marker.ptr<uchar>(y);

		// initialize col pointers
		no = &(pAbove[0]);
		ne = &(pAbove[1]);
		me = &(pCurr[0]);
		ea = &(pCurr[1]);
		so = &(pBelow[0]);
		se = &(pBelow[1]);

		for (x = 1; x < img.cols - 1; ++x) {
			// shift col pointers left by one (scan left to right)
			nw = no;
			no = ne;
			ne = &(pAbove[x + 1]);
			we = me;
			me = ea;
			ea = &(pCurr[x + 1]);
			sw = so;
			so = se;
			se = &(pBelow[x + 1]);

			int A = (*no == 0 && *ne == 1) + (*ne == 0 && *ea == 1) +
				(*ea == 0 && *se == 1) + (*se == 0 && *so == 1) +
				(*so == 0 && *sw == 1) + (*sw == 0 && *we == 1) +
				(*we == 0 && *nw == 1) + (*nw == 0 && *no == 1);
			int B = *no + *ne + *ea + *se + *so + *sw + *we + *nw;
			int m1 = iter == 0 ? (*no * *ea * *so) : (*no * *ea * *we);
			int m2 = iter == 0 ? (*ea * *so * *we) : (*no * *so * *we);

			if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
				pDst[x] = 1;
		}
	}

	img &= ~marker;
}

/**
* Function for thinning the given binary image
*
* Parameters:
* 		src  The source image, binary with range = [0,255]
* 		dst  The destination image
*/
void CDemoRegistreDlg::ZhangSuen(const cv::Mat& src, cv::Mat& dst)
{
	dst = src.clone();
	dst /= 255;         // convert to binary image

	cv::Mat prev = cv::Mat::zeros(dst.size(), CV_8UC1);
	cv::Mat diff;

	//int times = 0;

	//do {
	//	times++;
		thinningIteration(dst, 0);
		thinningIteration(dst, 1);
		cv::absdiff(dst, prev, diff);
		dst.copyTo(prev);
	//} while ((cv::countNonZero(diff) > 0)&&(times<1));

	dst *= 255;
}


void CDemoRegistreDlg::nonMaximumSuppresssion(cv::Mat &image, cv::Mat &gradMod, cv::Mat &gradPhase, cv::Mat &output)
{
	cv::Mat auxiliar = image.clone();
	output = cv::Mat::zeros(image.rows, image.cols, CV_8U);
	uint8_t *imData = auxiliar.data;
	float *gMData = (float*)gradMod.data;
	uint8_t *outData = output.data;
	float *gPData = (float*)gradPhase.data;
	int nrows = output.rows;
	int ncols = output.cols;
	for (int i = 0; i < output.rows; i++)
	{
		for (int j = 0; j < output.cols; j++)
		{
			if (evaluarMat(imData, i, j, ncols) > 0)
			{
				if (((j + 1) < ncols) && ((j - 1) > -1) && ((i + 1) < nrows) && ((i - 1) > -1))
				{
					//Horitzontal
					if (((gPData[i*ncols + j] >= -0.39f) && (gPData[i*ncols + j] <= 0.39f)) || (gPData[i*ncols + j] >= 2.74f) || (gPData[i*ncols + j] <= -2.74f))
					{
						if ((evaluarMat(imData, i, j - 1, ncols) > 0) && (evaluarMat(imData, i, j + 1, ncols) > 0))
						{
							if ((gMData[i*ncols + j - 1] <= gMData[i*ncols + j]) && (gMData[i*ncols + j] <= gMData[i*ncols + j + 1]))
							{
								outData[i*ncols + j] = 255;
							}
						}
						else if (evaluarMat(imData, i, j - 1, ncols) > 0)
						{
							if (gMData[i*ncols + j - 1] <= gMData[i*ncols + j])
							{
								outData[i*ncols + j] = 255;
							}
						}
						else if (evaluarMat(imData, i, j + 1, ncols) > 0)
						{
							if (gMData[i*ncols + j + 1] <= gMData[i*ncols + j])
							{
								outData[i*ncols + j] = 255;
							}
						}
						else
						{
							outData[i*ncols + j] = 255;
						}
					}
					//Diagonal x=y
					else if (((gPData[i*ncols + j] >= 0.39f) && (gPData[i*ncols + j] <= 1.17f)) || ((gPData[i*ncols + j] >= -2.74) && (gPData[i*ncols + j] <= -1.96f)))
					{
						if ((evaluarMat(imData, i - 1, j + 1, ncols) > 0) && (evaluarMat(imData, i + 1, j - 1, ncols) > 0))
						{
							if ((gMData[(i - 1) * ncols + j + 1] <= gMData[i*ncols + j]) && (gMData[(i + 1) * ncols + j - 1] <= gMData[i*ncols + j]))
							{
								outData[i*ncols + j] = 255;
							}
						}
						else if (evaluarMat(imData, i - 1, j + 1, ncols) > 0)
						{
							if (gMData[(i - 1) * ncols + j + 1] <= gMData[i*ncols + j])
							{
								outData[i*ncols + j] = 255;
							}
						}
						else if (evaluarMat(imData, i + 1, j - 1, ncols) > 0)
						{
							if (gMData[(i + 1) * ncols + j - 1] <= gMData[i*ncols + j])
							{
								outData[i*ncols + j] = 255;
							}
						}
						else
						{
							outData[i*ncols + j] = 255;
						}
					}
					//Vertical
					else if (((gPData[i*ncols + j] >= 1.17f) && (gPData[i*ncols + j] <= 1.96f)) || ((gPData[i*ncols + j] >= -1.96f) && (gPData[i*ncols + j] <= -1.17f)))
					{
						if ((evaluarMat(imData, i - 1, j, ncols) > 0) && (evaluarMat(imData, i + 1, j, ncols) > 0))
						{
							if ((gMData[(i - 1) * ncols + j] <= gMData[i*ncols + j]) && (gMData[(i + 1) * ncols + j] <= gMData[i*ncols + j]))
							{
								outData[i*ncols + j] = 255;
							}
						}
						else if (evaluarMat(imData, i - 1, j, ncols) > 0)
						{
							if (gMData[(i - 1) * ncols + j] <= gMData[i*ncols + j])
							{
								outData[i*ncols + j] = 255;
							}
						}
						else if (evaluarMat(imData, i + 1, j, ncols) > 0)
						{
							if (gMData[(i + 1) * ncols + j] <= gMData[i*ncols + j])
							{
								outData[i*ncols + j] = 255;
							}
						}
						else
						{
							outData[i*ncols + j] = 255;
						}
					}
					//Diagonal x=y
					else
					{
						if ((evaluarMat(imData, i + 1, j + 1, ncols) > 0) && (evaluarMat(imData, i - 1, j - 1, ncols) > 0))
						{
							if ((gMData[(i + 1) * ncols + j + 1] <= gMData[i*ncols + j]) && (gMData[(i - 1) * ncols + j - 1] <= gMData[i*ncols + j]))
							{
								outData[i*ncols + j] = 255;
							}
						}
						else if (evaluarMat(imData, i + 1, j + 1, ncols) > 0)
						{
							if (gMData[(i + 1) * ncols + j + 1] <= gMData[i*ncols + j])
							{
								outData[i*ncols + j] = 255;
							}
						}
						else if (evaluarMat(imData, i - 1, j - 1, ncols) > 0)
						{
							if (gMData[(i - 1) * ncols + j - 1] <= gMData[i*ncols + j])
							{
								outData[i*ncols + j] = 255;
							}
						}
						else
						{
							outData[i*ncols + j] = 255;
						}
					}
				}
			}
		}
	}
}


int CDemoRegistreDlg::creixementRegio2(cv::Mat &imatge, cv::Mat &mascara, int x, int y, int lbl, std::vector <cv::Point2i> &points)
{
	std::vector<cv::Point2i> cua;
	cv::Point2i pt = cv::Point2i(x, y);
	points.clear();
	points.push_back(pt);
	cua.push_back(pt);
	uint8_t *imData = imatge.data;
	uint8_t *masData = mascara.data;
	int DD[2][8] = { { -1, -1, -1, 0, 1, 1, 1, 0 },{ -1, 0, 1, 1, 1, 0, -1, -1 } };
	int nPunts = 1;
	bool trobat = false;
	masData[y * mascara.cols + x] = lbl;
	cv::Mat auxMat = cv::Mat::zeros(imatge.rows, imatge.cols, CV_8UC1);
	uint8_t *auxData = auxMat.data;
	while (cua.size() > 0)
	{
		pt = cua.back();
		cua.pop_back();
		for (int i = 0; i < 8; ++i)
		{
			if ((pt.x + DD[0][i] > -1) && (pt.y + DD[1][i] > -1) && (pt.x + DD[0][i] < mascara.cols) && (pt.y + DD[1][i] < mascara.rows))
			{
				if ((evaluarMat(imData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) == 0) && (evaluarMat(masData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) == 0))
				{
					if ((evaluarMat(imData, (pt.y + DD[1][modval(i + 1, 8)]), (pt.x + DD[0][modval(i + 1, 8)]), imatge.cols) == 0) || (evaluarMat(imData, (pt.y + DD[1][modval(i - 1, 8)]), (pt.x + DD[0][modval(i - 1, 8)]), imatge.cols) == 0))
					{
						trobat = 0;
						for (int j = 0; j < 8; ++j)
						{
							if (evaluarMat(imData, (pt.y + DD[1][i] + DD[1][j]), (pt.x + DD[0][i] + DD[0][j]), imatge.cols))
							{
								trobat = true;
								break;
							}
						}
						if (trobat)
						{
							cv::Point2i ptr = cv::Point2i(pt.x + DD[0][i], pt.y + DD[1][i]);
							cua.push_back(ptr);
							masData[(pt.y + DD[1][i]) * imatge.cols + (pt.x + DD[0][i])] = lbl;
						}
					}
				}
				else if ((evaluarMat(imData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) > 0) && (evaluarMat(auxData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) == 0))
				{
					auxData[(pt.y + DD[1][i]) * imatge.cols + (pt.x + DD[0][i])] = 1;
					cv::Point2i ptr = cv::Point2i((pt.x + DD[0][i]), (pt.y + DD[1][i]));
					points.push_back(ptr);
					nPunts++;
				}
			}
		}
	}
	return nPunts;
}

void CDemoRegistreDlg::correspondences(std::vector <cv::Point2f> * points2D, std::vector <cv::Point2f> * points3D, std::vector <int> * corresp, std::vector <int> * indPoints2D, std::vector <int> * indPoints3D)
{
	*corresp = std::vector <int>(indPoints2D->size());
	float distM = 1000000.0f;
	float auxNorm;
	int angleMig = 0;
	for (int i = 0; i < indPoints3D->size(); i++)
	{
		auxNorm = sqrt(((*points2D)[(*indPoints2D)[0]].x - (*points3D)[(*indPoints3D)[i]].x)*((*points2D)[(*indPoints2D)[0]].x - (*points3D)[(*indPoints3D)[i]].x) + ((*points2D)[(*indPoints2D)[0]].y - (*points3D)[(*indPoints3D)[i]].y)*((*points2D)[(*indPoints2D)[0]].y - (*points3D)[(*indPoints3D)[i]].y));
		if (auxNorm < distM)
		{
			distM = auxNorm;
			(*corresp)[0] = (*indPoints3D)[i];
			angleMig = i;
		}
	}
	int angleveins = ceil(indPoints2D->size() / 10.0f);
	int angaux;
	for (int i = 1; i < indPoints2D->size(); i++)
	{
		distM = 1000000.0f;
		angaux = 0;
		for (int j = -angleveins; j < angleveins + 1; j++)
		{
			auxNorm = normDistancia((*points2D)[(*indPoints2D)[i]].x, (*points2D)[(*indPoints2D)[i]].y, (*points3D)[(*indPoints3D)[modval(angleMig + j, indPoints3D->size())]].x, (*points3D)[(*indPoints3D)[modval(angleMig + j, indPoints3D->size())]].y); 
			if (auxNorm < distM)
			{
				distM = auxNorm;
				(*corresp)[i] = (*indPoints3D)[modval(angleMig + j, indPoints3D->size())]; 
				angaux = modval(angleMig + j, indPoints3D->size());
			}
		}
		angleMig = angaux;
	}
}

void CDemoRegistreDlg::Matriu2QuatandPos(Eigen::Matrix4f * Input, Eigen::Vector4f * quad, Eigen::Vector3f * pos)
{
	QuaternionFromMatrix(&(*quad)(0), &(*quad)(1), &(*quad)(2), &(*quad)(3), Input->block(0, 0, 3, 3));
	(*pos)(0) = (*Input)(0, 3);
	(*pos)(1) = (*Input)(1, 3);
	(*pos)(2) = (*Input)(2, 3);
}

void CDemoRegistreDlg::QuatandPos2Matriu(Eigen::Vector4f * quad, Eigen::Vector3f * pos, Eigen::Matrix4f * Output)
{
	Output->setZero();
	Eigen::Matrix3f aux;
	QuaternionToMatrix((*quad)(0), (*quad)(1), (*quad)(2), (*quad)(3), &aux);
	Output->block(0, 0, 3, 3) = aux;
	(*Output)(0, 3) = (*pos)(0);
	(*Output)(1, 3) = (*pos)(1);
	(*Output)(2, 3) = (*pos)(2);
	(*Output)(3, 3) = 1.0f;
}

void CDemoRegistreDlg::Matriu2EulerXYZandPos(Eigen::Matrix4f Input, Eigen::Vector3f * euler1, Eigen::Vector3f * euler2, Eigen::Vector3f * pos)
{
	float angx[2], angy[2], angz[2];
	EulerXYZFromMatrix(angx, angy, angz, Input.block(0, 0, 3, 3));
	(*euler1)(0) = angx[0];
	(*euler1)(1) = angy[0];
	(*euler1)(2) = angz[0];
	(*euler2)(0) = angx[1];
	(*euler2)(1) = angy[1];
	(*euler2)(2) = angz[1];
	(*pos)(0) = (Input)(0, 3);
	(*pos)(1) = (Input)(1, 3);
	(*pos)(2) = (Input)(2, 3);
}

void CDemoRegistreDlg::EulerXYZandPos2Matriu(Eigen::Vector3f * euler, Eigen::Vector3f * pos, Eigen::Matrix4f * Output)
{
	Output->setZero();
	Eigen::Matrix3f aux;
	EulerXYZtoMatrix((*euler)(0), (*euler)(1), (*euler)(2), &aux);
	Output->block(0, 0, 3, 3) = aux;
	(*Output)(0, 3) = (*pos)(0);
	(*Output)(1, 3) = (*pos)(1);
	(*Output)(2, 3) = (*pos)(2);
	(*Output)(3, 3) = 1.0f;
}

float CDemoRegistreDlg::ICPCurvatura2D3DIter(std::vector<cv::Point2f> * Punts2D, std::vector<cv::Point3f> * Punts3D, Eigen::Matrix4f * tIn, Eigen::Matrix4f * tOut, int nIn, int nDelta, int iterICP, float tol, int *nOut)
{
	Eigen::Vector4f quat;
	Eigen::Vector3f pos;
	Eigen::Matrix4f Trans, TransAns;
	Eigen::Matrix4Xf punts3DHom(4, Punts3D->size());
	Eigen::Matrix4Xf punts3DTrans(4, Punts3D->size());
	Eigen::Matrix2Xf punts3DProj(2, Punts3D->size());
	Eigen::Matrix2Xf punts2DMatrix(2, Punts2D->size());
	int iterat = 0;
	int nCurv = nIn;
	float diff;
	int nPuntsImp;
	std::vector<cv::Point2f> punts3DCont(Punts2D->size());
	std::vector<int> indPunts3DCont(Punts2D->size());
	std::vector<cv::Point2f> punts3DCurv, punts2DCurv;
	std::vector<int> indPunts3DCurv, indPunts2DCurv;
	std::vector<int> indPunts3DCurvOrd, indPunts2DCurvOrd;
	std::vector<int> indPunts3DCurvImp, indPunts2DCurvImp;
	std::vector<int> permPunts2D, permPunts3D;
	std::vector<int> corresp;
	Eigen::VectorXf x(7);
	std::vector<cv::Point2f> Normals2D = calculNormals(*Punts2D);
	float vNormaOptimit = TOLERANCIA;
	for (int i = 0; i < Punts3D->size(); i++)
	{
		punts3DHom(0, i) = (*Punts3D)[i].x;
		punts3DHom(1, i) = (*Punts3D)[i].y;
		punts3DHom(2, i) = (*Punts3D)[i].z;
		punts3DHom(3, i) = 1.0f;
	}
	for (int i = 0; i < Punts2D->size(); i++)
	{
		punts2DMatrix(0, i) = (*Punts2D)[i].x;
		punts2DMatrix(1, i) = (*Punts2D)[i].y;
	}
	Matriu2QuatandPos(tIn, &quat, &pos);

	//canvi de posicio baricentre
	bool canviPos = true;
	cv::Point2f meanP2D, meanP3D;
	Eigen::RowVectorXf oneVector(1,Punts3D->size());
	oneVector.setOnes();
	Eigen::VectorXf auxVector;

	while (iterat < iterICP)
	{
		QuatandPos2Matriu(&quat, &pos, &Trans);
		diff = (Trans - TransAns).cwiseAbs().maxCoeff();
		if (diff < tol)
			break;
		iterat++;

		//Busca punts 3D projectats contorn
		punts3DTrans = Trans * punts3DHom;
		punts3DProj.row(0) = punts3DTrans.row(0).cwiseQuotient(punts3DTrans.row(2));
		punts3DProj.row(1) = punts3DTrans.row(1).cwiseQuotient(punts3DTrans.row(2));

		if(canviPos)
		{
			canviPos = false;
			meanP2D = getMeanPunts2D(&punts2DMatrix);
			meanP3D = getMeanPunts2D(&punts3DProj);
			auxVector = oneVector.cwiseQuotient(punts3DTrans.row(2));
			float mean3F = auxVector.mean();
			Trans(0, 3) += (meanP2D.x - meanP3D.x) / mean3F;
			Trans(1, 3) += (meanP2D.y - meanP3D.y) / mean3F;
			Matriu2QuatandPos(&Trans, &quat, &pos);
			punts3DTrans = Trans * punts3DHom;
			punts3DProj.row(0) = punts3DTrans.row(0).cwiseQuotient(punts3DTrans.row(2));
			punts3DProj.row(1) = punts3DTrans.row(1).cwiseQuotient(punts3DTrans.row(2));
		}


		PuntsContornProjectats(&punts3DProj, Punts2D->size(), &punts3DCont, &indPunts3DCont);


		//Punts de maxima curvatura
		PuntsMaxCurvatura(punts3DCont, min(nCurv, (int)punts3DCont.size()), &punts3DCurv, &indPunts3DCurv, &nPuntsImp, &indPunts3DCurvOrd, &indPunts3DCurvImp, &permPunts3D);
		PuntsMaxCurvatura(*Punts2D, min(nCurv, (int)Punts2D->size()), &punts2DCurv, &indPunts2DCurv, &nPuntsImp, &indPunts2DCurvOrd, &indPunts2DCurvImp, &permPunts2D);
		//nPuntsImp = min(nPuntsImp, (int)Punts2D->size() / 5);

		/*	//BORRAR QUAN PUGUI
		CStdioFile file;
		CString filestr;
		CString filename = "PuntsDist.txt";
		file.Open(filename, CFile::modeCreate | CFile::modeWrite);
		file.WriteString(filestr);
		for (int i = 0; i<punts3DCont.size(); i++)
		{
			filestr.Format("%f %f\n", punts3DCont[i].x, punts3DCont[i].y);
			file.WriteString(filestr);
		}
		file.Close();
		//FINS AQU�*/


		TransAns = Trans;

		correspondences(Punts2D, &punts3DCont, &corresp, &indPunts2DCurvOrd, &indPunts3DCurvOrd);

		//Preparaci� variables per a la optimitzaci�
		std::vector<cv::Point2f> Punts2DOpt(indPunts2DCurvOrd.size());
		std::vector<cv::Point2f> Normals2DOpt(indPunts2DCurvOrd.size());
		std::vector<cv::Point3f> Punts3DOpt(indPunts2DCurvOrd.size());

		for (int i = 0; i < indPunts2DCurvImp.size(); i++)
		{
			Punts2DOpt[i] = (*Punts2D)[indPunts2DCurvImp[i]];
			Normals2DOpt[i] = Normals2D[indPunts2DCurvImp[i]];
			Punts3DOpt[permPunts2D[i]] = (*Punts3D)[indPunts3DCont[corresp[i]]];
		}
		x(0) = quat(0);
		x(1) = quat(1);
		x(2) = quat(2);
		x(3) = quat(3);
		x(4) = pos(0);
		x(5) = pos(1);
		x(6) = pos(2);

		//PAS D'OPTIMITZACI�

		ICP_optimization_functor_point_to_plane functor((int)Punts2DOpt.size(), &Punts2DOpt, &Punts3DOpt, &Normals2DOpt, nPuntsImp);
		Eigen::NumericalDiff<ICP_optimization_functor_point_to_plane> numDiff(functor);

		Eigen::LevenbergMarquardt<Eigen::NumericalDiff<ICP_optimization_functor_point_to_plane>, float> lm(numDiff);
		//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<ICP_optimization_functor_point_to_plane>, float>::JacobianType::RealScalar oing = lm.lm_param();
		//lm.resetParameters();
		//lm.parameters.ftol=lm.parameters.ftol*10.0;
		//lm.parameters.xtol=lm.parameters.xtol*10.0;
		//lm.parameters.maxfev=10;
		//lm.parameters.epsfcn=0.1;
		//lm.resetParameters();
		
		//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<lmder_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();

		//TO DO: BORRAR
		//float merdaResultat = evaluarICP((int)Punts2DOpt.size(), &Punts2DOpt, &Punts3DOpt, &Normals2DOpt, nPuntsImp, x);
		//FINS AQU�

		int info;
		CString provaString;
		TRACE("\n inici calcul Nolineal\n");
		info = lm.minimizeInit(x);
		for (int i = 0; i < MAXITERLM; i++)
		{
			info = lm.minimizeOneStep(x);
			vNormaOptimit = lm.fnorm;
			provaString.Format("FINAL OPTIMITZACI�: ITERICP: %d, Iteraci� LM %d, ERROR OPTIMITZACI�: %f\n", iterat, i, vNormaOptimit);
			TRACE("FINAL OPTIMITZACI�: ITERICP: %d, Iteraci� LM %d, ERROR OPTIMITZACI�: %f\n", iterat, i, vNormaOptimit);
			if (provaString[provaString.GetLength()-2]==')')
				int aaaaaa = 10;
			if (info != LevenbergMarquardtSpace::Running)
				break;
		}
		TRACE("final calcul Nolineal\n");

		//Retransformar soluci�
		quat(0) = x(0);
		quat(1) = x(1);
		quat(2) = x(2);
		quat(3) = x(3);
		pos(0) = x(4);
		pos(1) = x(5);
		pos(2) = x(6);
		quat /= quat.norm();


		//Falta fer tro� de adaptatiu de nCurv
		
		if (vNormaOptimit < MINVALNORMAOPTIMITZACIO)
		{
			nCurv = nCurv - nDelta;
			if (nCurv < Punts2D->size() / 5.0f)
				nCurv = ceil(Punts2D->size() / 5.0f);
		}
		else if (vNormaOptimit > MAXVALNORMAOPTIMITZACIO)
		{
			nCurv = nCurv + nDelta;
			if (nCurv > Punts2D->size())
				nCurv = Punts2D->size();
		}


		//

	}

	//Preparar Sortida
	*nOut = nCurv;
	QuatandPos2Matriu(&quat, &pos, tOut);
	return vNormaOptimit;
}


void CDemoRegistreDlg::printRegister()
{
	//cv::Mat auxiliar = cv::imread("C:\\Users\\UPC-ESAII\\Google Drive\\Files\\ESAII\\ModelsOrgans\\Fotos Ronyo\\ronyo01v2.png", CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat auxiliar = cv::Mat::zeros(pintarSize,CV_8UC3);
	Eigen::Matrix4f Transformation;
	Eigen::Matrix4Xf trans3D;
	Eigen::VectorXf STf1(PuntsPintar3D.cols()), STf2(PuntsPintar3D.cols());
	std::vector<cv::Point2f> PuntsPintar3DProj(PuntsPintar3D.cols());
	std::vector<cv::Point2i> PuntsPintar3DProjInt;
	std::vector<cv::Point2i> Punts2DDist;


	//Transform state to matrix to opperate
	currentStateMutex.lock();
	EulerXYZandPos2Matriu(&(Eigen::Vector3f(currentState[3] * M_PI / 180.0f, currentState[4] * M_PI / 180.0f, currentState[5] * M_PI / 180.0f)), &(Eigen::Vector3f(currentState[0], currentState[1], currentState[2])), &Transformation);
	currentStateMutex.unlock();

	trans3D = Transformation*PuntsPintar3D;

	STf1 = trans3D.row(0).cwiseQuotient(trans3D.row(2));
	STf2 = trans3D.row(1).cwiseQuotient(trans3D.row(2));

	for (int i = 0; i < STf1.rows(); ++i)
	{
		PuntsPintar3DProj[i].x = STf1(i);
		PuntsPintar3DProj[i].y = STf2(i);
	}
	distorcionarPunts(PuntsPintar3DProj, &PuntsPintar3DProjInt);
	
	PuntsPintar2D.clear();
	punts2DMutex.lock();
	std::copy(punts2D.begin(), punts2D.end(), std::back_inserter(PuntsPintar2D));
	punts2DMutex.unlock();


	uint8_t *imData = auxiliar.data;
	int nrows = auxiliar.rows;
	int ncols = auxiliar.cols;
	int deltax, deltay;

	for (int i = 0; i < PuntsPintar3DProjInt.size(); ++i)
	{
		for (int a = -2; a < 3; a++)
		{
			for (int b = -2; b < 3; b++)
			{
				if ((PuntsPintar3DProjInt[i].x + b >= auxiliar.cols - 2) || (PuntsPintar3DProjInt[i].y + a >= auxiliar.rows - 2) || (PuntsPintar3DProjInt[i].x + b < 2) || (PuntsPintar3DProjInt[i].y + a < 2))
				{
					continue;
				}
				imData[((PuntsPintar3DProjInt[i].y + a) * ncols + PuntsPintar3DProjInt[i].x + b) * 3] = 255;
				imData[((PuntsPintar3DProjInt[i].y + a) * ncols + PuntsPintar3DProjInt[i].x + b) * 3 + 1] = 0;
				imData[((PuntsPintar3DProjInt[i].y + a) * ncols + PuntsPintar3DProjInt[i].x + b) * 3 + 2] = 0;
			}
		}
	}

	if (PuntsPintar2D.size() > 0)
	{
		distorcionarPunts(PuntsPintar2D, &Punts2DDist);
		for (int i = 0; i < Punts2DDist.size(); i++)
		{
			for (int a = -2; a < 3; a++)
			{
				for (int b = -2; b < 3; b++)
				{
					if ((Punts2DDist[i].x + b >= auxiliar.cols - 2) || (Punts2DDist[i].y + a >= auxiliar.rows - 2) || (Punts2DDist[i].x + b < 2) || (Punts2DDist[i].y + a < 2))
					{
						continue;
					}
					imData[((Punts2DDist[i].y + a) * ncols + Punts2DDist[i].x + b) * 3] = 0;
					imData[((Punts2DDist[i].y + a) * ncols + Punts2DDist[i].x + b) * 3 + 1] = 0;
					imData[((Punts2DDist[i].y + a) * ncols + Punts2DDist[i].x + b) * 3 + 2] = 255;
				}
			}
		}

	}
	
	imshow("Kalman Registration", auxiliar);
	waitKey(1);
}

void CDemoRegistreDlg::acabarPintar()
{
	KillTimer(0);
	bPintant = false;
	Sleep(100);
	destroyWindow("Kalman Registration");
}


void CDemoRegistreDlg::distorcionarPunts(std::vector < cv::Point2f> Punts, std::vector<cv::Point2i> *PuntsPixels)
{
	*PuntsPixels = std::vector<cv::Point2i>(Punts.size());
	std::vector<cv::Point2f> auxiliar(Punts.size());
	float r2, consAux;
	cv::Point2f dx;
	for (int i = 0; i < Punts.size(); i++)
	{
		r2 = Punts[i].x + Punts[i].x + Punts[i].y*Punts[i].y;
		consAux = 1 + kc[0] * r2 + kc[1] * r2*r2 + kc[4] * r2*r2*r2;
		dx.x = 2.0f*kc[2] * Punts[i].x*Punts[i].y + kc[3] * (r2 + 2 * Punts[i].x*Punts[i].x);
		dx.x = kc[2] * (r2 + 2 * Punts[i].y*Punts[i].y) + 2.0f*kc[3] * Punts[i].x*Punts[i].y;
		auxiliar[i] = consAux * Punts[i] + dx;

		(*PuntsPixels)[i].x = floor(fc[0] * (auxiliar[i].x + alpha_c*auxiliar[i].y) + cc[0]);
		(*PuntsPixels)[i].y = fc[1] * auxiliar[i].y + cc[1];
	}
}


void CDemoRegistreDlg::OnBnClickedButprova()
{
	/*std::vector<cv::String> fn;
	glob("C:\\Users\\UPC-ESAII\\Desktop\\Nueva carpeta",fn,false);
	//std::vector<cv::Mat> images;
	//size_t count = fn.size();
	//for (size_t i = 0; i < count; i++)
	//	images.push_back(imread(fn[i]));
	cv::Mat imageProva2;
	cv::Mat imageProva = cv::imread("C:\\Users\\UPC-ESAII\\Google Drive\\Files\\ESAII\\ModelsOrgans\\Fotos Ronyo\\ronyo01v2.png", CV_LOAD_IMAGE_GRAYSCALE);
	//cv::Mat imageProva = cv::imread("./cor122.png", CV_LOAD_IMAGE_GRAYSCALE);
	/*std::vector<cv::Point2i> auxPointsContorn;
	SegmentacioImatge(&imageProva, &imageProva2);
	trobarPuntsContorn(&imageProva2, &auxPointsContorn, PUNTSCONTORNIMATGE2D);
	std::vector<cv::Point2f> pixelsDes;
	desdistorcionarPunts(&auxPointsContorn, &pixelsDes, auxPointsContorn.size());
	Eigen::Matrix4f tIn, tOut;
	
	CString filename = "ProvaTIn.txt";
	CStdioFile file;
	CString filestr;
	file.Open(filename, CFile::modeRead);
	file.SeekToBegin();
	file.ReadString(filestr);
	sscanf_s(filestr, "%g  %g  %g  %g  %g  %g  %g  %g  %g  %g %g  %g  %g  %g  %g  %g  %g", &tIn(0,0), &tIn(0, 1), &tIn(0, 2), &tIn(0, 3), &tIn(1, 0), &tIn(1, 1), &tIn(1, 2), &tIn(1, 3), &tIn(2, 0), &tIn(2, 1), &tIn(2, 2), &tIn(2, 3), &tIn(3, 0), &tIn(3, 1), &tIn(3, 2), &tIn(3, 3));
	file.Close();


	int nOut;

	float normOpt = ICPCurvatura2D3DIter(&pixelsDes, &punts3D, &tIn, &tOut, pixelsDes.size()-60,4, 10, 0.0001f, &nOut);

	filename = "PuntsDist.txt";
	file.Open(filename, CFile::modeCreate | CFile::modeWrite);
	for (int i = 0; i<auxPointsContorn.size(); i++)
	{
		filestr.Format("%d %d %f %f\n", auxPointsContorn[i].x, auxPointsContorn[i].y, pixelsDes[i].x, pixelsDes[i].y);
		file.WriteString(filestr);
	}
	file.Close();

	filename = "ProvaTOut.txt";
	file.Open(filename, CFile::modeCreate | CFile::modeWrite);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			filestr.Format("%g  ", tOut(i,j));
			file.WriteString(filestr);
		}
	}
	file.Close();

	cv::KalmanFilter KFX(KFSTATEDIM, 1, 0, CV_32F);*/
	//namedWindow("Display window", WINDOW_AUTOSIZE);// Create a window for display.
	//imshow("Display window", imageProva);
	vector<String> filenames;
	String folder = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgesRaw\\";
	glob(folder, filenames); // new function that does the job ;-)
	uint8_t *imData; //= auxiliar.data;
	int nrows; //= auxiliar.rows;
	int ncols;// = auxiliar.cols;
	int deltax, deltay;
	std::vector<cv::Point2i> auxPoints;
	//mean Segmentacio
	numSegm = 0;
	ultimsSegmentats.clear();
	ultimsSegmentats.resize(NUMMEANPUNTSCONTORN);
	for (int i = 0; i < NUMMEANPUNTSCONTORN; i++)
	{
		ultimsSegmentats[i].resize(PUNTSCONTORNIMATGE2D);
	}
	meanSegmentats.clear();
	meanSegmentats.resize(PUNTSCONTORNIMATGE2D);

	for (size_t i = 0; i < filenames.size(); ++i)
	{
		Mat src = imread(filenames[i], CV_LOAD_IMAGE_GRAYSCALE);

		if (!src.data)
			cerr << "Problem loading image!!!" << endl;

		/* do whatever you want with your images here */
	

		CString path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgeSeg\\";
		CString nameImage;
		globalImage = i + 1;
		cv::Mat gradMod, gradAngle;
		SegmentacioImatge(src, src, gradMod, gradAngle);

	
		nameImage.Format("Imatge_%d.png", i + 1);
		nameImage = path + nameImage;
		imwrite(nameImage.GetString(), src);



		trobarPuntsContorn(&(src), &auxPoints, PUNTSCONTORNIMATGE2D);
		std::vector<cv::Point2i> puntsSegmentatsvectProva;
		puntsSegmentatsvectProva.clear();
		std::copy(auxPoints.begin(), auxPoints.end(), std::back_inserter(puntsSegmentatsvectProva));
		cv::cvtColor(src, src, cv::COLOR_GRAY2BGR);


		////PER BORRAR
		uint8_t *imData = src.data;
		int nrows = src.rows;
		int ncols = src.cols;
		int deltax, deltay;

		for (int ii = 0; ii < puntsSegmentatsvectProva.size(); ++ii)
		{
			for (int a = -2; a < 3; a++)
			{
				for (int b = -2; b < 3; b++)
				{
					if ((puntsSegmentatsvectProva[ii].x == 0) || (puntsSegmentatsvectProva[ii].y == 0) || (puntsSegmentatsvectProva[ii].x + b >= src.cols - 2) || (puntsSegmentatsvectProva[ii].y + a >= src.rows - 2) || (puntsSegmentatsvectProva[ii].x + b < 2) || (puntsSegmentatsvectProva[ii].y + a < 2))
					{
						continue;
					}
					if (puntsSegmentatsvectProva[ii].y < nrows / 2.0f)
						deltay = 2;
					else
						deltay = -2;
					if (puntsSegmentatsvectProva[ii].x < ncols / 2.0f)
						deltax = 2;
					else
						deltax = -2;
					imData[((puntsSegmentatsvectProva[ii].y + deltay + a) * src.cols + puntsSegmentatsvectProva[ii].x + deltax + b) * 3] = 0;
					imData[((puntsSegmentatsvectProva[ii].y + deltay + a) * src.cols + puntsSegmentatsvectProva[ii].x + deltax + b) * 3 + 1] = 0;
					imData[((puntsSegmentatsvectProva[ii].y + deltay + a) * src.cols + puntsSegmentatsvectProva[ii].x + deltax + b) * 3 + 2] = 255;
				}
			}
		}

		path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImagesText\\ImatgePoints\\";
		nameImage.Format("Imatge_%d.png", i + 1);
		nameImage = path + nameImage;
		imwrite(nameImage.GetString(), src);

	}
	//mean Segmentaci�
	numSegm = 0;
	for (int i = 0; i<NUMMEANPUNTSCONTORN; i++)
	{
		ultimsSegmentats[i].clear();
	}

}


void CDemoRegistreDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case 0:
		if (bPintant)
			printRegister();
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}

float CDemoRegistreDlg::evaluarICP(int size, std::vector<cv::Point2f> * P2D, std::vector<cv::Point3f> * P3D, std::vector<cv::Point2f> * Normals, int numImp, Eigen::VectorXf x)
{
	int nPuntsImp;
	int nPunts;
	Eigen::Matrix2Xf Punts2D;
	Eigen::Matrix2Xf Normals2D;
	Eigen::Matrix4Xf Punts3D;
	nPunts = size;
	nPuntsImp = numImp;
	Punts2D = Eigen::Matrix2Xf(2, size);
	Normals2D = Eigen::Matrix2Xf(2, size);
	Punts3D = Eigen::Matrix4Xf(4, size);
	for (int i = 0; i < size; i++)
	{
		Punts2D(0, i) = (*P2D)[i].x;
		Punts2D(1, i) = (*P2D)[i].y;
		Normals2D(0, i) = (*Normals)[i].x;
		Normals2D(1, i) = (*Normals)[i].y;
		Punts3D(0, i) = (*P3D)[i].x;
		Punts3D(1, i) = (*P3D)[i].y;
		Punts3D(2, i) = (*P3D)[i].z;
		Punts3D(3, i) = 1.0f;
	}
	Eigen::Matrix3f Ri;
	Eigen::Matrix4f TransM;
	Eigen::Matrix4Xf trans3D;
	Eigen::VectorXf fvec(size + numImp);
	int count = 0;
	QuaternionToMatrix(x(0), x(1), x(2), x(3), &Ri);

	for (int i = 0; i<3; i++)
	{
		for (int j = 0; j < 3; j++)
			TransM(i, j) = Ri(i, j);
		TransM(i, 3) = x(4 + i);
		TransM(3, i) = 0.0f;
	}
	TransM(3, 3) = 1.0f;
	trans3D = TransM*Punts3D;

	Eigen::VectorXf STf1(nPunts), STf2(nPunts);

	STf1 = trans3D.row(0).cwiseQuotient(trans3D.row(2));
	STf2 = trans3D.row(1).cwiseQuotient(trans3D.row(2));

	for (int i = 0; i < nPuntsImp; i++)
	{
		fvec(i) = 2 * ((Punts2D(0, i) - STf1(i))*Normals2D(0, i) + (Punts2D(1, i) - STf2(i))*Normals2D(1, i));
		fvec(i + nPuntsImp) = 2 * ((Punts2D(0, i) - STf1(i))*(Punts2D(0, i) - STf1(i)) + (Punts2D(1, i) - STf2(i))*(Punts2D(1, i) - STf2(i)));
	}
	for (int i = nPuntsImp; i < nPunts; i++)
	{
		fvec(i + nPuntsImp) = ((Punts2D(0, i) - STf1(i))*Normals2D(0, i) + (Punts2D(1, i) - STf2(i))*Normals2D(1, i));
	}
	return fvec.norm();
}



void CDemoRegistreDlg::OnBnClickedProva2()
{
	
	CString filename = "Punts2D20171109.txt";
	CStdioFile file;
	CString filestr;
	cv::Point2f Ptr;
	int inici;
	float xval, yval;
	bool xturn;
	std::vector<std::vector<cv::Point2f>> Punts2D20171109(500);
	file.Open(filename, CFile::modeRead);
	file.SeekToBegin();
	for (int j = 0; j < 500; j++)
	{
		file.ReadString(filestr);
		inici = -1;
		xturn = true;
		for (int i = 0; i < filestr.GetLength(); i++)
		{
			if (filestr[i] == '\t')
			{
				if (xturn)
				{
					xval = stof(filestr.Mid(inici + 1, i - inici).GetString());
					xturn = false;
					inici = i;
				}
				else
				{
					yval = stof(filestr.Mid(inici + 1, i - inici).GetString());
					xturn = true;
					inici = i;
					Ptr.x = xval;
					Ptr.y = yval;
					Punts2D20171109[j].push_back(Ptr);
				}
			}
		}
	}
	file.Close();


	filename = "GroundKalmanFilter6dof.txt";
	float groundValues[500][7];
	file.Open(filename, CFile::modeRead);
	file.SeekToBegin();
	int columnallegida = 0;
	for (int j = 0; j < 500; j++)
	{
		file.ReadString(filestr);
		inici = -1;
		columnallegida = 0;
		for (int i = 0; i < filestr.GetLength(); i++)
		{
			if (filestr[i] == '\t')
			{
				if (columnallegida == 0)
				{
					groundValues[j][columnallegida] = (float)atoi(filestr.Mid(inici + 1, i - inici).GetString());
					inici = i;
					columnallegida++;
				}
				else
				{
					groundValues[j][columnallegida] = stof(filestr.Mid(inici + 1, i - inici).GetString());
					inici = i;
					columnallegida++;
				}
			}
		}
	}
	file.Close();

	//Read Punts 3D
	//CString OrganFilename = "Ronyo3D.txt";
	CString OrganFilename = "Cor3D.txt";
	CStdioFile organfile;
	CString fileorganstr;
	int sizeOrgan;

	organfile.Open(OrganFilename, CFile::modeRead);
	organfile.SeekToBegin();
	organfile.ReadString(fileorganstr);
	sscanf_s(fileorganstr, "%d", &sizeOrgan);
	punts3DMutex.lock();
	punts3D.resize(sizeOrgan);
	for (int i = 0; i < sizeOrgan; i++)
	{
		organfile.ReadString(fileorganstr);
		sscanf_s(fileorganstr, "%g %g %g", &punts3D[i].x, &punts3D[i].y, &punts3D[i].z);
	}
	punts3DMutex.unlock();
	organfile.Close();

	PuntsPintar3D = Eigen::Matrix4Xf(4, sizeOrgan);
	punts3DMutex.lock();
	for (int i = 0; i < punts3D.size(); i++)
	{
		PuntsPintar3D(0, i) = punts3D[i].x;
		PuntsPintar3D(1, i) = punts3D[i].y;
		PuntsPintar3D(2, i) = punts3D[i].z;
		PuntsPintar3D(3, i) = 1.0f;
	}
	punts3DMutex.unlock();

	//Initizlization pos and angles
	iniICPMutex.lock();
	IniICP[0] = -20.0f;
	IniICP[1] = 40.0f;
	IniICP[2] = 270.0f;
	IniICP[3] = 90.0f;
	IniICP[4] = 0.0f;
	IniICP[5] = -90.0f;
	iniICPMutex.unlock();

	currentStateMutex.lock();
	currentState[0] = -20.0f;
	currentState[1] = 40.0f;
	currentState[2] = 270.0f;
	currentState[3] = 90.0f;
	currentState[4] = 0.0f;
	currentState[5] = -90.0f;
	currentStateMutex.unlock();

	indKFMutex.lock();
	for (int i = 0; i < 6; i++)
	{
		indKF[i] = true;
	}
	indKFMutex.unlock();

	//Prova Inici
	finalICP.lock();
	bFinalICP = false;
	finalICP.unlock();
	finalKF.lock();
	bFinalKF = false;
	finalKF.unlock();

	nPuntsIn = 60;// (int)floor(Punts2D20171109[0].size() / 5.0f);
	nDelta = 0;// (int)floor(Punts2D20171109[0].size() / 20.0f);
	
	thICP = AfxBeginThread(ThreadICP, this, THREAD_PRIORITY_HIGHEST, 0, 0, NULL);
	for (int i = 0; i < 6; i++)
	{
		thKalmanFilter[i] = AfxBeginThread(ThreadKalmanFilter, this, THREAD_PRIORITY_HIGHEST, 0, 0, NULL);
		
	}

	bPintant = true;
	thPintar = AfxBeginThread(ThreadPintar, this, THREAD_PRIORITY_HIGHEST, 0, 0, NULL);
	for (int i = 0; i < Punts2D20171109.size(); i++)
	{
		punts2DMutex.lock();
		bPunts2D = true;
		punts2D.clear();
		std::copy(Punts2D20171109[i].begin(), Punts2D20171109[i].end(), std::back_inserter(punts2D));
		punts2DMutex.unlock();
		/*measureMutex.lock(); 
		for (int j = 0; j < 6; j++)
		{
			measurement[j] = groundValues[i][j + 1];
			dadaNova[j] = 1;
		}
		measureMutex.unlock();*/
		Sleep(30);
	}

	finalICP.lock();
	bFinalICP = true;
	finalICP.unlock();
	finalKF.lock();
	bFinalKF = true;
	finalKF.unlock();

	Sleep(1000);

	DWORD exit_code = NULL;
	for (int i = 0; i < 6; i++)
	{
		if (thKalmanFilter[i] != NULL)
		{
			GetExitCodeThread(thKalmanFilter[i]->m_hThread, &exit_code);
			if (exit_code == STILL_ACTIVE)
			{
				::TerminateThread(thKalmanFilter[i]->m_hThread, 0);
				CloseHandle(thKalmanFilter[i]->m_hThread);
			}
			thKalmanFilter[i]->m_hThread = NULL;
			
		}
	}
	for(int i = 0; i<6 ; i++)
		thKalmanFilter[i] = NULL;
	if (thICP != NULL)
	{
		/*GetExitCodeThread(thICP->m_hThread, &exit_code);
		if (exit_code == STILL_ACTIVE)
		{
			::TerminateThread(thICP->m_hThread, 0);
			CloseHandle(thICP->m_hThread);
		}
		thICP->m_hThread = NULL;
		thICP = NULL;*/

		//INI DEBUG: KILL THREAD WHAITING FOR DEAD
		if (WaitForSingleObject(thICP, INFINITE) == WAIT_OBJECT_0)
			;//all right
		else
			
		if (GetLastError() == WAIT_FAILED)
		{
			AfxMessageBox("ThICP alive");//all right;//report error
		}
		//END DEBUG: KILL THREAD WHAITING FOR DEAD
	}


	Sleep(1000);
	bPintant = false;
	Sleep(100);
	
	if (thPintar != NULL)
	{
		GetExitCodeThread(thPintar->m_hThread, &exit_code);
		if (exit_code == STILL_ACTIVE)
		{
			::TerminateThread(thPintar->m_hThread, 0);
			CloseHandle(thPintar->m_hThread);
		}
		
		thPintar->m_hThread = NULL;
		thPintar = NULL;
		
	}
}


void CDemoRegistreDlg::OnBnClickedCancel()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	CDialogEx::OnCancel();
}

inline cv::Mat CDemoRegistreDlg::imCompositeFilter(cv::Mat I)
{
	cv::Mat Aux1, Aux2;
	cv::morphologyEx(I, Aux1, MORPH_OPEN, crossKernel);
	cv::morphologyEx(Aux1, Aux1, MORPH_CLOSE, squareKernel);
	cv::morphologyEx(I, Aux2, MORPH_CLOSE, crossKernel);
	cv::morphologyEx(Aux2, Aux2, MORPH_OPEN, squareKernel);
	return (Aux1 / 2 + Aux2 / 2);
}

inline cv::Mat CDemoRegistreDlg::imMorphGrad(cv::Mat I, cv::Mat kernel)
{
	cv::Mat Aux, Aux1, Aux2;
	I.copyTo(Aux);
	cv::morphologyEx(Aux, Aux1, MORPH_CLOSE, kernel);
	cv::dilate(Aux1, Aux1, kernel);
	cv::morphologyEx(Aux, Aux2, MORPH_OPEN, kernel);
	cv::erode(Aux2, Aux2, kernel);
	return Aux1 - Aux2;
}

inline cv::Mat CDemoRegistreDlg::imMorphDilate(cv::Mat I, cv::Mat kernel)
{
	cv::Mat Aux, Aux1, Aux2;
	I.copyTo(Aux);
	cv::morphologyEx(Aux, Aux1, MORPH_CLOSE, kernel);
	cv::dilate(Aux1, Aux2, kernel);
	return Aux2 - Aux1;
}

inline cv::Mat CDemoRegistreDlg::imMorphErode(cv::Mat I, cv::Mat kernel)
{
	cv::Mat Aux, Aux1, Aux2;
	I.copyTo(Aux);
	cv::morphologyEx(Aux, Aux1, MORPH_CLOSE, kernel);
	cv::morphologyEx(Aux, Aux2, MORPH_OPEN, kernel);
	cv::erode(Aux2, Aux2, kernel);
	return Aux1 - Aux2;
}


void CDemoRegistreDlg::OnBnClickedProva3()
{
	cv::Mat auxiliar, auxiliar2;
	cv::Mat Gb1, Gb2, Gb3, Gb4;
	cv::Mat imageProva = cv::imread("C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistre\\DemoRegistre\\ImageRaw_5.png", CV_LOAD_IMAGE_GRAYSCALE);
	std::vector<cv::Point2i> auxPointsContorn = std::vector<cv::Point2i>();
	SegmentacioImatge(&imageProva, &(auxiliar));
	trobarPuntsContorn(&(auxiliar), &auxPointsContorn, PUNTSCONTORNIMATGE2D);
	std::vector<cv::Point2i> puntsSegmentatsvectProva;
	puntsSegmentatsvectProva.clear();
	std::copy(auxPointsContorn.begin(), auxPointsContorn.end(), std::back_inserter(puntsSegmentatsvectProva));
	cv::cvtColor(imageProva, auxiliar, cv::COLOR_GRAY2BGR);


	////PER BORRAR
	uint8_t *imData = auxiliar.data;
	int nrows = auxiliar.rows;
	int ncols = auxiliar.cols;
	int deltax, deltay;

	for (int i = 0; i < puntsSegmentatsvectProva.size(); ++i)
	{
		for (int a = -2; a < 3; a++)
		{
			for (int b = -2; b < 3; b++)
			{
				if ((puntsSegmentatsvectProva[i].x == 0) || (puntsSegmentatsvectProva[i].y == 0) || (puntsSegmentatsvectProva[i].x + b >= auxiliar.cols - 2) || (puntsSegmentatsvectProva[i].y + a >= auxiliar.rows - 2) || (puntsSegmentatsvectProva[i].x + b < 2) || (puntsSegmentatsvectProva[i].y + a < 2))
				{
					continue;
				}
				if (puntsSegmentatsvectProva[i].y < nrows / 2.0f)
					deltay = 2;
				else
					deltay = -2;
				if (puntsSegmentatsvectProva[i].x < ncols / 2.0f)
					deltax = 2;
				else
					deltax = -2;
				imData[((puntsSegmentatsvectProva[i].y + deltay + a) * auxiliar.cols + puntsSegmentatsvectProva[i].x + deltax + b) * 3] = 0;
				imData[((puntsSegmentatsvectProva[i].y + deltay + a) * auxiliar.cols + puntsSegmentatsvectProva[i].x + deltax + b) * 3 + 1] = 0;
				imData[((puntsSegmentatsvectProva[i].y + deltay + a) * auxiliar.cols + puntsSegmentatsvectProva[i].x + deltax + b) * 3 + 2] = 255;
			}
		}
	}

	namedWindow("Display window", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window", auxiliar);
}

void CDemoRegistreDlg::StartTracking()
{
	indKFMutex.lock();
	for (int i = 0; i < 6; i++)
	{
		indKF[i] = true;
	}
	indKFMutex.unlock();

	//Prova Inici
	finalICP.lock();
	bFinalICP = false;
	finalICP.unlock();
	finalKF.lock();
	bFinalKF = false;
	finalKF.unlock();

	nPuntsIn = 60;// (int)floor(Punts2D20171109[0].size() / 5.0f);
	nDelta = 0;// (int)floor(Punts2D20171109[0].size() / 20.0f);

	thICP = AfxBeginThread(ThreadICP, this, THREAD_PRIORITY_HIGHEST, 0, 0, NULL);
	for (int i = 0; i < 6; i++)
	{
		thKalmanFilter[i] = AfxBeginThread(ThreadKalmanFilter, this, THREAD_PRIORITY_BELOW_NORMAL, 0, 0, NULL);

	}

	bPintant = true;
	thPintar = AfxBeginThread(ThreadPintar, this, THREAD_PRIORITY_BELOW_NORMAL, 0, 0, NULL);
}

void CDemoRegistreDlg::StopTracking()
{
	finalICP.lock();
	bFinalICP = true;
	finalICP.unlock();
	finalKF.lock();
	bFinalKF = true;
	finalKF.unlock();

	Sleep(200);

	DWORD exit_code = NULL;
	for (int i = 0; i < 6; i++)
	{
		if (thKalmanFilter[i] != NULL)
		{
			GetExitCodeThread(thKalmanFilter[i]->m_hThread, &exit_code);
			if (exit_code == STILL_ACTIVE)
			{
				::TerminateThread(thKalmanFilter[i]->m_hThread, 0);
				CloseHandle(thKalmanFilter[i]->m_hThread);
			}
			thKalmanFilter[i]->m_hThread = NULL;

		}
	}
	for (int i = 0; i<6; i++)
		thKalmanFilter[i] = NULL;
	if (thICP != NULL)
	{
		/*GetExitCodeThread(thICP->m_hThread, &exit_code);
		if (exit_code == STILL_ACTIVE)
		{
		::TerminateThread(thICP->m_hThread, 0);
		CloseHandle(thICP->m_hThread);
		}
		thICP->m_hThread = NULL;
		thICP = NULL;*/

		//INI DEBUG: KILL THREAD WHAITING FOR DEAD
		if (WaitForSingleObject(thICP, INFINITE) == WAIT_OBJECT_0)
			;//all right
		else

			if (GetLastError() == WAIT_FAILED)
			{
				AfxMessageBox("ThICP alive");//all right;//report error
			}
		//END DEBUG: KILL THREAD WHAITING FOR DEAD
	}


	Sleep(200);
	bPintant = false;
	Sleep(100);

	if (thPintar != NULL)
	{
		GetExitCodeThread(thPintar->m_hThread, &exit_code);
		if (exit_code == STILL_ACTIVE)
		{
			::TerminateThread(thPintar->m_hThread, 0);
			CloseHandle(thPintar->m_hThread);
		}

		thPintar->m_hThread = NULL;
		thPintar = NULL;

	}
}

void CDemoRegistreDlg::OnBnClickedButtracking()
{
	if (btracking)
	{
		btracking = false;
		StopTracking();
		m_butTracking.SetWindowTextA(_T("Start Tracking"));
	}
	else
	{
		btracking = true;
		StartTracking();
		m_butTracking.SetWindowTextA(_T("Stop Tracking"));
	}
}

void CDemoRegistreDlg::HandleClientStandAlone()   /* TCP client handling function */
{

	double sendData[2];
	int sendDataSize = sizeof(double)*(2);
	memset(sendData, 0, sizeof(double)*(2));
	CString dataEnviar;
	double mitjaX, mitjaY, mitjaXant = 0.0 , mitjaYant = 0.0;

	while (clientConnectat) {
		Sleep(10);
		centreMutex.lock();
		mitjaX = 0.0f;
		mitjaY = 0.0f;
		for (int i = 0; i < 10; i++)
		{
			mitjaX += (double)valMigx[i];
			mitjaY += (double)valMigy[i];
		}
		mitjaX /= 10.0;
		mitjaY /= 10.0;
		sendData[0] = mitjaX - mitjaXant;
		sendData[1] = cos(22.0*M_PI/180.0)*(mitjaY - mitjaYant);
		mitjaXant = mitjaX;
		mitjaYant = mitjaY;
		centreMutex.unlock();
		dataEnviar.Format("%lf %lf", sendData[0], sendData[1]);
		send(clntdes, dataEnviar.GetBuffer(), dataEnviar.GetLength(), 0);

	}
}


void CDemoRegistreDlg::OnBnClickedButprova4()
{
	//vector<String> filenames;
	//CString path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistreElipse\\DemoRegistre\\ImagesText\\ImatgesDemoDiaAnterior\\";
	//CString nameImage;
	//int nSamples = 500;
	//cv::Mat imageProva = cv::Mat(ueCamera->GetImageSize(), CV_8UC1);
	//std::vector<cv::Mat> imageProva2(nSamples);
	//for (int i = 0; i < nSamples; i++)
	//{
	//	imageProva2[i] = cv::Mat(ueCamera->GetImageSize(), CV_8UC1);
	//}
	//InitializeVideoRecording();
	//Sleep(100);

	//for (size_t i = 0; i < nSamples; ++i)
	//{

	//	ueCamera->GetImage(imageProva.data);
	//	Sleep(50);
	//	imageProva2[i] = imageProva.clone();
	//	Sleep(50);

	//}

	//for (int i = 0; i < nSamples; i++)
	//{
	//	if (i < 10)
	//		nameImage.Format("Imatge_00%d.png", i + 1);
	//	else if (i < 100)
	//		nameImage.Format("Imatge_0%d.png", i + 1);
	//	else
	//		nameImage.Format("Imatge_%d.png", i + 1);
	//	nameImage = path + nameImage;
	//	imwrite(nameImage.GetString(), imageProva2[i]);
	//}

	//FinalizeVideoRecording();
	//Sleep(100);

	//vector<String> filenames;
	//vector<String> filenames2;
	//CString nameImage;
	//CString path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistreElipse\\DemoRegistre\\ImagesText\\ImatgesRaw\\";
	//glob(path.GetString(), filenames);
	//CString path1 = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistreElipse\\DemoRegistre\\ImagesText\\ImatgeSegFinal\\";
	//CString path2 = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistreElipse\\DemoRegistre\\ImagesText\\ImatgeNMS\\";
	//CString path3 = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistreElipse\\DemoRegistre\\ImagesText\\ImatgesRaw2\\";
	//std::vector<cv::Point2i> pointsSeg;




	////mean Segmentacio
	//numSegm = 0;
	//ultimsSegmentats.clear();
	//ultimsSegmentats.resize(NUMMEANPUNTSCONTORN);
	//for (int i = 0; i < NUMMEANPUNTSCONTORN; i++)
	//{
	//	ultimsSegmentats[i].resize(PUNTSCONTORNIMATGE2D);
	//}
	//meanSegmentats.clear();
	//meanSegmentats.resize(PUNTSCONTORNIMATGE2D);


	//for (size_t i = 0; i < filenames.size(); ++i)
	//{
	//	cv::Mat src = imread(filenames[i], CV_LOAD_IMAGE_GRAYSCALE);
	//	cv::Mat src2;
	//	//cv::Mat gradMod, gradAngle;

	//	cv::medianBlur(src, src, 5); //Median Filter of window 5x5
	//											//Imadjust(&auxiliar2, &auxiliar, 25, 140, 0, 255); //Adjust a part of the histogram
	//	cv::bilateralFilter(src, src2, 5, 150, 150);
	//	src = src2.clone();

	//	if (i < 10)
	//		nameImage.Format("Imatge_00%d.png", i + 1);
	//	else if (i < 100)
	//		nameImage.Format("Imatge_0%d.png", i + 1);
	//	else
	//		nameImage.Format("Imatge_%d.png", i + 1);
	//	nameImage = path3 + nameImage;
	//	imwrite(nameImage.GetString(), src);



	//	src = imCompositeFilter(src);
	//	cannyPF(src, 5, 110.0, src2);// , gradMod, gradAngle);
	//	/*nameImage.Format("Imatge_%dA.png", i + 1);
	//	nameImage = path2 + nameImage;
	//	imwrite(nameImage.GetString(), gradAngle);
	//	nameImage.Format("Imatge_%dM.png", i + 1);
	//	nameImage = path2 + nameImage;
	//	imwrite(nameImage.GetString(), gradMod);*/

	//	cv::morphologyEx(src2, src2, MORPH_CLOSE, squareKernel, cv::Point(-1, -1), 2);
	//	cv::dilate(src2, src2, segdilate);
	//	cv::erode(src2, src2, segerode);
	//	cv::filterSpeckles(src2, 0, 100, 0);
	//	//nonMaximumSuppresssion(src, gradMod, gradAngle, src);
	//	//getSkeleton(src, src);
	//	ZhangSuen(src2, src2);
	//	nameImage.Format("Imatge_%d.png", i + 1);
	//	nameImage = path2 + nameImage;
	//	imwrite(nameImage.GetString(), src2);
	//	TRACE("Imatge numero %d", i);


		//cv::Mat auxiliar;
	//	cv::Mat centroids, stats;
	//	//Etiquetem els conjunts de pixels
	//	int nLabels = cv::connectedComponentsWithStats(src2, auxiliar, stats, centroids);

	//	//Busquem el de m�nima d�stacia al centre
	//	double centreImgh = auxiliar.rows / 2.0;
	//	double centreImgw = auxiliar.cols / 2.0;
	//	/*double distMin = winSegSize.height + winSegSize.width + 1000.0;
	//	double distAux;
	//	int indexMin = 0;
	//	for (int i = 1; i < nLabels; ++i)
	//	{
	//	distAux = sqrt(pow(centreImgh - centroids.at<double>(i, 1), 2) + pow(centreImgw - centroids.at<double>(i, 0), 2));
	//	if (distAux < distMin)
	//	{
	//	distMin = distAux;
	//	indexMin = i;
	//	}
	//	}
	//	compare(auxiliar, indexMin, auxiliar2, CMP_EQ);
	//	(*sortida) = (auxiliar2).clone();*/
	//	cv::Mat auxiliar3 = src2.clone();
	//	double distMin = max(centreImgh, centreImgw) / 3.0;
	//	double distAux;
	//	auxiliar3.setTo(Scalar::all(0));
	//	for (int ii = 1; ii < nLabels; ++ii)
	//	{
	//		distAux = sqrt(pow(centreImgh - centroids.at<double>(ii, 1), 2) + pow(centreImgw - centroids.at<double>(ii, 0), 2));
	//		if (distAux < distMin)
	//		{
	//			compare(auxiliar, ii, src2, CMP_EQ);
	//			auxiliar3 += src2;
	//		}
	//	}
		/*nameImage.Format("Imatge_%d.png", i + 1);
		nameImage = path3 + nameImage;
		imwrite(nameImage.GetString(), auxiliar3);
		TRACE("Imatge numero %d", i);*/

		//mean Segmentacio
		//numSegm = 1;
		//ultimsSegmentats.clear();
		//ultimsSegmentats.resize(NUMMEANPUNTSCONTORN);
		//for (int i = 0; i < NUMMEANPUNTSCONTORN; i++)
		//{
		//	ultimsSegmentats[i].resize(PUNTSCONTORNIMATGE2D);
		//}
		//meanSegmentats.clear();
		//meanSegmentats.resize(PUNTSCONTORNIMATGE2D);
		//meanSegmentats[0].x = 328;
		//meanSegmentats[0].y = 272;

		//nameImage.Format("Imatge_2.png");
		//nameImage = path2 + nameImage;
		//cv::Mat src = imread(nameImage.GetString(), CV_LOAD_IMAGE_GRAYSCALE);
		//trobarPuntsElipse(src, pointsSeg, PUNTSCONTORNIMATGE2D);
		////trobarPuntsContorn(&auxiliar3, &pointsSeg, 100);

		//std::vector<cv::Point2f> puntsSeg(pointsSeg.size());
		//for (int ii = 0; ii < puntsSeg.size(); ii++)
		//{
		//	puntsSeg[ii].x = (float)pointsSeg[ii].x;
		//	puntsSeg[ii].y = (float)pointsSeg[ii].y;
		//}

		//float x0, y0, a_2, b_2, angE;
		//int boElipse = ParametresEllipseProbabilitat(puntsSeg, x0, y0, a_2, b_2, angE);

		//////PER BORRAR
		//cv::cvtColor(src, auxiliar, cv::COLOR_GRAY2BGR);
		//uint8_t *imData = auxiliar.data;
		//int nrows = auxiliar.rows;
		//int ncols = auxiliar.cols;
		//int deltax, deltay;

		//for (int ii = 0; ii < pointsSeg.size(); ++ii)
		//{
		//	for (int a = -2; a < 3; a++)
		//	{
		//		for (int b = -2; b < 3; b++)
		//		{
		//			if ((pointsSeg[ii].x == 0) || (pointsSeg[ii].y == 0) || (pointsSeg[ii].x + b >= auxiliar.cols - 2) || (pointsSeg[ii].y + a >= auxiliar.rows - 2) || (pointsSeg[ii].x + b < 2) || (pointsSeg[ii].y + a < 2))
		//			{
		//				continue;
		//			}
		//			imData[((pointsSeg[ii].y + a) * auxiliar.cols + pointsSeg[ii].x + b) * 3] = 0;
		//			imData[((pointsSeg[ii].y + a) * auxiliar.cols + pointsSeg[ii].x + b) * 3 + 1] = 0;
		//			imData[((pointsSeg[ii].y + a) * auxiliar.cols + pointsSeg[ii].x + b) * 3 + 2] = 255;
		//		}
		//	}
		//}
		////

//		nameImage.Format("Imatge_%d.png", i + 1);
	//	nameImage = path1 + nameImage;
//		imwrite(nameImage.GetString(), auxiliar);
//		TRACE("Imatge numero %d", i);
//	}
	vector<String> filenames;
	vector<String> filenames2;
	CString nameImage;
	CString path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistreElipse\\DemoRegistre\\ImagesText\\ImatgesDemoDiaAnterior\\";
	glob(path.GetString(), filenames);
	CString path1 = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoRegistreElipse\\DemoRegistre\\ImagesText\\ImatgesSegDemoDiaAnterior\\";
	int x, y;
	int tipusB;
	cv::Mat src;

	for (size_t i = 0; i < filenames.size(); ++i)
	{
		src = imread(filenames[i], CV_LOAD_IMAGE_GRAYSCALE);
		tipusB = BuscarEllipse(src, x, y);
/*		if (i < 10)
		{
			nameImage.Format("Imatge_solT_00%d.png", i+1);
		}
		else if (i < 100)
		{
			nameImage.Format("Imatge_solT_0%d.png", i+1);
		}
		else
		{
			nameImage.Format("Imatge_solT_%d.png", i+1);
		}
		nameImage = path1 + nameImage;


		cv::Mat auxiliar;
		cv::cvtColor(src, auxiliar, cv::COLOR_GRAY2BGR);

		////PER BORRAR
		uint8_t *imData = auxiliar.data;
		int nrows = auxiliar.rows;
		int ncols = auxiliar.cols;
		int deltax, deltay;
		if (tipusB < 0)
		{
			x = src.cols / 2;
			y = src.rows / 2;
		}
		for (int a = -2; a < 3; a++)
		{
			for (int b = -2; b < 3; b++)
			{
				if (tipusB < 0)
				{
					imData[((y + a) * auxiliar.cols + x + b) * 3] = 0;
					imData[((y + a) * auxiliar.cols + x + b) * 3 + 1] = 0;
					imData[((y + a) * auxiliar.cols + x + b) * 3 + 2] = 255;
				}
				else if (tipusB == 0)
				{
					imData[((y + a) * auxiliar.cols + x + b) * 3] = 255;
					imData[((y + a) * auxiliar.cols + x + b) * 3 + 1] = 0;
					imData[((y + a) * auxiliar.cols + x + b) * 3 + 2] = 0;
				}
				else
				{
					imData[((y + a) * auxiliar.cols + x + b) * 3] = 0;
					imData[((y + a) * auxiliar.cols + x + b) * 3 + 1] = 255;
					imData[((y + a) * auxiliar.cols + x + b) * 3 + 2] = 0;
				}
			}
		}


		imwrite(nameImage.GetString(), auxiliar);*/
	}

}
