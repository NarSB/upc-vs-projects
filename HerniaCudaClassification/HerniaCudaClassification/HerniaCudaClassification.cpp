﻿// HerniaCudaClassification.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <cuda_runtime.h>
#include <cublas.h>
#include <cublas_api.h>
#include <cublas_v2.h>
#include <vector>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <string>

extern cudaError_t setup_gpu(int gpu_id, cublasHandle_t &handle);
extern void close_gpu(cublasHandle_t &handle);
extern cudaError_t up_to_gpu(double * input, double ** dev_output, int size);
extern cudaError_t down_from_gpu(double * dev_input, double * output, int size);
extern cudaError_t save_memory_gpu(double ** dev_memory, int size);
extern cudaError_t elm_create_values(double * input, double ** output, int size);
extern cudaError_t elm_training_phase(cublasHandle_t &handle, double * dev_data, double *dev_values, double ** dev_output, double **dev_ones, int size, double kernel_par, double reg_coeff);
extern cudaError_t elm_output(cublasHandle_t &handle, double * dev_data, double *dev_values, double * dev_weights, double * dev_onesD, double ** dev_output, int sizeD, int sizeV, double kernel_par);

int main()
{
	//Files part
	int ntrain, ntest;
	std::string elmtxt;
	std::ifstream training_file("./trainingVideoReal.txt");
	std::ifstream testing_file("./testingVideoReal.txt");

	std::getline(training_file, elmtxt);
	std::istringstream issTrain(elmtxt);
	issTrain >> ntrain;

	std::getline(testing_file, elmtxt);
	std::istringstream issTest(elmtxt);
	issTest >> ntest;

	std::vector<std::vector<double>> trainData, testData;
	std::vector<double> trainValues, testValues;



	for (int i = 0; i < ntrain; i++)
	{
		double a, b, c, d;
		std::getline(training_file, elmtxt);
		std::istringstream issTrainC(elmtxt);
		issTrainC >> a >> b >> c >> d;

		trainValues.push_back(a);
		std::vector<double> aux(0);
		aux.push_back(b);
		aux.push_back(c);
		aux.push_back(d);
		trainData.push_back(aux);
	}

	for (int i = 0; i < ntest; i++)
	{
		double a, b, c, d;
		std::getline(testing_file, elmtxt);
		std::istringstream issTestC(elmtxt);
		issTestC >> a >> b >> c >> d;

		testValues.push_back(a);
		std::vector<double> aux(0);
		aux.push_back(b);
		aux.push_back(c);
		aux.push_back(d);
		testData.push_back(aux);
	}

	int gpu_id = 0;

	double* traindata_f = new double[3*ntrain];
	double* testdata_f = new double[3*ntest];
	double* trainvalue_f = new double[ntrain];
	double* testvalue_f = new double[ntest];
	
	auto tmp = traindata_f;
	auto tmpv = trainvalue_f;
	for (int i = 0; i < ntrain; i++) 
	{
		for (int j = 0; j < 3; j++)
		{
			*tmp++ = trainData[i][j];
		}
		*tmpv++ = trainValues[i];
	}

	tmp = testdata_f;
	tmpv = testvalue_f;
	for (int i = 0; i < ntest; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			*tmp++ = testData[i][j];
		}
		*tmpv++ = testValues[i];
	}

	cublasHandle_t handle;
	double* result = new double[ntrain*ntest];
	setup_gpu(gpu_id, handle);

	double * trainVal = 0;
	elm_create_values(trainvalue_f, &trainVal, ntrain);

	double * testVal = 0;
	elm_create_values(testvalue_f, &testVal, ntest);

	double * dev_weights = 0;
	double * dev_traindata = 0;
	double * dev_testdata = 0;
	double * dev_onesD = 0;

	cudaError_t cudaError = up_to_gpu(traindata_f, &dev_traindata, 3 * ntrain);
	cudaError = elm_training_phase(handle, dev_traindata, trainVal, &dev_weights, &dev_onesD, ntrain, 100.0, 1.0);
	double *weights = new double[3*ntrain];
	down_from_gpu(dev_weights, weights, 3*ntrain);


	double *dev_testclas = 0;
	cudaError = up_to_gpu(testdata_f, &dev_testdata, 3 * ntest);
	cudaError = elm_output(handle, dev_traindata, dev_testdata, dev_weights, dev_onesD, &dev_testclas, ntrain, ntest, 100.0);
	double * testClas = new double[3 * ntest];
	down_from_gpu(dev_testclas, testClas, 3 * ntest);

	int malC = 0;
	for (int i = 0; i < ntest; i++)
	{
		if (testClas[i] > testClas[ntest + i])
		{
			if (testClas[i] > testClas[2 * ntest + i])
			{
				if (abs(testValues[i] - 1.0) > 0.001)
				{
					malC++;
				}
			}
			else
			{
				if (abs(testValues[i] - 3.0) > 0.001)
				{
					malC++;
				}
			}
		}
		else
		{
			if (testClas[ntest + i] > testClas[2 * ntest + i])
			{
				if (abs(testValues[i] - 2.0) > 0.001)
				{
					malC++;
				}
			}
			else
			{
				if (abs(testValues[i] - 3.0) > 0.001)
				{
					malC++;
				}
			}
		}
	}


	std::ofstream weightfile;
	weightfile.open("weightsHerniaClassificationVideoReal.txt", std::ios::out | std::ios::trunc);
	weightfile << ntrain << std::endl;
	for (int i = 0; i < ntrain; i++)
	{
		weightfile << weights[i] << "\t" << weights[ntrain + i] << "\t" << weights[2 * ntrain + i] << std::endl;
	}
	weightfile.close();

	close_gpu(handle);
	if (trainVal) cudaFree(trainVal);
	if (testVal) cudaFree(testVal);
	if (dev_weights) cudaFree(dev_weights);
	if (dev_traindata) cudaFree(dev_traindata); 
	if (dev_testdata) cudaFree(dev_testdata); 
	if (dev_onesD) cudaFree(dev_onesD); 
	if (dev_testclas) cudaFree(dev_testclas);
	delete[] traindata_f;
	delete[] testdata_f;
	delete[] result;
	delete[] trainvalue_f;
	delete[] testvalue_f;
	delete[] testClas;
	delete[] weights;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
