//{{NO_DEPENDENCIES}}
// Archivo de inclusión generado de Microsoft Visual C++.
// Usado por CorrelacioImatges.rc
//
#define IDBTNSegmentation               3
#define IDD_CORRELACIOIMATGES_DIALOG    102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_ImageInicial                1001
#define IDC_ImageFinal                  1002
#define IDC_EDIT1                       1003
#define IDC_BUTCPU                      1004
#define IDC_EDITShift                   1005
#define IDC_BUTMOSAIC                   1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
