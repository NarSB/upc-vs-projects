
// CorrelacioImatgesDlg.h : header file
//

#pragma once
#include "afxwin.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// CCorrelacioImatgesDlg dialog
class CCorrelacioImatgesDlg : public CDialogEx
{
// Construction
public:
	CCorrelacioImatgesDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CORRELACIOIMATGES_DIALOG };
#endif

	CString m_strShift;

	cv::Size winSegSize;
	cv::Mat cvImgTmp;
	CImage* mfcImg;
	BITMAPINFO bitInfo;
	cv::Mat mask, maskTallada;
	cv::Mat segClose;
	double paramCercle[3];
	std::vector<cv::Point2i> puntsRadi;
	std::vector<cv::Point2d> angRadi;

	std::vector<std::vector<double>> secTrans;
	std::vector<cv::Mat> binImages;
	std::vector<cv::Mat> rawImages;

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

														// Generic functor

	template<typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
	struct Functor
	{
		typedef _Scalar Scalar;
		enum {
			InputsAtCompileTime = NX,
			ValuesAtCompileTime = NY
		};
		typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
		typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
		typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

		const int m_inputs, m_values;

		Functor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
		Functor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

		int inputs() const { return m_inputs; }
		int values() const { return m_values; }

		// you should define that in the subclass :
		//  void operator() (const InputType& x, ValueType* v, JacobianType* _j=0) const;
	};

	//ICP optimization functor
	struct optimization_functor : Functor<double>
	{
		optimization_functor(int size, std::vector<int> & rshift, std::vector<int> & cshift, std::vector<cv::Point2i> punts, double cx, double cy) : Functor<double>(4, 2 * size)
		{
			nPunts = size;
			Punts2D = Eigen::Matrix2Xd(2, size);
			Shifts = Eigen::Matrix2Xd(2, size);
			for (int i = 0; i < size; i++)
			{
				Punts2D(0, i) = (double)(punts[i].x - cx);
				Punts2D(1, i) = (double)(punts[i].y - cy);
				Shifts(0, i) = (double)(cshift[i]);
				Shifts(1, i) = (double)(rshift[i]);
			}
		}


		int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const
		{
			Eigen::Matrix2d Ri;
			Eigen::Matrix2Xd lside = Eigen::Matrix2Xd(2, nPunts);

			Ri << cos(x(0)), -sin(x(0)), sin(x(0)), cos(x(0));
			lside = x(1)*Ri*Punts2D;

			for (int i = 0; i < nPunts; i++)
			{
				fvec(2 * i) = lside(0, i) + x(2) - Punts2D(0, i) - Shifts(0, i);
				fvec(2 * i + 1) = lside(1, i) + x(3) - Punts2D(1, i) - Shifts(1, i);
			}
			return 0;
		}

		int nPunts;
		Eigen::Matrix2Xd Punts2D;
		Eigen::Matrix2Xd Shifts;
	};


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	void PosarImatgeInicial(cv::Mat & entrada);
	void PosarImatgeSegmentacio(cv::Mat & entrada);
	double CoefTipus(cv::Mat & imag1, cv::Mat & imag2, cv::Mat & m1, cv::Point2i & minPunt, int & tipus);
	double CoefCorr(cv::Mat & m1, cv::Mat & m2, cv::Mat& sortida, cv::Point2i & maxVal, int & tipus, int ri1, int ci1, int rf1, int cf1, int ri2, int ci2, int rf2, int cf2);
	void Binaritzacio(cv::Mat &entrada, cv::Mat & sortida, cv::Mat & mascara);
	void PuntsCercle(cv::Mat &entrada, cv::Point2d centre, int radi, std::vector<cv::Point2i> &punts, std::vector<cv::Point2d> &angPunts);
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_picImageInicial;
	CStatic m_picSegmentation;
	afx_msg	void OnBnClickedBtnsegmentation();
	CEdit m_txtTemps;
	afx_msg void OnBnClickedButcpu();
	double GetMSE(cv::Mat & im1, cv::Mat & im2);
	double getPSNR(cv::Mat & im1, cv::Mat & im2);
	void ScaleAndShift(cv::Mat & inIm, cv::Mat & outIm, double s, double rshift, double cshift);
	void ScaleRotateShift(cv::Mat & inIm, cv::Mat & outIm, double a, double s, double rshift, double cshift);
	inline cv::Mat getTransMat(double an, double s, double cx, double cy, double ax, double ay);
	inline Eigen::Matrix3d getTransMatE(double an, double s, double cx, double cy, double ax, double ay);
	void differenceImage(cv::Mat & im1, cv::Mat & im2, cv::Mat & dif);
	void differenceImageGray(cv::Mat & im1, cv::Mat & im2, cv::Mat & dif);
	void differenceImageColor(cv::Mat & im1, cv::Mat & im2, cv::Mat & dif);
	void HistogramDiferencia(cv::Mat & im1, cv::Mat & im2, std::vector<int> & hist);
	void differenceImageShift(cv::Mat & im1, cv::Mat & im2, cv::Mat & dif, int rshift, int cshift);
	Scalar getMSSIM(const Mat & i1, const Mat & i2);
	afx_msg void OnBnClickedButmosaic();
	int getTranfCorrelation(cv::Mat & dst2, cv::Mat & imaux1, cv::Mat & imaux2, std::vector<std::vector<cv::Point2i>> & puntsRadi1, std::vector<std::vector<cv::Point2i>> & puntsRadi2, std::vector<std::vector<cv::Point2d>> & angRadi1, std::vector<std::vector<cv::Point2d>> & angRadi2, std::vector<int> & rshifts, std::vector<int> & cshifts, int & rshiftL, int & cshiftL, double & sL, double &angL);
};
