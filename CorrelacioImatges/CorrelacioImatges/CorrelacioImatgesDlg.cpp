
// CorrelacioImatgesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CorrelacioImatges.h"
#include "CorrelacioImatgesDlg.h"
#include "afxdialogex.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCorrelacioImatgesDlg dialog



CCorrelacioImatgesDlg::CCorrelacioImatgesDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CORRELACIOIMATGES_DIALOG, pParent)
	, m_strShift(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCorrelacioImatgesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ImageInicial, m_picImageInicial);
	DDX_Control(pDX, IDC_ImageFinal, m_picSegmentation);
	DDX_Control(pDX, IDC_EDIT1, m_txtTemps);
	DDX_Text(pDX, IDC_EDITShift, m_strShift);
}

BEGIN_MESSAGE_MAP(CCorrelacioImatgesDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDBTNSegmentation, &CCorrelacioImatgesDlg::OnBnClickedBtnsegmentation)
	ON_BN_CLICKED(IDC_BUTCPU, &CCorrelacioImatgesDlg::OnBnClickedButcpu)
	ON_BN_CLICKED(IDC_BUTMOSAIC, &CCorrelacioImatgesDlg::OnBnClickedButmosaic)
END_MESSAGE_MAP()


// CCorrelacioImatgesDlg message handlers

BOOL CCorrelacioImatgesDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	RECT r;
	m_picSegmentation.GetClientRect(&r);
	winSegSize = cv::Size(r.right, r.bottom);
	if (winSegSize.width % 4 != 0)
	{
		winSegSize.width = 4 * (winSegSize.width / 4);
	}
	cvImgTmp = cv::Mat(winSegSize, CV_8UC3);
	bitInfo.bmiHeader.biBitCount = 24;
	bitInfo.bmiHeader.biWidth = winSegSize.width;
	bitInfo.bmiHeader.biHeight = winSegSize.height;
	bitInfo.bmiHeader.biPlanes = 1;
	bitInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfo.bmiHeader.biCompression = BI_RGB;
	bitInfo.bmiHeader.biClrImportant = 0;
	bitInfo.bmiHeader.biClrUsed = 0;
	bitInfo.bmiHeader.biSizeImage = 0;
	bitInfo.bmiHeader.biXPelsPerMeter = 0;
	bitInfo.bmiHeader.biYPelsPerMeter = 0;

	segClose = getStructuringElement(MORPH_ELLIPSE, Size(19, 19), Point(-1, -1));
	mask = cv::imread("./Images/maskVideo01.png", CV_LOAD_IMAGE_GRAYSCALE);
	//mask = cv::imread("./Images/maskSurgery200306.png", CV_LOAD_IMAGE_GRAYSCALE);
	//mask = cv::imread("./Images/mask.png", CV_LOAD_IMAGE_GRAYSCALE);
	cv::threshold(mask, mask, 128, 255, CV_THRESH_BINARY);

	//Cercle Parametres
	//CString filename = "paramCercleSurgery200306.txt";
	CString filename = "paramCercleVideo01.txt";
	CStdioFile file;
	CString filestr;
	file.Open(filename, CFile::modeRead);
	file.SeekToBegin();
	file.ReadString(filestr);
	sscanf_s(filestr, "%lf  %lf  %lf", &paramCercle[0], &paramCercle[1], &paramCercle[2]);
	file.Close();

	secTrans = std::vector<std::vector<double>>(0);
	binImages = std::vector<cv::Mat>(0);
	rawImages = std::vector<cv::Mat>(0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCorrelacioImatgesDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCorrelacioImatgesDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCorrelacioImatgesDlg::PosarImatgeInicial(cv::Mat & entrada)
{
	cv::Mat auxiliar;
	cv::cvtColor(entrada, auxiliar, cv::COLOR_GRAY2BGR);

	//If size does not match
	if (auxiliar.size() != winSegSize)
	{
		cv::resize(auxiliar, cvImgTmp, winSegSize);
	}
	else
	{
		cvImgTmp = auxiliar.clone();
	}


	//Rotate image
	cv::flip(cvImgTmp, cvImgTmp, 0);


	//Create MFC image
	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}

	mfcImg = new CImage();
	mfcImg->Create(winSegSize.width, winSegSize.height, 24);


	//Add header and OpenCV image to mfcImg
	StretchDIBits(mfcImg->GetDC(), 0, 0,
		winSegSize.width, winSegSize.height, 0, 0,
		winSegSize.width, winSegSize.height,
		cvImgTmp.data, &bitInfo, DIB_RGB_COLORS, SRCCOPY
	);

	//Display mfcImg in MFC window
	mfcImg->BitBlt(::GetDC(m_picImageInicial.m_hWnd), 0, 0);
}

void CCorrelacioImatgesDlg::PosarImatgeSegmentacio(cv::Mat & entrada)
{
	cv::Mat auxiliar;
	cv::cvtColor(entrada, auxiliar, cv::COLOR_GRAY2BGR);

	///INI DEBUG: MIRAR SI AGAFAR ELS PUNTS B�
	for (int i = 0; i < puntsRadi.size(); i++)
	{
		for (int ii = -4; ii < 5; ii++)
		{
			for (int jj = -4; jj < 5; jj++)
			{
				auxiliar.ptr(puntsRadi[i].y + jj)[3 * (puntsRadi[i].x + ii)] = 255;
				auxiliar.ptr(puntsRadi[i].y + jj)[3 * (puntsRadi[i].x + ii) + 1] = 0;
				auxiliar.ptr(puntsRadi[i].y + jj)[3 * (puntsRadi[i].x + ii) + 2] = 0;
			}
		}
	}
	///END DEBUG



	//If size does not match
	if (auxiliar.size() != winSegSize)
	{
		cv::resize(auxiliar, cvImgTmp, winSegSize);
	}
	else
	{
		cvImgTmp = auxiliar.clone();
	}


	//Rotate image
	cv::flip(cvImgTmp, cvImgTmp, 0);


	//Create MFC image
	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}

	mfcImg = new CImage();
	mfcImg->Create(winSegSize.width, winSegSize.height, 24);


	//Add header and OpenCV image to mfcImg
	StretchDIBits(mfcImg->GetDC(), 0, 0,
		winSegSize.width, winSegSize.height, 0, 0,
		winSegSize.width, winSegSize.height,
		cvImgTmp.data, &bitInfo, DIB_RGB_COLORS, SRCCOPY
	);

	//Display mfcImg in MFC window
	mfcImg->BitBlt(::GetDC(m_picSegmentation.m_hWnd), 0, 0);
}

void CCorrelacioImatgesDlg::Binaritzacio(cv::Mat &entrada, cv::Mat & sortida, cv::Mat & mascara)
{
	cv::Mat auxiliar,auxiliar2;
	Mat planes[3];
	CString namefile;
	split(entrada, planes);
	planes[1].copyTo(auxiliar, mascara);

	//namefile.Format("./binary1.png");
	//cv::imwrite(namefile.GetString(), entrada);

	cv::medianBlur(auxiliar, auxiliar, 3);

	//namefile.Format("./binary2.png");
	//cv::imwrite(namefile.GetString(), auxiliar);

	// apply the CLAHE algorithm to the L channel
	cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
	clahe->setClipLimit(4);
	cv::Mat dst;
	clahe->apply(auxiliar, auxiliar2);
	//namefile.Format("./binary3.png");
	//cv::imwrite(namefile.GetString(), auxiliar2);


	cv::adaptiveThreshold(auxiliar2, auxiliar, 255, ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 55, 5);
	//cv::adaptiveThreshold(auxiliar2, auxiliar, 255, ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 45, 10);
	
	auxiliar.copyTo(auxiliar2, mascara);
	//namefile.Format("./binary4.png");
	//cv::imwrite(namefile.GetString(), auxiliar2);


	auxiliar2 = 255 - auxiliar2;
	cv::filterSpeckles(auxiliar2, 0, 3000, 0);
	//cv::filterSpeckles(auxiliar2, 0, 1000, 0);

	//cv::Mat bin = 255 - auxiliar2;
	//namefile.Format("./binary5.png");
	//cv::imwrite(namefile.GetString(), bin);

	//cv::Canny(auxiliar2, auxiliar, 30, 120);
	cv::morphologyEx(auxiliar2, auxiliar, MORPH_CLOSE, segClose);

	auxiliar2 = 255 - auxiliar;

	//namefile.Format("./binary6.png");
	//cv::imwrite(namefile.GetString(), auxiliar2);

	auxiliar2.copyTo(sortida, mascara);
}

extern cudaError_t BinaritzacioGPU(cv::Mat& src, cv::Mat& dst, cv::Mat& mask);

void CCorrelacioImatgesDlg::PuntsCercle(cv::Mat &entrada, cv::Point2d centre, int radi, std::vector<cv::Point2i> &punts, std::vector<cv::Point2d> &angPunts)
{
	double delta = 2.0*M_PI / 120.0;
	double angPos = 0.0;
	int val1, val2;
	int rval1, cval1, rval2, cval2, rvalm, cvalm;
	double th1, th2, thm;

	punts = std::vector<cv::Point2i>(0);
	angPunts = std::vector<cv::Point2d>(0);

	while (angPos < 2.0 * M_PI)
	{
		rval1 = (int)floor(centre.y + radi*sin(angPos));
		cval1 = (int)floor(centre.x + radi*cos(angPos));
		val1 = entrada.ptr(rval1)[cval1];
		
		rval2 = (int)floor(centre.y + radi*sin(angPos + delta));
		cval2 = (int)floor(centre.x + radi*cos(angPos + delta));
		val2 = entrada.ptr(rval2)[cval2];

		if (val1 != val2)
		{
			th1 = angPos;
			th2 = angPos + delta;
			while (true)
			{
				thm = (th1 + th2) / 2.0;
				rvalm = (int)floor(centre.y + radi*sin(thm));
				cvalm = (int)floor(centre.x + radi*cos(thm));
				if ((rvalm == rval1) && (cvalm == cval1))
				{
					if (entrada.ptr(rvalm)[cvalm] > 0)
					{
						punts.push_back(cv::Point2i(cvalm, rvalm));
						angPunts.push_back(cv::Point2d(atan2(rvalm - centre.y, cvalm - centre.x),(double)radi));
					}
					else
					{
						punts.push_back(cv::Point2i(cval2, rval2));
						angPunts.push_back(cv::Point2d(atan2(rval2 - centre.y, cval2 - centre.x), (double)radi));
					}
					break;
				}
				else if ((rvalm == rval2) && (cvalm == cval2))
				{
					if (entrada.ptr(rvalm)[cvalm] > 0)
					{
						punts.push_back(cv::Point2i(cvalm, rvalm));
						angPunts.push_back(cv::Point2d(atan2(rvalm - centre.y, cvalm - centre.x), (double)radi));
					}
					else
					{
						punts.push_back(cv::Point2i(cval1, rval1));
						angPunts.push_back(cv::Point2d(atan2(rval1 - centre.y, cval1 - centre.x), (double)radi));
					}
					break;
				}
				if (entrada.ptr(rvalm)[cvalm] == entrada.ptr(rval1)[cval1])
				{
					rval1 = rvalm;
					cval1 = cvalm;
					th1 = thm;
				}
				else
				{
					rval2 = rvalm;
					cval2 = cvalm;
					th2 = thm;
				}
			}
		}
		angPos += delta;
	}
	if ((punts[punts.size() - 1].x == punts[0].x) && (punts[punts.size() - 1].y == punts[0].y))
	{
		punts.erase(punts.end()-1);
		angPunts.erase(angPunts.end()-1);
	}
}

inline double CorrSubVal(int a, int b)
{
	return (2.0 * (double)a/255.0 - 1.0)*(2.0 * (double)b/255.0 - 1.0);
}
double CCorrelacioImatgesDlg::CoefCorr(cv::Mat & m1, cv::Mat & m2, cv::Mat& sortida, cv::Point2i & maxVal, int & tipus, int ri1, int ci1, int h1, int w1, int ri2, int ci2, int h2, int w2)
{
	int csize = w1 - w2 + 1;
	int rsize = h1 - h2 + 1;
	sortida = cv::Mat(csize, rsize, CV_32FC1);
	int sizeW = h2*w2;

	int cind = 0, rind = 0;
	double maxCoef = -2.0*sizeW;

	double resVal = 0.0;

	uint8_t * img1, *img2;
	float * imgsortida;

	for (int ii = 0; ii < rsize; ii++)
	{
		imgsortida = sortida.ptr<float>(ii);
		for (int jj = 0; jj < csize; jj++)
		{
			
			resVal = 0.0;
			for (int i = 0; i < h2; i++)
			{
				img1 = m1.ptr<uint8_t>(ri1 + ii + i);
				img2 = m2.ptr<uint8_t>(ri2 + i);
				for (int j = 0; j < w2; j++)
				{
					resVal += CorrSubVal(img1[ci1 + jj + j], img2[ci2 + j]);//CorrSubVal(m1.ptr(ri1 + ii + i)[ci1 + jj + j], m2.ptr(ri2 + i)[ci2 + j]);
				}
			}
			resVal /= (double)sizeW;
			imgsortida[jj] = resVal;//sortida.ptr(ii)[jj] = resVal;
			if (resVal > maxCoef)
			{
				maxCoef = resVal;
				cind = jj;
				rind = ii;
			}

		}
	}
	maxVal = cv::Point2i(cind, rind);

	///INI DEBUG: FALTA CALCULAR TIPUS PER SI HI HA CORRELACIONS SEMBLANTS EN UNA DIRECCI�

	double stdZ = 0.0;
	double meanZ = 0.0;
	int sizeZ = 10;
	tipus = 0;

	//Direcci� horitzontal
	int cmin = max(0,cind - sizeZ);
	int cmax = min(cind + sizeZ + 1, csize);
	int valaux;

	if (cmax - cmin < 2 * sizeZ + 1)
	{
		if (cmin == 0)
		{
			cmax = min(2 * sizeZ + 1, csize);
		}
		else
		{
			cmin = max(0, cmax - 2 * sizeZ - 1);
		}
	}
	

	for (int i = cmin; i < cmax; i++)
	{
		valaux = sortida.ptr(rind)[i];
		meanZ += valaux;
		stdZ += valaux * valaux;
	}
	meanZ /= (double)(cmax - cmin);
	stdZ /= (double)(cmax - cmin);
	stdZ -= meanZ*meanZ;

	if (stdZ < 0.002)
	{
		tipus = 1;
	}

	//Direcci� vertical
	int rmin = max(0, rind - sizeZ);
	int rmax = min(rind + sizeZ + 1, rsize);

	if (rmax - rmin < 2 * sizeZ + 1)
	{
		if (rmin == 0)
		{
			rmax = min(2 * sizeZ + 1, rsize);
		}
		else
		{
			rmin = max(0, rmax - 2 * sizeZ - 1);
		}
	}

	meanZ = 0.0;
	stdZ = 0.0;

	for (int i = rmin; i < rmax; i++)
	{
		valaux = sortida.ptr(i)[cind];
		meanZ += valaux;
		stdZ += valaux * valaux;
	}
	meanZ /= (double)(rmax - rmin);
	stdZ /= (double)(rmax - rmin);
	stdZ -= meanZ*meanZ;

	if (stdZ < 0.002)
	{
		tipus = 2;
	}

	//Direcci� Diagonal positiva
	meanZ = 0.0;
	stdZ = 0.0;
	int nsize = 0;

	for (int i = -sizeZ; i < sizeZ + 1; i++)
	{
		if ((rind + i < 0) || (rind + i >= rsize) || (cind + i < 0) || (cind + i >= csize))
			continue;
		valaux = sortida.ptr(rind + i)[cind + i];
		meanZ += valaux;
		stdZ += valaux * valaux;
		nsize++;
	}
	meanZ /= (double)(nsize);
	stdZ /= (double)(nsize);
	stdZ -= meanZ*meanZ;

	if (stdZ < 0.002)
	{
		tipus = 3;
	}

	//Direcci� Diagonal positiva
	meanZ = 0.0;
	stdZ = 0.0;
	nsize = 0;

	for (int i = -sizeZ; i < sizeZ + 1; i++)
	{
		if ((rind - i < 0) || (rind - i >= rsize) || (cind + i < 0) || (cind + i >= csize))
			continue;
		valaux = sortida.ptr(rind - i)[cind + i];
		meanZ += valaux;
		stdZ += valaux * valaux;
		nsize++;
	}
	meanZ /= (double)(nsize);
	stdZ /= (double)(nsize);
	stdZ -= meanZ*meanZ;

	if (stdZ < 0.002)
	{
		tipus = 4;
	}

	///END DEBUG
	return maxCoef;
}


double CCorrelacioImatgesDlg::CoefTipus(cv::Mat & imag1, cv::Mat & imag2, cv::Mat & corrImg, cv::Point2i & minPunt, int & tipus)
{
	cv::matchTemplate(imag1, imag2, corrImg, CV_TM_CCORR_NORMED);// CV_TM_SQDIFF_NORMED);

	double minVal; double maxVal; Point minLoc; Point maxLoc;
	Point matchLoc;
	//cv::minMaxLoc(corrImg, &minVal, &maxVal, &minPunt, &maxLoc, Mat());
	cv::minMaxLoc(corrImg, &maxVal, &minVal, &maxLoc, &minPunt, Mat());
	///INI DEBUG: FALTA CALCULAR TIPUS PER SI HI HA CORRELACIONS SEMBLANTS EN UNA DIRECCI�

	double stdZ = 0.0;
	double meanZ = 0.0;
	int sizeZ = 10;
	tipus = 0;

	int csize = corrImg.cols;
	int rsize = corrImg.rows;
	int rind = minPunt.y;
	int cind = minPunt.x;

	//Direcci� horitzontal
	int cmin = max(0, cind - sizeZ);
	int cmax = min(cind + sizeZ + 1, csize);
	double valaux;

	if (cmax - cmin < 2 * sizeZ + 1)
	{
		if (cmin == 0)
		{
			cmax = min(2 * sizeZ + 1, csize);
		}
		else
		{
			cmin = max(0, cmax - 2 * sizeZ - 1);
		}
	}


	for (int i = cmin; i < cmax; i++)
	{
		valaux = corrImg.ptr<float>(rind)[i];// (1.0 - corrImg.ptr<float>(rind)[i]);
		meanZ += valaux;
		stdZ += valaux * valaux;
	}
	meanZ /= (double)(cmax - cmin);
	stdZ /= (double)(cmax - cmin);
	stdZ -= meanZ*meanZ;

	if (stdZ < 0.002)
	{
		tipus = 1;
	}

	//Direcci� vertical
	int rmin = max(0, rind - sizeZ);
	int rmax = min(rind + sizeZ + 1, rsize);

	if (rmax - rmin < 2 * sizeZ + 1)
	{
		if (rmin == 0)
		{
			rmax = min(2 * sizeZ + 1, rsize);
		}
		else
		{
			rmin = max(0, rmax - 2 * sizeZ - 1);
		}
	}

	meanZ = 0.0;
	stdZ = 0.0;

	for (int i = rmin; i < rmax; i++)
	{
		valaux = corrImg.ptr<float>(i)[cind];//(1.0 - corrImg.ptr<float>(i)[cind]);
		meanZ += valaux;
		stdZ += valaux * valaux;
	}
	meanZ /= (double)(rmax - rmin);
	stdZ /= (double)(rmax - rmin);
	stdZ -= meanZ*meanZ;

	if (stdZ < 0.002)
	{
		tipus = 2;
	}

	//Direcci� Diagonal positiva
	meanZ = 0.0;
	stdZ = 0.0;
	int nsize = 0;

	for (int i = -sizeZ; i < sizeZ + 1; i++)
	{
		if ((rind + i < 0) || (rind + i >= rsize) || (cind + i < 0) || (cind + i >= csize))
			continue;
		valaux = corrImg.ptr<float>(rind+i)[cind+i];// (1.0 - corrImg.ptr<float>(rind + i)[cind + i]);
		meanZ += valaux;
		stdZ += valaux * valaux;
		nsize++;
	}
	meanZ /= (double)(nsize);
	stdZ /= (double)(nsize);
	stdZ -= meanZ*meanZ;

	if (stdZ < 0.002)
	{
		tipus = 3;
	}

	//Direcci� Diagonal positiva
	meanZ = 0.0;
	stdZ = 0.0;
	nsize = 0;

	for (int i = -sizeZ; i < sizeZ + 1; i++)
	{
		if ((rind - i < 0) || (rind - i >= rsize) || (cind + i < 0) || (cind + i >= csize))
			continue;
		valaux = corrImg.ptr<float>(rind-i)[cind+i];// (1.0 - corrImg.ptr<float>(rind - i)[cind + i]);
		meanZ += valaux;
		stdZ += valaux * valaux;
		nsize++;
	}
	meanZ /= (double)(nsize);
	stdZ /= (double)(nsize);
	stdZ -= meanZ*meanZ;

	if (stdZ < 0.002)
	{
		tipus = 4;
	}

	///END DEBUG
	return minVal;
}

int CCorrelacioImatgesDlg::getTranfCorrelation(cv::Mat & dst2, cv::Mat & imaux1, cv::Mat & imaux2, std::vector<std::vector<cv::Point2i>> & puntsRadi1, std::vector<std::vector<cv::Point2i>> & puntsRadi2, std::vector<std::vector<cv::Point2d>> & angRadi1, std::vector<std::vector<cv::Point2d>> & angRadi2, std::vector<int> & rshifts, std::vector<int> & cshifts, int & rshiftL, int & cshiftL,double & sL, double &angL)
{
	/////INI DEBUG: Prova de correlacio local

	cv::Mat imagL1, imagL2, corrImgL;

	int fillImgL = 20;
	int rsizeL = 81;
	int csizeL = 81;
	int indL = 6;
	double minVal; double maxVal; Point minLoc; Point maxLoc;
	Point matchLoc;
	int rsizeLoc, csizeLoc;
	double rdownsamplingL, cdownsamplingL;
	cv::Point2i migP;
	double angP;
	bool trobat;
	rshifts.resize(0);
	cshifts.resize(0);
	std::vector<cv::Point2i> migPts(0);
	int stat = 0;

	CString PathC = "G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/";

	for (int k = 0; k < puntsRadi1.size(); k++)
	{
		stat = 0;
		for (int i = 0; i < puntsRadi1[k].size(); i = i + 2)
		{
			if (stat > 0)
				stat++;
			else
			{
				if (abs(angRadi1[k][i].x - angRadi1[k][(i + 1) % puntsRadi1[k].size()].x) > 10.0 / 180.0*M_PI)
				{
					stat++;
				}
			}
			if (stat == 0)
			{
				migP = (puntsRadi1[k][i] + puntsRadi1[k][(i + 1) % puntsRadi1[k].size()]) / 2;
				angP = (angRadi1[k][i].x + angRadi1[k][(i + 1) % puntsRadi1[k].size()].x) / 2.0;
			}
			else
			{
				migP = puntsRadi1[k][i];
				angP = angRadi1[k][i].x;
			}
			trobat = false;

			for (int j = 0; j < puntsRadi2[k].size(); j++)
			{
				if (abs(angP - angRadi2[k][j].x) < 10.0 / 180.0*M_PI)
				{
					trobat = true;
					break;
				}
			}
			if (!trobat)
			{
				if (stat > 0)
					i--;
				if (stat > 1)
					stat = 0;
				continue;
			}

			rsizeLoc = min(dst2.rows / 5, (int)paramCercle[2] - 2 - (int)(abs(paramCercle[2] - migP.y)));
			csizeLoc = min(dst2.cols / 5, (int)paramCercle[2] - 2 - (int)(abs(paramCercle[2] - migP.x)));
			imagL1 = cv::Mat::ones(2 * fillImgL + rsizeL, 2 * fillImgL + csizeL, dst2.type());
			imagL1 *= 255;

			rdownsamplingL = (2.0*rsizeLoc + 1.0) / (double)rsizeL;
			cdownsamplingL = (2.0*csizeLoc + 1.0) / (double)csizeL;


			cv::resize(imaux1(cv::Rect(migP.x - csizeLoc, migP.y - rsizeLoc, 2.0*csizeLoc + 1.0, 2.0*rsizeLoc + 1.0)), imagL1(cv::Rect(fillImgL, fillImgL, csizeL, rsizeL)), cv::Size(csizeL, rsizeL));
			cv::resize(imaux2(cv::Rect(migP.x - csizeLoc, migP.y - rsizeLoc, 2.0*csizeLoc + 1.0, 2.0*rsizeLoc + 1.0)), imagL2, cv::Size(csizeL, rsizeL));

			/*CString imName1;
			imName1.Format("Imatge1_%d.png", i);
			imName1 = PathC + imName1;
			CString imName2;
			imName2.Format("Imatge2_%d.png", i);
			imName2 = PathC + imName2;
			imwrite(imName1.GetString(), imagL1);
			imwrite(imName2.GetString(), imagL2);*/


			cv::matchTemplate(imagL1, imagL2, corrImgL, CV_TM_CCORR_NORMED);// CV_TM_SQDIFF_NORMED);


			//cv::minMaxLoc(corrImg, &minVal, &maxVal, &minPunt, &maxLoc, Mat());
			cv::minMaxLoc(corrImgL(cv::Rect(5, 5, corrImgL.cols - 10, corrImgL.rows - 10)), &minVal, &maxVal, &minLoc, &maxLoc, Mat());

			rshiftL = (-maxLoc.y + fillImgL - 5)*rdownsamplingL;
			rshifts.push_back(rshiftL);
			cshiftL = (-maxLoc.x + fillImgL - 5)*cdownsamplingL;
			cshifts.push_back(cshiftL);
			migPts.push_back(migP);
			if (stat > 0)
				i--;
			if (stat > 1)
			{
				stat = 0;
			}
		}
	}

	double rstd = 0.0;
	double cstd = 0.0;
	double rmean = 0.0;
	double cmean = 0.0;
	for (int i = 0; i < rshifts.size(); i++)
	{
		rstd += (double)rshifts[i] * rshifts[i];
		cstd += (double)cshifts[i] * cshifts[i];
		rmean += (double)rshifts[i];
		cmean += (double)cshifts[i];
	}
	rmean /= (double)rshifts.size();
	cmean /= (double)cshifts.size();
	rstd /= (double)rshifts.size();
	cstd /= (double)cshifts.size();
	rstd -= rmean * rmean;
	cstd -= cmean * cmean;
	rstd = sqrt(rstd);
	cstd = sqrt(cstd);



	int esborrats = 0;
	int rvsize = rshifts.size();
	for (int i = 0; i < rvsize - esborrats; i++)
	{
		if ((abs(rshifts[i] - rmean) > 1.5*rstd) || (abs(cshifts[i] - cmean) > 1.5*cstd))
		{
			rshifts.erase(rshifts.begin() + i);
			cshifts.erase(cshifts.begin() + i);
			migPts.erase(migPts.begin() + i);
			esborrats++;
			i--;
		}
	}
	if (rshifts.size() > 1)
	{

		/*cv::cvtColor(dstImg2, esb, cv::COLOR_GRAY2BGR);
		for (int kk = 0; kk < migPts.size(); kk++)
		{
			for (int iii = -rAlb; iii < rAlb + 1; iii++)
			{
				for (int jjj = -rAlb; jjj < rAlb + 1; jjj++)
				{
					if (iii*iii + jjj*jjj <= rAlb*rAlb)
					{
						esb.ptr(migPts[kk].y + jjj)[3 * (migPts[kk].x + iii)] = 0;
						esb.ptr(migPts[kk].y + jjj)[3 * (migPts[kk].x + iii) + 1] = 0;
						esb.ptr(migPts[kk].y + jjj)[3 * (migPts[kk].x + iii) + 2] = 255;
					}
				}
			}
		}
		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imBinFinalPoints_%d.png", ii + 1);
		cv::imwrite(pathIm.GetString(), esb);*/
		/*Eigen::MatrixX3d A(2 * rshifts.size(), 3);
		Eigen::VectorXd b(2 * rshifts.size());

			for (int i = 0; i < rshifts.size(); i++)
			{
				A(2 * i, 0) = (double)migPts[i].x - paramCercle[2];
				A(2 * i + 1, 0) = (double)migPts[i].y - paramCercle[2];
				A(2 * i, 1) = 1.0;
				A(2 * i, 2) = 0.0;
				A(2 * i + 1, 1) = 0.0;
				A(2 * i + 1, 2) = 1.0;
				b(2 * i) = double(migPts[i].x + cshifts[i] - paramCercle[2]);
				b(2 * i + 1) = double(migPts[i].y + rshifts[i] - paramCercle[2]);
			}
			Eigen::Vector3d X = (A.transpose()*A).inverse()*A.transpose()*b;
			rshiftL = X(2);
			cshiftL = X(1);
			sL = X(0);
			m_strShift.Format("sc: %.2lf Dx: %.2lf  Dy: %.2lf\n", X(0), X(1), X(2));
			strText.Format("%d\t%d\t1\t%.2lf\t%.2lf\t%.2lf\t%d\n", ii,ii + 1, sL, X(1), X(2),A.rows()/2);
			file.WriteString(strText);

			UpdateData(false);*/

		Eigen::VectorXd x = Eigen::VectorXd(4);
		x << 0.0, 1.0, cmean, rmean;
		optimization_functor functor(rshifts.size(), rshifts, cshifts, migPts, paramCercle[2], paramCercle[2]);
		Eigen::NumericalDiff<optimization_functor> numDiff(functor);

		Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_functor>, double> lm(numDiff);
		//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<ICP_optimization_functor_point_to_plane>, float>::JacobianType::RealScalar oing = lm.lm_param();
		//lm.resetParameters();
		//lm.parameters.ftol=lm.parameters.ftol*10.0;
		//lm.parameters.xtol=lm.parameters.xtol*10.0;
		//lm.parameters.maxfev=10;
		//lm.parameters.epsfcn=0.1;
		//lm.resetParameters();

		int info;
		double vNormaOptimit;
		CString provaString;
		TRACE("\n inici calcul Nolineal\n");
		info = lm.minimizeInit(x);
		for (int i = 0; i < 10; i++)
		{
			info = lm.minimizeOneStep(x);
			vNormaOptimit = lm.fnorm;
			provaString.Format("FINAL OPTIMITZACI�: Iteraci� LM %d, ERROR OPTIMITZACI�: %f\n", i, vNormaOptimit);
			TRACE("FINAL OPTIMITZACI�: Iteraci� LM %d, ERROR OPTIMITZACI�: %f\n", i, vNormaOptimit);
			if (provaString[provaString.GetLength() - 2] == ')')
				int aaaaaa = 10;
			if (info != Eigen::LevenbergMarquardtSpace::Running)
				break;
		}
		TRACE("final calcul Nolineal\n");
		rshiftL = x(3);
		cshiftL = x(2);
		sL = x(1);
		angL = x(0);
		

	}
	else if (rshifts.size() == 1)
	{
		rshiftL = rshifts[0];
		cshiftL = cshifts[0];
		sL = 1.0;
		angL = 0.0;
	}
	
	return rshifts.size();

}

void CCorrelacioImatgesDlg::OnBnClickedBtnsegmentation()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	cv::Mat src, src2, dst, dst2;

	_LARGE_INTEGER StartingTime, EndingTime, ElapsedMiliseconds;
	LARGE_INTEGER Frequency;
	CString strText;
	CStdioFile file;

	src = imread("./Images/NIKOLOVAMIGLENA_2018-05-25_113856_VID00200086.png");


	Mat planes[3];
	split(src, planes);
	planes[1].copyTo(src2, mask);
	PosarImatgeInicial(src2);

	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&StartingTime);

	//Binaritzacio(src, dst2);
	
	BinaritzacioGPU(src, dst2, mask);


	QueryPerformanceCounter(&EndingTime);
	ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
	ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
	ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
	strText.Format("Temps: %d\n", ElapsedMiliseconds.QuadPart);
	m_txtTemps.SetWindowTextA(strText.GetString());

	//PosarImatgeInicial(dst);
	PosarImatgeSegmentacio(dst2);
}


void CCorrelacioImatgesDlg::OnBnClickedButcpu()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	cv::Mat src, src2, dst, dst2;
	cv::Mat srcim2, dstImg2;

	_LARGE_INTEGER StartingTime, EndingTime, ElapsedMiliseconds;
	LARGE_INTEGER Frequency;
	CString strText;
	CStdioFile file, file2;
	int indInici = 851;
	int indFinal = 1250;
	secTrans.clear();
	binImages.clear();
	rawImages.clear();

	CString pathImages, pathIm;
	//pathImages.Format("D:\\Documents\\VideosTesi\\RecordedVideosOriginalSurgery\\2018-05-25_113856_VID003Cutted\\Imatge_0");
	//pathImages.Format("C:\\Users\\UPC-ESAII\\Desktop\\RecordedVideosOriginalSurgery\\merdanula\\Imatge_00");
	//pathImages.Format("C:\\Users\\UPC-ESAII\\Desktop\\RecordedVideosOriginalSurgery\\2018-06-14_101557_VID001Cutted\\Imatge_00");
	//pathImages.Format("C:\\Users\\UPC-ESAII\\Desktop\\RecordedVideosOriginalSurgery\\merdanula\\Imatge_00");

	//pathImages.Format("D:\\ttts\\image_0");
	pathImages.Format("G:\\Mi unidad\\Videos Fetal\\videos ttts\\Fetoscopy Placenta Dataset\\Vessel_registration_unannotated_clips\\video01\\images\\anon001_0");

	file.Open("./transformacions.txt", CFile::modeCreate | CFile::modeWrite);
	file2.Open("./errorSSIM.txt", CFile::modeCreate | CFile::modeWrite);

	for (int ii = indInici; ii < indFinal; ii++)
	{
		if ((int)log10(ii) < (int)log10(indFinal))
		{
			pathIm.Format("0%d.png", ii);
		}
		else
		{
			pathIm.Format("%d.png", ii);
		}

		pathIm = pathImages + pathIm;
		src = imread(pathIm.GetString(), IMREAD_COLOR);

		if ((int)log10(ii + 1) < (int)log10(indFinal))
		{
			pathIm.Format("0%d.png", ii + 1);
		}
		else
		{
			pathIm.Format("%d.png", ii + 1);
		}

		pathIm = pathImages + pathIm;
		srcim2 = imread(pathIm.GetString(), IMREAD_COLOR);
		cv::Mat srctallada = src(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1));
		maskTallada = mask(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1));

		Mat planes[3];
		split(src, planes);
		planes[1].copyTo(src2, mask);
		PosarImatgeInicial(src2);
		Binaritzacio(srctallada, dst2, maskTallada);

		QueryPerformanceFrequency(&Frequency);
		QueryPerformanceCounter(&StartingTime);

		std::vector<std::vector<cv::Point2i>> puntsRadi1(2);
		std::vector<std::vector<cv::Point2d>> angRadi1(2);
		Binaritzacio(srcim2(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1)), dstImg2, maskTallada);
		PuntsCercle(dst2, cv::Point2d(paramCercle[2], paramCercle[2]), 100, puntsRadi1[0], angRadi1[0]);
		PuntsCercle(dst2, cv::Point2d(paramCercle[2], paramCercle[2]), 150, puntsRadi1[1], angRadi1[1]);

		///INI DEBUG: ESBORRAR QUAN TOQUI
		cv::Mat esb;
		cv::cvtColor(dst2, esb, cv::COLOR_GRAY2BGR);
		int rAlb = 10;

		/*for (int kkk = 0; kkk < puntsRadi1.size(); kkk++)
		{
			for (int kk = 0; kk < puntsRadi1[kkk].size(); kk++)
			{
				for (int iii = -rAlb; iii < rAlb + 1; iii++)
				{
					for (int jjj = -rAlb; jjj < rAlb + 1; jjj++)
					{
						if (iii*iii + jjj*jjj <= rAlb*rAlb)
						{
							esb.ptr(puntsRadi1[kkk][kk].y + jjj)[3 * (puntsRadi1[kkk][kk].x + iii)] = 0;
							esb.ptr(puntsRadi1[kkk][kk].y + jjj)[3 * (puntsRadi1[kkk][kk].x + iii) + 1] = 0;
							esb.ptr(puntsRadi1[kkk][kk].y + jjj)[3 * (puntsRadi1[kkk][kk].x + iii) + 2] = 255;
						}
					}
				}
			}
		}*/
		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imBin_%d.png", ii);
		cv::imwrite(pathIm.GetString(), esb);
		///END DEBUG:ESBORRAR QUAN TOQUI

		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imRaw_%d.png", ii );
		cv::imwrite(pathIm.GetString(), src(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1)));

		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imRaw_%d.png", ii + 1);
		cv::imwrite(pathIm.GetString(), srcim2(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1)));
		

		std::vector<std::vector<cv::Point2i>> puntsRadi2(2);
		std::vector<std::vector<cv::Point2d>> angRadi2(2);
		PuntsCercle(dstImg2, cv::Point2d(paramCercle[2], paramCercle[2]), 100, puntsRadi2[0], angRadi2[0]);
		PuntsCercle(dstImg2, cv::Point2d(paramCercle[2], paramCercle[2]), 150, puntsRadi2[1], angRadi2[1]);


		///INI DEBUG: ESBORRAR QUAN TOQUI
		cv::cvtColor(dstImg2, esb, cv::COLOR_GRAY2BGR);
		

	/*	for (int kkk = 0; kkk < puntsRadi2.size(); kkk++)
		{
			for (int kk = 0; kk < puntsRadi2[kkk].size(); kk++)
			{
				for (int iii = -rAlb; iii < rAlb + 1; iii++)
				{
					for (int jjj = -rAlb; jjj < rAlb + 1; jjj++)
					{
						if (iii*iii + jjj*jjj <= rAlb*rAlb)
						{
							esb.ptr(puntsRadi2[kkk][kk].y + jjj)[3 * (puntsRadi2[kkk][kk].x + iii)] = 0;
							esb.ptr(puntsRadi2[kkk][kk].y + jjj)[3 * (puntsRadi2[kkk][kk].x + iii) + 1] = 0;
							esb.ptr(puntsRadi2[kkk][kk].y + jjj)[3 * (puntsRadi2[kkk][kk].x + iii) + 2] = 255;
						}
					}
				}
			}
		}*/

		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imBin_%d.png", ii + 1);
		cv::imwrite(pathIm.GetString(), esb);
		///END DEBUG:ESBORRAR QUAN TOQUI

		//BinaritzacioGPU(src, dst2, mask);
		cv::Mat corr;
		cv::Point2i ccind;
		int tipus;


		//std::vector<cv::Point2i> lstPoints = std::vector<cv::Point2i>(0);
		//std::vector<double> coeffPoints = std::vector<double>(0);
		//std::vector<int> tipusPoints = std::vector<int>(0);
		//double cc;

		//for (int i = 0; i < puntsRadi.size(); i++)
		//{
		//	cc = CoefTipus(dstImg2(cv::Rect(puntsRadi[i].x - 100, puntsRadi[i].y - 100, 201, 201)), dst2(cv::Rect(puntsRadi[i].x - 50, puntsRadi[i].y - 50, 101, 101)), corr, ccind, tipus);
		//	lstPoints.push_back(ccind);
		//	coeffPoints.push_back(cc);
		//	tipusPoints.push_back(tipus);
		//	cv::Mat provaW = 255 * corr;
		//	imwrite("./correlacio.png", provaW);
		//}


		//cc = CoefTipus(dst2(cv::Rect(635 - 81, 500 - 81, 161, 161)), dst2(cv::Rect(635 - 35, 500 - 35, 71, 71)), corr, ccind, tipus);
		//double cc = CoefTipus(dst2, dst2(cv::Rect(635 - 35, 500 - 35, 71, 71)), corr, ccind, tipus);

		cv::Mat imaux1, imaux2;
		imaux1 = cv::Mat(dst2.rows, dst2.cols, dst2.type());
		imaux2 = cv::Mat(dst2.rows, dst2.cols, dst2.type());
		uchar *imd1, *imd2, *imdaux1, *imdaux2;
		int ycentre = (dst2.rows - 1) / 2;
		int xcentre = (dst2.cols - 1) / 2;
		int radiAux = paramCercle[2] - 10;

		for (int i = 0; i < imaux1.rows; i++)
		{
			imd1 = dst2.ptr(i);
			imd2 = dstImg2.ptr(i);
			imdaux1 = imaux1.ptr(i);
			imdaux2 = imaux2.ptr(i);
			for (int j = 0; j < imaux1.cols; j++)
			{
				if ((i - ycentre)*(i - ycentre) + (j - xcentre)*(j - xcentre) < radiAux*radiAux)
				{
					imdaux1[j] = imd1[j];
					imdaux2[j] = imd2[j];
				}
				else
				{
					imdaux1[j] = 255;
					imdaux2[j] = 255;
				}
			}
		}



		//cv::Mat imag1, imag2, corrImg;

		//int downsampling = 6;
		//int fillImg = 20;

		//int csize = dst2.cols / downsampling;
		//int rsize = dst2.rows / downsampling;
		//imag1 = cv::Mat::ones(2*fillImg + rsize, 2*fillImg + csize, dst2.type());
		//imag1 *= 255;
		//cv::resize(imaux1, imag1(cv::Rect(fillImg, fillImg, csize, rsize)), cv::Size(csize, rsize));
		//cv::resize(imaux2, imag2, cv::Size(csize, rsize));

		//cv::matchTemplate(imag1, imag2, corrImg, CV_TM_CCORR_NORMED);// CV_TM_SQDIFF_NORMED);

		////double minVal; double maxVal; Point minLoc; Point maxLoc;
		////Point matchLoc;
		////cv::minMaxLoc(corrImg, &minVal, &maxVal, &minPunt, &maxLoc, Mat());
		//cv::minMaxLoc(corrImg, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
		//

		////cv::minMaxLoc(corrImg(cv::Rect(-318 / 6 + fillImg -10, 120 / 6 + fillImg - 10, 20, 20)), &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat());

		//int rshift = (-maxLoc.y + fillImg)*downsampling;
		//int cshift = (-maxLoc.x + fillImg)*downsampling;

	
	std::vector<int> rshifts; 
	std::vector<int> cshifts;
	int rshiftL, cshiftL;
	double sL, angL;

	int rshval = getTranfCorrelation(dst2, imaux1, imaux2, puntsRadi1, puntsRadi2, angRadi1, angRadi2, rshifts, cshifts, rshiftL,cshiftL,sL,angL);

	if (rshval > 1)
	{

		m_strShift.Format("ang: %.2lf sc: %.2lf Dx: %.2lf  Dy: %.2lf\n", angL, sL, cshiftL, rshiftL);
		strText.Format("%d\t%d\t1\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%d\n", ii, ii + 1, angL, sL, cshiftL, rshiftL, rshifts.size());
		file.WriteString(strText);

		UpdateData(false);

	}
	else if (rshval == 1)
	{

		strText.Format("%d\t%d\t2\t%.2lf\t%.2lf\t%.2lf\t1\n", ii, ii + 1, angL, sL, (double)cshiftL, (double)rshiftL);
		file.WriteString(strText);
		m_strShift.Format("ang: %.2lf sc: %.2lf Dx: %.2lf  Dy: %.2lf\n", 0.0, 1.0, (double)cshifts[0], (double)rshifts[0]);
		UpdateData(false);
	}
	else
	{
		m_strShift.Format("Not found\n");
		strText.Format("ERROR\n", sL, (double)cshiftL, (double)rshiftL);
		file.WriteString(strText);
		UpdateData(false);
		continue;
	}



		//

		QueryPerformanceCounter(&EndingTime);
		ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
		ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
		ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
		strText.Format("Temps: %d\n", ElapsedMiliseconds.QuadPart);
		m_txtTemps.SetWindowTextA(strText.GetString());

		///*cv::namedWindow("WHAT", CV_WINDOW_AUTOSIZE);
		//cv::imshow("WHAT", dst2(cv::Rect(puntsRadi[3].x - 50, puntsRadi[3].y - 50, 101, 101)));
		//cv::waitKey(1);

		//cv::namedWindow("WHAT2", CV_WINDOW_AUTOSIZE);
		//cv::imshow("WHAT2", dstImg2(cv::Rect(puntsRadi[3].x - 100, puntsRadi[3].y - 100, 201, 201)));
		//cv::waitKey(1);*/

		////PosarImatgeInicial(dst);
		//
		PosarImatgeSegmentacio(dst2);

		PosarImatgeInicial(dstImg2);

		//cv::Mat dif;
		//differenceImage(dst2, dstImg2, dif);
		//cv::imshow("differnce", dif);
		//waitKey(1);

		//cv::Mat dif2;
		//differenceImageShift(dst2, dstImg2, dif2, rshift, cshift);
		//cv::imshow("differnce2", dif2);
		//waitKey(1);

		/*cv::Mat difL;
		differenceImageShift(dst2, dstImg2, difL, rshiftL, cshiftL);

		cvtColor(difL, difL, cv::COLOR_GRAY2RGB);

		for (int ii = -4; ii < 5; ii++)
		{
			for (int jj = -4; jj < 5; jj++)
			{
				difL.ptr(puntsRadi[indL].y + jj)[3 * (puntsRadi[indL].x + ii)] = 255;
				difL.ptr(puntsRadi[indL].y + jj)[3 * (puntsRadi[indL].x + ii) + 1] = 0;
				difL.ptr(puntsRadi[indL].y + jj)[3 * (puntsRadi[indL].x + ii) + 2] = 0;
			}
		}*/
		//cv::imshow("differnceL", difL);
		//waitKey(1);

		cv::Mat imshift, imshift2;
		//ScaleAndShift(dst2, imshift, sL, rshiftL, cshiftL);
		std::vector<double> variables = std::vector<double>(4);
		variables[0] = angL;
		variables[1] = sL;
		variables[2] = cshiftL;
		variables[3] = rshiftL;
		secTrans.push_back(variables);
		binImages.push_back(dst2);
		
		ScaleRotateShift(dst2, imshift, angL*180.0 / M_PI, sL, rshiftL, cshiftL);
		//cv::imshow("S&S", imshift);
		//waitKey(1);

		//cv::Mat T = getTransMat(angL, sL, dst2.cols / 2 + 1, dst2.rows / 2 + 1, cshiftL, rshiftL);
		//cv::warpAffine(dst2, imshift2, T, cv::Size(dst2.cols, dst2.rows));


		cv::Mat dif2;
		differenceImage(imshift, dstImg2, dif2);
		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imCorr_%d_%d.png", ii, ii + 1);
		cv::imwrite(pathIm.GetString(), dif2);
		cv::imshow("differnce", dif2);
		waitKey(1);


		cv::Mat imG1, imG1SL, imG2;
		planes[3];
		split(src, planes);
		planes[1].copyTo(imG1, mask);
		imG1 = imG1(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1));
		//ScaleAndShift(imG1, imG1SL, sL, rshiftL, cshiftL);
		rawImages.push_back(src(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1)));//imG1);
		ScaleRotateShift(imG1, imG1SL, angL*180.0 / M_PI, sL, rshiftL, cshiftL);

		cv::Mat srcTM;
		ScaleRotateShift(srctallada, srcTM, angL*180.0 / M_PI, sL, rshiftL, cshiftL);

		cv::Mat srcTM2 = srcim2(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1));


		planes[3];
		split(srcim2, planes);
		planes[1].copyTo(imG2, mask);
		imG2 = imG2(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1));
		cv::Mat dif3;
		differenceImageGray(imG1SL, imG2, dif3);
		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imCorrG_%d_%d.png", ii, ii + 1);
		cv::imwrite(pathIm.GetString(), dif3);
		cv::imshow("differnceG", dif3);
		waitKey(1);

		if (ii == indFinal - 1)
		{
			rawImages.push_back(srcim2(cv::Rect(paramCercle[0] - paramCercle[2], paramCercle[1] - paramCercle[2], 2 * paramCercle[2] + 1, 2 * paramCercle[2] + 1)));//imG2);
			binImages.push_back(dstImg2);
		}

		differenceImageColor(imG1SL, imG2, dif3);
		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imCorrCol_%d_%d.png", ii, ii + 1);
		cv::imwrite(pathIm.GetString(), dif3);

		cv::Mat imDif;
		std::vector<int> histDif;
		imDif = imG1SL(cv::Rect(imG1SL.cols / 6, imG1SL.rows / 6, 4 * imG1SL.cols / 6, 4 * imG1SL.rows / 6)) - imG2(cv::Rect(imG2.cols / 6, imG2.rows / 6, 4 * imG2.cols / 6, 4 * imG2.rows / 6));
		HistogramDiferencia(imG1SL(cv::Rect(imG1SL.cols / 5, imG1SL.rows / 5, 3 * imG1SL.cols / 5, 3 * imG1SL.rows / 5)), imG2(cv::Rect(imG2.cols / 5, imG2.rows / 5, 3 * imG2.cols / 5, 3 * imG2.rows / 5)), histDif);

		cv::Scalar ssim = getMSSIM(imG1SL(cv::Rect(imG1SL.cols / 5, imG1SL.rows / 5, 3 * imG1SL.cols / 5, 3 * imG1SL.rows / 5)), imG2(cv::Rect(imG2.cols / 5, imG2.rows / 5, 3 * imG2.cols / 5, 3 * imG2.rows / 5)));
		//cv::Scalar ssim2 = getMSSIM(imG1(cv::Rect(imG1.cols / 5, imG1.rows / 5, 3 * imG1.cols / 5, 3 * imG1.rows / 5)), imG2(cv::Rect(imG2.cols / 5, imG2.rows / 5, 3 * imG2.cols / 5, 3 * imG2.rows / 5)));
		double mse = GetMSE(imG1SL(cv::Rect(imG1SL.cols / 5, imG1SL.rows / 5, 3 * imG1SL.cols / 5, 3 * imG1SL.rows / 5)), imG2(cv::Rect(imG2.cols / 5, imG2.rows / 5, 3 * imG2.cols / 5, 3 * imG2.rows / 5)));
		double psnr = getPSNR(imG1SL(cv::Rect(imG1SL.cols / 5, imG1SL.rows / 5, 3 * imG1SL.cols / 5, 3 * imG1SL.rows / 5)), imG2(cv::Rect(imG2.cols / 5, imG2.rows / 5, 3 * imG2.cols / 5, 3 * imG2.rows / 5)));

		double mseC = GetMSE(srcTM(cv::Rect(imG1SL.cols / 5, imG1SL.rows / 5, 3 * imG1SL.cols / 5, 3 * imG1SL.rows / 5)), srcTM2(cv::Rect(imG2.cols / 5, imG2.rows / 5, 3 * imG2.cols / 5, 3 * imG2.rows / 5)));
		double psnrC = getPSNR(srcTM(cv::Rect(imG1SL.cols / 5, imG1SL.rows / 5, 3 * imG1SL.cols / 5, 3 * imG1SL.rows / 5)), srcTM2(cv::Rect(imG2.cols / 5, imG2.rows / 5, 3 * imG2.cols / 5, 3 * imG2.rows / 5)));


		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imCut%d_2.png", ii);
		imwrite(pathIm.GetString(), imG1SL(cv::Rect(imG1SL.cols / 5, imG1SL.rows / 5, 3 * imG1SL.cols / 5, 3 * imG1SL.rows / 5)));
		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imCut%d_1.png", ii + 1);
		imwrite(pathIm.GetString(), imG2(cv::Rect(imG2.cols / 5, imG2.rows / 5, 3 * imG2.cols / 5, 3 * imG2.rows / 5)));

		strText.Format("%d\t%d\t%.3lf\t%.3lf\t%.3lf\t%.3lf\t%.3lf\n", ii, ii + 1, mse, psnr, mseC, psnrC, ssim[0]);
		file2.WriteString(strText);

		cv::medianBlur(imDif, imDif, 5);
		pathIm.Format("G:/Mi unidad/NACompartida/Fetal/Images/Projects/CorrelacioImatges/imDif_%d_%d.png", ii, ii + 1);
		cv::imwrite(pathIm.GetString(), imDif);
		cv::imshow("Dif", imDif);
		waitKey(1);
	}
	file.Close();
	file2.Close();
}

double CCorrelacioImatgesDlg::GetMSE(cv::Mat &im1, cv::Mat &im2)
{
	uchar *imd1, *imd2;
	double rmse = 0.0;

	for (int i = 0; i < im1.rows; i++)
	{
		imd1 = im1.ptr(i);
		imd2 = im2.ptr(i);

		for (int j = 0; j < im1.cols; j++)
		{
			rmse += ((double)imd1[j] - (double)imd2[j])*((double)imd1[j] - (double)imd2[j]);
		}
	}

	return rmse / (double)(im1.rows*im1.cols*im1.channels());

}

double CCorrelacioImatgesDlg::getPSNR(cv::Mat &im1, cv::Mat &im2)
{
	double mse = GetMSE(im1, im2);
	double minVal; double maxVal; Point minLoc; Point maxLoc;
	uchar *imd1;
	double maxAux = 0.0;

	if (im1.channels() == 1) {
		cv::minMaxLoc(im1, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
	}
	else
	{
		for (int i = 0; i < im1.rows; i++)
		{
			imd1 = im1.ptr(i);
			for (int j = 0; j < im1.cols; j++)
			{
				if (sqrt(imd1[3 * j] * imd1[3 * j] + imd1[3 * j + 1] * imd1[3 * j + 1] + imd1[3 * j + 2] * imd1[3 * j + 2]) > maxAux)
				{
					maxAux = sqrt(imd1[3 * j] * imd1[3 * j] + imd1[3 * j + 1] * imd1[3 * j + 1] + imd1[3 * j + 2] * imd1[3 * j + 2]);
				}
			}
		}
		maxVal = maxAux / 3.0;
	}

	return 10.0*log10(maxVal*maxVal / mse);

}

void CCorrelacioImatgesDlg::ScaleAndShift(cv::Mat & inIm, cv::Mat & outIm, double s, double rshift, double cshift)
{
	cv::Mat aux1;
	cv::Mat aux2 = cv::Mat::zeros(inIm.rows, inIm.cols, inIm.type());

	cv::resize(inIm, aux1, cv::Size(floor(s*aux2.cols), floor(s*aux2.rows)));
	
	uchar *imd1, *imd2;
	int rs = (aux2.rows - aux1.rows) / 2;
	int cs = (aux2.cols - aux1.cols) / 2;
	for (int i = 0; i < aux1.rows; i++)
	{
		if ((i + rs + (int)rshift > -1) && (i + rs + (int)rshift < aux2.rows))
		{
			imd1 = aux1.ptr(i);
			imd2 = aux2.ptr(i + rs + (int)rshift);
			for (int j = 0; j < aux1.cols; j++)
			{
				if ((j + cs + (int)cshift > -1) && (j + cs + (int)cshift< aux2.cols))
				{
					imd2[j + cs + (int)cshift] = imd1[j];
				}
			}
		}
	}

	aux2.copyTo(outIm);
}

void CCorrelacioImatgesDlg::ScaleRotateShift(cv::Mat & inIm, cv::Mat & outIm, double a, double s, double rshift, double cshift)
{
	cv::Mat aux1;
	cv::Mat aux2 = cv::Mat::zeros(inIm.rows, inIm.cols, inIm.type());
	cv::Mat M;

	M = cv::getRotationMatrix2D(cv::Point2d(inIm.cols / 2 + 1, inIm.rows / 2 + 1), a, s);
	cv::warpAffine(inIm, aux1, M, cv::Size(inIm.cols, inIm.rows));
	
	uchar *imd1, *imd2;
	int nch = aux2.channels();
	for (int i = 0; i < aux1.rows; i++)
	{
		if ((i + (int)rshift > -1) && (i + (int)rshift < aux2.rows))
		{
			imd1 = aux1.ptr(i);
			imd2 = aux2.ptr(i + (int)rshift);
			for (int j = 0; j < aux1.cols; j++)
			{
				if ((j + (int)cshift > -1) && (j + (int)cshift< aux2.cols))
				{
					for (int k = 0; k < nch; k++)
					{
						imd2[nch*(j + (int)cshift) + k] = imd1[nch*j + k];
					}
				}
			}
		}
	}

	aux2.copyTo(outIm);
}

inline cv::Mat CCorrelacioImatgesDlg::getTransMat(double an, double s, double cx, double cy, double ax, double ay)
{
	cv::Mat M = cv::Mat(2, 3, CV_64FC1);
	M.ptr<double>(0)[0] = s*cos(an);
	M.ptr<double>(0)[1] = s*sin(an);
	M.ptr<double>(1)[0] = -s*sin(an);
	M.ptr<double>(1)[1] = s*cos(an);
	M.ptr<double>(0)[2] = cx + ax - s*cos(an)*cx - s*sin(an)*cy; 
	M.ptr<double>(1)[2] = cy + ay + s*sin(an)*cx - s*cos(an)*cy;
	return M;
}

inline Eigen::Matrix3d CCorrelacioImatgesDlg::getTransMatE(double an, double s, double cx, double cy, double ax, double ay)
{
	Eigen::Matrix3d M;
	M << s*cos(an), s*sin(an), cx + ax - s*cos(an)*cx - s*sin(an)*cy, -s*sin(an), s*cos(an), cy + ay + s*sin(an)*cx - s*cos(an)*cy, 0.0, 0.0, 1.0;
	return M;
}

void CCorrelacioImatgesDlg::differenceImage(cv::Mat & im1, cv::Mat & im2, cv::Mat & dif)
{
	dif = cv::Mat(im1.rows, im1.cols, im1.type());
	uchar *imd1, *imd2, *difd;
	for (int i = 0; i < im1.rows; i++)
	{
		imd1 = im1.ptr(i);
		imd2 = im2.ptr(i);
		difd = dif.ptr(i);
		for (int j = 0; j < im1.cols; j++)
		{
			if (imd1[j] == imd2[j])
			{
				if (imd1[j] == 0)
				{
					difd[j] = 0;
				}
				else
				{
					difd[j] = 255;
				}
			}
			else
			{
				if (imd1[j] == 0)
					difd[j] = 128;
				else
					difd[j] = 64;
			}
		}
	}
}

void CCorrelacioImatgesDlg::differenceImageGray(cv::Mat & im1, cv::Mat & im2, cv::Mat & dif)
{
	dif = im1 / 2 + im2 / 2;//cv::Mat(im1.rows, im1.cols, im1.type());
}

void CCorrelacioImatgesDlg::differenceImageColor(cv::Mat & im1, cv::Mat & im2, cv::Mat & dif)
{
	dif = cv::Mat(im1.rows, im1.cols, CV_8UC3);
	uchar *imd1, *imd2, *difd;

	for (int i = 0; i < im1.rows; i++)
	{
		imd1 = im1.ptr(i);
		imd2 = im2.ptr(i);
		difd = dif.ptr(i);
		for (int j = 0; j < im1.cols; j++)
		{
			difd[3 * j] = imd1[j];
			difd[3 * j + 1] = imd2[j] / 2 + imd1[j] / 2;
			difd[3 * j + 2] = imd2[j];
		}
	}
}

void CCorrelacioImatgesDlg::HistogramDiferencia(cv::Mat & im1, cv::Mat & im2, std::vector<int> & hist)
{
	hist = std::vector<int>(0);

	for (int i = 0; i < 256; i++)
		hist.push_back(0);
	uchar *imd1, *imd2;

	for (int i = 0; i < im1.rows; i++)
	{
		imd1 = im1.ptr(i);
		imd2 = im2.ptr(i);
		for (int j = 0; j < im1.cols; j++)
		{
			hist[abs(imd1[j] - imd2[j])]++;
		}
	}
}

void CCorrelacioImatgesDlg::differenceImageShift(cv::Mat & im1, cv::Mat & im2, cv::Mat & dif, int rshift, int cshift)
{
	dif = cv::Mat(im1.rows, im1.cols, im1.type());
	uchar *imd1, *imd2, *difd;
	imd2 = NULL;
	bool rpos;
	for (int i = 0; i < im1.rows; i++)
	{
		imd1 = im1.ptr(i);
		if ((i + rshift > -1) && (i + rshift < im2.rows))
		{
			imd2 = im2.ptr(i + rshift);
			rpos = true;
		}
		else
			rpos = false;
		difd = dif.ptr(i);
		for (int j = 0; j < im1.cols; j++)
		{
			if (rpos)
			{
				if ((j + cshift > -1) && (j + cshift < im2.cols))
				{
					if (imd1[j] == imd2[j + cshift])
					{
						if (imd1[j] == 0)
						{
							difd[j] = 0;
						}
						else
						{
							difd[j] = 255;
						}
					}
					else
					{
						if (imd1[j] == 0)
							difd[j] = 128;
						else
							difd[j] = 64;
					}
				}
				else
					difd[j] = imd1[j];
			}
			else
			{
				difd[j] = imd1[j];
			}
		}
	}
}

Scalar CCorrelacioImatgesDlg::getMSSIM(const Mat& i1, const Mat& i2)
{
	const double C1 = 6.5025, C2 = 58.5225;
	/***************************** INITS **********************************/
	int d = CV_32F;

	Mat I1, I2;
	i1.convertTo(I1, d);           // cannot calculate on one byte large values
	i2.convertTo(I2, d);

	Mat I2_2 = I2.mul(I2);        // I2^2
	Mat I1_2 = I1.mul(I1);        // I1^2
	Mat I1_I2 = I1.mul(I2);        // I1 * I2

								   /***********************PRELIMINARY COMPUTING ******************************/

	Mat mu1, mu2;   //
	GaussianBlur(I1, mu1, Size(11, 11), 1.5);
	GaussianBlur(I2, mu2, Size(11, 11), 1.5);

	Mat mu1_2 = mu1.mul(mu1);
	Mat mu2_2 = mu2.mul(mu2);
	Mat mu1_mu2 = mu1.mul(mu2);

	Mat sigma1_2, sigma2_2, sigma12;

	GaussianBlur(I1_2, sigma1_2, Size(11, 11), 1.5);
	sigma1_2 -= mu1_2;

	GaussianBlur(I2_2, sigma2_2, Size(11, 11), 1.5);
	sigma2_2 -= mu2_2;

	GaussianBlur(I1_I2, sigma12, Size(11, 11), 1.5);
	sigma12 -= mu1_mu2;

	///////////////////////////////// FORMULA ////////////////////////////////
	Mat t1, t2, t3;

	t1 = 2 * mu1_mu2 + C1;
	t2 = 2 * sigma12 + C2;
	t3 = t1.mul(t2);              // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))

	t1 = mu1_2 + mu2_2 + C1;
	t2 = sigma1_2 + sigma2_2 + C2;
	t1 = t1.mul(t2);               // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))

	Mat ssim_map;
	divide(t3, t1, ssim_map);      // ssim_map =  t3./t1;

	Scalar mssim = mean(ssim_map); // mssim = average of ssim map
	return mssim;
}


void CCorrelacioImatgesDlg::OnBnClickedButmosaic()
{
	Eigen::Matrix3d THT;
	THT.setIdentity();
	Eigen::Matrix3d THA;
	cv::Mat TImg = cv::Mat(2, 3, CV_64FC1);
	std::vector<cv::Mat> rawImTrans = std::vector<cv::Mat>(0);
	std::vector<cv::Mat> binImTrans = std::vector<cv::Mat>(0);

	

	if (binImages.size() > 0)
	{
		cv::Mat binM = cv::Mat::zeros(3 * binImages[0].rows, 3 * binImages[0].cols, binImages[0].type());
		cv::Mat rawM = cv::Mat::zeros(3 * rawImages[0].rows, 3 * rawImages[0].cols, rawImages[0].type());

		cv::Mat binM2 = cv::Mat::zeros(3 * binImages[0].rows, 3 * binImages[0].cols, binImages[0].type());
		cv::Mat rawM2 = cv::Mat::zeros(3 * rawImages[0].rows, 3 * rawImages[0].cols, rawImages[0].type());

		cv::Mat rawM3 = cv::Mat::zeros(3 * rawImages[0].rows, 3 * rawImages[0].cols, rawImages[0].type());

		cv::Mat binAcc = cv::Mat::zeros(binM.size(), CV_32FC1);
		cv::Mat rawAcc = cv::Mat::zeros(rawM.size(), CV_32FC3);

		binImages[binImages.size() - 1].copyTo(binM(cv::Rect(binImages[binImages.size() - 1].cols, binImages[binImages.size() - 1].rows, binImages[binImages.size() - 1].cols, binImages[binImages.size() - 1].rows)));
		rawImages[rawImages.size() - 1].copyTo(rawM(cv::Rect(rawImages[rawImages.size() - 1].cols, rawImages[rawImages.size() - 1].rows, rawImages[rawImages.size() - 1].cols, rawImages[binImages.size() - 1].rows)));
		binImages[binImages.size() - 1](cv::Rect(binImages[binImages.size() - 1].cols / 5, binImages[binImages.size() - 1].rows / 5, 3 * binImages[binImages.size() - 1].cols / 5, 3 * binImages[binImages.size() - 1].rows / 5)).copyTo(binM2(cv::Rect(binImages[binImages.size() - 1].cols + binImages[binImages.size() - 1].cols / 5, binImages[binImages.size() - 1].rows + binImages[binImages.size() - 1].rows / 5, 3 * binImages[binImages.size() - 1].cols / 5, 3 * binImages[binImages.size() - 1].rows / 5)));
		rawImages[rawImages.size() - 1](cv::Rect(rawImages[rawImages.size() - 1].cols / 5, rawImages[rawImages.size() - 1].rows / 5, 3 * rawImages[rawImages.size() - 1].cols / 5, 3 * rawImages[rawImages.size() - 1].rows / 5)).copyTo(rawM2(cv::Rect(rawImages[rawImages.size() - 1].cols + rawImages[rawImages.size() - 1].cols / 5, rawImages[rawImages.size() - 1].rows + rawImages[rawImages.size() - 1].rows / 5, 3 * rawImages[rawImages.size() - 1].cols / 5, 3 * rawImages[binImages.size() - 1].rows / 5)));

		
		binImTrans.push_back(binM);
		rawImTrans.push_back(rawM);
		cv::accumulate(binM, binAcc);
		cv::accumulate(rawM, rawAcc);

		for (int i = binImages.size() - 2; i > -1; i--)
		{
			binM = cv::Mat::zeros(3 * binImages[i].rows, 3 * binImages[i].cols, binImages[i].type());
			rawM = cv::Mat::zeros(3 * rawImages[i].rows, 3 * rawImages[i].cols, rawImages[i].type());
			binImages[i].copyTo(binM(cv::Rect(binImages[i].cols, binImages[i].rows, binImages[i].cols, binImages[i].rows)));
			rawImages[i].copyTo(rawM(cv::Rect(rawImages[i].cols, rawImages[i].rows, rawImages[i].cols, rawImages[i].rows)));
			THA = getTransMatE(secTrans[i][0], secTrans[i][1], binImages[i].cols / 2 + 1 + binImages[i].cols, binImages[i].rows / 2 + 1 + binImages[i].rows, secTrans[i][2], secTrans[i][3]);
			THT = THT*THA;
			TImg.ptr<double>(0)[0] = THT(0, 0);
			TImg.ptr<double>(0)[1] = THT(0, 1);
			TImg.ptr<double>(0)[2] = THT(0, 2);
			TImg.ptr<double>(1)[0] = THT(1, 0);
			TImg.ptr<double>(1)[1] = THT(1, 1);
			TImg.ptr<double>(1)[2] = THT(1, 2);

			cv::Mat binAux;
			cv::Mat rawAux;
			cv::warpAffine(binM, binAux, TImg, cv::Size(binM.cols, binM.rows));
			cv::warpAffine(rawM, rawAux, TImg, cv::Size(rawM.cols, rawM.rows));
			binImTrans.push_back(binAux);
			rawImTrans.push_back(rawAux);
			cv::accumulate(binAux, binAcc);
			cv::accumulate(rawAux, rawAcc);

			///MOSAIC 2
			Eigen::Vector3d V1, V2, V3, V4;
			V1 << (double)binImages[i].cols + binImages[i].cols / 5.0, (double)binImages[i].rows + binImages[i].rows / 5.0, 1.0;
			V2 << (double)binImages[i].cols + 4.0*binImages[i].cols / 5.0, (double)binImages[i].rows + binImages[i].rows / 5.0, 1.0;
			V3 << (double)binImages[i].cols + binImages[i].cols / 5.0, (double)binImages[i].rows + 4.0*binImages[i].rows / 5.0, 1.0;
			V4 << (double)binImages[i].cols + 4.0*binImages[i].cols / 5.0, (double)binImages[i].rows + 4.0*binImages[i].rows / 5.0, 1.0;

			V1 = THT*V1;
			V2 = THT*V2;
			V3 = THT*V3;
			V4 = THT*V4;

			int rmin = max(floor(V1(1)), floor(V2(1)));
			int rmax = min(floor(V3(1)), floor(V4(1)));
			int cmin = max(floor(V1(0)), floor(V3(0)));
			int cmax = min(floor(V2(0)), floor(V4(0)));
			binAux(cv::Rect(cmin, rmin, cmax - cmin, rmax - rmin)).copyTo(binM2(cv::Rect(cmin, rmin, cmax - cmin, rmax - rmin)));
			rawAux(cv::Rect(cmin, rmin, cmax - cmin, rmax - rmin)).copyTo(rawM2(cv::Rect(cmin, rmin, cmax - cmin, rmax - rmin)));
			///

		}
		binAcc /= (double)binImages.size();
		rawAcc /= (double)rawImages.size();
		binAcc.convertTo(binM, CV_8UC1);
		rawAcc.convertTo(rawM, CV_8UC3);

		CString path;
		path.Format("./mosaicBin.png");
		cv::imwrite(path.GetString(), binM);
		path.Format("./mosaicRaw.png");
		cv::imwrite(path.GetString(), rawM);
		path.Format("./mosaicBin2.png");
		cv::imwrite(path.GetString(), binM2);
		path.Format("./mosaicRaw2.png");
		cv::imwrite(path.GetString(), rawM2);

		cv::Mat rawAcc2= cv::Mat::zeros(rawAcc.rows, rawAcc.cols, rawAcc.type());
		cv::Mat nrawpunts = cv::Mat::zeros(rawAcc.rows, rawAcc.cols, CV_8UC1);
		uchar *raw1, *nrawpts;
		float *raw2;
		bool trobat;
		int rawch = rawImTrans[0].channels();

		for (int ii = 0; ii < rawImTrans.size(); ii++)
		{
			for (int jj = 0; jj < rawImTrans[ii].rows; jj++)
			{
				raw1 = rawImTrans[ii].ptr(jj);
				raw2 = rawAcc2.ptr<float>(jj);
				nrawpts = nrawpunts.ptr(jj);
				for (int kk = 0; kk < rawImTrans[ii].cols; kk++)
				{
					trobat = false;
					for (int ll = 0; ll < rawch; ll++)
					{
						if (raw1[rawch*kk + ll] > 0)
						{
							trobat = true;
						}
						raw2[rawch*kk + ll] += (float)raw1[rawch*kk + ll];
					}
					if (trobat)
					{
						nrawpts[kk]++;
					}
				}
			}
		}

		for (int jj = 0; jj < nrawpunts.rows; jj++)
		{
			raw2 = rawAcc2.ptr<float>(jj);
			nrawpts = nrawpunts.ptr(jj);
			for (int kk = 0; kk < nrawpunts.cols; kk++)
			{
				for (int ll = 0; ll < rawch; ll++)
				{
					
					raw2[rawch*kk + ll] /= max((float)nrawpts[kk], 1.0f);
				}
			}
		}

		rawAcc2.convertTo(rawM3, CV_8UC3);
		path.Format("./mosaicRaw3.png");
		cv::imwrite(path.GetString(), rawM3);
	}
}
