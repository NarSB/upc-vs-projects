#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "opencv2\highgui\highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/core/cuda.hpp"
#include "opencv2/core/cuda.inl.hpp"
#include "opencv2/core/cuda_stream_accessor.hpp"
#include "opencv2/core/cuda_types.hpp"
#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudafilters.hpp"
#include "opencv2/cudafeatures2d.hpp"
#include "opencv2/cudalegacy.hpp"

#include <stdio.h>

cudaError_t BinaritzacioGPU(cv::Mat& src, cv::Mat& dst, cv::Mat& mask);

__global__ void adaptiveThreasholdGPU(cv::cuda::PtrStepSz<uchar> src, cv::cuda::PtrStepSz<int> srcInt, cv::cuda::PtrStepSz<uchar> dst, cv::cuda::PtrStepSz<uchar> mask, int t, int s, int rows, int cols)
{
	int ind = blockIdx.x*blockDim.x + threadIdx.x;
	int rind = ind / cols;
	int cind = ind - rind*cols;
	int count;
	int val;
	int valI;

	



	int nw, ne, sw, se;
	int cmin, cmax, rmax, rmin;
	cmin = max(1, cind - s / 2);
	cmax = min(cols - 1, cind + s / 2);
	rmin = max(1, rind - s / 2);
	rmax = min(rows - 1, rind + s / 2);

	count = (cmax - cmin)*(rmax - rmin);

	nw = srcInt.ptr(rmin-1)[cmin-1];
	ne = srcInt.ptr(rmin-1)[cmax];
	sw = srcInt.ptr(rmax)[cmin-1];
	se = srcInt.ptr(rmax)[cmax];

	val = se - sw - ne + nw;
	valI = src.ptr(rind)[cind];

	//if (valI*count < (int)(val*(100.0 - t) / 100.0))
	if ((valI + t)*count <= val)
	{
		dst.ptr(rind)[cind] = 0;
	}
	else
	{
		dst.ptr(rind)[cind] = 255;
	}

	int maskVal = mask.ptr(rind)[cind];
	if (maskVal == 0)
	{
		dst.ptr(rind)[cind] = 0;
	}
}

extern cudaError_t BinaritzacioGPU(cv::Mat& src, cv::Mat& dst, cv::Mat& mask)
{
	cudaError_t cudaStatus;
	cv::cuda::GpuMat auxiliar(src);
	cv::cuda::GpuMat auxiliar2;
	cv::cuda::GpuMat intImage;
	cv::cuda::GpuMat planes[3];
	cv::cuda::GpuMat maskGpu(mask);
	cv::cuda::split(src, planes);
	planes[1].copyTo(auxiliar2, maskGpu);

	// apply the CLAHE algorithm to the L channel
	cv::Ptr<cv::cuda::CLAHE> clahe = cv::cuda::createCLAHE();
	clahe->setClipLimit(2);
	clahe->apply(auxiliar2, auxiliar2);

	cv::cuda::integral(auxiliar2, intImage);

	int cols = auxiliar2.cols;
	int rows = auxiliar2.rows;
	int threadBlock =  min(max(32, cols*rows), 128);
	int blockSize = (cols*rows + threadBlock - 1) / threadBlock;

	adaptiveThreasholdGPU << <blockSize, threadBlock >> > (auxiliar2, intImage, auxiliar2, maskGpu, 10, 60, rows, cols);

	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	//cv::adaptiveThreshold(auxiliar2, auxiliar, 255, ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 45, 5);
	auxiliar2.copyTo(auxiliar2, maskGpu);

	

	//cv::Ptr<cv::cuda::Filter> median = cv::cuda::createMedianFilter(CV_8UC1, 3);
	//median->apply(auxiliar2, auxiliar2);

	cv::Mat auxCPU;
	auxiliar2.download(auxCPU);
	cv::medianBlur(auxCPU, auxCPU, 3);

	/*cv::cuda::threshold(auxiliar2, auxiliar2, 128, 255, cv::THRESH_BINARY_INV);*/
	//auxiliar2.copyTo(auxiliar2, maskGpu);
	//cv::cuda::bitwise_not(auxiliar3, auxiliar2, maskGpu);
	auxCPU = 255 - auxCPU;

	//cv::Mat openKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5), cv::Point(-1, -1));
	//cv::cuda::GpuMat openGPU(openKernel);
	//cv::Ptr<cv::cuda::Filter>morphopen = cv::cuda::createMorphologyFilter(cv::MORPH_CLOSE, CV_8UC1, openKernel);
	//morphopen->apply(auxiliar3, auxiliar3);


	//NO VA
	//cv::cuda::GpuMat maskCon;
	//maskCon.create(auxiliar2.rows, auxiliar2.cols, CV_8UC1);
	//cv::cuda::GpuMat components;
	//components.create(auxiliar2.rows, auxiliar2.cols, CV_32SC1);
	//cv::cuda::connectivityMask(auxiliar2, maskCon, cv::Scalar::all(0), cv::Scalar::all(2));
	//
	//cv::cuda::labelComponents(maskCon, components);
	//NO VA END


	cv::filterSpeckles(auxCPU, 0, 3000, 0);

	//cv::Canny(auxiliar2, auxiliar, 30, 120);

	cv::Mat closeKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(19, 19), cv::Point(-1, -1));
	//cv::cuda::GpuMat closeGPU(closeKernel);
	//cv::Ptr<cv::cuda::Filter>morphclose = cv::cuda::createMorphologyFilter(cv::MORPH_CLOSE, CV_8UC1, closeKernel);
	//morphclose->apply(auxiliar2, auxiliar2);

	cv::morphologyEx(auxCPU, auxCPU, cv::MORPH_CLOSE, closeKernel);

	/*cv::cuda::threshold(components, auxiliar2, 128, 255, cv::THRESH_BINARY_INV);*/
	//cv::cuda::bitwise_not(auxiliar2, auxiliar3, maskGpu);
	dst = 255 - auxCPU;
	//auxiliar2.download(dst);
	return cudaSuccess;// cudaStatus;
}