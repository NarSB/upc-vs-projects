
// DemoFetalDlg.h: archivo de encabezado
//

#pragma once
#include "afxwin.h"
#include "uEyeCam.h"

#define TOLERANCIA 0.001
#define SIZETAQUES 10
#define SIZEBUSCARULTIMESTAQUES 4
#define SIZECONTORN 16
#define XSIZEIMAGE 600

//Vision Communication codes
#define VISION_NOCHANGE				-1
#define VISION_CLOSECONN			0
#define VISION_UPDATEUI				1

#define VISION_SCOPEOFF				0
#define VISION_SCOPEON				1

#define	VISION_MODE_OFF				0
#define	VISION_MODE_FREENAV			1
#define	VISION_MODE_TARGETING		2
#define	VISION_MODE_ONTARGET		3

#define VISION_TARGETSTAT_OFF		0
#define VISION_TARGETSTAT_DETECTED	1
#define VISION_TARGETSTAT_CUTTTED	2
#define VISION_TARGETSTAT_REVIEWED	3

#define VISION_TARGETTYPE_OFF		0
#define VISION_TARGETTYPE_AA		1
#define VISION_TARGETTYPE_AV		2
#define VISION_TARGETTYPE_VV		3

#define VISION_SELECT_OFF			0
#define VISION_SELECT_ON			1
//


#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

struct tacaStruct {
	cv::Point2f mean;
	double distCercle;
	int size;
	int minX;
	int maxX;
	int minY;
	int maxY;
	std::vector<cv::Point2i> points;
	std::vector<cv::Point2i> contorn;
};

struct indexosTaca {
	bool trobat;
	int nNuls;
	int igualNTaques;
};


// Cuadro de di�logo de CDemoFetalDlg
class CDemoFetalDlg : public CDialogEx
{
// Construcci�n
public:
	CDemoFetalDlg(CWnd* pParent = NULL);	// Constructor est�ndar

	CWinThread * thRecording;
	bool bRecording;	//True: Thread active, False: Kill Thread


	uEyeCam*        ueCamera;       //camera
	
									//CvVideoWriter*  VideoWriter;    //The video writer
	CvSize          VideoSize;      //Size of the image (video frame) to be saves
	CvSize          VideoSizeUI;    //Size of the image (video frame) to be displayed on UI
	IplImage*       VideoImage;     //Video image (frame) to be saved
	IplImage*       VideoImageUI;   //Video image (frame) to be displayed on UI
	double          VideoFrameRate; //Video frame rate (FPS)
	int ctrlVideoImageReady;
	cv::Mat lastImage;
	cv::Mat cameraMatrix, distCoef;
	float fc[2], cc[2], alpha_c;
	double kc[5];
	int iTotalFramesRecorded;

	VideoCapture cam;
	VideoWriter video;
	bool bvideo;
	bool bPoints;

	cv::Size winSegSize;
	cv::Mat cvImgTmp;
	CImage* mfcImg;
	BITMAPINFO bitInfo;

	//Segmentacio
	int indSegmentacio;
	cv::Mat masCercle;
	double paramCercle[3];
	cv::Mat imSegmentacio;
	cv::Mat imScope[2];
	cv::Mat imMode[3];
	cv::Mat imTargStatus[3];
	cv::Mat imTargType[3];
	cv::Mat imButtons[2];

	std::vector<std::vector<std::vector<tacaStruct>>> ultTaques;
	std::vector<indexosTaca> indUltTaques;
	
	int indlastTaques;
	std::vector<tacaStruct> lastTaques[SIZETAQUES];

	//Socket Albert
	SOCKET servdes;                 /* Socket descriptor for server */
	struct sockaddr_in ServAddr;	/* Local address */
	struct sockaddr_in ClntAddr;	/* Client address */
	int claddrlength;				/* Length of client addr data structure*/
	bool ClientConnected;
	double DataToSend[29]; //[7+16+6]
	bool DataReadyToBeSent;
	bool dadesReady;
	int imatgesMostrar[5];
	bool endProg;
	

// Datos del cuadro de di�logo
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEMOFETALJULIOL2018_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	
	afx_msg HCURSOR OnQueryDragIcon();
	//uEye Message Map
	LRESULT OnUEyeMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_picImage;
	CButton m_butCamera;
	afx_msg void OnBnClickedButton1();
	bool InitializeVideoRecording();
	void FinalizeVideoRecording();
	void PosarImatgePictureBox(cv::Mat & entrada);
	void OverlayImage(cv::Mat & source, cv::Mat & overlay, cv::Mat & output, CvPoint location);
	void Segmentacio(cv::Mat & entrada, cv::Mat & sortida);
	afx_msg void OnDestroy();
	void OnConnected(int id, int nErrorCode);
	void OnClosed(int id, int nErrorCode);
	void OnRecieve(int id, int nErrorCode);
	afx_msg void OnBnClickedButvideo();
	void BuscarTaques(cv::Mat & image, std::vector<tacaStruct>& taques);
	int InterseccioTaques(tacaStruct & A, tacaStruct & B, double & distancia);
	void CalcularCadenaTaques(std::vector<tacaStruct>& novaTaca);
	tacaStruct creixementRegio(cv::Mat & imatge, cv::Mat & mascara, int x, int y, int lbl);
	CButton m_butVideo;
	afx_msg void OnBnClickedButprova();
	CButton m_butPoints;
};
