
// DemoFetalJuliol2018.h: archivo de encabezado principal para la aplicación PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "incluir 'stdafx.h' antes de incluir este archivo para PCH"
#endif

#include "resource.h"		// Símbolos principales


// CDemoFetalApp:
// Consulte la sección DemoFetalJuliol2018.cpp para obtener información sobre la implementación de esta clase
//

class CDemoFetalApp : public CWinApp
{
public:
	CDemoFetalApp();

// Reemplazos
public:
	virtual BOOL InitInstance();

// Implementación

	DECLARE_MESSAGE_MAP()
};

extern CDemoFetalApp theApp;