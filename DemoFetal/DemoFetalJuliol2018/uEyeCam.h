#pragma once
class uEyeCam
{
public:

	/*CImage      * m_iImage;
	char*       byteStorage;

	IplImage    * iImage;*/

	//Acquisition Parameters
	HIDS        m_hCam;							// handle to camera
	HWND        m_hWndDisplay;					// handle to diplay window
	INT         m_nColorMode;					// Y8/RGB16/RGB24/REG32
	INT         m_nBitsPerPixel;				// number of bits needed store one pixel
	INT         m_nSizeX;						// width of image
	INT         m_nSizeY;						// height of image
	INT         m_nPosX;						// left offset of image
	INT         m_nPosY;						// right offset of image

												// memory needed for live display while using DIB
	INT         m_lMemoryId;					// camera memory - buffer ID
	char*       m_pcImageMemory;				// camera memory - pointer to buffer
	SENSORINFO  m_sInfo;						// sensor information struct
	INT         m_nRenderMode;					// render  mode
	INT         m_nFlipHor;						// horizontal flip flag
	INT         m_nFlipVert;					// vertical flip flag

	bool        bInited;
	bool        bRunning;
	
	int counter;
	//Construct
	uEyeCam();

	//Destruct
	~uEyeCam();

	//INI DEBUG: DEFINITION OF OpenCamera() WITHOUT WId parameter
	//bool OpenCamera(WId wid, HWND hWnd, int id, CStringA paramsfile);
	bool OpenCamera(HWND hWnd, int id, CStringA paramsfile);
	//END DEBUG: DEFINITION OF OpenCamera() WITHOUT WId parameter

	//INI DEBUG: Definition of InitAcquisition without WId parameter
	//bool InitAcquisition(WId wid);
	bool InitAcquisition(HWND hWnd);
	//END DEBUG: Definition of InitAcquisition without WId parameter
	
	bool FinalizeAcquisition();
	void ExitCamera();
	//bool CaptureFrame();
	//bool GetImage(IplImage * image);
	bool GetImage(uchar * image);
	CvSize GetImageSize();
	int GetImageBPP();
	int GetExposureTime();
	int  InitMemory();
	double GetFPS();
	bool GetStatus();
	bool GetButtonStatus();
	bool SwitchFlash(bool b);
	
	//void LoadParameters();

private:

	int InitCamera(HIDS *hCam, HWND hWnd);
	void GetMaxImageSize(INT *pnSizeX, INT *pnSizeY);
};

