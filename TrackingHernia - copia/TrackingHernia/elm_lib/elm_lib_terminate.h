/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * elm_lib_terminate.h
 *
 * Code generation for function 'elm_lib_terminate'
 *
 */

#ifndef ELM_LIB_TERMINATE_H
#define ELM_LIB_TERMINATE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "elm_lib_types.h"

/* Function Declarations */
extern void elm_lib_terminate();

#endif

/* End of code generation (elm_lib_terminate.h) */
