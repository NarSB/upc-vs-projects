/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * elm_create_values.h
 *
 * Code generation for function 'elm_create_values'
 *
 */

#ifndef ELM_CREATE_VALUES_H
#define ELM_CREATE_VALUES_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "elm_lib_types.h"

/* Function Declarations */
extern void elm_create_values(const emxArray_real_T *D, double n,
  emxArray_real_T *V);

#endif

/* End of code generation (elm_create_values.h) */
