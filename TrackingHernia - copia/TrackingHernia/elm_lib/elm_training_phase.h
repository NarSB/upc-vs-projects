/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * elm_training_phase.h
 *
 * Code generation for function 'elm_training_phase'
 *
 */

#ifndef ELM_TRAINING_PHASE_H
#define ELM_TRAINING_PHASE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "elm_lib_types.h"

/* Function Declarations */
extern void elm_training_phase(const emxArray_real_T *Values, const
  emxArray_real_T *Data, double regularization_coeff, double kernel_pars,
  emxArray_real_T *OutputWeight);

#endif

/* End of code generation (elm_training_phase.h) */
