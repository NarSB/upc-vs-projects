/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * rdivide.h
 *
 * Code generation for function 'rdivide'
 *
 */

#ifndef RDIVIDE_H
#define RDIVIDE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "elm_lib_types.h"

/* Function Declarations */
extern void rdivide(const emxArray_real_T *x, double y, emxArray_real_T *z);

#endif

/* End of code generation (rdivide.h) */
