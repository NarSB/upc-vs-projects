/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * elm_lib.cpp
 *
 * Code generation for function 'elm_lib'
 *
 */

/* Include files */
#include <cmath>
#include "rt_nonfinite.h"
#include "elm_lib.h"
#include "elm_lib_emxutil.h"
#include "stdafx.h"

/* Function Declarations */
static void b_exp(emxArray_real_T *x);
static void eml_signed_integer_colon(int b, emxArray_int32_T *y);
static void lusolve(const emxArray_real_T *A, emxArray_real_T *B);
static void power(const emxArray_real_T *a, emxArray_real_T *y);
static void rdivide(const emxArray_real_T *x, double y, emxArray_real_T *z);
static double rt_hypotd_snf(double u0, double u1);
static void sum(const emxArray_real_T *x, emxArray_real_T *y);
static void xgeqp3(emxArray_real_T *A, emxArray_real_T *tau, emxArray_int32_T
                   *jpvt);
static double xnrm2(int n, const emxArray_real_T *x, int ix0);
static void xscal(int n, double a, emxArray_real_T *x, int ix0);

/* Function Definitions */
static void b_exp(emxArray_real_T *x)
{
  int nx;
  int k;
  nx = x->size[0] * x->size[1];
  for (k = 0; k < nx; k++) {
    x->data[k] = std::exp(x->data[k]);
  }
}

static void eml_signed_integer_colon(int b, emxArray_int32_T *y)
{
  int n;
  int yk;
  int k;
  if (b < 1) {
    n = 0;
  } else {
    n = b;
  }

  yk = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = n;
  emxEnsureCapacity_int32_T(y, yk);
  if (n > 0) {
    y->data[0] = 1;
    yk = 1;
    for (k = 2; k <= n; k++) {
      yk++;
      y->data[k - 1] = yk;
    }
  }
}

static void lusolve(const emxArray_real_T *A, emxArray_real_T *B)
{
  emxArray_real_T *b_A;
  int n;
  int i1;
  int iy;
  emxArray_int32_T *ipiv;
  int u1;
  int j;
  int kAcol;
  int jA;
  int mmj;
  double smax;
  int c;
  int k;
  int ix;
  double s;
  int ijA;
  emxInit_real_T(&b_A, 2);
  n = A->size[1];
  i1 = b_A->size[0] * b_A->size[1];
  b_A->size[0] = A->size[0];
  b_A->size[1] = A->size[1];
  emxEnsureCapacity_real_T(b_A, i1);
  iy = A->size[0] * A->size[1];
  for (i1 = 0; i1 < iy; i1++) {
    b_A->data[i1] = A->data[i1];
  }

  emxInit_int32_T(&ipiv, 2);
  iy = A->size[1];
  eml_signed_integer_colon(iy, ipiv);
  if (!(A->size[1] < 1)) {
    iy = A->size[1] - 1;
    u1 = A->size[1];
    if (iy < u1) {
      u1 = iy;
    }

    for (j = 0; j < u1; j++) {
      mmj = n - j;
      c = j * (n + 1);
      if (mmj < 1) {
        jA = -1;
      } else {
        jA = 0;
        if (mmj > 1) {
          ix = c;
          smax = std::abs(b_A->data[c]);
          for (k = 2; k <= mmj; k++) {
            ix++;
            s = std::abs(b_A->data[ix]);
            if (s > smax) {
              jA = k - 1;
              smax = s;
            }
          }
        }
      }

      if (b_A->data[c + jA] != 0.0) {
        if (jA != 0) {
          ipiv->data[j] = (j + jA) + 1;
          ix = j;
          iy = j + jA;
          for (k = 1; k <= n; k++) {
            smax = b_A->data[ix];
            b_A->data[ix] = b_A->data[iy];
            b_A->data[iy] = smax;
            ix += n;
            iy += n;
          }
        }

        i1 = c + mmj;
        for (jA = c + 1; jA < i1; jA++) {
          b_A->data[jA] /= b_A->data[c];
        }
      }

      kAcol = n - j;
      jA = (c + n) + 1;
      iy = c + n;
      for (k = 1; k < kAcol; k++) {
        smax = b_A->data[iy];
        if (b_A->data[iy] != 0.0) {
          ix = c + 1;
          i1 = mmj + jA;
          for (ijA = jA; ijA < i1 - 1; ijA++) {
            b_A->data[ijA] += b_A->data[ix] * -smax;
            ix++;
          }
        }

        iy += n;
        jA += n;
      }
    }
  }

  for (iy = 0; iy < n - 1; iy++) {
    if (ipiv->data[iy] != iy + 1) {
      kAcol = ipiv->data[iy] - 1;
      for (jA = 0; jA < 3; jA++) {
        smax = B->data[iy + B->size[0] * jA];
        B->data[iy + B->size[0] * jA] = B->data[kAcol + B->size[0] * jA];
        B->data[kAcol + B->size[0] * jA] = smax;
      }
    }
  }

  emxFree_int32_T(&ipiv);
  if (B->size[0] != 0) {
    for (j = 0; j < 3; j++) {
      iy = n * j - 1;
      for (k = 1; k <= n; k++) {
        kAcol = n * (k - 1);
        if (B->data[k + iy] != 0.0) {
          for (jA = k + 1; jA <= n; jA++) {
            B->data[jA + iy] -= B->data[k + iy] * b_A->data[(jA + kAcol) - 1];
          }
        }
      }
    }
  }

  if (B->size[0] != 0) {
    for (j = 0; j < 3; j++) {
      iy = n * j - 1;
      for (k = n; k > 0; k--) {
        kAcol = n * (k - 1) - 1;
        if (B->data[k + iy] != 0.0) {
          B->data[k + iy] /= b_A->data[k + kAcol];
          for (jA = 1; jA < k; jA++) {
            B->data[jA + iy] -= B->data[k + iy] * b_A->data[jA + kAcol];
          }
        }
      }
    }
  }

  emxFree_real_T(&b_A);
}

static void power(const emxArray_real_T *a, emxArray_real_T *y)
{
  int nx;
  unsigned int a_idx_0;
  int k;
  nx = y->size[0] * y->size[1];
  y->size[0] = a->size[0];
  y->size[1] = 3;
  emxEnsureCapacity_real_T(y, nx);
  a_idx_0 = (unsigned int)a->size[0];
  nx = (int)a_idx_0 * 3;
  for (k = 0; k < nx; k++) {
    y->data[k] = a->data[k] * a->data[k];
  }
}

static void rdivide(const emxArray_real_T *x, double y, emxArray_real_T *z)
{
  int i0;
  int loop_ub;
  i0 = z->size[0] * z->size[1];
  z->size[0] = x->size[0];
  z->size[1] = x->size[1];
  emxEnsureCapacity_real_T(z, i0);
  loop_ub = x->size[0] * x->size[1];
  for (i0 = 0; i0 < loop_ub; i0++) {
    z->data[i0] = x->data[i0] / y;
  }
}

static double rt_hypotd_snf(double u0, double u1)
{
  double y;
  double a;
  double b;
  a = std::abs(u0);
  b = std::abs(u1);
  if (a < b) {
    a /= b;
    y = b * std::sqrt(a * a + 1.0);
  } else if (a > b) {
    b /= a;
    y = a * std::sqrt(b * b + 1.0);
  } else if (rtIsNaN(b)) {
    y = b;
  } else {
    y = a * 1.4142135623730951;
  }

  return y;
}

static void sum(const emxArray_real_T *x, emxArray_real_T *y)
{
  int vstride;
  int j;
  int k;
  int xoffset;
  if (x->size[0] == 0) {
    j = y->size[0];
    y->size[0] = 0;
    emxEnsureCapacity_real_T1(y, j);
  } else {
    vstride = x->size[0];
    j = y->size[0];
    y->size[0] = x->size[0];
    emxEnsureCapacity_real_T1(y, j);
    for (j = 0; j < vstride; j++) {
      y->data[j] = x->data[j];
    }

    for (k = 0; k < 2; k++) {
      xoffset = (k + 1) * vstride;
      for (j = 0; j < vstride; j++) {
        y->data[j] += x->data[xoffset + j];
      }
    }
  }
}

static void xgeqp3(emxArray_real_T *A, emxArray_real_T *tau, emxArray_int32_T
                   *jpvt)
{
  int m;
  int n;
  int itemp;
  int mn;
  int i2;
  emxArray_real_T *work;
  emxArray_real_T *vn1;
  emxArray_real_T *vn2;
  int k;
  int nmi;
  int i;
  int i_i;
  int mmi;
  int ix;
  int pvt;
  double smax;
  int iy;
  double atmp;
  double d0;
  double s;
  int i_ip1;
  int lastv;
  int lastc;
  boolean_T exitg2;
  int exitg1;
  m = A->size[0];
  n = A->size[1];
  itemp = A->size[0];
  mn = A->size[1];
  if (itemp < mn) {
    mn = itemp;
  }

  i2 = tau->size[0];
  tau->size[0] = mn;
  emxEnsureCapacity_real_T1(tau, i2);
  eml_signed_integer_colon(A->size[1], jpvt);
  if (!((A->size[0] == 0) || (A->size[1] == 0))) {
    emxInit_real_T1(&work, 1);
    itemp = A->size[1];
    i2 = work->size[0];
    work->size[0] = itemp;
    emxEnsureCapacity_real_T1(work, i2);
    for (i2 = 0; i2 < itemp; i2++) {
      work->data[i2] = 0.0;
    }

    emxInit_real_T1(&vn1, 1);
    emxInit_real_T1(&vn2, 1);
    itemp = A->size[1];
    i2 = vn1->size[0];
    vn1->size[0] = itemp;
    emxEnsureCapacity_real_T1(vn1, i2);
    i2 = vn2->size[0];
    vn2->size[0] = vn1->size[0];
    emxEnsureCapacity_real_T1(vn2, i2);
    k = 1;
    for (nmi = 0; nmi < n; nmi++) {
      vn1->data[nmi] = xnrm2(m, A, k);
      vn2->data[nmi] = vn1->data[nmi];
      k += m;
    }

    for (i = 0; i < mn; i++) {
      i_i = i + i * m;
      nmi = n - i;
      mmi = (m - i) - 1;
      if (nmi < 1) {
        itemp = -1;
      } else {
        itemp = 0;
        if (nmi > 1) {
          ix = i;
          smax = std::abs(vn1->data[i]);
          for (k = 0; k + 2 <= nmi; k++) {
            ix++;
            s = std::abs(vn1->data[ix]);
            if (s > smax) {
              itemp = k + 1;
              smax = s;
            }
          }
        }
      }

      pvt = i + itemp;
      if (pvt + 1 != i + 1) {
        ix = m * pvt;
        iy = m * i;
        for (k = 1; k <= m; k++) {
          smax = A->data[ix];
          A->data[ix] = A->data[iy];
          A->data[iy] = smax;
          ix++;
          iy++;
        }

        itemp = jpvt->data[pvt];
        jpvt->data[pvt] = jpvt->data[i];
        jpvt->data[i] = itemp;
        vn1->data[pvt] = vn1->data[i];
        vn2->data[pvt] = vn2->data[i];
      }

      if (i + 1 < m) {
        atmp = A->data[i_i];
        d0 = 0.0;
        if (!(1 + mmi <= 0)) {
          smax = xnrm2(mmi, A, i_i + 2);
          if (smax != 0.0) {
            s = rt_hypotd_snf(A->data[i_i], smax);
            if (A->data[i_i] >= 0.0) {
              s = -s;
            }

            if (std::abs(s) < 1.0020841800044864E-292) {
              itemp = 0;
              do {
                itemp++;
                xscal(mmi, 9.9792015476736E+291, A, i_i + 2);
                s *= 9.9792015476736E+291;
                atmp *= 9.9792015476736E+291;
              } while (!(std::abs(s) >= 1.0020841800044864E-292));

              s = rt_hypotd_snf(atmp, xnrm2(mmi, A, i_i + 2));
              if (atmp >= 0.0) {
                s = -s;
              }

              d0 = (s - atmp) / s;
              xscal(mmi, 1.0 / (atmp - s), A, i_i + 2);
              for (k = 1; k <= itemp; k++) {
                s *= 1.0020841800044864E-292;
              }

              atmp = s;
            } else {
              d0 = (s - A->data[i_i]) / s;
              smax = 1.0 / (A->data[i_i] - s);
              xscal(mmi, smax, A, i_i + 2);
              atmp = s;
            }
          }
        }

        tau->data[i] = d0;
        A->data[i_i] = atmp;
      } else {
        tau->data[i] = 0.0;
      }

      if (i + 1 < n) {
        atmp = A->data[i_i];
        A->data[i_i] = 1.0;
        i_ip1 = (i + (i + 1) * m) + 1;
        if (tau->data[i] != 0.0) {
          lastv = mmi;
          itemp = i_i + mmi;
          while ((lastv + 1 > 0) && (A->data[itemp] == 0.0)) {
            lastv--;
            itemp--;
          }

          lastc = nmi - 1;
          exitg2 = false;
          while ((!exitg2) && (lastc > 0)) {
            itemp = i_ip1 + (lastc - 1) * m;
            k = itemp;
            do {
              exitg1 = 0;
              if (k <= itemp + lastv) {
                if (A->data[k - 1] != 0.0) {
                  exitg1 = 1;
                } else {
                  k++;
                }
              } else {
                lastc--;
                exitg1 = 2;
              }
            } while (exitg1 == 0);

            if (exitg1 == 1) {
              exitg2 = true;
            }
          }
        } else {
          lastv = -1;
          lastc = 0;
        }

        if (lastv + 1 > 0) {
          if (lastc != 0) {
            for (iy = 1; iy <= lastc; iy++) {
              work->data[iy - 1] = 0.0;
            }

            iy = 0;
            i2 = i_ip1 + m * (lastc - 1);
            itemp = i_ip1;
            while ((m > 0) && (itemp <= i2)) {
              ix = i_i;
              smax = 0.0;
              pvt = itemp + lastv;
              for (k = itemp; k <= pvt; k++) {
                smax += A->data[k - 1] * A->data[ix];
                ix++;
              }

              work->data[iy] += smax;
              iy++;
              itemp += m;
            }
          }

          if (!(-tau->data[i] == 0.0)) {
            itemp = 0;
            for (nmi = 1; nmi <= lastc; nmi++) {
              if (work->data[itemp] != 0.0) {
                smax = work->data[itemp] * -tau->data[i];
                ix = i_i;
                i2 = lastv + i_ip1;
                for (pvt = i_ip1; pvt <= i2; pvt++) {
                  A->data[pvt - 1] += A->data[ix] * smax;
                  ix++;
                }
              }

              itemp++;
              i_ip1 += m;
            }
          }
        }

        A->data[i_i] = atmp;
      }

      for (nmi = i + 1; nmi < n; nmi++) {
        if (vn1->data[nmi] != 0.0) {
          smax = std::abs(A->data[i + A->size[0] * nmi]) / vn1->data[nmi];
          smax = 1.0 - smax * smax;
          if (smax < 0.0) {
            smax = 0.0;
          }

          s = vn1->data[nmi] / vn2->data[nmi];
          s = smax * (s * s);
          if (s <= 1.4901161193847656E-8) {
            if (i + 1 < m) {
              vn1->data[nmi] = xnrm2(mmi, A, (i + m * nmi) + 2);
              vn2->data[nmi] = vn1->data[nmi];
            } else {
              vn1->data[nmi] = 0.0;
              vn2->data[nmi] = 0.0;
            }
          } else {
            vn1->data[nmi] *= std::sqrt(smax);
          }
        }
      }
    }

    emxFree_real_T(&vn2);
    emxFree_real_T(&vn1);
    emxFree_real_T(&work);
  }
}

static double xnrm2(int n, const emxArray_real_T *x, int ix0)
{
  double y;
  double scale;
  int kend;
  int k;
  double absxk;
  double t;
  y = 0.0;
  if (!(n < 1)) {
    if (n == 1) {
      y = std::abs(x->data[ix0 - 1]);
    } else {
      scale = 3.3121686421112381E-170;
      kend = (ix0 + n) - 1;
      for (k = ix0; k <= kend; k++) {
        absxk = std::abs(x->data[k - 1]);
        if (absxk > scale) {
          t = scale / absxk;
          y = 1.0 + y * t * t;
          scale = absxk;
        } else {
          t = absxk / scale;
          y += t * t;
        }
      }

      y = scale * std::sqrt(y);
    }
  }

  return y;
}

static void xscal(int n, double a, emxArray_real_T *x, int ix0)
{
  int i3;
  int k;
  i3 = (ix0 + n) - 1;
  for (k = ix0; k <= i3; k++) {
    x->data[k - 1] *= a;
  }
}

void elm_RBF_Kernel_2_params(const emxArray_real_T *Xtrain, double kernel_pars,
  emxArray_real_T *omega)
{
  emxArray_real_T *XXh;
  emxArray_real_T *r6;
  emxArray_real_T *r7;
  int unnamed_idx_1;
  int i;
  int coffset;
  int boffset;
  emxArray_real_T *b;
  emxArray_real_T *y;
  int m;
  emxArray_real_T *b_XXh;
  int k;
  int aoffset;
  emxInit_real_T(&XXh, 2);
  emxInit_real_T(&r6, 2);
  emxInit_real_T1(&r7, 1);

  /* %%%%%%%%%%%%%%%%% Kernel Matrix %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  power(Xtrain, r6);
  sum(r6, r7);
  unnamed_idx_1 = Xtrain->size[0];
  i = XXh->size[0] * XXh->size[1];
  XXh->size[0] = r7->size[0];
  XXh->size[1] = unnamed_idx_1;
  emxEnsureCapacity_real_T(XXh, i);
  coffset = r7->size[0];
  emxFree_real_T(&r6);
  for (i = 0; i < coffset; i++) {
    for (boffset = 0; boffset < unnamed_idx_1; boffset++) {
      XXh->data[i + XXh->size[0] * boffset] = r7->data[i];
    }
  }

  emxFree_real_T(&r7);
  emxInit_real_T(&b, 2);
  i = b->size[0] * b->size[1];
  b->size[0] = 3;
  b->size[1] = Xtrain->size[0];
  emxEnsureCapacity_real_T(b, i);
  coffset = Xtrain->size[0];
  for (i = 0; i < coffset; i++) {
    for (boffset = 0; boffset < 3; boffset++) {
      b->data[boffset + b->size[0] * i] = Xtrain->data[i + Xtrain->size[0] *
        boffset];
    }
  }

  emxInit_real_T(&y, 2);
  m = Xtrain->size[0];
  i = y->size[0] * y->size[1];
  y->size[0] = Xtrain->size[0];
  y->size[1] = b->size[1];
  emxEnsureCapacity_real_T(y, i);
  for (unnamed_idx_1 = 0; unnamed_idx_1 < b->size[1]; unnamed_idx_1++) {
    coffset = unnamed_idx_1 * m - 1;
    boffset = unnamed_idx_1 * 3;
    for (i = 1; i <= m; i++) {
      y->data[coffset + i] = 0.0;
    }

    for (k = 0; k < 3; k++) {
      if (b->data[boffset + k] != 0.0) {
        aoffset = k * m;
        for (i = 1; i <= m; i++) {
          y->data[coffset + i] += b->data[boffset + k] * Xtrain->data[(aoffset +
            i) - 1];
        }
      }
    }
  }

  emxFree_real_T(&b);
  emxInit_real_T(&b_XXh, 2);
  i = b_XXh->size[0] * b_XXh->size[1];
  b_XXh->size[0] = XXh->size[0];
  b_XXh->size[1] = XXh->size[1];
  emxEnsureCapacity_real_T(b_XXh, i);
  coffset = XXh->size[1];
  for (i = 0; i < coffset; i++) {
    unnamed_idx_1 = XXh->size[0];
    for (boffset = 0; boffset < unnamed_idx_1; boffset++) {
      b_XXh->data[boffset + b_XXh->size[0] * i] = -((XXh->data[boffset +
        XXh->size[0] * i] + XXh->data[i + XXh->size[0] * boffset]) - 2.0 *
        y->data[boffset + y->size[0] * i]);
    }
  }

  emxFree_real_T(&y);
  emxFree_real_T(&XXh);
  rdivide(b_XXh, kernel_pars, omega);
  b_exp(omega);
  emxFree_real_T(&b_XXh);
}

void elm_RBF_Kernel_3_params(const emxArray_real_T *Xtrain, double kernel_pars,
  const emxArray_real_T *Xt, emxArray_real_T *omega)
{
  emxArray_real_T *y;
  int k;
  int boffset;
  emxArray_real_T *b;
  emxArray_real_T *r0;
  int i;
  int m;
  int j;
  int coffset;
  emxArray_real_T *r1;
  emxArray_real_T *r2;
  emxArray_real_T *r3;
  emxArray_real_T *r4;
  emxArray_real_T *r5;
  int aoffset;
  emxInit_real_T(&y, 2);

  /* %%%%%%%%%%%%%%%%% Kernel Matrix %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  k = y->size[0] * y->size[1];
  y->size[0] = Xtrain->size[0];
  y->size[1] = 3;
  emxEnsureCapacity_real_T(y, k);
  boffset = Xtrain->size[0] * Xtrain->size[1];
  for (k = 0; k < boffset; k++) {
    y->data[k] = 2.0 * Xtrain->data[k];
  }

  emxInit_real_T(&b, 2);
  k = b->size[0] * b->size[1];
  b->size[0] = 3;
  b->size[1] = Xt->size[0];
  emxEnsureCapacity_real_T(b, k);
  boffset = Xt->size[0];
  for (k = 0; k < boffset; k++) {
    for (i = 0; i < 3; i++) {
      b->data[i + b->size[0] * k] = Xt->data[k + Xt->size[0] * i];
    }
  }

  emxInit_real_T(&r0, 2);
  m = y->size[0];
  k = r0->size[0] * r0->size[1];
  r0->size[0] = y->size[0];
  r0->size[1] = b->size[1];
  emxEnsureCapacity_real_T(r0, k);
  for (j = 0; j < b->size[1]; j++) {
    coffset = j * m - 1;
    boffset = j * 3;
    for (i = 1; i <= m; i++) {
      r0->data[coffset + i] = 0.0;
    }

    for (k = 0; k < 3; k++) {
      if (b->data[boffset + k] != 0.0) {
        aoffset = k * m;
        for (i = 1; i <= m; i++) {
          r0->data[coffset + i] += b->data[boffset + k] * y->data[(aoffset + i)
            - 1];
        }
      }
    }
  }

  emxFree_real_T(&b);
  emxInit_real_T1(&r1, 1);
  emxInit_real_T1(&r2, 1);
  emxInit_real_T(&r3, 2);
  emxInit_real_T(&r4, 2);
  emxInit_real_T(&r5, 2);
  power(Xtrain, y);
  sum(y, r1);
  power(Xt, y);
  sum(y, r2);
  j = Xt->size[0];
  coffset = Xtrain->size[0];
  k = r4->size[0] * r4->size[1];
  r4->size[0] = r1->size[0];
  r4->size[1] = j;
  emxEnsureCapacity_real_T(r4, k);
  boffset = r1->size[0];
  emxFree_real_T(&y);
  for (k = 0; k < boffset; k++) {
    for (i = 0; i < j; i++) {
      r4->data[k + r4->size[0] * i] = r1->data[k];
    }
  }

  emxFree_real_T(&r1);
  k = r5->size[0] * r5->size[1];
  r5->size[0] = coffset;
  r5->size[1] = r2->size[0];
  emxEnsureCapacity_real_T(r5, k);
  for (k = 0; k < coffset; k++) {
    boffset = r2->size[0];
    for (i = 0; i < boffset; i++) {
      r5->data[k + r5->size[0] * i] = r2->data[i];
    }
  }

  emxFree_real_T(&r2);
  k = r3->size[0] * r3->size[1];
  r3->size[0] = r4->size[0];
  r3->size[1] = r4->size[1];
  emxEnsureCapacity_real_T(r3, k);
  boffset = r4->size[1];
  for (k = 0; k < boffset; k++) {
    j = r4->size[0];
    for (i = 0; i < j; i++) {
      r3->data[i + r3->size[0] * k] = -((r4->data[i + r4->size[0] * k] +
        r5->data[i + r5->size[0] * k]) - r0->data[i + r0->size[0] * k]);
    }
  }

  emxFree_real_T(&r5);
  emxFree_real_T(&r4);
  emxFree_real_T(&r0);
  rdivide(r3, kernel_pars, omega);
  b_exp(omega);
  emxFree_real_T(&r3);
}

void elm_create_values(const emxArray_real_T *D, double n, emxArray_real_T *V)
{
  int i;
  int loop_ub;

  /* %%%%%%%%%%%%%%%%%%%% create Values %%%%%%%%%%%%%%%%%%%%%%% */
  i = V->size[0] * V->size[1];
  V->size[0] = D->size[0];
  V->size[1] = (int)n;
  emxEnsureCapacity_real_T(V, i);
  loop_ub = D->size[0] * (int)n;
  for (i = 0; i < loop_ub; i++) {
    V->data[i] = -1.0;
  }

  for (i = 0; i < D->size[0]; i++) {
    V->data[i + V->size[0] * ((int)D->data[i] - 1)] = 1.0;
  }
}

void elm_lib_initialize()
{
  rt_InitInfAndNaN(8U);
}

void elm_lib_terminate()
{
  /* (no terminate code required) */
}

void elm_output(const emxArray_real_T *train, const emxArray_real_T *input,
                const emxArray_real_T *Weight, double kernel_para,
                emxArray_real_T *Output)
{
  emxArray_real_T *Omega_test;
  emxArray_real_T *a;
  int k;
  int loop_ub;
  int coffset;
  int boffset;
  int m;
  int inner;
  int i;
  int aoffset;
  emxInit_real_T(&Omega_test, 2);
  emxInit_real_T(&a, 2);

  /* %%%%%%%%% Testing phase %%%%%%%%%%%%%%%%%%%%%%%%%%% */
  elm_RBF_Kernel_3_params(train, kernel_para, input, Omega_test);
  k = a->size[0] * a->size[1];
  a->size[0] = Omega_test->size[1];
  a->size[1] = Omega_test->size[0];
  emxEnsureCapacity_real_T(a, k);
  loop_ub = Omega_test->size[0];
  for (k = 0; k < loop_ub; k++) {
    coffset = Omega_test->size[1];
    for (boffset = 0; boffset < coffset; boffset++) {
      a->data[boffset + a->size[0] * k] = Omega_test->data[k + Omega_test->size
        [0] * boffset];
    }
  }

  emxFree_real_T(&Omega_test);
  if ((a->size[1] == 1) || (Weight->size[0] == 1)) {
    k = Output->size[0] * Output->size[1];
    Output->size[0] = a->size[0];
    Output->size[1] = 3;
    emxEnsureCapacity_real_T(Output, k);
    loop_ub = a->size[0];
    for (k = 0; k < loop_ub; k++) {
      for (boffset = 0; boffset < 3; boffset++) {
        Output->data[k + Output->size[0] * boffset] = 0.0;
        coffset = a->size[1];
        for (i = 0; i < coffset; i++) {
          Output->data[k + Output->size[0] * boffset] += a->data[k + a->size[0] *
            i] * Weight->data[i + Weight->size[0] * boffset];
        }
      }
    }
  } else {
    m = a->size[0];
    inner = a->size[1];
    k = Output->size[0] * Output->size[1];
    Output->size[0] = a->size[0];
    Output->size[1] = 3;
    emxEnsureCapacity_real_T(Output, k);
    for (loop_ub = 0; loop_ub < 3; loop_ub++) {
      coffset = loop_ub * m - 1;
      boffset = loop_ub * inner - 1;
      for (i = 1; i <= m; i++) {
        Output->data[coffset + i] = 0.0;
      }

      for (k = 1; k <= inner; k++) {
        if (Weight->data[boffset + k] != 0.0) {
          aoffset = (k - 1) * m;
          for (i = 1; i <= m; i++) {
            Output->data[coffset + i] += Weight->data[boffset + k] * a->data
              [(aoffset + i) - 1];
          }
        }
      }
    }
  }

  emxFree_real_T(&a);
}

void elm_training_phase(const emxArray_real_T *Values, const emxArray_real_T
  *Data, double regularization_coeff, double kernel_pars, emxArray_real_T
  *OutputWeight)
{
  emxArray_real_T *Omega_train;
  emxArray_real_T *I;
  int maxmn;
  int minmn;
  emxArray_real_T *r8;
  emxArray_real_T *tau;
  emxArray_int32_T *jpvt;
  emxArray_real_T *B;
  unsigned int unnamed_idx_0;
  int rankR;
  double tol;
  int m;
  int mn;
  int i;
  emxInit_real_T(&Omega_train, 2);
  emxInit_real_T(&I, 2);

  /* %%%%%%%%%% Training Phase %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  elm_RBF_Kernel_2_params(Data, kernel_pars, Omega_train);
  maxmn = I->size[0] * I->size[1];
  I->size[0] = Values->size[0];
  I->size[1] = Values->size[0];
  emxEnsureCapacity_real_T(I, maxmn);
  minmn = Values->size[0] * Values->size[0];
  for (maxmn = 0; maxmn < minmn; maxmn++) {
    I->data[maxmn] = 0.0;
  }

  if (Values->size[0] > 0) {
    for (minmn = 0; minmn < Values->size[0]; minmn++) {
      I->data[minmn + I->size[0] * minmn] = 1.0;
    }
  }

  emxInit_real_T(&r8, 2);
  rdivide(I, regularization_coeff, r8);
  minmn = Omega_train->size[0] * Omega_train->size[1] - 1;
  maxmn = Omega_train->size[0] * Omega_train->size[1];
  emxEnsureCapacity_real_T(Omega_train, maxmn);
  emxFree_real_T(&I);
  for (maxmn = 0; maxmn <= minmn; maxmn++) {
    Omega_train->data[maxmn] += r8->data[maxmn];
  }

  emxFree_real_T(&r8);
  emxInit_real_T1(&tau, 1);
  emxInit_int32_T(&jpvt, 2);
  emxInit_real_T(&B, 2);
  if ((Omega_train->size[0] == 0) || (Omega_train->size[1] == 0) ||
      (Values->size[0] == 0)) {
    unnamed_idx_0 = (unsigned int)Omega_train->size[1];
    maxmn = OutputWeight->size[0] * OutputWeight->size[1];
    OutputWeight->size[0] = (int)unnamed_idx_0;
    OutputWeight->size[1] = 3;
    emxEnsureCapacity_real_T(OutputWeight, maxmn);
    minmn = (int)unnamed_idx_0 * 3;
    for (maxmn = 0; maxmn < minmn; maxmn++) {
      OutputWeight->data[maxmn] = 0.0;
    }
  } else if (Omega_train->size[0] == Omega_train->size[1]) {
    maxmn = OutputWeight->size[0] * OutputWeight->size[1];
    OutputWeight->size[0] = Values->size[0];
    OutputWeight->size[1] = 3;
    emxEnsureCapacity_real_T(OutputWeight, maxmn);
    minmn = Values->size[0] * Values->size[1];
    for (maxmn = 0; maxmn < minmn; maxmn++) {
      OutputWeight->data[maxmn] = Values->data[maxmn];
    }

    lusolve(Omega_train, OutputWeight);
  } else {
    xgeqp3(Omega_train, tau, jpvt);
    rankR = 0;
    if (Omega_train->size[0] < Omega_train->size[1]) {
      minmn = Omega_train->size[0];
      maxmn = Omega_train->size[1];
    } else {
      minmn = Omega_train->size[1];
      maxmn = Omega_train->size[0];
    }

    if (minmn > 0) {
      tol = (double)maxmn * std::abs(Omega_train->data[0]) *
        2.2204460492503131E-16;
      while ((rankR < minmn) && (!(std::abs(Omega_train->data[rankR +
                Omega_train->size[0] * rankR]) <= tol))) {
        rankR++;
      }
    }

    maxmn = B->size[0] * B->size[1];
    B->size[0] = Values->size[0];
    B->size[1] = 3;
    emxEnsureCapacity_real_T(B, maxmn);
    minmn = Values->size[0] * Values->size[1];
    for (maxmn = 0; maxmn < minmn; maxmn++) {
      B->data[maxmn] = Values->data[maxmn];
    }

    minmn = Omega_train->size[1];
    maxmn = OutputWeight->size[0] * OutputWeight->size[1];
    OutputWeight->size[0] = minmn;
    OutputWeight->size[1] = 3;
    emxEnsureCapacity_real_T(OutputWeight, maxmn);
    minmn *= 3;
    for (maxmn = 0; maxmn < minmn; maxmn++) {
      OutputWeight->data[maxmn] = 0.0;
    }

    m = Omega_train->size[0];
    minmn = Omega_train->size[0];
    mn = Omega_train->size[1];
    if (minmn < mn) {
      mn = minmn;
    }

    for (maxmn = 0; maxmn < mn; maxmn++) {
      if (tau->data[maxmn] != 0.0) {
        for (minmn = 0; minmn < 3; minmn++) {
          tol = B->data[maxmn + B->size[0] * minmn];
          for (i = maxmn + 1; i < m; i++) {
            tol += Omega_train->data[i + Omega_train->size[0] * maxmn] * B->
              data[i + B->size[0] * minmn];
          }

          tol *= tau->data[maxmn];
          if (tol != 0.0) {
            B->data[maxmn + B->size[0] * minmn] -= tol;
            for (i = maxmn + 1; i < m; i++) {
              B->data[i + B->size[0] * minmn] -= Omega_train->data[i +
                Omega_train->size[0] * maxmn] * tol;
            }
          }
        }
      }
    }

    for (minmn = 0; minmn < 3; minmn++) {
      for (i = 0; i < rankR; i++) {
        OutputWeight->data[(jpvt->data[i] + OutputWeight->size[0] * minmn) - 1] =
          B->data[i + B->size[0] * minmn];
      }

      for (maxmn = rankR - 1; maxmn + 1 > 0; maxmn--) {
        OutputWeight->data[(jpvt->data[maxmn] + OutputWeight->size[0] * minmn) -
          1] /= Omega_train->data[maxmn + Omega_train->size[0] * maxmn];
        for (i = 0; i < maxmn; i++) {
          OutputWeight->data[(jpvt->data[i] + OutputWeight->size[0] * minmn) - 1]
            -= OutputWeight->data[(jpvt->data[maxmn] + OutputWeight->size[0] *
            minmn) - 1] * Omega_train->data[i + Omega_train->size[0] * maxmn];
        }
      }
    }
  }

  emxFree_real_T(&B);
  emxFree_int32_T(&jpvt);
  emxFree_real_T(&tau);
  emxFree_real_T(&Omega_train);
}

/* End of code generation (elm_lib.cpp) */
