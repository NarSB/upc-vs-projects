/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * elm_lib.h
 *
 * Code generation for function 'elm_lib'
 *
 */

#ifndef ELM_LIB_H
#define ELM_LIB_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "elm_lib_types.h"
#include "elm_lib_emxutil.h"
#include "stdafx.h"

/* Function Declarations */
extern void elm_RBF_Kernel_2_params(const emxArray_real_T *Xtrain, double
  kernel_pars, emxArray_real_T *omega);
extern void elm_RBF_Kernel_3_params(const emxArray_real_T *Xtrain, double
  kernel_pars, const emxArray_real_T *Xt, emxArray_real_T *omega);
extern void elm_create_values(const emxArray_real_T *D, double n,
  emxArray_real_T *V);
extern void elm_lib_initialize();
extern void elm_lib_terminate();
extern void elm_output(const emxArray_real_T *train, const emxArray_real_T
  *input, const emxArray_real_T *Weight, double kernel_para, emxArray_real_T
  *Output);
extern void elm_training_phase(const emxArray_real_T *Values, const
  emxArray_real_T *Data, double regularization_coeff, double kernel_pars,
  emxArray_real_T *OutputWeight);

#endif

/* End of code generation (elm_lib.h) */
