/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * rdivide.cpp
 *
 * Code generation for function 'rdivide'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "elm_RBF_Kernel_2_params.h"
#include "elm_RBF_Kernel_3_params.h"
#include "elm_create_values.h"
#include "elm_output.h"
#include "elm_training_phase.h"
#include "rdivide.h"
#include "elm_lib_emxutil.h"

/* Function Definitions */
void rdivide(const emxArray_real_T *x, double y, emxArray_real_T *z)
{
  int i5;
  int loop_ub;
  i5 = z->size[0] * z->size[1];
  z->size[1] = x->size[1];
  z->size[0] = x->size[0];
  emxEnsureCapacity_real_T(z, i5);
  loop_ub = x->size[1] * x->size[0];
  for (i5 = 0; i5 < loop_ub; i5++) {
    z->data[i5] = x->data[i5] / y;
  }
}

/* End of code generation (rdivide.cpp) */
