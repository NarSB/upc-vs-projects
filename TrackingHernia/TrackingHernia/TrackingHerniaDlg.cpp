
// TrackingHerniaDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TrackingHernia.h"
#include "TrackingHerniaDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//Global Inlines
inline int modval(int a, int b) { return (a % b + b) % b; }

inline int evaluarMat(uint8_t * matrix, int r, int c, int ncols)
{
	if ((r < 0) || (c < 0) || (c >= ncols))
		return -1;
	return matrix[r * ncols + c];
}

inline float normDistancia(float x1, float y1, float x2, float y2)
{
	return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
}

inline float normCvPoint2f(cv::Point2f P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint2i(cv::Point2i P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint3f(cv::Point3f P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline float normCvPoint3i(cv::Point3i P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline int float2int(float x)
{
	return (int)(x + TOLERANCIA);
}

inline float FuncioSigne(float x)
{
	if (std::abs(x) < TOLERANCIA)
		return 0.0;
	else if (x > 0)
		return 1.0;
	else
		return -1.0;
}

inline void GetElapsedMiliseconds(_LARGE_INTEGER &ElapsedMiliseconds, _LARGE_INTEGER StartingTime, _LARGE_INTEGER Frequency)
{
	_LARGE_INTEGER Time;
	QueryPerformanceCounter(&Time);
	ElapsedMiliseconds.QuadPart = Time.QuadPart - StartingTime.QuadPart;
	ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
	ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
}

inline double EvaluateEllipse(ellipseParams & ellipse, cv::Point & point)
{
	return ellipse.a*point.x*point.x + ellipse.b*point.x*point.y + ellipse.c*point.y*point.y + ellipse.d*point.x + ellipse.e*point.y + ellipse.f;
}

inline double EvaluateEllipse(ellipseParams & ellipse, cv::Point2d & point)
{
	return ellipse.a*point.x*point.x + ellipse.b*point.x*point.y + ellipse.c*point.y*point.y + ellipse.d*point.x + ellipse.e*point.y + ellipse.f;
}

inline double SampsonDistance(ellipseParams & ellipse, cv::Point & point)
{
	return abs((double)ellipse.a*point.x*point.x + ellipse.b*point.x*point.y + ellipse.c*point.y*point.y + ellipse.d*point.x + ellipse.e*point.y + ellipse.f) / 2.0 / sqrt((ellipse.a*point.x + ellipse.b*point.y / 2.0 + ellipse.d / 2.0)*(ellipse.a*point.x + ellipse.b*point.y / 2.0 + ellipse.d / 2.0) + (ellipse.b*point.x / 2.0 + ellipse.c*point.y + ellipse.e / 2.0)*(ellipse.b*point.x / 2.0 + ellipse.c*point.y + ellipse.e / 2.0));
}

inline double SampsonDistance(ellipseParams & ellipse, cv::Point2d & point)
{
	return abs(ellipse.a*point.x*point.x + ellipse.b*point.x*point.y + ellipse.c*point.y*point.y + ellipse.d*point.x + ellipse.e*point.y + ellipse.f) / 2.0 / sqrt((ellipse.a*point.x + ellipse.b*point.y / 2.0 + ellipse.d / 2.0)*(ellipse.a*point.x + ellipse.b*point.y / 2.0 + ellipse.d / 2.0) + (ellipse.b*point.x / 2.0 + ellipse.c*point.y + ellipse.e / 2.0)*(ellipse.b*point.x / 2.0 + ellipse.c*point.y + ellipse.e / 2.0));
}

inline cv::Point2d GetCenterEllipse(ellipseParams & ellipse)
{
	cv::Point2d res;
	res.x = (2.0*ellipse.c*ellipse.d - ellipse.b*ellipse.e) / (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);
	res.y = (2.0*ellipse.a*ellipse.e - ellipse.b*ellipse.d) / (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);
	return res;
}

inline cv::Point2d GetSemiAxesEllipse(ellipseParams & ellipseP)
{
	cv::Point2d res;
	res.x = -sqrt(2.0*(ellipseP.a*ellipseP.e*ellipseP.e + ellipseP.c*ellipseP.d*ellipseP.d - ellipseP.b*ellipseP.d*ellipseP.e + (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c)*ellipseP.f)*((ellipseP.a + ellipseP.c) + sqrt((ellipseP.a - ellipseP.c)*(ellipseP.a - ellipseP.c) + ellipseP.b*ellipseP.b)));
	res.x /= (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c);
	res.y = -sqrt(2.0*(ellipseP.a*ellipseP.e*ellipseP.e + ellipseP.c*ellipseP.d*ellipseP.d - ellipseP.b*ellipseP.d*ellipseP.e + (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c)*ellipseP.f)*((ellipseP.a + ellipseP.c) - sqrt((ellipseP.a - ellipseP.c)*(ellipseP.a - ellipseP.c) + ellipseP.b*ellipseP.b)));
	res.y /= (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c);
	return res;
}

inline double GetAreaEllipse(ellipseParams & ellipse)
{
	cv::Point2d axis = GetSemiAxesEllipse(ellipse);
	return M_PI * axis.x*axis.y;
}

extern cudaError_t setup_gpu(int gpu_id, cublasHandle_t &handle);
extern void close_gpu(cublasHandle_t &handle);
extern cudaError_t up_to_gpu(double * input, double ** dev_output, int size);
extern cudaError_t down_from_gpu(double * dev_input, double * output, int size);
extern cudaError_t save_memory_gpu(double ** dev_memory, int size);
extern cudaError_t elm_create_values(double * input, double ** output, int size);
extern cudaError_t elm_training_phase(cublasHandle_t &handle, double * dev_data, double *dev_values, double ** dev_output, double **dev_ones, int size, double kernel_par, double reg_coeff);
extern cudaError_t elm_output(cublasHandle_t &handle, double * dev_data, double *dev_values, double * dev_weights, double * dev_onesD, double ** dev_output, int sizeD, int sizeV, double kernel_par);


double test(Eigen::VectorXd &x, Eigen::VectorXd &fvec, int nPoints, std::vector<cv::Point2i> &points, std::vector<int> &indOut)
{
	Eigen::Matrix2Xd PointsElli = Eigen::Matrix2Xd(2, nPoints);
	for (int i = 0; i < nPoints; i++)
	{
		PointsElli(0, i) = (double)(points[i].x);
		PointsElli(1, i) = (double)(points[i].y);
	}
	fvec(0) = 0.0;
	std::vector<distStruct> distV(0);
	for (int i = 0; i < nPoints; i++)
	{
		if (abs(PointsElli(0, i)) < 0.00001)
		{
			continue;
		}
		double s = abs(PointsElli(0, i)*PointsElli(0, i) + x(0)*PointsElli(0, i)*PointsElli(1, i) + x(1)*PointsElli(1, i)*PointsElli(1, i) + x(2)*PointsElli(0, i) + x(3)*PointsElli(1, i) + x(4)) / 2.0 / sqrt((PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0)*(PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0) + (x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0)*(x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0));
		distStruct ds;
		ds.ind = i;
		ds.dist = s;
		distV.push_back(ds);

	}
	std::sort(distV.begin(), distV.end(), [](distStruct a, distStruct b) { return a.dist < b.dist; });
	int indL = min(nPoints*PERCENTAGEPOINTSOULIERS, 0.9*distV.size());
	for (int i = 0; i < indL; i++)
	{
		double s = distV[i].dist;
		///x(0)*PointsElli(0, i)*PointsElli(0, i) + x(1)*PointsElli(0, i)*PointsElli(1, i) + x(2)*PointsElli(1, i)*PointsElli(1, i) + x(3)*PointsElli(0, i) + x(4)*PointsElli(1, i) + x(5);
		if (s < ELLIPSETHRESHOLD)
		{
			fvec(0) += s * s / 2.0;
		}
		else
		{
			fvec(0) += ELLIPSETHRESHOLD * s - ELLIPSETHRESHOLD * ELLIPSETHRESHOLD / 2.0;
		}
	}
	indOut.resize(0);
	for (int i = indL; i < distV.size(); i++)
	{
		indOut.push_back(distV[i].ind);
	}
	double disc = (x(0)*x(0) - 4.0*x(1));
	for (int i = 1; i < nPoints; i++)
	{
		if (disc < 0.0)
		{
			fvec(i) = 0.0;
		}
		else if (disc < 1.0)
		{
			fvec(i) = 100000.0 + 1000.0*disc;
		}
		else
		{
			fvec(i) = 100000.0 + 1000.0*disc*disc*disc;
		}
	}
	return fvec(0);
}

// CTrackingHerniaDlg diaclog

CTrackingHerniaDlg::CTrackingHerniaDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TRACKINGHERNIA_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTrackingHerniaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROC, m_butProc);
	DDX_Control(pDX, IDC_IMGINITIAL, m_picImageInitial);
	DDX_Control(pDX, IDC_IMGPROC, m_picImageProc);
}

BEGIN_MESSAGE_MAP(CTrackingHerniaDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_PROC, &CTrackingHerniaDlg::OnBnClickedProc)
	ON_BN_CLICKED(IDC_BUTTON2, &CTrackingHerniaDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDOK, &CTrackingHerniaDlg::OnBnClickedOk)
	ON_BN_CLICKED(BTN_GetPointsSynth, &CTrackingHerniaDlg::OnBnClickedGetpointssynth)
	ON_BN_CLICKED(BN_Synthtic, &CTrackingHerniaDlg::OnBnClickedSynthtic)
END_MESSAGE_MAP()


// CTrackingHerniaDlg message handlers

BOOL CTrackingHerniaDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	RECT r;
	m_picImageProc.GetClientRect(&r);
	winSegSizeProc = cv::Size(r.right, r.bottom);
	if (winSegSizeProc.width % 4 != 0)
	{
		winSegSizeProc.width = 4 * (winSegSizeProc.width / 4);
	}
	cvImgTmpProc = cv::Mat(winSegSizeProc, CV_8UC3);
	bitInfoProc.bmiHeader.biBitCount = 24;
	bitInfoProc.bmiHeader.biWidth = winSegSizeProc.width;
	bitInfoProc.bmiHeader.biHeight = winSegSizeProc.height;
	bitInfoProc.bmiHeader.biPlanes = 1;
	bitInfoProc.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfoProc.bmiHeader.biCompression = BI_RGB;
	bitInfoProc.bmiHeader.biClrImportant = 0;
	bitInfoProc.bmiHeader.biClrUsed = 0;
	bitInfoProc.bmiHeader.biSizeImage = 0;
	bitInfoProc.bmiHeader.biXPelsPerMeter = 0;
	bitInfoProc.bmiHeader.biYPelsPerMeter = 0;

	m_picImageInitial.GetClientRect(&r);
	winSegSizeIni = cv::Size(r.right, r.bottom);
	if (winSegSizeIni.width % 4 != 0)
	{
		winSegSizeIni.width = 4 * (winSegSizeIni.width / 4);
	}
	cvImgTmpIni = cv::Mat(winSegSizeIni, CV_8UC3);
	bitInfoIni.bmiHeader.biBitCount = 24;
	bitInfoIni.bmiHeader.biWidth = winSegSizeIni.width;
	bitInfoIni.bmiHeader.biHeight = winSegSizeIni.height;
	bitInfoIni.bmiHeader.biPlanes = 1;
	bitInfoIni.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfoIni.bmiHeader.biCompression = BI_RGB;
	bitInfoIni.bmiHeader.biClrImportant = 0;
	bitInfoIni.bmiHeader.biClrUsed = 0;
	bitInfoIni.bmiHeader.biSizeImage = 0;
	bitInfoIni.bmiHeader.biXPelsPerMeter = 0;
	bitInfoIni.bmiHeader.biYPelsPerMeter = 0;

	squareKernel = getStructuringElement(MORPH_RECT, Size(3, 3), Point(-1, -1));
	datacross[0][0] = 1; datacross[0][1] = 0; datacross[0][2] = 1; datacross[1][0] = 0; datacross[1][1] = 1; datacross[1][2] = 0; datacross[2][0] = 1; datacross[2][1] = 0; datacross[2][2] = 1;
	crossKernel = cv::Mat(3, 3, CV_8U, datacross);
	segerode = getStructuringElement(MORPH_CROSS, Size(3, 3), Point(-1, -1));
	segdilate = getStructuringElement(MORPH_RECT, Size(5, 5), Point(-1, -1));
	segClose = getStructuringElement(MORPH_ELLIPSE, Size(19, 19), Point(-1, -1));

	/*imgPath.Format(_T("G:\\Mi unidad\\Videos Fetal\\videos hernia\\hernia 11\\"));
	outPath.Format(_T("G:\\Mi unidad\\NACompartida\\Fetal\\Tracking\\Hernia\\ImgResults\\hernia 11\\"));*/

	imgPath.Format(_T("G:\\Mi unidad\\Videos Fetal\\videos hernia\\hernia 11\\"));
	//imgPath.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Videos\\SHEEPFRSB170401_2017-12-12_054209_VID001\\"));
	outPath.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Results\\hernia 11\\"));
	//outPath.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Results\\SHEEPFRSB170401_2017-12-12_054209_VID001\\"));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTrackingHerniaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTrackingHerniaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CTrackingHerniaDlg::SetInitialImage(cv::Mat & entrada)
{
	cv::Mat auxiliar;
	cv::cvtColor(entrada, auxiliar, cv::COLOR_GRAY2BGR);

	//If size does not match
	if (auxiliar.size() != winSegSizeIni)
	{
		cv::resize(auxiliar, cvImgTmpIni, winSegSizeIni);
	}
	else
	{
		cvImgTmpIni = auxiliar.clone();
	}


	//Rotate image
	cv::flip(cvImgTmpIni, cvImgTmpIni, 0);


	//Create MFC image
	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}

	mfcImg = new CImage();
	mfcImg->Create(winSegSizeIni.width, winSegSizeIni.height, 24);


	//Add header and OpenCV image to mfcImg
	StretchDIBits(mfcImg->GetDC(), 0, 0,
		winSegSizeIni.width, winSegSizeIni.height, 0, 0,
		winSegSizeIni.width, winSegSizeIni.height,
		cvImgTmpIni.data, &bitInfoIni, DIB_RGB_COLORS, SRCCOPY
	);

	//Display mfcImg in MFC window
	mfcImg->BitBlt(::GetDC(m_picImageInitial.m_hWnd), 0, 0);
}

void CTrackingHerniaDlg::SetProcImage(cv::Mat & entrada)
{
	cv::Mat auxiliar;
	//cv::cvtColor(entrada, auxiliar, cv::COLOR_GRAY2BGR);
	auxiliar = entrada.clone();

	
	//If size does not match
	if (auxiliar.size() != winSegSizeProc)
	{
		cv::resize(auxiliar, cvImgTmpProc, winSegSizeProc);
	}
	else
	{
		cvImgTmpProc = auxiliar.clone();
	}


	//Rotate image
	cv::flip(cvImgTmpProc, cvImgTmpProc, 0);


	//Create MFC image
	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}

	mfcImg = new CImage();
	mfcImg->Create(winSegSizeProc.width, winSegSizeProc.height, 24);


	//Add header and OpenCV image to mfcImg
	StretchDIBits(mfcImg->GetDC(), 0, 0,
		winSegSizeProc.width, winSegSizeProc.height, 0, 0,
		winSegSizeProc.width, winSegSizeProc.height,
		cvImgTmpProc.data, &bitInfoProc, DIB_RGB_COLORS, SRCCOPY
	);

	//Display mfcImg in MFC window
	mfcImg->BitBlt(::GetDC(m_picImageProc.m_hWnd), 0, 0);
}

inline cv::Mat CTrackingHerniaDlg::imCompositeFilter(cv::Mat &I)
{
	cv::Mat Aux1, Aux2;
	cv::morphologyEx(I, Aux1, MORPH_OPEN, crossKernel);
	cv::morphologyEx(Aux1, Aux1, MORPH_CLOSE, squareKernel);
	cv::morphologyEx(I, Aux2, MORPH_CLOSE, crossKernel);
	cv::morphologyEx(Aux2, Aux2, MORPH_OPEN, squareKernel);
	return (Aux1 / 2 + Aux2 / 2);
}

void CTrackingHerniaDlg::thinningIteration(cv::Mat& img, int iter)
{
	CV_Assert(img.channels() == 1);
	CV_Assert(img.depth() != sizeof(uchar));
	CV_Assert(img.rows > 3 && img.cols > 3);

	cv::Mat marker = cv::Mat::zeros(img.size(), CV_8UC1);

	int nRows = img.rows;
	int nCols = img.cols;

	if (img.isContinuous()) {
		nCols *= nRows;
		nRows = 1;
	}

	int x, y;
	uchar *pAbove;
	uchar *pCurr;
	uchar *pBelow;
	uchar *nw, *no, *ne;    // north (pAbove)
	uchar *we, *me, *ea;
	uchar *sw, *so, *se;    // south (pBelow)

	uchar *pDst;

	// initialize row pointers
	pAbove = NULL;
	pCurr = img.ptr<uchar>(0);
	pBelow = img.ptr<uchar>(1);

	for (y = 1; y < img.rows - 1; ++y) {
		// shift the rows up by one
		pAbove = pCurr;
		pCurr = pBelow;
		pBelow = img.ptr<uchar>(y + 1);

		pDst = marker.ptr<uchar>(y);

		// initialize col pointers
		no = &(pAbove[0]);
		ne = &(pAbove[1]);
		me = &(pCurr[0]);
		ea = &(pCurr[1]);
		so = &(pBelow[0]);
		se = &(pBelow[1]);

		for (x = 1; x < img.cols - 1; ++x) {
			// shift col pointers left by one (scan left to right)
			nw = no;
			no = ne;
			ne = &(pAbove[x + 1]);
			we = me;
			me = ea;
			ea = &(pCurr[x + 1]);
			sw = so;
			so = se;
			se = &(pBelow[x + 1]);

			int A = (*no == 0 && *ne == 1) + (*ne == 0 && *ea == 1) +
				(*ea == 0 && *se == 1) + (*se == 0 && *so == 1) +
				(*so == 0 && *sw == 1) + (*sw == 0 && *we == 1) +
				(*we == 0 && *nw == 1) + (*nw == 0 && *no == 1);
			int B = *no + *ne + *ea + *se + *so + *sw + *we + *nw;
			int m1 = iter == 0 ? (*no * *ea * *so) : (*no * *ea * *we);
			int m2 = iter == 0 ? (*ea * *so * *we) : (*no * *so * *we);

			if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
				pDst[x] = 1;
		}
	}

	img &= ~marker;
}

void CTrackingHerniaDlg::ZhangSuen(const cv::Mat& src, cv::Mat& dst)
{
	dst = src.clone();
	dst /= 255;         // convert to binary image

	cv::Mat prev = cv::Mat::zeros(dst.size(), CV_8UC1);
	cv::Mat diff;

	int times = 0;

	do {
		times++;
		thinningIteration(dst, 0);
		thinningIteration(dst, 1);
		cv::absdiff(dst, prev, diff);
		dst.copyTo(prev);
	} while ((cv::countNonZero(diff) > 0) && (times < 6));

	dst *= 255;
}

void CTrackingHerniaDlg::SegmentacioImatge(cv::Mat &entrada, cv::Mat &sortida)
{

	cv::Mat auxiliar, auxiliar2;


	auxiliar = entrada.clone();
	auxiliar = auxiliar(cv::Rect(segcoli, segrowi, segcolsize, segrowsize));
	cv::medianBlur(auxiliar, auxiliar2, 11); //Median Filter of window 5x5

	auxiliar = imCompositeFilter(auxiliar);

	//cannyPF(auxiliar, 5, 130.0, auxiliar2);
	cv::Mat filteredImage;
	cv::GaussianBlur(auxiliar, filteredImage, cv::Size(11, 11), 4.0);
	auxiliar2.copyTo(filteredImage);
	//grayImage.release();

	//get gradient map and orientation map
	cv::Mat gradientMap = cv::Mat::zeros(filteredImage.rows, filteredImage.cols, CV_32FC1);
	cv::Mat dx(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));
	cv::Mat dy(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));

	int apertureSize = 3;
	cv::Sobel(filteredImage, dx, CV_16S, 1, 0, apertureSize, 1, 0, cv::BORDER_REPLICATE);
	cv::Sobel(filteredImage, dy, CV_16S, 0, 1, apertureSize, 1, 0, cv::BORDER_REPLICATE);
	//calculate gradient and orientation
	int totalNum = 0;
	int times = 8;
	float thGradientLow = 2.0;// 1.3333;
	std::vector<int> histogram(200, 0);
	float maxValue = 0.0;
	for (int i = 0; i < dx.rows; ++i)
	{
		float *ptrG = gradientMap.ptr<float>(i);
		short *ptrX = dx.ptr<short>(i);
		short *ptrY = dy.ptr<short>(i);
		for (int j = 0; j < dx.cols; ++j)
		{
			float gx = ptrX[j];
			float gy = ptrY[j];

			ptrG[j] = sqrt(gx*gx + gy * gy);
			if (ptrG[j] < thGradientLow)
				ptrG[j] = 0.0;
			else
			{
				if (ptrG[j] > maxValue)
					maxValue = ptrG[j];
			}
		}
	}

	//CString nameImage;
	//CString pathD = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\RegistreElipse\\RegistreElipse\\Imatges\\Process\\";
	//nameImage.Format("Imatge_Canny.png");
	//nameImage = pathD + nameImage;
	//imwrite(nameImage.GetString(), gradientMap);

	int totalHist = 0;
	for (int i = 0; i < dx.rows; ++i)
	{
		float *ptrG = gradientMap.ptr<float>(i);
		for (int j = 0; j < dx.cols; ++j)
		{
			if (fabs(ptrG[j] - maxValue) < 0.001*maxValue)
			{
				histogram[199]++;
				totalHist++;
			}
			else if (ptrG[j] > 0.0)
			{
				histogram[(int)(ptrG[j] / maxValue * 200.0)]++;
				totalHist++;
			}
		}
	}

	int sumi = 0;
	float wb = 0.0;
	float wf;
	int sumb = 0;
	float between, maxBetween = 0.0;
	float mf;
	for (int i = 0; i < 200; i++)
	{
		sumi += i * histogram[i];
	}

	float levelOtsu = 0.0f;
	for (int i = 0; i < 200; i++)
	{
		wb += histogram[i];
		wf = totalHist - wb;
		if (wb == 0 || wf == 0)
			continue;
		sumb += i * histogram[i];
		mf = (float)(sumi - sumb) / (float)wf;
		between = wb * wf*((float)sumb / (float)wb - mf)*((float)sumb / (float)wb - mf);
		if (between >= maxBetween)
		{
			levelOtsu = i;
			maxBetween = between;
		}
	}

	double thres_val = 1.0 * levelOtsu / 200.0f * maxValue;//cv::threshold(gradientMap, auxiliar2, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	Canny(dx, dy, auxiliar2, 0.05/6.0*thres_val, 1.0*thres_val, true); //1.0 / 12.0*thres_val, thres_val, true);

	cv::morphologyEx(auxiliar2, auxiliar2, MORPH_CLOSE, squareKernel, cv::Point(-1, -1), 2);
	cv::dilate(auxiliar2, auxiliar, segdilate);
	cv::erode(auxiliar, auxiliar2, segerode);



	//Filter small blobs
	cv::filterSpeckles(auxiliar2, 0, 1000, 0);

	ZhangSuen(auxiliar2, auxiliar2);

	sortida = cv::Mat::zeros(entrada.rows, entrada.cols, auxiliar2.type());
	auxiliar2.copyTo(sortida(cv::Rect(segcoli, segrowi, segcolsize, segrowsize)));
}

int CTrackingHerniaDlg::creixementRegioContorn(cv::Mat & imatge, cv::Mat & mascara, int x, int y, int lbl, std::vector <cv::Point2i> & points)
{
	std::vector<cv::Point2i> cua;
	cv::Point2i pt = cv::Point2i(x, y);
	points.clear();
	points.push_back(pt);
	cua.push_back(pt);
	uint8_t *imData = imatge.data;
	uint8_t *masData = mascara.data;
	int valInicial = evaluarMat(imData, y, x, imatge.cols);
	//int DD[2][4] = { { -1, 0, 1, 0 },{ 0, -1, 0, 1 } };
	int DD[2][8] = { { -1, -1, -1, 0, 1, 1, 1, 0 },{ -1, 0, 1, 1, 1, 0, -1, -1 } };
	int nPunts = 1;
	int trobatlbl, trobatblanc;
	masData[y * mascara.cols + x] = lbl;
	while (cua.size() > 0)
	{
		pt = cua.back();
		cua.pop_back();
		for (int i = 0; i < 8; ++i)
		{
			if ((pt.x + DD[0][i] > -1) && (pt.y + DD[1][i] > -1) && (pt.x + DD[0][i] < mascara.cols) && (pt.y + DD[1][i] < mascara.rows))
			{
				if ((evaluarMat(imData, pt.y + DD[1][i], pt.x + DD[0][i], imatge.cols) == valInicial) && (evaluarMat(masData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) == 0))
				{
					if ((evaluarMat(imData, pt.y + DD[1][modval(i + 1, 8)], pt.x + DD[0][modval(i + 1, 8)], imatge.cols) != valInicial) && (evaluarMat(imData, pt.y + DD[1][modval(i - 1, 8)], pt.x + DD[0][modval(i - 1, 8)], imatge.cols) == valInicial))
					{
						cv::Point2i ptr = cv::Point2i(pt.x + DD[0][i], pt.y + DD[1][i]);
						cua.push_back(ptr);
						masData[(pt.y + DD[1][i]) * imatge.cols + (pt.x + DD[0][i])] = lbl;
						points.push_back(ptr);
						nPunts++;
					}
					else if ((evaluarMat(imData, pt.y + DD[1][modval(i - 1, 8)], pt.x + DD[0][modval(i - 1, 8)], imatge.cols) != valInicial) && (evaluarMat(imData, pt.y + DD[1][modval(i + 1, 8)], pt.x + DD[0][modval(i + 1, 8)], imatge.cols) == valInicial))
					{
						cv::Point2i ptr = cv::Point2i(pt.x + DD[0][i], pt.y + DD[1][i]);
						cua.push_back(ptr);
						masData[(pt.y + DD[1][i]) * imatge.cols + (pt.x + DD[0][i])] = lbl;
						points.push_back(ptr);
						nPunts++;
					}
				}
			}
		}
	}
	return nPunts;
}

int CTrackingHerniaDlg::creixementRegio(cv::Mat & imatge, cv::Mat & mascara, int x, int y, int lbl, std::vector <cv::Point2i> & points)
{
	std::vector<cv::Point2i> cua;
	cv::Point2i pt = cv::Point2i(x, y);
	points.clear();
	points.push_back(pt);
	cua.push_back(pt);
	uint8_t *imData = imatge.data;
	uint8_t *masData = mascara.data;
	int valInicial = evaluarMat(imData, y, x, imatge.cols);
	//int DD[2][4] = { { -1, 0, 1, 0 },{ 0, -1, 0, 1 } };
	int DD[2][8] = { { -1, -1, -1, 0, 1, 1, 1, 0 },{ -1, 0, 1, 1, 1, 0, -1, -1 } };
	int nPunts = 1;
	int trobatlbl, trobatblanc;
	masData[y * mascara.cols + x] = lbl;
	while (cua.size() > 0)
	{
		pt = cua.back();
		cua.pop_back();
		for (int i = 0; i < 8; ++i)
		{
			if ((pt.x + DD[0][i] > -1) && (pt.y + DD[1][i] > -1) && (pt.x + DD[0][i] < mascara.cols) && (pt.y + DD[1][i] < mascara.rows))
			{
				if ((evaluarMat(imData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) == valInicial) && (evaluarMat(masData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) == 0))
				{
					if (((evaluarMat(imData, (pt.y + DD[1][modval(i + 1, 8)]), (pt.x + DD[0][modval(i + 1, 8)]), imatge.cols) == valInicial) && (evaluarMat(imData, (pt.y + DD[1][modval(i - 1, 8)]), (pt.x + DD[0][modval(i - 1, 8)]), imatge.cols) != valInicial)) || ((evaluarMat(imData, (pt.y + DD[1][modval(i + 1, 8)]), (pt.x + DD[0][modval(i + 1, 8)]), imatge.cols) != valInicial) && (evaluarMat(imData, (pt.y + DD[1][modval(i - 1, 8)]), (pt.x + DD[0][modval(i - 1, 8)]), imatge.cols) == valInicial)))
					{
						cv::Point2i ptr = cv::Point2i(pt.x + DD[0][i], pt.y + DD[1][i]);
						cua.push_back(ptr);
						masData[(pt.y + DD[1][i]) * imatge.cols + (pt.x + DD[0][i])] = lbl;
						points.push_back(ptr);
						nPunts++;
					}
					else
					{
						trobatlbl = 0;
						trobatblanc = 0;
						for (int j = 0; j < 8; j++)
						{
							if (evaluarMat(masData, (pt.y + DD[1][i] + DD[1][j]), (pt.x + DD[0][i] + DD[0][j]), imatge.cols) == lbl)
							{
								trobatlbl++;
							}
							else if (evaluarMat(imData, (pt.y + DD[1][i] + DD[1][j]), (pt.x + DD[0][i] + DD[0][j]), imatge.cols) != valInicial)
							{
								trobatblanc++;
							}
							if ((trobatblanc > 0) && (trobatlbl > 1))
								break;
						}
						if ((trobatblanc > 0) && (trobatlbl > 1))
						{
							cv::Point2i ptr = cv::Point2i(pt.x + DD[0][i], pt.y + DD[1][i]);
							cua.push_back(ptr);
							masData[(pt.y + DD[1][i]) * imatge.cols + (pt.x + DD[0][i])] = lbl;
							points.push_back(ptr);
							nPunts++;
						}
					}
				}
			}
		}
	}
	return nPunts;
}

void CTrackingHerniaDlg::trobarPuntsElipse(cv::Mat &imatge, std::vector<cv::Point2i> &punts, int nPunts, int rowInici, int colInici)
{
	cv::Mat imatgeAux = imatge.clone();
	uint8_t *imData = imatgeAux.data;
	cv::Mat mascara = cv::Mat::zeros(imatgeAux.rows, imatgeAux.cols, CV_8UC1);
	uint8_t *masData = mascara.data;
	std::vector<cv::Point2i> points = std::vector<cv::Point2i>();
	std::vector<cv::Point2i> pointsaux = std::vector<cv::Point2i>();
	int rows = imatgeAux.rows;
	int cols = imatgeAux.cols;
	int val, vmas;

	int rowC = rowInici;
	int colC = colInici;
	if ((rowC < segrowi) || (rowC > segrowi + segrowsize))
	{
		rowC = imatgeAux.rows / 2;
	}
	if ((colC < segcoli) || (colC > segcoli + segcolsize))
	{
		colC = imatgeAux.cols / 2;
	}

	int valInicial = evaluarMat(imData, rowC, colC, cols);
	int valBuscant;
	bool buscant;
	int indBusc;
	int nSeg;

	//Buscarem en la direccions (1,0)
	buscant = true;
	indBusc = colC;
	while (buscant)
	{
		indBusc--;
		valBuscant = evaluarMat(imData, rowC, indBusc, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegioContorn(imatgeAux, mascara, indBusc + 1, rowC, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				buscant = false;
			}
		}
		if ((indBusc - colC > 200) || (indBusc + 1 >= cols))
		{
			buscant = false;
		}

	}
	/*cv::Mat imgWhat;
	cv::cvtColor(imatge, imgWhat, cv::COLOR_GRAY2BGR);

	uint8_t *imDataW = imgWhat.data;

	for (int k = 0; k < points.size(); k++)
	{
		for (int ii = -2; ii < 3; ii++)
		{
			for (int jj = -2; jj < 3; jj++)
			{
				if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= imgWhat.cols - 2) || (points[k].y + ii >= imgWhat.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
				{
					continue;
				}

				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 1] = 255;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 2] = 0;
			}
		}
	}
	int nsizeant = points.size();*/


	//Buscarem en la direccions (-1,0)
	/*buscant = true;
	indBusc = colC;
	while (buscant)
	{
		indBusc--;
		valBuscant = evaluarMat(imData, rowC, indBusc, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegioContorn(imatgeAux, mascara, indBusc + 1, rowC, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				buscant = false;
			}
		}
		if ((colC - indBusc > 200) || (indBusc < 1))
		{
			buscant = false;
		}

	}*/

	/*for (int k = nsizeant; k < points.size(); k++)
	{
		for (int ii = -2; ii < 3; ii++)
		{
			for (int jj = -2; jj < 3; jj++)
			{
				if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= imgWhat.cols - 2) || (points[k].y + ii >= imgWhat.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
				{
					continue;
				}

				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 1] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 2] = 255;
			}
		}
	}
	nsizeant = points.size();*/




	//Buscarem en la direccions (0,1)
	/*buscant = true;
	indBusc = rowC;
	while (buscant)
	{
		indBusc++;
		valBuscant = evaluarMat(imData, indBusc, colC, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegioContorn(imatgeAux, mascara, colC, indBusc - 1, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				buscant = false;
			}
		}
		if ((indBusc - rowC > 200) || (indBusc + 1 >= rows))
		{
			buscant = false;
		}

	}*/

	/*for (int k = nsizeant; k < points.size(); k++)
	{
		for (int ii = -2; ii < 3; ii++)
		{
			for (int jj = -2; jj < 3; jj++)
			{
				if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= imgWhat.cols - 2) || (points[k].y + ii >= imgWhat.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
				{
					continue;
				}

				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 1] = 255;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 2] = 255;
			}
		}
	}
	nsizeant = points.size();*/




	//Buscarem en la direccions (0, -1)
	/*buscant = true;
	indBusc = rowC;
	while (buscant)
	{
		indBusc--;
		valBuscant = evaluarMat(imData, indBusc, colC, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegioContorn(imatgeAux, mascara, colC, indBusc + 1, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				buscant = false;
			}
		}
		if ((rowC - indBusc > 200) || (indBusc < 1))
		{
			buscant = false;
		}

	}*/

	/*for (int k = nsizeant; k < points.size(); k++)
	{
		for (int ii = -2; ii < 3; ii++)
		{
			for (int jj = -2; jj < 3; jj++)
			{
				if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= imgWhat.cols - 2) || (points[k].y + ii >= imgWhat.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
				{
					continue;
				}

				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3] = 255;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 1] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 2] = 255;
			}
		}
	}
	nsizeant = points.size();*/



	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	punts = std::vector<cv::Point2i>(nPunts);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPunts);
	std::vector<float> rmaxP(nPunts);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPunts; i++)
	{
		rmaxP[i] = 0.0f;// 100.0f*(cols + rows) * (cols + rows);
	}

	meanVal.x = (float)colC;
	meanVal.y = (float)rowC;

	/*for (int i = 0; i < points.size(); ++i)
	{
		meanVal.x += (float)points[i].x;
		meanVal.y += (float)points[i].y;
	}
	meanVal /= (float)points.size();*/

	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPunts);
		if (indEx >= nPunts)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] < raux)
		{
			rmaxP[indEx] = raux;
			(punts)[indEx].x = points[i].x;
			(punts)[indEx].y = points[i].y;
		}
	}
	cv::Point2f centerElipse(0.0f, 0.0f);
	int esborrats = 0;
	for (int i = 0; i < punts.size(); i++)
	{
		if ((punts[i].x == 0) || (punts[i].y == 0))
		{
			esborrats++;
		}
		else if ((normCvPoint2i(punts[i] - punts[modval(i + 1, punts.size())]) > DISTENTREPUNTS) && (normCvPoint2i(punts[i] - punts[modval(i - 1, punts.size())]) > DISTENTREPUNTS))
		{
			punts[i].x = 0;
			punts[i].y = 0;
			esborrats++;
		}
		else
		{
			centerElipse.x = centerElipse.x + punts[i].x;
			centerElipse.y = centerElipse.y + punts[i].y;
		}
	}
	centerElipse /= (float)(punts.size() - esborrats);

	//Zona mean contorn
	if (numSegm < 1)
	{
		if (numSegm == 0)
		{
			meanSegmentats[0] = centerElipse;
		}
		numSegm++;

	}
	else
	{
		if (normCvPoint2f(meanSegmentats[0] - centerElipse) < DISTANCIACENTREELIPSE)
		{
			meanSegmentats[0] = centerElipse;
		}
	}
}


void CTrackingHerniaDlg::trobarPuntsElipseHSV(cv::Mat &imatge, std::vector<cv::Point2i> &punts, int nPunts, int rowInici, int colInici)
{
	cv::Mat imatgeAux = imatge.clone();
	uint8_t *imData = imatgeAux.data;
	cv::Mat mascara = cv::Mat::zeros(imatgeAux.rows, imatgeAux.cols, CV_8UC1);
	uint8_t *masData = mascara.data;
	std::vector<cv::Point2i> points = std::vector<cv::Point2i>();
	std::vector<cv::Point2i> pointsaux = std::vector<cv::Point2i>();
	int rows = imatgeAux.rows;
	int cols = imatgeAux.cols;
	int val, vmas;

	int rowC = rowInici;
	int colC = colInici;
	if ((rowC < segrowi) || (rowC > segrowi + segrowsize))
	{
		rowC = imatgeAux.rows / 2;
	}
	if ((colC < segcoli) || (colC > segcoli + segcolsize))
	{
		colC = imatgeAux.cols / 2;
	}

	int valInicial = evaluarMat(imData, rowC, colC, cols);
	int valBuscant;
	bool buscant;
	int indBusc;
	int nSeg;

	//Buscarem en la direccions (1,0)
	buscant = true;
	indBusc = colC;
	while (buscant)
	{
		indBusc--;
		valBuscant = evaluarMat(imData, rowC, indBusc, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegioContorn(imatgeAux, mascara, indBusc + 1, rowC, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				buscant = false;
			}
		}
		if ((indBusc - colC > 200) || (indBusc + 1 >= cols))
		{
			buscant = false;
		}

	}
	/*cv::Mat imgWhat;
	cv::cvtColor(imatge, imgWhat, cv::COLOR_GRAY2BGR);

	uint8_t *imDataW = imgWhat.data;

	for (int k = 0; k < points.size(); k++)
	{
		for (int ii = -2; ii < 3; ii++)
		{
			for (int jj = -2; jj < 3; jj++)
			{
				if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= imgWhat.cols - 2) || (points[k].y + ii >= imgWhat.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
				{
					continue;
				}

				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 1] = 255;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 2] = 0;
			}
		}
	}
	int nsizeant = points.size();*/


	//Buscarem en la direccions (-1,0)
	/*buscant = true;
	indBusc = colC;
	while (buscant)
	{
		indBusc--;
		valBuscant = evaluarMat(imData, rowC, indBusc, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegioContorn(imatgeAux, mascara, indBusc + 1, rowC, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				buscant = false;
			}
		}
		if ((colC - indBusc > 200) || (indBusc < 1))
		{
			buscant = false;
		}

	}*/

	/*for (int k = nsizeant; k < points.size(); k++)
	{
		for (int ii = -2; ii < 3; ii++)
		{
			for (int jj = -2; jj < 3; jj++)
			{
				if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= imgWhat.cols - 2) || (points[k].y + ii >= imgWhat.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
				{
					continue;
				}

				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 1] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 2] = 255;
			}
		}
	}
	nsizeant = points.size();*/




	//Buscarem en la direccions (0,1)
	/*buscant = true;
	indBusc = rowC;
	while (buscant)
	{
		indBusc++;
		valBuscant = evaluarMat(imData, indBusc, colC, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegioContorn(imatgeAux, mascara, colC, indBusc - 1, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				buscant = false;
			}
		}
		if ((indBusc - rowC > 200) || (indBusc + 1 >= rows))
		{
			buscant = false;
		}

	}*/

	/*for (int k = nsizeant; k < points.size(); k++)
	{
		for (int ii = -2; ii < 3; ii++)
		{
			for (int jj = -2; jj < 3; jj++)
			{
				if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= imgWhat.cols - 2) || (points[k].y + ii >= imgWhat.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
				{
					continue;
				}

				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 1] = 255;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 2] = 255;
			}
		}
	}
	nsizeant = points.size();*/




	//Buscarem en la direccions (0, -1)
	/*buscant = true;
	indBusc = rowC;
	while (buscant)
	{
		indBusc--;
		valBuscant = evaluarMat(imData, indBusc, colC, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegioContorn(imatgeAux, mascara, colC, indBusc + 1, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
				buscant = false;
			}
		}
		if ((rowC - indBusc > 200) || (indBusc < 1))
		{
			buscant = false;
		}

	}*/

	/*for (int k = nsizeant; k < points.size(); k++)
	{
		for (int ii = -2; ii < 3; ii++)
		{
			for (int jj = -2; jj < 3; jj++)
			{
				if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= imgWhat.cols - 2) || (points[k].y + ii >= imgWhat.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
				{
					continue;
				}

				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3] = 255;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 1] = 0;
				imDataW[((points[k].y + ii) * imgWhat.cols + points[k].x + jj) * 3 + 2] = 255;
			}
		}
	}
	nsizeant = points.size();*/



	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	punts = std::vector<cv::Point2i>(nPunts);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPunts);
	std::vector<float> rmaxP(nPunts);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPunts; i++)
	{
		rmaxP[i] = 0.0f;// 100.0f*(cols + rows) * (cols + rows);
	}

	meanVal.x = (float)colC;
	meanVal.y = (float)rowC;

	/*for (int i = 0; i < points.size(); ++i)
	{
		meanVal.x += (float)points[i].x;
		meanVal.y += (float)points[i].y;
	}
	meanVal /= (float)points.size();*/

	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPunts);
		if (indEx >= nPunts)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] < raux)
		{
			rmaxP[indEx] = raux;
			(punts)[indEx].x = points[i].x;
			(punts)[indEx].y = points[i].y;
		}
	}
	cv::Point2f centerElipse(0.0f, 0.0f);
	int esborrats = 0;
	for (int i = 0; i < punts.size(); i++)
	{
		if ((punts[i].x == 0) || (punts[i].y == 0))
		{
			esborrats++;
		}
		else if ((normCvPoint2i(punts[i] - punts[modval(i + 1, punts.size())]) > DISTENTREPUNTS) && (normCvPoint2i(punts[i] - punts[modval(i - 1, punts.size())]) > DISTENTREPUNTS))
		{
			punts[i].x = 0;
			punts[i].y = 0;
			esborrats++;
		}
		else
		{
			centerElipse.x = centerElipse.x + punts[i].x;
			centerElipse.y = centerElipse.y + punts[i].y;
		}
	}
	centerElipse /= (float)(punts.size() - esborrats);

	//Zona mean contorn
	if (numSegm < 1)
	{
		if (numSegm == 0)
		{
			meanSegmentats[0] = centerElipse;
		}
		numSegm++;

	}
	else
	{
		if (normCvPoint2f(meanSegmentats[0] - centerElipse) < DISTANCIACENTREELIPSE)
		{
			meanSegmentats[0] = centerElipse;
		}
	}
}

void CTrackingHerniaDlg::OnBnClickedProc()
{
	_LARGE_INTEGER StartingTime, StepTime, EndingTime, ElapsedMiliseconds1, ElapsedMiliseconds2;
	LARGE_INTEGER Frequency;
	CString strText;
	CStdioFile fileTime;
	CStdioFile fileEllipseData;
	CStdioFile filePoints;

	fileTime.Open(_T("./times.txt"), CFile::modeCreate | CFile::modeWrite);

	fileEllipseData.Open(_T("./ellipsedata_synthetic_red_per_defecte.txt"), CFile::modeCreate | CFile::modeWrite);

	strText.Format(_T("Image\tEDa\tEDb\tEDc\tEDd\tEDe\tEDf\tEDcx\tEDcy\tEDsa\tEDsb\tEDAr\tELSMa\tELSMb\tELSMc\tELSMd\tELSMe\tELSMf\tELSMcx\tELSMcy\tELSMsa\tELSMsb\tELSMAr\tEPCAa\tEPCAb\tEPCAc\tEPCAd\tEPCAe\tEPCAf\tEPCAcx\tEPCAcy\tEPCAsa\tEPCAsb\tEPCAAr\n"));
	fileEllipseData.WriteString(strText);

	filePoints.Open(_T("./PointsSynth.txt"), CFile::modeCreate | CFile::modeWrite);
	
	//ELM Part
	CStdioFile training_file, weights_file;
	CString elmtxt;
	//weights_file.Open(_T("./weightsHerniaClassificationVideoReal.txt"), CFile::modeRead);
	weights_file.Open(_T("./weightsHerniaClassificationPhantom.txt"), CFile::modeRead);
	
	weights_file.ReadString(elmtxt);
	int nweights = _ttoi(elmtxt);

	double * weights = new double[3* nweights];

	for (int i = 0; i < nweights; i++)
	{
		weights_file.ReadString(elmtxt);
		int curPos = 0;
		CString strToken = elmtxt.Tokenize(_T("\t"), curPos);
		weights[i] = _ttof(strToken);
		for(int j = 1; j < 3; j++)
		{
			strToken = elmtxt.Tokenize(_T("\t"), curPos);
			weights[j*nweights + i] = _ttof(strToken);
		}	
	}
	weights_file.Close();

	training_file.Open(_T("./trainingPhantom.txt"), CFile::modeRead);
	//training_file.Open(_T("./trainingVideoReal.txt"), CFile::modeRead);

	training_file.ReadString(elmtxt);
	int ntrain = _ttoi(elmtxt);

	double * train_data = new double[3 * ntrain];

	for (int i = 0; i < ntrain; i++)
	{
		training_file.ReadString(elmtxt);
		int curPos = 0;
		CString strToken = elmtxt.Tokenize(_T("\t"), curPos);
		strToken = elmtxt.Tokenize(_T("\t"), curPos);
		train_data[3*i] = _ttof(strToken);
		for (int j = 1; j < 3; j++)
		{
			strToken = elmtxt.Tokenize(_T("\t"), curPos);
			train_data[3*i + j] = _ttof(strToken);
		}
	}
	training_file.Close();

	cublasHandle_t handle;
	double * dev_weights = 0;
	double * dev_train_data = 0;
	double * dev_onesTrain = 0;
	int gpu_id = 0;

	setup_gpu(gpu_id, handle);
	cudaError_t cudaError = up_to_gpu(weights, &dev_weights, 3 * ntrain);
	cudaError = up_to_gpu(train_data, &dev_train_data, 3 * ntrain);

	double * onesData = new double[ntrain];

	auto tmp = onesData;
	for (int i = 0; i < ntrain; i++)
	{
		*tmp++ = 1.0;
	}
	cudaError = up_to_gpu(onesData, &dev_onesTrain, ntrain);

	//cv::Mat src;
	int imgIni = 936;
	int imgEnd = 1481;
	CString pathInFile;
	CString fileBaseNameIn, fileBaseNameOut;
	fileBaseNameIn.Format(_T("Image_"));
	fileBaseNameOut.Format(_T("image_Seq3_75_n11_2color_"));

	for (int i = imgIni; i < imgEnd + 1; i++)
	{
		if (i == 947)
		{
			int aiii = 0;
		}
		QueryPerformanceFrequency(&Frequency);
		QueryPerformanceCounter(&StartingTime);

		if (i < 10)
		{
			pathInFile.Format(_T("00000%d.png"), i);
		}
		else if (i < 100)
		{
			pathInFile.Format(_T("0000%d.png"), i);
		}
		else if (i < 1000)
		{
			pathInFile.Format(_T("000%d.png"), i);
		}
		else if (i < 10000)
		{
			pathInFile.Format(_T("00%d.png"), i);
		}
		else if (i < 100000)
		{
			pathInFile.Format(_T("0%d.png"), i);
		}
		else
		{
			pathInFile.Format(_T("%d.png"), i);
		}
		
		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiString(imgPath + fileBaseNameIn + pathInFile);

		// construct a std::string using the LPCSTR input
		std::string strStd(pszConvertedAnsiString);


		cv::Mat srcColor = cv::imread(strStd, IMREAD_COLOR);
		
		cv::Mat srcHSV, outBin;
		cv::cvtColor(srcColor, srcHSV, cv::COLOR_BGR2HSV);
		Binaritzacion(srcHSV, outBin);


		segrowi = outBin.rows / 4;
		segcoli = outBin.cols / 4;
		segrowsize = outBin.rows / 2;
		segcolsize = outBin.cols / 2;


		SetInitialImage(outBin);



		std::vector<cv::Point> points;

		if (numSegm == 0)
		{
			int rinici, cinici;
			rinici = 550;
			cinici = 850;
			meanSegmentats.clear();
			meanSegmentats.resize(1);
			meanSegmentats[0].x = outBin.cols / 2 - 50;
			meanSegmentats[0].y = outBin.rows / 2;
			trobarPuntsElipseHSV(outBin, points, 100, rinici, cinici);
			lastEllipse.a = 1.00; //1.0;
			lastEllipse.b = -0.34; // 0.0;
			lastEllipse.c = 0.94;// 1.0;
			lastEllipse.d = -1586.17;// -1920.0;
			lastEllipse.e = -675.21;// -1080.0;
			lastEllipse.f = 865507.01;// 1203200.0;

			ellipseParams elliaux;
			elliaux.a = 1.0; //1.0;
			elliaux.b = -0.36; // 0.0;
			elliaux.c = 0.94;// 1.0;
			elliaux.d = -2.0*896.0;// -1920.0;
			elliaux.e = -2.0*600.0;// -1080.0;
			elliaux.f = 896.0*896.0 + 600.0*600.0 - 146.0*146.0;// 1203200.0;
			lastEllipse = GetEllipseMixNormN2(points, elliaux);
			cv::Point2d p = GetCenterEllipse(lastEllipse);
			double a = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) + sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			a /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			double b = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) - sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			b /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			int r = (int)min(max(abs(a), abs(b)), outBin.rows / 5.0);
			points = GetPointsUsingLastEllipseHSV(outBin, lastEllipse, (int)p.x - r - ELLIPSETHRESHOLD, (int)p.y - r - ELLIPSETHRESHOLD, 2 * (r + ELLIPSETHRESHOLD), 2 * (r + ELLIPSETHRESHOLD), 100);
		}
		else
		{
			cv::Point2d p = GetCenterEllipse(lastEllipse);
			double a = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) + sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			a /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			double b = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) - sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			b /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			int r = (int)min(max(abs(a), abs(b)), outBin.rows / 5.0);
			points = GetPointsUsingLastEllipseCompleteHSV(outBin, lastEllipse, lastPoints, (int)p.x - r - ELLIPSETHRESHOLD, (int)p.y - r - ELLIPSETHRESHOLD, 2 * (r + ELLIPSETHRESHOLD), 2 * (r + ELLIPSETHRESHOLD), 100);
		}

		//points = EraseToolFromPoints(points, srcColor);
		int nAD = 121;
		int nminC, nmaxC;
		nminC = -5;
		nmaxC = 5;
		double * pointsData = new double[nAD*3* points.size()];
		double * dev_points = 0;
		double * dev_pointsclas = 0;
		uint8_t *imDataI = srcColor.data;
		int AD[2][121];

		
		for (int iAD = nminC; iAD < nmaxC + 1; iAD++)
		{
			for (int jAD = nminC; jAD < nmaxC + 1; jAD++)
			{
				AD[0][11 * (iAD - nminC) + (jAD - nminC)] = iAD;
				AD[1][11 * (iAD - nminC) + (jAD - nminC)] = jAD;
			}
		}

		for (int k = 0; k < points.size(); k++)
		{
			for (int ii = 0; ii < nAD; ii++)
			{
				if ((points[k].x + AD[0][ii] == 0) || (points[k].y + AD[1][ii] == 0) || (points[k].x + AD[0][ii] >= srcColor.cols - 2) || (points[k].y + AD[1][ii] >= srcColor.rows - 2) || (points[k].x + AD[0][ii] < 2) || (points[k].y + AD[1][ii] < 2))
				{
					pointsData[nAD * 3 * k + 3 * ii] = 0.0;
					pointsData[nAD * 3 * k + 3 * ii + 1] = 0.0;
					pointsData[nAD * 3 * k + 3 * ii + 2] = 0.0;
					continue;
				}
				pointsData[nAD * 3 * k + 3 * ii + 2] = imDataI[((points[k].y + AD[1][ii]) * srcColor.cols + points[k].x + AD[0][ii]) * 3];
				pointsData[nAD * 3 * k + 3 * ii + 1] = imDataI[((points[k].y + AD[1][ii]) * srcColor.cols + points[k].x + AD[0][ii]) * 3 + 1];
				pointsData[nAD * 3 * k + 3 * ii] = imDataI[((points[k].y + AD[1][ii]) * srcColor.cols + points[k].x + AD[0][ii]) * 3 + 2];
			}
		}
		up_to_gpu(pointsData, &dev_points, nAD*3*points.size());
		cudaError = elm_output(handle, dev_train_data, dev_points, dev_weights, dev_onesTrain, &dev_pointsclas, ntrain, nAD*(int)points.size(), 100.0);
		double * pointsclas = new double[nAD*3 * points.size()];
		down_from_gpu(dev_pointsclas, pointsclas, nAD *3 * points.size());
		points = EraseToolFromPoints(points, pointsclas,nAD);

		if (dev_points) cudaFree(dev_points);
		if (dev_pointsclas) cudaFree(dev_pointsclas);
		delete[] pointsData;
		delete[] pointsclas;


		cv::Mat srcSegColor;
		cv::cvtColor(outBin, srcSegColor, cv::COLOR_GRAY2BGR);//srcSegColor, cv::COLOR_GRAY2BGR);

		uint8_t *imData = srcColor.data;
		for (int k = 0; k < points.size(); k++)
		{
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= srcSegColor.cols - 2) || (points[k].y + ii >= srcSegColor.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
					{
						continue;
					}
					if (ii*ii + jj * jj <= 9)
					{
						imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3] = 0;
						imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 1] = 0;
						imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 2] = 0;
					}
				}
			}
		}

		ellipseParams elliN1, elliN12, elliN22, elliLMS, elliPCA;

		std::vector<cv::Point> pointsClean(0);

		strText.Format(_T("%d "),i);

		for (int i = 0; i < points.size(); i++)
		{
			if (points[i].x == 0 || points[i].y == 0)
			{
				continue;
			}
			cv::Point pp;
			pp.x = points[i].x;
			pp.y = points[i].y;
			pointsClean.push_back(pp);

			CString auxPoint;
			auxPoint.Format(_T("%d %d "), pp.x, pp.y);
			strText += auxPoint;

		}

		strText += _T("\n");
		filePoints.WriteString(strText);

		elliN1 = GetEllipseMixNorm(pointsClean, lastEllipse);
		elliLMS = GetEllipseLMS(pointsClean);
		elliPCA = GetEllipsePCA(pointsClean);
		lastEllipse = elliN1;
		lastPoints.resize(0);
		std::copy(points.begin(), points.end(), back_inserter(lastPoints));
		outBin.copyTo(last_seg_image);
		paintEllipse(elliN1, srcColor, 0, 0, 255);
		paintEllipse(elliLMS, srcColor, 255, 0, 0);
		//paintEllipse(elliPCA, srcColor, 0, 255, 0);

		cv::Point2d cN1 = GetCenterEllipse(elliN1);
		cv::Point2d cLMS = GetCenterEllipse(elliLMS);
		cv::Point2d cPCA = GetCenterEllipse(elliPCA);

		cv::Point2d axN1 = GetSemiAxesEllipse(elliN1);
		cv::Point2d axLMS = GetSemiAxesEllipse(elliLMS);
		cv::Point2d axPCA = GetSemiAxesEllipse(elliPCA);

		double ArN1 = GetAreaEllipse(elliN1);
		double ArLMS = GetAreaEllipse(elliLMS);
		double ArPCA = GetAreaEllipse(elliPCA);

		strText.Format(_T("%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n"), i,elliN1.a,elliN1.b, elliN1.c, elliN1.d, elliN1.e, elliN1.f, cN1.x,cN1.y,axN1.x,axN1.y,ArN1, elliLMS.a, elliLMS.b, elliLMS.c, elliLMS.d, elliLMS.e, elliLMS.f, cLMS.x, cLMS.y, axLMS.x, axLMS.y, ArLMS, elliPCA.a, elliPCA.b, elliPCA.c, elliPCA.d, elliPCA.e, elliPCA.f, cPCA.x, cPCA.y, axPCA.x, axPCA.y, ArPCA);
		fileEllipseData.WriteString(strText);


		Eigen::VectorXd x = Eigen::VectorXd(5);
		x << lastEllipse.b, lastEllipse.c, lastEllipse.d, lastEllipse.e, lastEllipse.f;
		Eigen::VectorXd fvec = Eigen::VectorXd(points.size());
		std::vector<int> indOut;
		test(x, fvec, points.size(), points, indOut);

		/*for (int k = 0; k < points.size(); k++)
		{
			bool outpoint = false;
			for (int kk = 0; kk < indOut.size(); kk++)
			{
				if (indOut[kk] == k)
				{
					outpoint = true;
					break;
				}
			}
			if(!outpoint)
			{
				continue;
			}
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= srcSegColor.cols - 2) || (points[k].y + ii >= srcSegColor.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
					{
						continue;
					}

					imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3] = 0;
					imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 1] = 0;
					imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 2] = 0;
				}
			}
		}

		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((cN1.x + jj >= srcSegColor.cols - 2) || (cN1.y + ii >= srcSegColor.rows - 2) || (cN1.x + jj < 2) || (cN1.y + ii < 2))
				{
					continue;
				}

				imData[(((int)cN1.y + ii) * srcSegColor.cols + (int)cN1.x + jj) * 3] = 255;
				imData[(((int)cN1.y + ii) * srcSegColor.cols + (int)cN1.x + jj) * 3 + 1] = 50;
				imData[(((int)cN1.y + ii) * srcSegColor.cols + (int)cN1.x + jj) * 3 + 2] = 50;
			}
		}*/

		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiStringOut(outPath + fileBaseNameOut + pathInFile);
		CString fileSeg;
		fileSeg.Format(_T("Segm_.png"), i);
		CT2CA pszConvertedAnsiStringOutSeg(outPath + fileBaseNameOut + fileSeg + pathInFile);

		// construct a std::string using the LPCSTR input
		std::string strStdOut(pszConvertedAnsiStringOut);
		std::string strStdOutSeg(pszConvertedAnsiStringOutSeg);

		cv::imwrite(strStdOut, srcColor);
		cv::imwrite(strStdOutSeg, outBin);
		SetProcImage(srcColor);

	}
	fileTime.Close();
	fileEllipseData.Close();
	close_gpu(handle);
	delete[] weights;
	delete[] train_data;
	delete[] onesData;
	if (dev_weights) cudaFree(dev_weights);
	if (dev_train_data) cudaFree(dev_train_data);
	if (dev_onesTrain) cudaFree(dev_onesTrain);
}



/*		Mat channel[3];
		split(srcColor, channel);
		cv::Mat src;
		channel[1].copyTo(src);
		//cv::Mat src = cv::imread(strStd, IMREAD_GRAYSCALE);




		segrowi = src.rows / 4;
		segcoli = src.cols / 4;
		segrowsize = src.rows / 2;
		segcolsize = src.cols / 2;




		cv::Mat srcSeg;
		SegmentacioImatge(src, srcSeg);
		SetInitialImage(srcSeg);



		std::vector<cv::Point> points;
		
		if (numSegm == 0)
		{
			int rinici, cinici;
			rinici = 550;
			cinici = 850;
			meanSegmentats.clear();
			meanSegmentats.resize(1);
			meanSegmentats[0].x = srcSeg.cols / 2 - 50;
			meanSegmentats[0].y = srcSeg.rows / 2;
			trobarPuntsElipse(srcSeg,points, 100, rinici, cinici);
			lastEllipse.a = 1.00; //1.0;
			lastEllipse.b = -0.34; // 0.0;
			lastEllipse.c = 0.94;// 1.0;
			lastEllipse.d = -1586.17;// -1920.0;
			lastEllipse.e = -675.21;// -1080.0;
			lastEllipse.f = 865507.01;// 1203200.0;

			ellipseParams elliaux;
			elliaux.a = 1.0; //1.0;
			elliaux.b = -0.36; // 0.0;
			elliaux.c = 0.94;// 1.0;
			elliaux.d = -2.0*896.0;// -1920.0;
			elliaux.e = -2.0*600.0;// -1080.0;
			elliaux.f = 896.0*896.0+600.0*600.0 - 146.0*146.0;// 1203200.0;
			lastEllipse = GetEllipseMixNormN2(points, elliaux);
			cv::Point2d p = GetCenterEllipse(lastEllipse);
			double a = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) + sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			a /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			double b = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) - sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			b /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			int r = (int)min(max(abs(a), abs(b)), src.rows / 5.0);
			points = GetPointsUsingLastEllipse(srcSeg, lastEllipse, (int)p.x - r - ELLIPSETHRESHOLD, (int)p.y - r - ELLIPSETHRESHOLD, 2 * (r + ELLIPSETHRESHOLD), 2 * (r + ELLIPSETHRESHOLD), 100);
		}
		else
		{
			cv::Point2d p = GetCenterEllipse(lastEllipse);
			double a = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) + sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			a /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			double b = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) - sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			b /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			int r = (int)min(max(abs(a), abs(b)),src.rows / 5.0);
			points = GetPointsUsingLastEllipse(srcSeg, lastEllipse, lastPoints, (int)p.x - r - ELLIPSETHRESHOLD, (int)p.y - r - ELLIPSETHRESHOLD, 2 * (r + ELLIPSETHRESHOLD), 2 * (r + ELLIPSETHRESHOLD), 100);
		}

		GetElapsedMiliseconds(ElapsedMiliseconds1, StartingTime, Frequency);
		strText.Format(_T("Temps TrobPunts:\t%d\t%d\n"), ElapsedMiliseconds1.QuadPart, ElapsedMiliseconds1.QuadPart);
		fileTime.WriteString(strText);

		points = EraseToolFromPoints(points, srcColor);

		GetElapsedMiliseconds(ElapsedMiliseconds2, StartingTime, Frequency);
		strText.Format(_T("Temps EraseTool:\t%d\t%d\n"), ElapsedMiliseconds2.QuadPart - ElapsedMiliseconds1.QuadPart, ElapsedMiliseconds2.QuadPart);
		fileTime.WriteString(strText);

		///INI DEBUG:Afegir punts dolents per mirar com funciona
		points.push_back(cv::Point(916, 517));
		points.push_back(cv::Point(1252, 414));
		points.push_back(cv::Point(810,306));
		points.push_back(cv::Point(1020,289));
		points.push_back(cv::Point(849,499));
		points.push_back(cv::Point(747,711));
		points.push_back(cv::Point(500,650));
		points.push_back(cv::Point(777,333));
		points.push_back(cv::Point(1140,780));
		points.push_back(cv::Point(585,450));
		///END DEBUG:Fins aqu�

		cv::Mat srcSegColor;
		cv::cvtColor(srcSeg, srcSegColor, cv::COLOR_GRAY2BGR);//srcSegColor, cv::COLOR_GRAY2BGR);

		uint8_t *imData = srcColor.data;

		for (int k = 0; k < points.size(); k++)
		{
			for (int ii = -1; ii < 2; ii++)
			{
				for (int jj = -1; jj < 2; jj++)
				{
					if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= srcSegColor.cols - 2) || (points[k].y + ii >= srcSegColor.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
					{
						continue;
					}

					imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3] = 0;
					imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 1] = 0;
					imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 2] = 255;
				}
			}
		}

		GetElapsedMiliseconds(ElapsedMiliseconds1, StartingTime, Frequency);
		strText.Format(_T("Temps PintarPunts:\t%d\t%d\n"), ElapsedMiliseconds1.QuadPart - ElapsedMiliseconds2.QuadPart, ElapsedMiliseconds1.QuadPart);
		fileTime.WriteString(strText);
		
		ellipseParams elliN2 = GetEllipse(points);
		double nN2 = elliN2.a;
		elliN2.a /= nN2;
		elliN2.b /= nN2;
		elliN2.c /= nN2;
		elliN2.d /= nN2;
		elliN2.e /= nN2;
		elliN2.f /= nN2;

		GetElapsedMiliseconds(ElapsedMiliseconds2, StartingTime, Frequency);
		strText.Format(_T("Temps EllipseL2:\t%d\t%d\n"), ElapsedMiliseconds2.QuadPart - ElapsedMiliseconds1.QuadPart, ElapsedMiliseconds2.QuadPart);
		fileTime.WriteString(strText);

		TRACE("Disc: %.2lf\n", elliN2.b + elliN2.b - 4.0*elliN2.a*elliN2.c);
		ellipseParams elliN1, elliN12, elliN22;
		if (elliN2.b*elliN2.b - 4.0*elliN2.a*elliN2.c > 0.0)
		{
			ellipseParams elliaux;
			elliaux.a = 1.00; //1.0;
			elliaux.b = -0.34; // 0.0;
			elliaux.c = 0.94;// 1.0;
			elliaux.d = -1586.17;// -1920.0;
			elliaux.e = -675.21;// -1080.0;
			elliaux.f = 865507.01;// 1203200.0;
			elliN1 = GetEllipseMixNorm(points, elliaux);
			//elliN12 = GetEllipseMixNorm2(points, elliaux);
			//elliN22 = GetEllipseMixNormN2(points, elliaux);
		}
		else
		{
			elliN1 = GetEllipseMixNorm(points, lastEllipse);
			//elliN12 = GetEllipseMixNorm2(points, elliN2);
			//elliN22 = GetEllipseMixNormN2(points, elliN2);
		}


		elliN1 = GetEllipseMixNorm(points, lastEllipse);
		lastEllipse = elliN1;
		lastPoints.resize(0);
		std::copy(points.begin(), points.end(), back_inserter(lastPoints));

		GetElapsedMiliseconds(ElapsedMiliseconds1, StartingTime, Frequency);
		strText.Format(_T("Temps EllipseL1:\t%d\t%d\n"), ElapsedMiliseconds1.QuadPart - ElapsedMiliseconds2.QuadPart, ElapsedMiliseconds1.QuadPart);
		fileTime.WriteString(strText);

		//paintEllipse(elliN2, srcSegColor, 0, 255, 0);
		paintEllipse(elliN1, srcColor, 0, 0, 255);
		//paintEllipse(elliN1, srcColor, 0, 0, 255);
		//paintEllipse(elliN12, srcSegColor, 0, 255, 255);
		//paintEllipse(elliN22, srcSegColor, 128, 128, 255);

		//cv::Point2d cN2 = GetCenterEllipse(elliN2);
		cv::Point2d cN1 = GetCenterEllipse(elliN1);
		//cv::Point2d cN12 = GetCenterEllipse(elliN12);
		//cv::Point2d cN22 = GetCenterEllipse(elliN22);

		GetElapsedMiliseconds(ElapsedMiliseconds2, StartingTime, Frequency);
		strText.Format(_T("Temps PinEllipse:\t%d\t%d\n"), ElapsedMiliseconds2.QuadPart - ElapsedMiliseconds1.QuadPart, ElapsedMiliseconds2.QuadPart);
		fileTime.WriteString(strText);
		

		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((cN2.x + jj >= srcSegColor.cols - 2) || (cN2.y + ii >= srcSegColor.rows - 2) || (cN2.x + jj < 2) || (cN2.y + ii < 2))
				{
					continue;
				}

				imData[(((int)cN2.y + ii) * srcSegColor.cols + (int)cN2.x + jj) * 3] = 50;
				imData[(((int)cN2.y + ii) * srcSegColor.cols + (int)cN2.x + jj) * 3 + 1] = 255;
				imData[(((int)cN2.y + ii) * srcSegColor.cols + (int)cN2.x + jj) * 3 + 2] = 50;
			}
		}

		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((cN1.x + jj >= srcSegColor.cols - 2) || (cN1.y + ii >= srcSegColor.rows - 2) || (cN1.x + jj < 2) || (cN1.y + ii < 2))
				{
					continue;
				}

				imData[(((int)cN1.y + ii) * srcSegColor.cols + (int)cN1.x + jj) * 3] = 255;
				imData[(((int)cN1.y + ii) * srcSegColor.cols + (int)cN1.x + jj) * 3 + 1] = 50;
				imData[(((int)cN1.y + ii) * srcSegColor.cols + (int)cN1.x + jj) * 3 + 2] = 50;
			}
		}

		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((cN12.x + jj >= srcSegColor.cols - 2) || (cN12.y + ii >= srcSegColor.rows - 2) || (cN12.x + jj < 2) || (cN12.y + ii < 2))
				{
					continue;
				}

				imData[(((int)cN12.y + ii) * srcSegColor.cols + (int)cN12.x + jj) * 3] = 255;
				imData[(((int)cN12.y + ii) * srcSegColor.cols + (int)cN12.x + jj) * 3 + 1] = 255;
				imData[(((int)cN12.y + ii) * srcSegColor.cols + (int)cN12.x + jj) * 3 + 2] = 50;
			}
		}

		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((cN22.x + jj >= srcSegColor.cols - 2) || (cN22.y + ii >= srcSegColor.rows - 2) || (cN22.x + jj < 2) || (cN22.y + ii < 2))
				{
					continue;
				}

				imData[(((int)cN22.y + ii) * srcSegColor.cols + (int)cN22.x + jj) * 3] = 150;
				imData[(((int)cN22.y + ii) * srcSegColor.cols + (int)cN22.x + jj) * 3 + 1] = 150;
				imData[(((int)cN22.y + ii) * srcSegColor.cols + (int)cN22.x + jj) * 3 + 2] = 255;
			}
		}

		
		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiStringOut(outPath + fileBaseNameOut + pathInFile);
		CString fileSeg;
		fileSeg.Format(_T("Segm_.png"), i);
		CT2CA pszConvertedAnsiStringOutSeg(outPath + fileBaseNameOut + fileSeg + pathInFile);

		// construct a std::string using the LPCSTR input
		std::string strStdOut(pszConvertedAnsiStringOut);
		std::string strStdOutSeg(pszConvertedAnsiStringOutSeg);

		cv::imwrite(strStdOut, srcColor);
		cv::imwrite(strStdOutSeg, srcSeg);
		SetProcImage(srcColor);

		GetElapsedMiliseconds(ElapsedMiliseconds1, StartingTime, Frequency);
		strText.Format(_T("Temps SaveImage:  \t%d\t%d\n"), ElapsedMiliseconds1.QuadPart - ElapsedMiliseconds2.QuadPart, ElapsedMiliseconds1.QuadPart);
		fileTime.WriteString(strText);

	}
	fileTime.Close();
}*/

ellipseParams CTrackingHerniaDlg::GetEllipsePCA(std::vector<cv::Point> & points)
{
	int n = points.size();

	int dolentes = 0;

	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			dolentes++;
		}
	}

	int nbons = n - dolentes;

	double cx, cy;
	cx = 0.0;
	cy = 0.0;
	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			continue;
		}
		cx += (double)points[i].x;
		cy += (double)points[i].y;
	}
	cx /= (double)nbons;
	cy /= (double)nbons;


	Eigen::MatrixXd A = Eigen::MatrixXd::Zero(2, nbons);

	int ii = 0;
	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			continue;
		}

		A(0, ii) = points[i].x - cx;
		A(1, ii) = points[i].y - cy;
		ii++;
	}
	Eigen::Matrix2d B = A * A.transpose();

	Eigen::EigenSolver<Eigen::Matrix2d> eigenSol;
	eigenSol.compute(B);

	double l1 = eigenSol.eigenvalues().x().real();
	double l2 = eigenSol.eigenvalues().y().real();

	double a = sqrt(l1 / M_PI) / 4.0;
	double b = sqrt(l2 / M_PI) / 4.0;

	double ux = eigenSol.eigenvectors().col(0).x().real();
	double uy = eigenSol.eigenvectors().col(0).y().real();

	double vx = eigenSol.eigenvectors().col(1).x().real();
	double vy = eigenSol.eigenvectors().col(1).y().real();

	double alpha = acos( ux/ (ux*ux+uy*uy));

	ellipseParams res;

	res.a = cos(alpha)*cos(alpha) / a / a + sin(alpha)*sin(alpha) / b / b;
	res.b = 2.0*cos(alpha)*sin(alpha)*(1 / a / a - 1 / b / b);
	res.c = sin(alpha)*sin(alpha) / a / a + cos(alpha)*cos(alpha) / b / b;
	res.d = 2.0*(-cx * cos(alpha)*cos(alpha) / a / a - cy * cos(alpha)*sin(alpha) / a / a - cx * sin(alpha)*sin(alpha) / b / b + cy * cos(alpha)*sin(alpha) / b / b);
	res.e = 2.0*(-cy * sin(alpha)*sin(alpha) / a / a - cx * cos(alpha)*sin(alpha) / a / a - cy * cos(alpha)*cos(alpha) / b / b + cx * cos(alpha)*sin(alpha) / b / b);
	res.f = 1 / a / a * (cx*cx*cos(alpha)*cos(alpha) + cy * cy*sin(alpha)*sin(alpha) + 2.0*cx*cy*cos(alpha)*sin(alpha)) + 1 / b / b * (cx*cx*sin(alpha)*sin(alpha) + cy * cy*cos(alpha)*cos(alpha) - 2.0*cx*cy*cos(alpha)*sin(alpha)) - 1.0;

	res.b /= res.a;
	res.c /= res.a;
	res.d /= res.a;
	res.e /= res.a;
	res.f /= res.a;
	res.a /= res.a;

	return res;


	//https://math.stackexchange.com/questions/937259/whats-the-parametric-equation-for-the-general-form-of-an-ellipse-rotated-by-any
	//https://www.maa.org/external_archive/joma/Volume8/Kalman/General.html
}

ellipseParams CTrackingHerniaDlg::GetEllipseLMS(std::vector<cv::Point> & points)
{
	int n = points.size();
	int dolentes = 0;

	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			dolentes++;
		}
	}

	Eigen::MatrixXd A = Eigen::MatrixXd::Zero(n-dolentes, 5);
	Eigen::VectorXd b = Eigen::VectorXd::Zero(n-dolentes);

	int ii = 0;
	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			continue;
		}
		A(ii, 0) = points[i].x*points[i].y;
		A(ii, 1) = points[i].y*points[i].y;
		A(ii, 2) = points[i].x;
		A(ii, 3) = points[i].y;
		A(ii, 4) = 1.0;
		b(ii) = points[i].x*points[i].x;
		ii++;
	}

	Eigen::VectorXd param = (A.transpose()*A).inverse()*A.transpose()*b;

	ellipseParams res;
	res.a = 1.0;
	res.b = -param(0);
	res.c = -param(1);
	res.d = -param(2);
	res.e = -param(3);
	res.f = -param(4);
	return res;
}

ellipseParams CTrackingHerniaDlg::GetEllipseMixNorm(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(5);
	x << initial.b, initial.c, initial.d, initial.e, initial.f;
	optimization_ellipse_functor_opt2 functor(points.size(),points);
	Eigen::NumericalDiff<optimization_ellipse_functor_opt2> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor_opt2>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
	TRACE("\n inici calcul Nolineal\n");
	info = lm.minimizeInit(x);
	for (int i = 0; i < NITERATION; i++)
	{
		info = lm.minimizeOneStep(x);
		vNormaOptimit = lm.fnorm;
		TRACE("FINAL OPTIMITZACI�: Iteraci� LM %d, ERROR OPTIMITZACI�: %f\n", i, vNormaOptimit);
		if (info != Eigen::LevenbergMarquardtSpace::Running)
			break;
	}
	TRACE("final calcul Nolineal\n");
	TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0), x(1), x(2), x(3), x(4));
	ellipseParams res;
	res.a = 1.0;
	res.b = x(0);
	res.c = x(1);
	res.d = x(2);
	res.e = x(3);
	res.f = x(4);
	return res;	
}

ellipseParams CTrackingHerniaDlg::GetEllipseMixNormRegression(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(5);
	x << initial.b, initial.c, initial.d, initial.e, initial.f;
	optimization_ellipse_functor_Regression functor(points.size(), points, initial);
	Eigen::NumericalDiff<optimization_ellipse_functor_Regression> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor_Regression>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
	TRACE("\n inici calcul Nolineal\n");
	info = lm.minimizeInit(x);
	for (int i = 0; i < NITERATION; i++)
	{
		info = lm.minimizeOneStep(x);
		vNormaOptimit = lm.fnorm;
		TRACE("FINAL OPTIMITZACI�: Iteraci� LM %d, ERROR OPTIMITZACI�: %f\n", i, vNormaOptimit);
		if (info != Eigen::LevenbergMarquardtSpace::Running)
			break;
	}
	TRACE("final calcul Nolineal\n");
	TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0), x(1), x(2), x(3), x(4));
	ellipseParams res;
	res.a = 1.0;
	res.b = x(0);
	res.c = x(1);
	res.d = x(2);
	res.e = x(3);
	res.f = x(4);
	return res;
}

ellipseParams CTrackingHerniaDlg::GetEllipseMixNorm2(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(5);
	x << initial.b, initial.c, initial.d, initial.e, initial.f;
	optimization_ellipse_functor2 functor(points.size(), points);
	Eigen::NumericalDiff<optimization_ellipse_functor2> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor2>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
	TRACE("\n inici calcul Nolineal\n");
	info = lm.minimizeInit(x);
	for (int i = 0; i < NITERATION; i++)
	{
		info = lm.minimizeOneStep(x);
		vNormaOptimit = lm.fnorm;
		TRACE("FINAL OPTIMITZACI�: Iteraci� LM %d, ERROR OPTIMITZACI�: %f\n", i, vNormaOptimit);
		if (info != Eigen::LevenbergMarquardtSpace::Running)
			break;
	}
	TRACE("final calcul Nolineal\n");
	TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0), x(1), x(2), x(3), x(4));
	ellipseParams res;
	res.a = 1.0;
	res.b = x(0);
	res.c = x(1);
	res.d = x(2);
	res.e = x(3);
	res.f = x(4);
	return res;
}

ellipseParams CTrackingHerniaDlg::GetEllipseMixNormN2(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(5);
	x << initial.b, initial.c, initial.d, initial.e, initial.f;
	optimization_ellipse_functorN2 functor(points.size(), points);
	Eigen::NumericalDiff<optimization_ellipse_functorN2> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functorN2>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
TRACE("\n inici calcul Nolineal\n");
info = lm.minimizeInit(x);
for (int i = 0; i < NITERATION; i++)
{
	info = lm.minimizeOneStep(x);
	vNormaOptimit = lm.fnorm;
	TRACE("FINAL OPTIMITZACI�: Iteraci� LM %d, ERROR OPTIMITZACI�: %f\n", i, vNormaOptimit);
	if (info != Eigen::LevenbergMarquardtSpace::Running)
		break;
}
TRACE("final calcul Nolineal\n");
TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0), x(1), x(2), x(3), x(4));
ellipseParams res;
res.a = 1.0;
res.b = x(0);
res.c = x(1);
res.d = x(2);
res.e = x(3);
res.f = x(4);
return res;
}

ellipseParams CTrackingHerniaDlg::GetEllipseConstant(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(1);
	x << initial.f;
	optimization_ellipse_constant_functor functor(points.size(), points, initial);
	Eigen::NumericalDiff<optimization_ellipse_constant_functor> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_constant_functor>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
	TRACE("\n inici calcul Nolineal\n");
	info = lm.minimizeInit(x);
	for (int i = 0; i < NITERATION; i++)
	{
		info = lm.minimizeOneStep(x);
		vNormaOptimit = lm.fnorm;
		TRACE("FINAL OPTIMITZACI�: Iteraci� LM %d, ERROR OPTIMITZACI�: %f\n", i, vNormaOptimit);
		if (info != Eigen::LevenbergMarquardtSpace::Running)
			break;
	}
	TRACE("final calcul Nolineal\n");
	TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0));
	ellipseParams res;
	res.a = initial.a;
	res.b = initial.b;
	res.c = initial.c;
	res.d = initial.d;
	res.e = initial.e;
	res.f = x(0);
	return res;
}

void CTrackingHerniaDlg::paintEllipse(ellipseParams &ellipseP, cv::Mat &image, double red, double green, double blue)
{
	uint8_t *imData = image.data;

	double a = -sqrt(2.0*(ellipseP.a*ellipseP.e*ellipseP.e + ellipseP.c*ellipseP.d*ellipseP.d - ellipseP.b*ellipseP.d*ellipseP.e + (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c)*ellipseP.f)*((ellipseP.a + ellipseP.c) + sqrt((ellipseP.a - ellipseP.c)*(ellipseP.a - ellipseP.c) + ellipseP.b*ellipseP.b)));
	a /= (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c);
	double b = -sqrt(2.0*(ellipseP.a*ellipseP.e*ellipseP.e + ellipseP.c*ellipseP.d*ellipseP.d - ellipseP.b*ellipseP.d*ellipseP.e + (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c)*ellipseP.f)*((ellipseP.a + ellipseP.c) - sqrt((ellipseP.a - ellipseP.c)*(ellipseP.a - ellipseP.c) + ellipseP.b*ellipseP.b)));
	b /= (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c);
	int r = (int)max(a, b);
	if (r <= 0)
	{
		return;
	}
	if (r > min(image.cols, image.rows))
	{
		r = min(image.cols, image.rows);
	}
	int x = (int)((2.0*ellipseP.c*ellipseP.d - ellipseP.b*ellipseP.e) / (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c));
	int y = (int)((2.0*ellipseP.a*ellipseP.e - ellipseP.b*ellipseP.d) / (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c));


	for (int i = y - r - 5; i <= y + r + 6; i++)
	{
		for (int j = x - r - 5; j <= x + r + 6; j++)
		{
			if ((j >= image.cols - 2) || (i >= image.rows - 2) || (j < 2) || (i < 2))
			{
				continue;
			}
			cv::Point2d p;
			p.x = (double)j;
			p.y = (double)i;
			double val = SampsonDistance(ellipseP, p);//EvaluateEllipse(ellipseP, p);
			if (abs(val) < 2.0)
			{
				imData[(i * image.cols + j) * 3] = blue;
				imData[(i * image.cols + j) * 3 + 1] = green;
				imData[(i * image.cols + j) * 3 + 2] = red;
			}
		}
	}
}

bool CTrackingHerniaDlg::isToolPoint(double * points, int index, int numPunts, int totalPoints)
{
	int nIndef = 0;
	int nEina = 0;
	for (int i = 0; i < numPunts; i++)
	{
		double c1 = points[index + i];
		double c2 = points[totalPoints + index + i];
		double c3 = points[2 * totalPoints + index + i];
		if (c1 > std::max(c2, c3))
		{
			nEina++;
		}
		if (std::max(c1, std::max(c2, c3)) < 0.1)
		{
			nIndef++;
		}
		if (nEina > 10)
		{
			return true;
		}
		if (nIndef > 10)
		{
			return true;
		}
	}
	return false;
}

bool CTrackingHerniaDlg::isToolPoint(cv::Point point, cv::Mat &image)
{
	uint8_t *imData = image.data;
	for (int i = -2; i < 3; i++)
	{
		for (int j = -2; j < 3; j++)
		{
			if ((point.x == 0) || (point.y == 0) || (point.x + j >= image.cols - 2) || (point.y + i >= image.rows - 2) || (point.x + j < 2) || (point.y + i < 2))
			{
				continue;
			}
			int blue = imData[((point.y + i) * image.cols + point.x + j) * 3];
			int green = imData[((point.y + i) * image.cols + point.x + j) * 3 + 1];
			int red = imData[((point.y + i) * image.cols + point.x + j) * 3 + 2];

			if (blue > max(green, red) + 10)
			{
				return true;
			}
			if (max(red, max(blue, green)) < 70)
			{
				return true;
			}

			/*if (min(blue, min(green, red)) > 110)
			{
				continue;
			}
			else
			{
				if (red > max(green, blue) + 10)
				{
					continue;
				}
				if (max(green, max(blue, red)) < min(green, min(blue, red)) + 10)
				{
					return true;
				}
				if (red > blue + 20)
				{
					continue;
				}
				if (blue > max(green, red) + 10)
				{
					return true;
				}
			}*/
		}
	}
	return false;
}


std::vector<cv::Point> CTrackingHerniaDlg::EraseToolFromPoints(std::vector<cv::Point2i> &points, cv::Mat &image)
{
	std::vector<cv::Point2i> res((int)points.size());

	for (int i = 0; i < points.size(); i++)
	{
		if (!isToolPoint(points[i], image))
		{
			res[i].x = points[i].x;
			res[i].y = points[i].y;
		}
		else
		{
			res[i].x = 0;
			res[i].y = 0;
		}
	}
	return res;
}

std::vector<cv::Point> CTrackingHerniaDlg::EraseToolFromPoints(std::vector<cv::Point2i> &points, double * clasValues, int samplesPerPoint)
{
	int n = (int)points.size();
	std::vector<cv::Point2i> res(n);
	for (int i = 0; i < n; i++)
	{
		if (!isToolPoint(clasValues, i*samplesPerPoint, samplesPerPoint, samplesPerPoint*n))//isToolPoint(clasValues[i],clasValues[n+i], clasValues[2*n + i]))
		{
			res[i].x = points[i].x;
			res[i].y = points[i].y;
		}
		else
		{
			res[i].x = 0;
			res[i].y = 0;
		}
	}
	return res;
}

std::vector<cv::Point> CTrackingHerniaDlg::GetPointsUsingLastEllipseHSV(cv::Mat &image, ellipseParams & ellipse, std::vector<cv::Point> &last_points, int x, int y, int width, int height, int nPoints)
{
	std::vector<cv::Point> points(0);
	std::vector<double> dists(0);
	uint8_t *imData = image.data;
	int DD[2][8] = { {-1, 0, 1, 1, 1, 0, -1, -1},{-1, -1 , -1, 0, 1, 1, 1, 0} };
	/*for (int i = x; i < x + width; i++)
	{
		for (int j = y; j < y + height; j++)
		{
			if ((i >= image.cols - 2) || (j >= image.rows - 2) || (i < 2) || (j < 2))
			{
				continue;
			}
			if (evaluarMat(imData, j, i, image.cols) > 0)
			{
				bool edgepoint = false;
				for (int ii = 0; ii < 8; ii++)
				{
					if (evaluarMat(imData, j + DD[1][ii], i + DD[0][ii], image.cols) == 0)
					{
						edgepoint = true;
						break;
					}
				}
				if (edgepoint)
				{
					double d = SampsonDistance(ellipse, cv::Point(i, j));
					if (d < 1.2*ELLIPSETHRESHOLD)
					{
						points.push_back(cv::Point(i, j));
						dists.push_back(d);
					}
				}
			}
		}
	}

	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	std::vector<cv::Point2i>res = std::vector<cv::Point2i>(nPoints);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPoints);
	std::vector<float> rmaxP(nPoints);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPoints; i++)
	{
		rmaxP[i] = 10000000.0f;// 100.0f*(cols + rows) * (cols + rows);
	}

	meanVal.x = (float)x + width / 2.0;
	meanVal.y = (float)y + height / 2.0;

	std::vector<float> xMov(nPoints);
	std::vector<float> yMov(nPoints);

	for (int i = 0; i < nPoints; i++)
	{
		xMov[i] = 100000000000.0;
		yMov[i] = 100000000000.0;
	}

	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		//raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPoints);
		if (indEx >= nPoints)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] > dists[i])//if (rmaxP[indEx] < raux)
		{
			//rmaxP[indEx] = raux;
			rmaxP[indEx] = dists[i];
			(res)[indEx].x = points[i].x;
			(res)[indEx].y = points[i].y;

			//Mov statistic
			float daux = 100000000.0;
			for (int indAux = -nPoints / 20; indAux < nPoints / 20 + 1; indAux++)
			{
				if ((last_points[modval(indEx + indAux, nPoints)].x > 0) && (last_points[modval(indEx + indAux, nPoints)].y > 0))
				{
					float dd = ((res)[indEx].x - last_points[modval(indEx + indAux, nPoints)].x)*((res)[indEx].x - last_points[modval(indEx + indAux, nPoints)].x) + ((res)[indEx].y - last_points[modval(indEx + indAux, nPoints)].y)*((res)[indEx].y - last_points[modval(indEx + indAux, nPoints)].y);
					if (dd < daux)
					{
						daux = dd;
						xMov[indEx] = (res)[indEx].x - last_points[modval(indEx + indAux, nPoints)].x;
						yMov[indEx] = (res)[indEx].y - last_points[modval(indEx + indAux, nPoints)].y;
					}
				}

			}
		}
	}

	cv::Mat colorAux;
	cv::cvtColor(image, colorAux, cv::COLOR_GRAY2BGR);
	uint8_t *imDataAux = colorAux.data;

	for (int k = 0; k < res.size(); k++)
	{
		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((res[k].x == 0) || (res[k].y == 0) || (res[k].x + jj >= colorAux.cols - 2) || (res[k].y + ii >= colorAux.rows - 2) || (res[k].x + jj < 2) || (res[k].y + ii < 2))
				{
					continue;
				}

				imDataAux[((res[k].y + ii) * colorAux.cols + res[k].x + jj) * 3] = 0;
				imDataAux[((res[k].y + ii) * colorAux.cols + res[k].x + jj) * 3 + 1] = 255;
				imDataAux[((res[k].y + ii) * colorAux.cols + res[k].x + jj) * 3 + 2] = 0;
			}
		}
	}

	for (int k = 0; k < last_points.size(); k++)
	{
		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((last_points[k].x == 0) || (last_points[k].y == 0) || (last_points[k].x + jj >= colorAux.cols - 2) || (last_points[k].y + ii >= colorAux.rows - 2) || (last_points[k].x + jj < 2) || (last_points[k].y + ii < 2))
				{
					continue;
				}

				imDataAux[((last_points[k].y + ii) * colorAux.cols + last_points[k].x + jj) * 3] = 0;
				imDataAux[((last_points[k].y + ii) * colorAux.cols + last_points[k].x + jj) * 3 + 1] = 0;
				imDataAux[((last_points[k].y + ii) * colorAux.cols + last_points[k].x + jj) * 3 + 2] = 255;
			}
		}
	}

	std::sort(xMov.begin(), xMov.end());
	std::sort(yMov.begin(), yMov.end());

	int nxMode = 0, nyMode = 0;
	float xMode, yMode;

	int nxAux = 1, nyAux = 1;
	float xAux = xMov[0], yAux = yMov[0];
	for (int i = 1; i < xMov.size(); i++)
	{
		if (xMov[i] < 2.0*ELLIPSETHRESHOLD)
		{
			if (xMov[i] - xMov[i - 1] > MODETHREASHOLD)
			{
				if (nxMode < nxAux)
				{
					nxMode = nxAux;
					xMode = xAux / nxAux;
					nxAux = 1;
					xAux = xMov[i];
				}
			}
			else
			{
				nxAux++;
				xAux += xMov[i];
			}
		}
		if (yMov[i] < 2.0*ELLIPSETHRESHOLD)
		{
			if (yMov[i] - yMov[i - 1] > MODETHREASHOLD)
			{
				if (nyMode < nyAux)
				{
					nyMode = nyAux;
					yMode = yAux / nyAux;
					nyAux = 1;
					yAux = yMov[i];
				}
			}
			else
			{
				nyAux++;
				yAux += yMov[i];
			}
		}
	}

	if (nxAux > 1)
	{
		if (nxMode < nxAux)
		{
			nxMode = nxAux;
			xMode = xAux / nxAux;
		}
	}

	if (nyAux > 1)
	{
		if (nyMode < nyAux)
		{
			nyMode = nyAux;
			yMode = yAux / nyAux;
		}
	}*/

	cv::Mat imagC1, imagC2;

	int rLoc = max((int)ELLIPSETHRESHOLD, y);
	int cLoc = max((int)ELLIPSETHRESHOLD, x);
	int rsizeLoc = min(min(height, image.rows / 2) , image.rows - rLoc - 2* (int)ELLIPSETHRESHOLD);
	int csizeLoc = min(min(width, image.rows / 2), image.cols - cLoc - 2 * (int)ELLIPSETHRESHOLD);

	imagC1 = last_seg_image(cv::Rect(cLoc - ELLIPSETHRESHOLD, rLoc - ELLIPSETHRESHOLD, csizeLoc + 2* ELLIPSETHRESHOLD, rsizeLoc + 2* ELLIPSETHRESHOLD));
	imagC2 = image(cv::Rect(cLoc,rLoc,csizeLoc,rsizeLoc));

	cv::Mat corrImg;
	cv::matchTemplate(imagC1, imagC2, corrImg, CV_TM_CCORR_NORMED);// CV_TM_SQDIFF_NORMED);


	//cv::minMaxLoc(corrImg, &minVal, &maxVal, &minPunt, &maxLoc, Mat());
	double minVal; double maxVal; Point minLoc; Point maxLoc;
	cv::minMaxLoc(corrImg, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

	int xMode = -maxLoc.x + ELLIPSETHRESHOLD;
	int yMode = -maxLoc.y + ELLIPSETHRESHOLD;

	cv::Point cElli = GetCenterEllipse(ellipse);
	double a = -sqrt(2.0*(ellipse.a*ellipse.e*ellipse.e + ellipse.c*ellipse.d*ellipse.d - ellipse.b*ellipse.d*ellipse.e + (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c)*ellipse.f)*((ellipse.a + ellipse.c) + sqrt((ellipse.a - ellipse.c)*(ellipse.a - ellipse.c) + ellipse.b*ellipse.b)));
	a /= (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);
	double b = -sqrt(2.0*(ellipse.a*ellipse.e*ellipse.e + ellipse.c*ellipse.d*ellipse.d - ellipse.b*ellipse.d*ellipse.e + (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c)*ellipse.f)*((ellipse.a + ellipse.c) - sqrt((ellipse.a - ellipse.c)*(ellipse.a - ellipse.c) + ellipse.b*ellipse.b)));
	b /= (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);

	ellipseParams movElliAux;
	movElliAux.a = ellipse.a;
	movElliAux.b = ellipse.b;
	movElliAux.c = ellipse.c;
	movElliAux.d = -2 * ellipse.a*xMode - ellipse.b*yMode + ellipse.d;
	movElliAux.e = -2 * ellipse.c*yMode - ellipse.b*xMode + ellipse.e;
	movElliAux.f = ellipse.f - ellipse.e*yMode + ellipse.c*yMode*yMode - ellipse.d*xMode + ellipse.b*yMode*xMode + ellipse.a*xMode*xMode;
	
	//movElliAux.d = -2.0*movElliAux.a*(cElli.x + xMode) - ellipse.b*(cElli.y + yMode);
	//movElliAux.e = -movElliAux.b*(cElli.x + xMode) - 2.0*ellipse.c*(cElli.y + yMode);
	//movElliAux.f = movElliAux.a*(cElli.x + xMode)*(cElli.x + xMode) + movElliAux.b*(cElli.x + xMode)*(cElli.y + yMode) + movElliAux.c*(cElli.y + yMode)*(cElli.y + yMode) - a * b;

	//ellipseParams movElli = GetEllipseConstant(res, movElliAux);
	ellipseParams movElli = movElliAux;

	cv::Mat colorAux;
	cv::cvtColor(image, colorAux, cv::COLOR_GRAY2BGR);
	uint8_t *imDataAux = colorAux.data;
	paintEllipse(ellipse, colorAux, 255, 0, 0);
	//paintEllipse(movElliAux, colorAux, 0, 255, 255);
	paintEllipse(movElli, colorAux, 0, 0, 255);
	


	points.resize(0);
	dists.resize(0);
	for (int i = x; i < x + width; i++)
	{
		for (int j = y; j < y + height; j++)
		{
			if ((i >= image.cols - 2) || (j >= image.rows - 2) || (i < 2) || (j < 2))
			{
				continue;
			}
			if (evaluarMat(imData, j, i, image.cols) > 0)
			{
				bool edgepoint = false;
				for (int ii = 0; ii < 8; ii++)
				{
					if (evaluarMat(imData, j + DD[1][ii], i + DD[0][ii], image.cols) == 0)
					{
						edgepoint = true;
						break;
					}
				}
				if (edgepoint)
				{
					double d = SampsonDistance(movElli, cv::Point(i, j));
					if (d < 1.2*ELLIPSETHRESHOLD)
					{
						points.push_back(cv::Point(i, j));
						dists.push_back(d);
					}
				}
			}
		}
	}

	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	std::vector<cv::Point2i>res = std::vector<cv::Point2i>(nPoints);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPoints);
	std::vector<float> rmaxP(nPoints);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;
	res = std::vector<cv::Point2i>(nPoints);

	for (int i = 0; i < nPoints; i++)
	{
		rmaxP[i] = 10000000.0f;// 100.0f*(cols + rows) * (cols + rows);
	}

	meanVal.x = (float)x + width / 2.0;
	meanVal.y = (float)y + height / 2.0;


	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		//raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPoints);
		if (indEx >= nPoints)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] > dists[i])//if (rmaxP[indEx] < raux)
		{
			//rmaxP[indEx] = raux;
			rmaxP[indEx] = dists[i];
			(res)[indEx].x = points[i].x;
			(res)[indEx].y = points[i].y;
		}
	}

	for (int k = 0; k < res.size(); k++)
	{
		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((res[k].x == 0) || (res[k].y == 0) || (res[k].x + jj >= colorAux.cols - 2) || (res[k].y + ii >= colorAux.rows - 2) || (res[k].x + jj < 2) || (res[k].y + ii < 2))
				{
					continue;
				}

				imDataAux[((res[k].y + ii) * colorAux.cols + res[k].x + jj) * 3] = 0;
				imDataAux[((res[k].y + ii) * colorAux.cols + res[k].x + jj) * 3 + 1] = 255;
				imDataAux[((res[k].y + ii) * colorAux.cols + res[k].x + jj) * 3 + 2] = 0;
			}
		}
	}

	for (int k = 0; k < last_points.size(); k++)
	{
		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((last_points[k].x == 0) || (last_points[k].y == 0) || (last_points[k].x + jj >= colorAux.cols - 2) || (last_points[k].y + ii >= colorAux.rows - 2) || (last_points[k].x + jj < 2) || (last_points[k].y + ii < 2))
				{
					continue;
				}

				imDataAux[((last_points[k].y + ii) * colorAux.cols + last_points[k].x + jj) * 3] = 0;
				imDataAux[((last_points[k].y + ii) * colorAux.cols + last_points[k].x + jj) * 3 + 1] = 0;
				imDataAux[((last_points[k].y + ii) * colorAux.cols + last_points[k].x + jj) * 3 + 2] = 255;
			}
		}
	}


	cv::Point2f centerElipse(0.0f, 0.0f);
	int esborrats = 0;
	for (int i = 0; i < res.size(); i++)
	{
		if ((res[i].x == 0) || (res[i].y == 0))
		{
			esborrats++;
		}
		else if ((normCvPoint2i(res[i] - res[modval(i + 1, res.size())]) > DISTENTREPUNTS) && (normCvPoint2i(res[i] - res[modval(i - 1, res.size())]) > DISTENTREPUNTS))
		{
			res[i].x = 0.0;
			res[i].y = 0.0;
			esborrats++;
		}
		else
		{
			centerElipse.x = centerElipse.x + res[i].x;
			centerElipse.y = centerElipse.y + res[i].y;
		}
	}
	centerElipse /= (float)(res.size() - esborrats);

	//Zona mean contorn
	if (numSegm < 1)
	{
		if (numSegm == 0)
		{
			meanSegmentats[0] = centerElipse;
		}
		numSegm++;

	}
	else
	{
		if (normCvPoint2f(meanSegmentats[0] - centerElipse) < DISTANCIACENTREELIPSE)
		{
			meanSegmentats[0] = centerElipse;
		}
	}

	return res;
}


std::vector<cv::Point> CTrackingHerniaDlg::ReducePointsCNNUsingLastEllipse(cv::Mat & image, ellipseParams & ellipse, std::vector<cv::Point> &points)
{
	uint8_t *imData = image.data;
	
	cv::Mat imagC1, imagC2;

	cv::Point2d cElli = GetCenterEllipse(ellipse);
	double x = cElli.x;
	double y = cElli.y;

	double a = -sqrt(2.0*(ellipse.a*ellipse.e*ellipse.e + ellipse.c*ellipse.d*ellipse.d - ellipse.b*ellipse.d*ellipse.e + (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c)*ellipse.f)*((ellipse.a + ellipse.c) + sqrt((ellipse.a - ellipse.c)*(ellipse.a - ellipse.c) + ellipse.b*ellipse.b)));
	a /= (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);
	double b = -sqrt(2.0*(ellipse.a*ellipse.e*ellipse.e + ellipse.c*ellipse.d*ellipse.d - ellipse.b*ellipse.d*ellipse.e + (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c)*ellipse.f)*((ellipse.a + ellipse.c) - sqrt((ellipse.a - ellipse.c)*(ellipse.a - ellipse.c) + ellipse.b*ellipse.b)));
	b /= (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);

	int r = (int)min(max(abs(a), abs(b)), image.rows / 5.0);

	int height = 2 * (r + (int)ELLIPSETHRESHOLD);
	int width = 2 * (r + (int)ELLIPSETHRESHOLD);

	int rLoc = max((int)ELLIPSETHRESHOLD, (int)y);
	int cLoc = max((int)ELLIPSETHRESHOLD, (int)x);
	int rsizeLoc = min(min(height, image.rows / 2), image.rows - rLoc - 2 * (int)ELLIPSETHRESHOLD);
	int csizeLoc = min(min(width, image.rows / 2), image.cols - cLoc - 2 * (int)ELLIPSETHRESHOLD);

	imagC1 = last_seg_image(cv::Rect(cLoc - ELLIPSETHRESHOLD, rLoc - ELLIPSETHRESHOLD, csizeLoc + 2 * ELLIPSETHRESHOLD, rsizeLoc + 2 * ELLIPSETHRESHOLD));
	imagC2 = image(cv::Rect(cLoc, rLoc, csizeLoc, rsizeLoc));

	cv::Mat corrImg;
	cv::matchTemplate(imagC1, imagC2, corrImg, CV_TM_CCORR_NORMED);// CV_TM_SQDIFF_NORMED);


	//cv::minMaxLoc(corrImg, &minVal, &maxVal, &minPunt, &maxLoc, Mat());
	double minVal; double maxVal; Point minLoc; Point maxLoc;
	cv::minMaxLoc(corrImg, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

	int xMode = -maxLoc.x + ELLIPSETHRESHOLD;
	int yMode = -maxLoc.y + ELLIPSETHRESHOLD;

	//cv::Point cElli = GetCenterEllipse(ellipse);
	

	ellipseParams movElliAux;
	movElliAux.a = ellipse.a;
	movElliAux.b = ellipse.b;
	movElliAux.c = ellipse.c;
	movElliAux.d = -2 * ellipse.a*xMode - ellipse.b*yMode + ellipse.d;
	movElliAux.e = -2 * ellipse.c*yMode - ellipse.b*xMode + ellipse.e;
	movElliAux.f = ellipse.f - ellipse.e*yMode + ellipse.c*yMode*yMode - ellipse.d*xMode + ellipse.b*yMode*xMode + ellipse.a*xMode*xMode;


	cv::Point cElliMov = GetCenterEllipse(movElliAux);
	//Part Reduccio Punts
	std::vector<cv::Point> res;
	for (int i = 0; i < points.size(); i++)
	{
		double Ax = cElliMov.x - points[i].x;
		double Ay = cElliMov.y - points[i].y;

		double nA = sqrt(Ax*Ax + Ay * Ay);
		Ax = 8 * Ax / nA;
		int iAx = (int)Ax;

		Ay = 8 * Ay / nA;
		int iAy = (int)Ay;

		if (evaluarMat(imData, points[i].y + iAy, points[i].x + iAx, image.cols) == 0)
		{
			if (evaluarMat(imData, points[i].y - iAy, points[i].x - iAx, image.cols) > 0)
			{
				double d = SampsonDistance(movElliAux, points[i]); //SampsonDistance(movElliAux, points[i]);
				if (d < 0.7*DISTANCECNNREDUCED)
				{
					cv::Point p;
					p.x = points[i].x;
					p.y = points[i].y;
					res.push_back(p);
				}
			}
		}
	}
	//
	return res;

}


std::vector<cv::Point> CTrackingHerniaDlg::GetPointsUsingLastEllipseCompleteHSV(cv::Mat &image, ellipseParams & ellipse, std::vector<cv::Point> &last_points, int x, int y, int width, int height, int nPoints)
{
	std::vector<cv::Point> points(0);
	std::vector<double> dists(0);
	uint8_t *imData = image.data;
	int DD[2][8] = { {-1, 0, 1, 1, 1, 0, -1, -1},{-1, -1 , -1, 0, 1, 1, 1, 0} };
	

	cv::Mat imagC1, imagC2;

	int rLoc = max((int)ELLIPSETHRESHOLD, y);
	int cLoc = max((int)ELLIPSETHRESHOLD, x);
	int rsizeLoc = min(min(height, image.rows / 2), image.rows - rLoc - 2 * (int)ELLIPSETHRESHOLD);
	int csizeLoc = min(min(width, image.rows / 2), image.cols - cLoc - 2 * (int)ELLIPSETHRESHOLD);

	imagC1 = last_seg_image(cv::Rect(cLoc - ELLIPSETHRESHOLD, rLoc - ELLIPSETHRESHOLD, csizeLoc + 2 * ELLIPSETHRESHOLD, rsizeLoc + 2 * ELLIPSETHRESHOLD));
	imagC2 = image(cv::Rect(cLoc, rLoc, csizeLoc, rsizeLoc));

	cv::Mat corrImg;
	cv::matchTemplate(imagC1, imagC2, corrImg, CV_TM_CCORR_NORMED);// CV_TM_SQDIFF_NORMED);


	//cv::minMaxLoc(corrImg, &minVal, &maxVal, &minPunt, &maxLoc, Mat());
	double minVal; double maxVal; Point minLoc; Point maxLoc;
	cv::minMaxLoc(corrImg, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

	int xMode = -maxLoc.x + ELLIPSETHRESHOLD;
	int yMode = -maxLoc.y + ELLIPSETHRESHOLD;

	cv::Point cElli = GetCenterEllipse(ellipse);
	double a = -sqrt(2.0*(ellipse.a*ellipse.e*ellipse.e + ellipse.c*ellipse.d*ellipse.d - ellipse.b*ellipse.d*ellipse.e + (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c)*ellipse.f)*((ellipse.a + ellipse.c) + sqrt((ellipse.a - ellipse.c)*(ellipse.a - ellipse.c) + ellipse.b*ellipse.b)));
	a /= (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);
	double b = -sqrt(2.0*(ellipse.a*ellipse.e*ellipse.e + ellipse.c*ellipse.d*ellipse.d - ellipse.b*ellipse.d*ellipse.e + (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c)*ellipse.f)*((ellipse.a + ellipse.c) - sqrt((ellipse.a - ellipse.c)*(ellipse.a - ellipse.c) + ellipse.b*ellipse.b)));
	b /= (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);

	ellipseParams movElliAux;
	movElliAux.a = ellipse.a;
	movElliAux.b = ellipse.b;
	movElliAux.c = ellipse.c;
	movElliAux.d = -2 * ellipse.a*xMode - ellipse.b*yMode + ellipse.d;
	movElliAux.e = -2 * ellipse.c*yMode - ellipse.b*xMode + ellipse.e;
	movElliAux.f = ellipse.f - ellipse.e*yMode + ellipse.c*yMode*yMode - ellipse.d*xMode + ellipse.b*yMode*xMode + ellipse.a*xMode*xMode;

	//movElliAux.d = -2.0*movElliAux.a*(cElli.x + xMode) - ellipse.b*(cElli.y + yMode);
	//movElliAux.e = -movElliAux.b*(cElli.x + xMode) - 2.0*ellipse.c*(cElli.y + yMode);
	//movElliAux.f = movElliAux.a*(cElli.x + xMode)*(cElli.x + xMode) + movElliAux.b*(cElli.x + xMode)*(cElli.y + yMode) + movElliAux.c*(cElli.y + yMode)*(cElli.y + yMode) - a * b;

	//ellipseParams movElli = GetEllipseConstant(res, movElliAux);
	ellipseParams movElli = movElliAux;

	cv::Mat colorAux;
	cv::cvtColor(image, colorAux, cv::COLOR_GRAY2BGR);
	uint8_t *imDataAux = colorAux.data;
	paintEllipse(ellipse, colorAux, 255, 0, 0);
	//paintEllipse(movElliAux, colorAux, 0, 255, 255);
	paintEllipse(movElli, colorAux, 0, 0, 255);


	cv::Point cElliMov = GetCenterEllipse(movElli);
	points.resize(0);
	dists.resize(0);
	for (int i = x; i < x + width; i++)
	{
		for (int j = y; j < y + height; j++)
		{
			if ((i >= image.cols - 2) || (j >= image.rows - 2) || (i < 2) || (j < 2))
			{
				continue;
			}
			if (evaluarMat(imData, j, i, image.cols) > 0)
			{
				
				bool edgepoint = false;
				for (int ii = 0; ii < 8; ii++)
				{
					if (evaluarMat(imData, j + DD[1][ii], i + DD[0][ii], image.cols) == 0)
					{
						edgepoint = true;
						break;
					}
				}
				if (edgepoint)
				{
					double Ax = cElliMov.x - i;
					double Ay = cElliMov.y - j;

					double nA = sqrt(Ax*Ax + Ay * Ay);
					Ax = 4 * Ax / nA;
					int iAx = (int)Ax;

					Ay = 4 * Ay / nA;
					int iAy = (int)Ay;

					if (evaluarMat(imData, j + iAy, i + iAx, image.cols) == 0)
					{



						double d = SampsonDistance(movElli, cv::Point(i, j));
						if (d < 1.2*ELLIPSETHRESHOLD)
						{
							points.push_back(cv::Point(i, j));
							dists.push_back(d);
						}
					}
				}
			}
		}
	}

	//Part de seccions
	std::vector<cv::Point> res(nPoints);
	cv::Point2d meanVal = GetCenterEllipse(ellipse);
	std::vector<float> dmaxP(nPoints);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPoints; i++)
	{
		dmaxP[i] = 10000000.0f;// 100.0f*(cols + rows) * (cols + rows);
	}


	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2d) points[i] - meanVal;
		//raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPoints);
		if (indEx >= nPoints)
			indEx = 0;

		double dist = SampsonDistance(movElli, points[i]);
			//abs(lastEllipse.a*points[i].x*points[i].x + lastEllipse.b*points[i].x*points[i].y + lastEllipse.c*points[i].y*points[i].y + lastEllipse.d*points[i].x + lastEllipse.e*points[i].y + lastEllipse.f) / 2.0 / sqrt((lastEllipse.a*points[i].x + lastEllipse.b*points[i].y / 2.0 + lastEllipse.d / 2.0)*(lastEllipse.a*points[i].x + lastEllipse.b*points[i].y / 2.0 + lastEllipse.d / 2.0) + (lastEllipse.b*points[i].x / 2.0 + lastEllipse.c*points[i].y + lastEllipse.e / 2.0)*(lastEllipse.b*points[i].x / 2.0 + lastEllipse.c*points[i].y + lastEllipse.e / 2.0) + 1.0);

		if (dmaxP[indEx] > dist)//if (rmaxP[indEx] < raux)
		{
			//rmaxP[indEx] = raux;
			dmaxP[indEx] = dist;
			(res)[indEx].x = points[i].x;
			(res)[indEx].y = points[i].y;
		}
	}

	cv::Point2f centerElipse(0.0f, 0.0f);
	int esborrats = 0;
	for (int i = 0; i < res.size(); i++)
	{
		if ((res[i].x == 0) || (res[i].y == 0))
		{
			esborrats++;
		}
		else if ((normCvPoint2i(res[i] - res[modval(i + 1, res.size())]) > DISTENTREPUNTS) && (normCvPoint2i(res[i] - res[modval(i - 1, res.size())]) > DISTENTREPUNTS))
		{
			res[i].x = 0.0;
			res[i].y = 0.0;
			esborrats++;
		}
		else
		{
			centerElipse.x = centerElipse.x + res[i].x;
			centerElipse.y = centerElipse.y + res[i].y;
		}
	}
	centerElipse /= (float)(res.size() - esborrats);

	//Zona mean contorn
	if (numSegm < 1)
	{
		if (numSegm == 0)
		{
			meanSegmentats[0] = centerElipse;
		}
		numSegm++;

	}
	else
	{
		if (normCvPoint2f(meanSegmentats[0] - centerElipse) < DISTANCIACENTREELIPSE)
		{
			meanSegmentats[0] = centerElipse;
		}
	}

	return res;
}

std::vector<cv::Point> CTrackingHerniaDlg::GetPointsUsingLastEllipseHSV(cv::Mat &image, ellipseParams & ellipse, int x, int y, int width, int height, int nPoints)
{
	std::vector<cv::Point> points(0);
	std::vector<double> dists(0);
	uint8_t *imData = image.data;
	int DD[2][8] = { {-1, 0, 1, 1, 1, 0, -1, -1},{-1, -1 , -1, 0, 1, 1, 1, 0} };
	for (int i = x; i < x + width; i++)
	{
		for (int j = y; j < y + height; j++)
		{
			if (evaluarMat(imData, j, i, image.cols) > 0)
			{
				bool edgepoint = false;
				for (int ii = 0; ii < 8; ii++)
				{
					if (evaluarMat(imData, j + DD[1][ii], i + DD[0][ii], image.cols) == 0)
					{
						edgepoint = true;
						break;
					}
				}
				if (edgepoint)
				{
					double d = SampsonDistance(ellipse, cv::Point(i, j));
					if (d < 1.2*ELLIPSETHRESHOLD)
					{
						points.push_back(cv::Point(i, j));
						dists.push_back(d);
					}
				}
			}
		}
	}

	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	std::vector<cv::Point2i>res = std::vector<cv::Point2i>(nPoints);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPoints);
	std::vector<float> rmaxP(nPoints);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPoints; i++)
	{
		rmaxP[i] = 10000000.0f;// 100.0f*(cols + rows) * (cols + rows);
	}

	meanVal.x = (float)x + width / 2.0;
	meanVal.y = (float)y + height / 2.0;

	/*for (int i = 0; i < points.size(); ++i)
	{
		meanVal.x += (float)points[i].x;
		meanVal.y += (float)points[i].y;
	}
	meanVal /= (float)points.size();*/

	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		//raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPoints);
		if (indEx >= nPoints)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] > dists[i])//if (rmaxP[indEx] < raux)
		{
			//rmaxP[indEx] = raux;
			rmaxP[indEx] = dists[i];
			(res)[indEx].x = points[i].x;
			(res)[indEx].y = points[i].y;
		}
	}
	cv::Point2f centerElipse(0.0f, 0.0f);
	int esborrats = 0;
	for (int i = 0; i < res.size(); i++)
	{
		if ((res[i].x == 0) || (res[i].y == 0))
		{
			esborrats++;
		}
		else if ((normCvPoint2i(res[i] - res[modval(i + 1, res.size())]) > DISTENTREPUNTS) && (normCvPoint2i(res[i] - res[modval(i - 1, res.size())]) > DISTENTREPUNTS))
		{
			esborrats++;
		}
		else
		{
			centerElipse.x = centerElipse.x + res[i].x;
			centerElipse.y = centerElipse.y + res[i].y;
		}
	}
	centerElipse /= (float)(res.size() - esborrats);

	//Zona mean contorn
	if (numSegm < 1)
	{
		if (numSegm == 0)
		{
			meanSegmentats[0] = centerElipse;
		}
		numSegm++;

	}
	else
	{
		if (normCvPoint2f(meanSegmentats[0] - centerElipse) < DISTANCIACENTREELIPSE)
		{
			meanSegmentats[0] = centerElipse;
		}
	}

	return res;
}

std::vector<cv::Point> CTrackingHerniaDlg::GetPointsUsingLastEllipse(cv::Mat &image, ellipseParams & ellipse, std::vector<cv::Point> &last_points, int x, int y, int width, int height, int nPoints)
{
	std::vector<cv::Point> points(0);
	std::vector<double> dists(0);
	uint8_t *imData = image.data;
	for (int i = x; i < x + width; i++)
	{
		for (int j = y; j < y + height; j++)
		{
			if (evaluarMat(imData, j, i, image.cols) > 0)
			{
				double d = SampsonDistance(ellipse, cv::Point(i, j));
				if (d < 2.0*ELLIPSETHRESHOLD)
				{
					points.push_back(cv::Point(i, j));
					dists.push_back(d);
				}
			}
		}
	}

	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	std::vector<cv::Point2i>res = std::vector<cv::Point2i>(nPoints);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPoints);
	std::vector<float> rmaxP(nPoints);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPoints; i++)
	{
		rmaxP[i] = 10000000.0f;// 100.0f*(cols + rows) * (cols + rows);
	}

	meanVal.x = (float)x + width / 2.0;
	meanVal.y = (float)y + height / 2.0;

	/*for (int i = 0; i < points.size(); ++i)
	{
		meanVal.x += (float)points[i].x;
		meanVal.y += (float)points[i].y;
	}
	meanVal /= (float)points.size();*/

	std::vector<float> xMov(nPoints);
	std::vector<float> yMov(nPoints);

	for (int i = 0; i < nPoints; i++)
	{
		xMov[i] = 100000000000.0;
		yMov[i] = 100000000000.0;
	}

	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		//raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPoints);
		if (indEx >= nPoints)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] > dists[i])//if (rmaxP[indEx] < raux)
		{
			//rmaxP[indEx] = raux;
			rmaxP[indEx] = dists[i];
			(res)[indEx].x = points[i].x;
			(res)[indEx].y = points[i].y;

			//Mov statistic
			float daux = 100000000.0;
			for (int indAux = -nPoints / 20; indAux < nPoints / 20 + 1; indAux++)
			{
				if ((last_points[modval(indEx + indAux, nPoints)].x > 0) && (last_points[modval(indEx + indAux, nPoints)].y > 0))
				{
					float dd = ((res)[indEx].x - last_points[modval(indEx + indAux, nPoints)].x)*((res)[indEx].x - last_points[modval(indEx + indAux, nPoints)].x) + ((res)[indEx].y - last_points[modval(indEx + indAux, nPoints)].y)*((res)[indEx].y - last_points[modval(indEx + indAux, nPoints)].y);
					if (dd < daux)
					{
						daux = dd;
						xMov[indEx] = (res)[indEx].x - last_points[modval(indEx + indAux, nPoints)].x;
						yMov[indEx] = (res)[indEx].y - last_points[modval(indEx + indAux, nPoints)].y;
					}
				}
				
			}
		}
	}

	cv::Mat colorAux;
	cv::cvtColor(image, colorAux, cv::COLOR_GRAY2BGR);
	uint8_t *imDataAux = colorAux.data;

	for (int k = 0; k < res.size(); k++)
	{
		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((res[k].x == 0) || (res[k].y == 0) || (res[k].x + jj >= colorAux.cols - 2) || (res[k].y + ii >= colorAux.rows - 2) || (res[k].x + jj < 2) || (res[k].y + ii < 2))
				{
					continue;
				}

				imDataAux[((res[k].y + ii) * colorAux.cols + res[k].x + jj) * 3] = 0;
				imDataAux[((res[k].y + ii) * colorAux.cols + res[k].x + jj) * 3 + 1] = 255;
				imDataAux[((res[k].y + ii) * colorAux.cols + res[k].x + jj) * 3 + 2] = 0;
			}
		}
	}

	for (int k = 0; k < last_points.size(); k++)
	{
		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((last_points[k].x == 0) || (last_points[k].y == 0) || (last_points[k].x + jj >= colorAux.cols - 2) || (last_points[k].y + ii >= colorAux.rows - 2) || (last_points[k].x + jj < 2) || (last_points[k].y + ii < 2))
				{
					continue;
				}

				imDataAux[((last_points[k].y + ii) * colorAux.cols + last_points[k].x + jj) * 3] = 0;
				imDataAux[((last_points[k].y + ii) * colorAux.cols + last_points[k].x + jj) * 3 + 1] = 0;
				imDataAux[((last_points[k].y + ii) * colorAux.cols + last_points[k].x + jj) * 3 + 2] = 255;
			}
		}
	}

	std::sort(xMov.begin(), xMov.end());
	std::sort(yMov.begin(), yMov.end());

	int nxMode = 0, nyMode = 0;
	float xMode, yMode;

	int nxAux = 1, nyAux = 1;
	float xAux = xMov[0], yAux = yMov[0];
	for (int i = 1; i < xMov.size(); i++)
	{
		if (xMov[i] < 2.0*ELLIPSETHRESHOLD)
		{
			if (xMov[i] - xMov[i - 1] > MODETHREASHOLD)
			{
				if (nxMode < nxAux)
				{
					nxMode = nxAux;
					xMode = xAux / nxAux;
					nxAux = 1;
					xAux = xMov[i];
				}
			}
			else
			{
				nxAux++;
				xAux += xMov[i];
			}
		}
		if (yMov[i] < 2.0*ELLIPSETHRESHOLD)
		{
			if (yMov[i] - yMov[i - 1] > MODETHREASHOLD)
			{
				if (nyMode < nyAux)
				{

					nyMode = nyAux;
					yMode = yAux / nyAux;
					nyAux = 1;
					yAux = yMov[i];
				}
			}
			else
			{
				nyAux++;
				yAux += yMov[i];
			}
		}
	}

	if (nxAux > 1)
	{
		if (nxMode < nxAux)
		{
			nxMode = nxAux;
			xMode = xAux / nxAux;
		}
	}

	if (nyAux > 1)
	{
		if (nyMode < nyAux)
		{
			nyMode = nyAux;
			yMode = yAux / nyAux;
		}
	}

	cv::Point cElli = GetCenterEllipse(ellipse);
	double a = -sqrt(2.0*(ellipse.a*ellipse.e*ellipse.e + ellipse.c*ellipse.d*ellipse.d - ellipse.b*ellipse.d*ellipse.e + (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c)*ellipse.f)*((ellipse.a + ellipse.c) + sqrt((ellipse.a - ellipse.c)*(ellipse.a - ellipse.c) + ellipse.b*ellipse.b)));
	a /= (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);
	double b = -sqrt(2.0*(ellipse.a*ellipse.e*ellipse.e + ellipse.c*ellipse.d*ellipse.d - ellipse.b*ellipse.d*ellipse.e + (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c)*ellipse.f)*((ellipse.a + ellipse.c) - sqrt((ellipse.a - ellipse.c)*(ellipse.a - ellipse.c) + ellipse.b*ellipse.b)));
	b /= (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);

	ellipseParams movElli;
	movElli.a = ellipse.a;
	movElli.b = ellipse.b;
	movElli.c = ellipse.c;
	movElli.d = -2.0*movElli.a*(cElli.x + xMode) - ellipse.b*(cElli.y + yMode);
	movElli.e = -movElli.b*(cElli.x + xMode) - 2.0*ellipse.c*(cElli.y + yMode);
	movElli.f = movElli.a*(cElli.x + xMode)*(cElli.x + xMode) + movElli.b*(cElli.x + xMode)*(cElli.y + yMode) + movElli.c*(cElli.y + yMode)*(cElli.y + yMode) - a * b;




	points.resize(0);
	dists.resize(0);
	for (int i = x; i < x + width; i++)
	{
		for (int j = y; j < y + height; j++)
		{
			if (evaluarMat(imData, j, i, image.cols) > 0)
			{
				double d = SampsonDistance(movElli, cv::Point(i, j));
				if (d < 1.2*ELLIPSETHRESHOLD)
				{
					points.push_back(cv::Point(i, j));
					dists.push_back(d);
				}
			}
		}
	}

	//Part de seccions
	res = std::vector<cv::Point2i>(nPoints);
	
	for (int i = 0; i < nPoints; i++)
	{
		rmaxP[i] = 10000000.0f;// 100.0f*(cols + rows) * (cols + rows);
	}

	meanVal.x = (float)x + width / 2.0;
	meanVal.y = (float)y + height / 2.0;


	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		//raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPoints);
		if (indEx >= nPoints)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] > dists[i])//if (rmaxP[indEx] < raux)
		{
			//rmaxP[indEx] = raux;
			rmaxP[indEx] = dists[i];
			(res)[indEx].x = points[i].x;
			(res)[indEx].y = points[i].y;
		}
	}




	cv::Point2f centerElipse(0.0f, 0.0f);
	int esborrats = 0;
	for (int i = 0; i < res.size(); i++)
	{
		if ((res[i].x == 0) || (res[i].y == 0))
		{
			esborrats++;
		}
		else if ((normCvPoint2i(res[i] - res[modval(i + 1, res.size())]) > DISTENTREPUNTS) && (normCvPoint2i(res[i] - res[modval(i - 1, res.size())]) > DISTENTREPUNTS))
		{
			res[i].x = 0.0;
			res[i].y = 0.0;
			esborrats++;
		}
		else
		{
			centerElipse.x = centerElipse.x + res[i].x;
			centerElipse.y = centerElipse.y + res[i].y;
		}
	}
	centerElipse /= (float)(res.size() - esborrats);

	//Zona mean contorn
	if (numSegm < 1)
	{
		if (numSegm == 0)
		{
			meanSegmentats[0] = centerElipse;
		}
		numSegm++;

	}
	else
	{
		if (normCvPoint2f(meanSegmentats[0] - centerElipse) < DISTANCIACENTREELIPSE)
		{
			meanSegmentats[0] = centerElipse;
		}
	}

	return res;
}



std::vector<cv::Point> CTrackingHerniaDlg::GetPointsUsingLastEllipse(cv::Mat &image, ellipseParams & ellipse, int x, int y, int width, int height, int nPoints)
{
	std::vector<cv::Point> points(0);
	std::vector<double> dists(0);
	uint8_t *imData = image.data;
	for (int i = x; i < x + width; i++)
	{
		for (int j = y; j < y + height; j++)
		{
			if (evaluarMat(imData, j, i, image.cols) > 0)
			{
				double d = SampsonDistance(ellipse, cv::Point(i, j));
				if (d < 1.2*ELLIPSETHRESHOLD)
				{
					points.push_back(cv::Point(i, j));
					dists.push_back(d);
				}
			}
		}
	}

	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	std::vector<cv::Point2i>res = std::vector<cv::Point2i>(nPoints);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPoints);
	std::vector<float> rmaxP(nPoints);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPoints; i++)
	{
		rmaxP[i] = 10000000.0f;// 100.0f*(cols + rows) * (cols + rows);
	}

	meanVal.x = (float)x + width / 2.0;
	meanVal.y = (float)y + height / 2.0;

	/*for (int i = 0; i < points.size(); ++i)
	{
		meanVal.x += (float)points[i].x;
		meanVal.y += (float)points[i].y;
	}
	meanVal /= (float)points.size();*/

	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		//raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPoints);
		if (indEx >= nPoints)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if(rmaxP[indEx] > dists[i])//if (rmaxP[indEx] < raux)
		{
			//rmaxP[indEx] = raux;
			rmaxP[indEx] = dists[i];
			(res)[indEx].x = points[i].x;
			(res)[indEx].y = points[i].y;
		}
	}
	cv::Point2f centerElipse(0.0f, 0.0f);
	int esborrats = 0;
	for (int i = 0; i < res.size(); i++)
	{
		if ((res[i].x == 0) || (res[i].y == 0))
		{
			esborrats++;
		}
		else if ((normCvPoint2i(res[i] - res[modval(i + 1, res.size())]) > DISTENTREPUNTS) && (normCvPoint2i(res[i] - res[modval(i - 1, res.size())]) > DISTENTREPUNTS))
		{
			esborrats++;
		}
		else
		{
			centerElipse.x = centerElipse.x + res[i].x;
			centerElipse.y = centerElipse.y + res[i].y;
		}
	}
	centerElipse /= (float)(res.size() - esborrats);

	//Zona mean contorn
	if (numSegm < 1)
	{
		if (numSegm == 0)
		{
			meanSegmentats[0] = centerElipse;
		}
		numSegm++;

	}
	else
	{
		if (normCvPoint2f(meanSegmentats[0] - centerElipse) < DISTANCIACENTREELIPSE)
		{
			meanSegmentats[0] = centerElipse;
		}
	}

	return res;
}

void CTrackingHerniaDlg::Binaritzacion(cv::Mat &entrada, cv::Mat & sortida)
{
	cv::Mat auxiliar, auxiliar2;
	Mat planes[3];
	CString namefile;
	split(entrada, planes);
	planes[1].copyTo(auxiliar);

	//namefile.Format("./binary1.png");
	//cv::imwrite(namefile.GetString(), entrada);

	cv::medianBlur(auxiliar, auxiliar2, 11);

	//namefile.Format("./binary2.png");
	//cv::imwrite(namefile.GetString(), auxiliar);

	// apply the CLAHE algorithm to the L channel
	/*cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
	clahe->setClipLimit(4);
	cv::Mat dst;
	clahe->apply(auxiliar, auxiliar2);
	//namefile.Format("./binary3.png");*/
	//cv::imwrite(namefile.GetString(), auxiliar2);


	cv::adaptiveThreshold(auxiliar2, auxiliar, 255, ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 55, 5);
	//cv::adaptiveThreshold(auxiliar2, auxiliar, 255, ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 45, 10);

	auxiliar.copyTo(auxiliar2);
	//namefile.Format("./binary4.png");
	//cv::imwrite(namefile.GetString(), auxiliar2);


	auxiliar2 = 255 - auxiliar2;
	cv::filterSpeckles(auxiliar2, 0, 3000, 0);
	//cv::filterSpeckles(auxiliar2, 0, 1000, 0);

	//cv::Mat bin = 255 - auxiliar2;
	//namefile.Format("./binary5.png");
	//cv::imwrite(namefile.GetString(), bin);

	//cv::Canny(auxiliar2, auxiliar, 30, 120);
	cv::morphologyEx(auxiliar2, auxiliar, MORPH_CLOSE, segClose);

	//auxiliar2 = 255 - auxiliar;

	//namefile.Format("./binary6.png");
	//cv::imwrite(namefile.GetString(), auxiliar2);

	auxiliar.copyTo(sortida);
}

Eigen::MatrixXd CTrackingHerniaDlg::elm_create_values_eigen(Eigen::VectorXd & input, int nclasses)
{
	Eigen::MatrixXd res(nclasses, input.size());
	res.setConstant(-1.0);
	for (int i = 0; i < input.size(); i++)
	{
		int ind = (int)input(i) - 1;
		res(ind, i) = 1.0;
	}
	return res;
}

Eigen::MatrixXd CTrackingHerniaDlg::elm_training_phase_eigen(Eigen::MatrixXd & values, Eigen::MatrixXd & data, double regularization_coefficient, double kernel_pars)
{
	int n = values.cols();
	int m = values.rows();
	Eigen::MatrixXd dataT = data.transpose();
	Eigen::MatrixXd omega_train = elm_RBF_Kernel_eigen(dataT, kernel_pars);
	Eigen::MatrixXd I(n,n);
	I.setIdentity();
	I = I / regularization_coefficient;
	Eigen::MatrixXd A = omega_train + I;

	Eigen::MatrixXd valuesT = values.transpose();
	Eigen::MatrixXd res(valuesT.rows(), valuesT.cols());
	Eigen::PartialPivLU<Eigen::MatrixXd> ASVD = A.partialPivLu();

	for (int i = 0; i < m; i++)
	{
		Eigen::VectorXd V = values.col(i);
		res.block(0, i, n, 1) = ASVD.solve(V);
	}
	return res;
}

Eigen::MatrixXd CTrackingHerniaDlg::elm_output_eigen(Eigen::MatrixXd & training_data, Eigen::MatrixXd & input_data, Eigen::MatrixXd & weights, double kernel_pars)
{
	Eigen::MatrixXd dataT = input_data.transpose();
	Eigen::MatrixXd trainT = training_data.transpose();
	Eigen::MatrixXd omega_test = elm_RBF_Kernel_eigen(trainT, kernel_pars, dataT);
	Eigen::MatrixXd res = (omega_test.transpose()*weights).transpose();
	return res;
}

Eigen::MatrixXd CTrackingHerniaDlg::elm_RBF_Kernel_eigen(Eigen::MatrixXd & train_data, double kernel_pars, Eigen::MatrixXd Xt)
{
	int td_size = train_data.rows();
	int xt_size = Xt.rows();
	Eigen::MatrixXd XXh1Aux(td_size, 1);
	Eigen::MatrixXd XXh2Aux(xt_size, 1);
	for (int i = 0; i < td_size; i++)
	{
		XXh1Aux(i) = train_data(i, 0)*train_data(i, 0) + train_data(i, 1)*train_data(i, 1) + train_data(i, 2)*train_data(i, 2);
	}
	for (int i = 0; i < xt_size; i++)
	{
		XXh2Aux(i) = Xt(i, 0)*Xt(i, 0) + Xt(i, 1)*Xt(i, 1) + Xt(i, 2)*Xt(i, 2);
	}
	Eigen::MatrixXd XXh1 = XXh1Aux * Eigen::MatrixXd::Ones(1, xt_size);
	Eigen::MatrixXd XXh2 = XXh2Aux * Eigen::MatrixXd::Ones(1, td_size);

	Eigen::MatrixXd res = XXh1 + XXh2.transpose() - 2.0*train_data*Xt.transpose();
	for (int i = 0; i < res.rows(); i++)
	{
		for (int j = 0; j < res.cols(); j++)
		{
			res(i, j) = exp(-res(i, j) / kernel_pars);
		}
	}
	return res;
}

Eigen::MatrixXd CTrackingHerniaDlg::elm_RBF_Kernel_eigen(Eigen::MatrixXd & train_data, double kernel_pars)
{
	int td_size = train_data.rows();
	Eigen::MatrixXd XXhAux(td_size, 1);
	for (int i = 0; i < td_size; i++)
	{
		XXhAux(i) = train_data(i, 0)*train_data(i, 0) + train_data(i, 1)*train_data(i, 1) + train_data(i, 2)*train_data(i, 2);
	}
	Eigen::MatrixXd XXh = XXhAux * Eigen::MatrixXd::Ones(1, td_size);

	Eigen::MatrixXd res = XXh + XXh.transpose() - 2.0*train_data*train_data.transpose();
	for (int i = 0; i < res.rows(); i++)
	{
		for (int j = 0; j < res.cols(); j++)
		{
			res(i, j) = exp(-res(i, j) / kernel_pars);
		}
	}
	return res;
}


std::vector<cv::Point> CTrackingHerniaDlg::GetPointsRealImageUsingLastEllipse(cv::Mat &image, ellipseParams & ellipse, std::vector<cv::Point> & last_points, int x, int y, int width, int height)
{
	std::vector<cv::Point> res;

	//FALTA FER CAS HI HAGI FOR�A TRASLACI� ENTRE FRAMES. MOURE LA ANTINGA ELLIPSE VIA CORRELACI� ENTRE IMG ACTUAL I ANTERIOR

	//

	for (int i = 0; i < last_points.size(); i++)
	{
		double d = SampsonDistance(ellipse, last_points[i]);
		if (d < 0.75*ELLIPSETHRESHOLD)
		{
			cv::Point p;
			p.x = last_points[i].x;
			p.y = last_points[i].y;
			res.push_back(p);
		}
	}

	return res;
}



std::vector<cv::Point> CTrackingHerniaDlg::ReducePoints(std::vector<cv::Point> & points, ellipseParams & ellipse)
{
	int nPoints = 100;

	//Part de seccions
	std::vector<cv::Point> res(nPoints);
	cv::Point2d meanVal = GetCenterEllipse(ellipse);
	std::vector<float> dmaxP(nPoints);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPoints; i++)
	{
		dmaxP[i] = 10000000.0f;// 100.0f*(cols + rows) * (cols + rows);
	}


	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2d) points[i] - meanVal;
		//raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPoints);
		if (indEx >= nPoints)
			indEx = 0;

		double dist = abs(lastEllipse.a*points[i].x*points[i].x + lastEllipse.b*points[i].x*points[i].y + lastEllipse.c*points[i].y*points[i].y + lastEllipse.d*points[i].x + lastEllipse.e*points[i].y + lastEllipse.f) / 2.0 / sqrt((lastEllipse.a*points[i].x + lastEllipse.b*points[i].y / 2.0 + lastEllipse.d / 2.0)*(lastEllipse.a*points[i].x + lastEllipse.b*points[i].y / 2.0 + lastEllipse.d / 2.0) + (lastEllipse.b*points[i].x / 2.0 + lastEllipse.c*points[i].y + lastEllipse.e / 2.0)*(lastEllipse.b*points[i].x / 2.0 + lastEllipse.c*points[i].y + lastEllipse.e / 2.0) + 1.0);

		if (dmaxP[indEx] > dist)//if (rmaxP[indEx] < raux)
		{
			//rmaxP[indEx] = raux;
			dmaxP[indEx] = dist;
			(res)[indEx].x = points[i].x;
			(res)[indEx].y = points[i].y;
		}
	}

	return res;
}






void CTrackingHerniaDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
	CString strText;
	CStdioFile fileTime;
	CStdioFile fileEllipseData;
	CStdioFile filePoints;

	filePoints.Open(_T("./PointsRel_d.txt"), CFile::modeCreate | CFile::modeWrite);

	fileEllipseData.Open(_T("./ellipsedata_Seq4.txt"), CFile::modeCreate | CFile::modeWrite);

	strText.Format(_T("Image\tEDa\tEDb\tEDc\tEDd\tEDe\tEDf\tEDcx\tEDcy\tEDsa\tEDsb\tEDAr\tELSMa\tELSMb\tELSMc\tELSMd\tELSMe\tELSMf\tELSMcx\tELSMcy\tELSMsa\tELSMsb\tELSMAr\tEPCAa\tEPCAb\tEPCAc\tEPCAd\tEPCAe\tEPCAf\tEPCAcx\tEPCAcy\tEPCAsa\tEPCAsb\tEPCAAr\n"));
	fileEllipseData.WriteString(strText);

	//Read list points
	CString path_listPoints;
	//path_listPoints.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Videos\\SubSeq2\\"));
	path_listPoints.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Videos\\SubSeq4\\"));
	CString txt_seedName, img_seedName, img_seedOutName;
	txt_seedName.Format(_T("PointsImage_"));
	img_seedName.Format(_T("Image_"));
	img_seedOutName.Format(_T("OutEllipseImage_Red_"));

	int iniFile = 9700;
	int endFile = 10255;


	CStdioFile listPointsFile;
	CString numTxt,numImg;
	CString elmtxt;
	std::vector<cv::Point> listPoints;

	CString zerStr;

	int numImage = 0;

	int lfin = (int) ceil(log10(endFile + 1));

	for (int ii = iniFile; ii < endFile + 1; ii++)
	{
		if (ii == 8245)
			continue;
		listPoints.resize(0);
		int lImg = (int)ceil(log10(ii + 1));
		if (lImg < lfin)
		{
			numTxt.Format(_T("0%d.txt"), ii);
			numImg.Format(_T("0%d.png"), ii);
		}
		else 
		{
			numTxt.Format(_T("%d.txt"), ii);
			numImg.Format(_T("%d.png"), ii);
		}
		
		listPointsFile.Open(path_listPoints + txt_seedName + numTxt, CFile::modeRead);
		listPointsFile.ReadString(elmtxt);
		while (listPointsFile.ReadString(elmtxt))
		{
			int curPos = 0;
			CString strToken = elmtxt.Tokenize(_T(" "), curPos);
			cv::Point p;
			p.y = _ttoi(strToken);
			for (int j = 1; j < 2; j++)
			{
				strToken = elmtxt.Tokenize(_T(" "), curPos);
				p.x = _ttoi(strToken);
			}
			listPoints.push_back(p);
		}
		listPointsFile.Close();

		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiString(path_listPoints + img_seedName + numImg);

		// construct a std::string using the LPCSTR input
		std::string strStd(pszConvertedAnsiString);


		cv::Mat srcColor = cv::imread(strStd, IMREAD_COLOR);


		std::vector<cv::Point> points, pointsRed;

		if (numImage == 0)
		{
			std::copy(listPoints.begin(), listPoints.end(), std::back_inserter(points));
			std::copy(listPoints.begin(), listPoints.end(), std::back_inserter(pointsRed));
			numImage = 1;
		}
		else
		{
			cv::Point2d p = GetCenterEllipse(lastEllipse);
			double a = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) + sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			a /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			double b = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) - sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			b /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			int r = (int)min(max(abs(a), abs(b)), srcColor.rows / 5.0);
			points = GetPointsRealImageUsingLastEllipse(srcColor,lastEllipse,listPoints, (int)p.x - r - ELLIPSETHRESHOLD, (int)p.y - r - ELLIPSETHRESHOLD, 2 * (r + ELLIPSETHRESHOLD), 2 * (r + ELLIPSETHRESHOLD));
			pointsRed = ReducePoints(points, lastEllipse);
			numImage = 2;
		}



		uint8_t *imData = srcColor.data;
		for (int k = 0; k < pointsRed.size(); k++)
		{
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((pointsRed[k].x == 0) || (pointsRed[k].y == 0) || (pointsRed[k].x + jj >= srcColor.cols - 2) || (pointsRed[k].y + ii >= srcColor.rows - 2) || (pointsRed[k].x + jj < 2) || (pointsRed[k].y + ii < 2))
					{
						continue;
					}
					if (ii*ii + jj * jj <= 3)
					{
						imData[((pointsRed[k].y + ii) * srcColor.cols + pointsRed[k].x + jj) * 3] = 0;
						imData[((pointsRed[k].y + ii) * srcColor.cols + pointsRed[k].x + jj) * 3 + 1] = 0;
						imData[((pointsRed[k].y + ii) * srcColor.cols + pointsRed[k].x + jj) * 3 + 2] = 0;
					}
				}
			}
		}

		ellipseParams elliN1, elliN12, elliN22, elliLMS, elliPCA;

		std::vector<cv::Point> pointsClean(0), pointsCleanRed(0);

		strText.Format(_T("%d "), ii);

		for (int i = 0; i < points.size(); i++)
		{
			if (points[i].x == 0 || points[i].y == 0)
			{
				continue;
			}
			cv::Point pp;
			pp.x = points[i].x;
			pp.y = points[i].y;
			pointsClean.push_back(pp);

			CString auxPoint;
			auxPoint.Format(_T("%d %d "), pp.x, pp.y);
			strText += auxPoint;

		}

		for (int i = 0; i < pointsRed.size(); i++)
		{
			if (pointsRed[i].x == 0 || pointsRed[i].y == 0)
			{
				continue;
			}
			cv::Point pp;
			pp.x = pointsRed[i].x;
			pp.y = pointsRed[i].y;
			pointsCleanRed.push_back(pp);
		}

		strText += _T("\n");
		filePoints.WriteString(strText);

		elliLMS = GetEllipseLMS(pointsCleanRed);
		elliPCA = GetEllipsePCA(pointsClean);
		if (numImage == 1)
		{
			elliN1 = GetEllipseMixNorm(pointsCleanRed, elliLMS);
		}
		else
		{
			elliN1 = GetEllipseMixNorm(pointsCleanRed, lastEllipse);
		}
		lastEllipse = elliN1;
		lastPoints.resize(0);
		std::copy(points.begin(), points.end(), back_inserter(lastPoints));
		paintEllipse(elliN1, srcColor, 0, 0, 255);
		paintEllipse(elliLMS, srcColor, 255, 0, 0);
		//paintEllipse(elliPCA, srcColor, 0, 255, 0);

		cv::Point2d cN1 = GetCenterEllipse(elliN1);
		cv::Point2d cLMS = GetCenterEllipse(elliLMS);
		cv::Point2d cPCA = GetCenterEllipse(elliPCA);

		cv::Point2d axN1 = GetSemiAxesEllipse(elliN1);
		cv::Point2d axLMS = GetSemiAxesEllipse(elliLMS);
		cv::Point2d axPCA = GetSemiAxesEllipse(elliPCA);

		double ArN1 = GetAreaEllipse(elliN1);
		double ArLMS = GetAreaEllipse(elliLMS);
		double ArPCA = GetAreaEllipse(elliPCA);

		strText.Format(_T("%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n"), ii, elliN1.a, elliN1.b, elliN1.c, elliN1.d, elliN1.e, elliN1.f, cN1.x, cN1.y, axN1.x, axN1.y, ArN1, elliLMS.a, elliLMS.b, elliLMS.c, elliLMS.d, elliLMS.e, elliLMS.f, cLMS.x, cLMS.y, axLMS.x, axLMS.y, ArLMS, elliPCA.a, elliPCA.b, elliPCA.c, elliPCA.d, elliPCA.e, elliPCA.f, cPCA.x, cPCA.y, axPCA.x, axPCA.y, ArPCA);
		fileEllipseData.WriteString(strText);


		Eigen::VectorXd x = Eigen::VectorXd(5);
		x << lastEllipse.b, lastEllipse.c, lastEllipse.d, lastEllipse.e, lastEllipse.f;
		Eigen::VectorXd fvec = Eigen::VectorXd(points.size());
		std::vector<int> indOut;
		test(x, fvec, points.size(), points, indOut);

		

		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiStringOut(path_listPoints + img_seedOutName + numImg);

		// construct a std::string using the LPCSTR input
		std::string strStdOut(pszConvertedAnsiStringOut);

		cv::imwrite(strStdOut, srcColor);
		SetProcImage(srcColor);

	}

}


void CTrackingHerniaDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}


void CTrackingHerniaDlg::OnBnClickedGetpointssynth()
{
	// TODO: Add your control notification handler code here

	_LARGE_INTEGER StartingTime, StepTime, EndingTime, ElapsedMiliseconds1, ElapsedMiliseconds2;
	LARGE_INTEGER Frequency;
	CString strText;
	CStdioFile fileTime;
	CStdioFile fileEllipseData;
	CStdioFile filePoints;

	fileTime.Open(_T("./times.txt"), CFile::modeCreate | CFile::modeWrite);

	

	//cv::Mat src;
	int imgIni = 936;
	int imgEnd = 1481;
	CString pathInFile;
	CString fileBaseNameIn, fileBaseNameOut;
	CString fileInNameExt, fileOutNameExt;
	fileBaseNameIn.Format(_T("Image_"));
	fileBaseNameOut.Format(_T("Image_Points_"));

	fileInNameExt.Format(_T(".png"));
	fileOutNameExt.Format(_T(".txt"));

	for (int i = imgIni; i < imgEnd + 1; i++)
	{
		QueryPerformanceFrequency(&Frequency);
		QueryPerformanceCounter(&StartingTime);

		if (i < 10)
		{
			pathInFile.Format(_T("00000%d"), i);
		}
		else if (i < 100)
		{
			pathInFile.Format(_T("0000%d"), i);
		}
		else if (i < 1000)
		{
			pathInFile.Format(_T("000%d"), i);
		}
		else if (i < 10000)
		{
			pathInFile.Format(_T("00%d"), i);
		}
		else if (i < 100000)
		{
			pathInFile.Format(_T("0%d"), i);
		}
		else
		{
			pathInFile.Format(_T("%d"), i);
		}

		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiString(imgPath + fileBaseNameIn + pathInFile + fileInNameExt);
		CT2CA pszConvertedAnsiStringOut(outPath + fileBaseNameOut + pathInFile + fileOutNameExt);

		// construct a std::string using the LPCSTR input
		std::string strStd(pszConvertedAnsiString);
		std::string strStdOut(pszConvertedAnsiStringOut);


		cv::Mat srcColor = cv::imread(strStd, IMREAD_COLOR);

		cv::Mat srcHSV, outBin;
		cv::cvtColor(srcColor, srcHSV, cv::COLOR_BGR2HSV);
		Binaritzacion(srcHSV, outBin);

		cv::Mat mask = cv::Mat::zeros(outBin.rows, outBin.cols, outBin.type());

		uint8_t *imData = outBin.data;
		uint8_t *maskData = mask.data;

		int lbl = 0;

		std::vector<cv::Point> allPoints(0);
		for (int irow = 50; irow < outBin.rows - 50; irow++)
		{
			int lastcolor = evaluarMat(imData, irow, 50, outBin.cols);
			for (int jcol = 51; jcol < outBin.cols - 50; jcol++)
			{
				if (evaluarMat(imData, irow, jcol, outBin.cols) != lastcolor)
				{
					if (evaluarMat(maskData, irow, jcol, mask.cols) == 0)
					{
						std::vector<cv::Point> pointsImi(0);
						lbl++;
						int n = creixementRegioContorn(outBin, mask, jcol, irow, lbl, pointsImi);
						allPoints.insert(allPoints.end(), pointsImi.begin(), pointsImi.end());
						lastcolor = evaluarMat(imData, irow, jcol, outBin.cols);
					}
					else
					{
						lastcolor = evaluarMat(imData, irow, jcol, outBin.cols);
					}
				}
			}
		}


		//Downsample
		std::vector<cv::Point> pointsEnd(0);
		for (int jp = 0; jp < allPoints.size(); jp = jp + 20)
		{
			cv::Point2i ptr = cv::Point2i(allPoints[jp].x, allPoints[jp].y);
			pointsEnd.push_back(ptr);
		}


		uint8_t *imSrc = srcColor.data;
		for (int k = 0; k < pointsEnd.size(); k++)
		{
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((pointsEnd[k].x == 0) || (pointsEnd[k].y == 0) || (pointsEnd[k].x + jj >= srcColor.cols - 2) || (pointsEnd[k].y + ii >= srcColor.rows - 2) || (pointsEnd[k].x + jj < 2) || (pointsEnd[k].y + ii < 2))
					{
						continue;
					}
					if (ii*ii + jj * jj <= 9)
					{
						imSrc[((pointsEnd[k].y + ii) * srcColor.cols + pointsEnd[k].x + jj) * 3] = 0;
						imSrc[((pointsEnd[k].y + ii) * srcColor.cols + pointsEnd[k].x + jj) * 3 + 1] = 0;
						imSrc[((pointsEnd[k].y + ii) * srcColor.cols + pointsEnd[k].x + jj) * 3 + 2] = 0;
					}
				}
			}
		}
		fileEllipseData.Open(outPath + fileBaseNameOut + pathInFile + fileOutNameExt, CFile::modeCreate | CFile::modeWrite);

		strText.Format(_T("Row\tColumn\n"));
		fileEllipseData.WriteString(strText);
		for (int iP = 0; iP < pointsEnd.size(); iP++)
		{
			strText.Format(_T("%d\t%d\n"), pointsEnd[iP].y, pointsEnd[iP].x);
			fileEllipseData.WriteString(strText);
		}
		fileEllipseData.Close();
	}
}


void CTrackingHerniaDlg::OnBnClickedSynthtic()
{
	// TODO: Add your control notification handler code here

	_LARGE_INTEGER StartingTime, StepTime, EndingTime, ElapsedMiliseconds1, ElapsedMiliseconds2;
	LARGE_INTEGER Frequency;
	CString strText;
	CStdioFile fileTime;
	CStdioFile fileEllipseData;
	CStdioFile filePoints;
	CStdioFile FileCNNPoints;

	CString CNNPointsPath;
	CNNPointsPath.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Results\\hernia 11\\PointsImageOut_"));

	fileTime.Open(_T("./times.txt"), CFile::modeCreate | CFile::modeWrite);

	fileEllipseData.Open(_T("./ellipsedata_synthetic.txt"), CFile::modeCreate | CFile::modeWrite);

	strText.Format(_T("Image\tEDa\tEDb\tEDc\tEDd\tEDe\tEDf\tEDcx\tEDcy\tEDsa\tEDsb\tEDAr\tEMLSa\tEMLSb\tEMLSc\tEMLSd\tEMLSe\tEMLSf\tEMLScx\tEMLScy\tEMLSsa\tEMLSsb\tEMLSAr\tEPCAa\tEPCAb\tEPCAc\tEPCAd\tEPCAe\tEPCAf\tEPCAcx\tEPCAcy\tEPCAsa\tEPCAsb\tEPCAAr\n"));
	fileEllipseData.WriteString(strText);

	filePoints.Open(_T("./PointsSynth.txt"), CFile::modeCreate | CFile::modeWrite);

	//cv::Mat src;
	int imgIni = 936;
	int imgEnd = 1481;
	CString pathInFile;
	CString fileBaseNameIn, fileBaseNameOut;
	fileBaseNameIn.Format(_T("Image_"));
	fileBaseNameOut.Format(_T("image_Synth_"));

	for (int i = imgIni; i < imgEnd + 1; i++)
	{
		if (i == 940)
		{
			int aiii = 0;
		}
		QueryPerformanceFrequency(&Frequency);
		QueryPerformanceCounter(&StartingTime);

		if (i < 10)
		{
			pathInFile.Format(_T("00000%d"), i);
		}
		else if (i < 100)
		{
			pathInFile.Format(_T("0000%d"), i);
		}
		else if (i < 1000)
		{
			pathInFile.Format(_T("000%d"), i);
		}
		else if (i < 10000)
		{
			pathInFile.Format(_T("00%d"), i);
		}
		else if (i < 100000)
		{
			pathInFile.Format(_T("0%d"), i);
		}
		else
		{
			pathInFile.Format(_T("%d"), i);
		}

		CString fileTxtExt, fileImgExt;
		fileTxtExt.Format(_T(".txt"));
		fileImgExt.Format(_T(".png"));

		CString cnntxt;
		std::vector<cv::Point> listPoints(0);

		FileCNNPoints.Open(CNNPointsPath + pathInFile + fileTxtExt, CFile::modeRead);
		FileCNNPoints.ReadString(cnntxt);
		while (FileCNNPoints.ReadString(cnntxt))
		{
			int curPos = 0;
			CString strToken = cnntxt.Tokenize(_T(" "), curPos);
			cv::Point p;
			p.y = _ttoi(strToken);
			for (int j = 1; j < 2; j++)
			{
				strToken = cnntxt.Tokenize(_T(" "), curPos);
				p.x = _ttoi(strToken);
			}
			listPoints.push_back(p);
		}
		FileCNNPoints.Close();

		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiString(imgPath + fileBaseNameIn + pathInFile + fileImgExt);

		// construct a std::string using the LPCSTR input
		std::string strStd(pszConvertedAnsiString);


		cv::Mat srcColor = cv::imread(strStd, IMREAD_COLOR);
		cv::Mat srcColor2;
		srcColor.copyTo(srcColor2);

		cv::Mat srcHSV, outBin;
		cv::cvtColor(srcColor, srcHSV, cv::COLOR_BGR2HSV);
		Binaritzacion(srcHSV, outBin);


		segrowi = outBin.rows / 4;
		segcoli = outBin.cols / 4;
		segrowsize = outBin.rows / 2;
		segcolsize = outBin.cols / 2;


		SetInitialImage(outBin);



		std::vector<cv::Point> points;

		if (numSegm == 0)
		{
			int rinici, cinici;
			rinici = 550;
			cinici = 850;
			meanSegmentats.clear();
			meanSegmentats.resize(1);
			meanSegmentats[0].x = outBin.cols / 2 - 50;
			meanSegmentats[0].y = outBin.rows / 2;
			trobarPuntsElipseHSV(outBin, points, 100, rinici, cinici);
			lastEllipse.a = 1.00; //1.0;
			lastEllipse.b = -0.34; // 0.0;
			lastEllipse.c = 0.94;// 1.0;
			lastEllipse.d = -1586.17;// -1920.0;
			lastEllipse.e = -675.21;// -1080.0;
			lastEllipse.f = 865507.01;// 1203200.0;

			ellipseParams elliaux;
			elliaux.a = 1.0; //1.0;
			elliaux.b = -0.36; // 0.0;
			elliaux.c = 0.94;// 1.0;
			elliaux.d = -2.0*896.0;// -1920.0;
			elliaux.e = -2.0*600.0;// -1080.0;
			elliaux.f = 896.0*896.0 + 600.0*600.0 - 146.0*146.0;// 1203200.0;
			lastEllipse = GetEllipseMixNormN2(points, elliaux);
			cv::Point2d p = GetCenterEllipse(lastEllipse);
			double a = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) + sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			a /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			double b = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) - sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			b /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			int r = (int)min(max(abs(a), abs(b)), outBin.rows / 5.0);
			points = GetPointsUsingLastEllipseHSV(outBin, lastEllipse, (int)p.x - r - ELLIPSETHRESHOLD, (int)p.y - r - ELLIPSETHRESHOLD, 2 * (r + ELLIPSETHRESHOLD), 2 * (r + ELLIPSETHRESHOLD), 100);
		}
		else
		{
			cv::Point2d p = GetCenterEllipse(lastEllipse);
			double a = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) + sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			a /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			double b = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) - sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			b /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			int r = (int)min(max(abs(a), abs(b)), outBin.rows / 5.0);
			points = ReducePointsCNNUsingLastEllipse(outBin, lastEllipse, listPoints);
		}

		srcColor.copyTo(last_src_image);
		
		//points = EraseToolFromPoints(points, srcColor);
		


		cv::Mat srcSegColor;
		cv::cvtColor(outBin, srcSegColor, cv::COLOR_GRAY2BGR);//srcSegColor, cv::COLOR_GRAY2BGR);

		uint8_t *imData = srcColor.data;
		for (int k = 0; k < points.size(); k++)
		{
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= srcSegColor.cols - 2) || (points[k].y + ii >= srcSegColor.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
					{
						continue;
					}
					if (ii*ii + jj * jj <= 9)
					{
						imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3] = 0;
						imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 1] = 0;
						imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 2] = 0;
					}
				}
			}
		}


		uint8_t *imData2 = srcColor2.data;
		for (int k = 0; k < listPoints.size(); k++)
		{
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((listPoints[k].x == 0) || (listPoints[k].y == 0) || (listPoints[k].x + jj >= srcColor2.cols - 2) || (listPoints[k].y + ii >= srcColor2.rows - 2) || (listPoints[k].x + jj < 2) || (listPoints[k].y + ii < 2))
					{
						continue;
					}
					if (ii*ii + jj * jj <= 9)
					{
						imData2[((listPoints[k].y + ii) * srcColor2.cols + listPoints[k].x + jj) * 3] = 0;
						imData2[((listPoints[k].y + ii) * srcColor2.cols + listPoints[k].x + jj) * 3 + 1] = 0;
						imData2[((listPoints[k].y + ii) * srcColor2.cols + listPoints[k].x + jj) * 3 + 2] = 0;
					}
				}
			}
		}

		ellipseParams elliN1, elliN12, elliN22, elliLMS, elliPCA;

		std::vector<cv::Point> pointsClean(0);

		strText.Format(_T("%d "), i);

		for (int ii = 0; ii < points.size(); ii++)
		{
			if (points[ii].x == 0 || points[ii].y == 0)
			{
				continue;
			}
			cv::Point pp;
			pp.x = points[ii].x;
			pp.y = points[ii].y;
			pointsClean.push_back(pp);

			CString auxPoint;
			auxPoint.Format(_T("%d %d "), pp.x, pp.y);
			strText += auxPoint;

		}

		strText += _T("\n");
		filePoints.WriteString(strText);

		elliN1 = GetEllipseMixNormRegression(pointsClean, lastEllipse);
		elliLMS = GetEllipseLMS(pointsClean);
		elliPCA = GetEllipsePCA(pointsClean);
		lastEllipse = elliN1;
		lastPoints.resize(0);
		std::copy(points.begin(), points.end(), back_inserter(lastPoints));
		outBin.copyTo(last_seg_image);
		paintEllipse(elliN1, srcColor, 0, 0, 255);
		paintEllipse(elliLMS, srcColor, 255, 0, 0);
		//paintEllipse(elliPCA, srcColor, 0, 255, 0);

		cv::Point2d cN1 = GetCenterEllipse(elliN1);
		cv::Point2d cLMS = GetCenterEllipse(elliLMS);
		cv::Point2d cPCA = GetCenterEllipse(elliPCA);

		cv::Point2d axN1 = GetSemiAxesEllipse(elliN1);
		cv::Point2d axLMS = GetSemiAxesEllipse(elliLMS);
		cv::Point2d axPCA = GetSemiAxesEllipse(elliPCA);

		double ArN1 = GetAreaEllipse(elliN1);
		double ArLMS = GetAreaEllipse(elliLMS);
		double ArPCA = GetAreaEllipse(elliPCA);

		strText.Format(_T("%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n"), i, elliN1.a, elliN1.b, elliN1.c, elliN1.d, elliN1.e, elliN1.f, cN1.x, cN1.y, axN1.x, axN1.y, ArN1, elliLMS.a, elliLMS.b, elliLMS.c, elliLMS.d, elliLMS.e, elliLMS.f, cLMS.x, cLMS.y, axLMS.x, axLMS.y, ArLMS, elliPCA.a, elliPCA.b, elliPCA.c, elliPCA.d, elliPCA.e, elliPCA.f, cPCA.x, cPCA.y, axPCA.x, axPCA.y, ArPCA);
		fileEllipseData.WriteString(strText);


		Eigen::VectorXd x = Eigen::VectorXd(5);
		x << lastEllipse.b, lastEllipse.c, lastEllipse.d, lastEllipse.e, lastEllipse.f;
		Eigen::VectorXd fvec = Eigen::VectorXd(points.size());
		std::vector<int> indOut;
		test(x, fvec, points.size(), points, indOut);

		/*for (int k = 0; k < points.size(); k++)
		{
			bool outpoint = false;
			for (int kk = 0; kk < indOut.size(); kk++)
			{
				if (indOut[kk] == k)
				{
					outpoint = true;
					break;
				}
			}
			if(!outpoint)
			{
				continue;
			}
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= srcSegColor.cols - 2) || (points[k].y + ii >= srcSegColor.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
					{
						continue;
					}

					imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3] = 0;
					imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 1] = 0;
					imData[((points[k].y + ii) * srcSegColor.cols + points[k].x + jj) * 3 + 2] = 0;
				}
			}
		}

		for (int ii = -1; ii < 2; ii++)
		{
			for (int jj = -1; jj < 2; jj++)
			{
				if ((cN1.x + jj >= srcSegColor.cols - 2) || (cN1.y + ii >= srcSegColor.rows - 2) || (cN1.x + jj < 2) || (cN1.y + ii < 2))
				{
					continue;
				}

				imData[(((int)cN1.y + ii) * srcSegColor.cols + (int)cN1.x + jj) * 3] = 255;
				imData[(((int)cN1.y + ii) * srcSegColor.cols + (int)cN1.x + jj) * 3 + 1] = 50;
				imData[(((int)cN1.y + ii) * srcSegColor.cols + (int)cN1.x + jj) * 3 + 2] = 50;
			}
		}*/

		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiStringOut(outPath + fileBaseNameOut + pathInFile + fileImgExt);
		CString fileSeg;
		CString filePoints;
		fileSeg.Format(_T("Segm_.png"), i);
		filePoints.Format(_T("Points_"));
		CT2CA pszConvertedAnsiStringOutSeg(outPath + fileBaseNameOut + fileSeg + pathInFile + fileImgExt);
		CT2CA pszConvertedAnsiStringOutPoints(outPath + fileBaseNameOut + filePoints + pathInFile + fileImgExt);

		// construct a std::string using the LPCSTR input
		std::string strStdOut(pszConvertedAnsiStringOut);
		std::string strStdOutSeg(pszConvertedAnsiStringOutSeg);
		std::string strStdOutPoints(pszConvertedAnsiStringOutPoints);

		cv::imwrite(strStdOutPoints, srcColor2);
		cv::imwrite(strStdOut, srcColor);


		cv::Mat outBinColor;

		cv::cvtColor(outBin, outBinColor, cv::COLOR_GRAY2BGR);


		uint8_t *imData3 = outBinColor.data;

		for (int k = 0; k < listPoints.size(); k++)
		{
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((listPoints[k].x == 0) || (listPoints[k].y == 0) || (listPoints[k].x + jj >= outBinColor.cols - 2) || (listPoints[k].y + ii >= outBinColor.rows - 2) || (listPoints[k].x + jj < 2) || (listPoints[k].y + ii < 2))
					{
						continue;
					}
					if (ii*ii + jj * jj <= 9)
					{
						imData3[((listPoints[k].y + ii) * outBinColor.cols + listPoints[k].x + jj) * 3] = 0;
						imData3[((listPoints[k].y + ii) * outBinColor.cols + listPoints[k].x + jj) * 3 + 1] = 0;
						imData3[((listPoints[k].y + ii) * outBinColor.cols + listPoints[k].x + jj) * 3 + 2] = 255;
					}
				}
			}
		}

		for (int k = 0; k < points.size(); k++)
		{
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((points[k].x == 0) || (points[k].y == 0) || (points[k].x + jj >= outBinColor.cols - 2) || (points[k].y + ii >= outBinColor.rows - 2) || (points[k].x + jj < 2) || (points[k].y + ii < 2))
					{
						continue;
					}
					if (ii*ii + jj * jj <= 9)
					{
						imData3[((points[k].y + ii) * outBinColor.cols + points[k].x + jj) * 3] = 0;
						imData3[((points[k].y + ii) * outBinColor.cols + points[k].x + jj) * 3 + 1] = 255;
						imData3[((points[k].y + ii) * outBinColor.cols + points[k].x + jj) * 3 + 2] = 0;
					}
				}
			}
		}


		cv::imwrite(strStdOutSeg, outBinColor);
		SetProcImage(srcColor);

	}
	fileTime.Close();
	fileEllipseData.Close();
	

}
