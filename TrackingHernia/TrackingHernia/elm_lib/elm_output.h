/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * elm_output.h
 *
 * Code generation for function 'elm_output'
 *
 */

#ifndef ELM_OUTPUT_H
#define ELM_OUTPUT_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "elm_lib_types.h"

/* Function Declarations */
extern void elm_output(const emxArray_real_T *train, const emxArray_real_T
  *input, const emxArray_real_T *Weight, double kernel_para, emxArray_real_T
  *Output);

#endif

/* End of code generation (elm_output.h) */
