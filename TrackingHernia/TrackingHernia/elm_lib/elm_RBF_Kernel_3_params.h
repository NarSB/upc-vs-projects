/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * elm_RBF_Kernel_3_params.h
 *
 * Code generation for function 'elm_RBF_Kernel_3_params'
 *
 */

#ifndef ELM_RBF_KERNEL_3_PARAMS_H
#define ELM_RBF_KERNEL_3_PARAMS_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "elm_lib_types.h"

/* Function Declarations */
extern void elm_RBF_Kernel_3_params(const emxArray_real_T *Xtrain, double
  kernel_pars, const emxArray_real_T *Xt, emxArray_real_T *omega);

#endif

/* End of code generation (elm_RBF_Kernel_3_params.h) */
