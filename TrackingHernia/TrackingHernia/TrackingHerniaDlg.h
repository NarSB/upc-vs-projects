
// TrackingHerniaDlg.h : header file
//

#pragma once

#include <algorithm>

//DEFINE CONTSTANTS
#define PUNTSCONTORNIMATGE2D 100
#define TOLERANCIA 0.0001
#define MINTAMANYZONACONTORN 50
#define MAXTAMANYZONACONTORN 4000
#define DISTANCIACENTREELIPSE 50
#define DISTENTREPUNTS 30
#define ELLIPSETHRESHOLD 50.0
#define NITERATION 2000
#define PERCENTAGEPOINTSOULIERS 0.95
#define MODETHREASHOLD 3.0
#define DISTANCECNNREDUCED 30
#define REGRESSIONVALUE 0.003//0.001

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

struct ellipseParams 
{
	double a;
	double b;
	double c;
	double d;
	double e;
	double f;
};

struct distStruct
{
	int ind;
	double dist;
};

template<typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
struct Functor
{
	typedef _Scalar Scalar;
	enum {
		InputsAtCompileTime = NX,
		ValuesAtCompileTime = NY
	};
	typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

	const int m_inputs, m_values;

	Functor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
	Functor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

	int inputs() const { return m_inputs; }
	int values() const { return m_values; }

	// you should define that in the subclass :
	//  void operator() (const InputType& x, ValueType* v, JacobianType* _j=0) const;
};

//Get ellipse Params
struct optimization_ellipse_functor : Functor<double>
{
	optimization_ellipse_functor(int size, std::vector<cv::Point2i> points) : Functor<double>(5, size)
	{
		nPoints = size;
		PointsElli = Eigen::Matrix2Xd(2, size);
		for (int i = 0; i < size; i++)
		{
			PointsElli(0, i) = (double)(points[i].x);
			PointsElli(1, i) = (double)(points[i].y);
		}
	}


	int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const
	{
		fvec(0) = 0.0;
		std::vector<distStruct> distV(0);
		for (int i = 0; i < nPoints; i++)
		{
			if (abs(PointsElli(0, i)) < 0.00001)
			{
				continue;
			}
			double s = abs(PointsElli(0, i)*PointsElli(0, i) + x(0)*PointsElli(0, i)*PointsElli(1, i) + x(1)*PointsElli(1, i)*PointsElli(1, i) + x(2)*PointsElli(0, i) + x(3)*PointsElli(1, i) + x(4)) / 2.0 / sqrt((PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0)*(PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0) + (x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0)*(x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0));
			distStruct ds;
			ds.ind = i;
			ds.dist = s;
			distV.push_back(ds);
		
		}
		std::sort(distV.begin(), distV.end(), [](distStruct a, distStruct b) { return a.dist < b.dist; });
		
		for(int i = 0; i < min(nPoints*PERCENTAGEPOINTSOULIERS, 0.9*distV.size()); i++)
		{
			double s = distV[i].dist;
			///x(0)*PointsElli(0, i)*PointsElli(0, i) + x(1)*PointsElli(0, i)*PointsElli(1, i) + x(2)*PointsElli(1, i)*PointsElli(1, i) + x(3)*PointsElli(0, i) + x(4)*PointsElli(1, i) + x(5);
			if (s < ELLIPSETHRESHOLD)
			{
				fvec(0) += s * s / 2.0;
			}
			else
			{
				fvec(0) += ELLIPSETHRESHOLD * s - ELLIPSETHRESHOLD * ELLIPSETHRESHOLD / 2.0;
			}
		}
		double disc = (x(0)*x(0) - 4.0*x(1));
		for (int i = 1; i < nPoints; i++)
		{
			if (disc < 0.0)
			{
				fvec(i) = 0.0;
			}
			else if (disc < 1.0)
			{
				fvec(i) = 100000.0 + 1000.0*disc;
			}
			else
			{
				fvec(i) = 100000.0 + 1000.0*disc*disc*disc;
			}
		}
		return 0;
	}

	int nPoints;
	Eigen::Matrix2Xd PointsElli;
};

struct optimization_ellipse_functor_opt2 : Functor<double>
{
	optimization_ellipse_functor_opt2(int size, std::vector<cv::Point2i> points) : Functor<double>(5, size+1)
	{
		nPoints = size;
		PointsElli = Eigen::Matrix2Xd(2, size);
		for (int i = 0; i < size; i++)
		{
			PointsElli(0, i) = (double)(points[i].x);
			PointsElli(1, i) = (double)(points[i].y);
		}
	}


	int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const
	{
		fvec(0) = 0.0;
		std::vector<distStruct> distV(0);
		for (int i = 0; i < nPoints; i++)
		{
			fvec(i) = 0.0;
			if (abs(PointsElli(0, i)) < 0.00001)
			{
				fvec(i) = 0.0;
				continue;
			}
			double s = abs(PointsElli(0, i)*PointsElli(0, i) + x(0)*PointsElli(0, i)*PointsElli(1, i) + x(1)*PointsElli(1, i)*PointsElli(1, i) + x(2)*PointsElli(0, i) + x(3)*PointsElli(1, i) + x(4)) / 2.0 / sqrt((PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0)*(PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0) + (x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0)*(x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0)+0.0001);
			distStruct ds;
			ds.ind = i;
			ds.dist = s;
			distV.push_back(ds);

		}
		std::sort(distV.begin(), distV.end(), [](distStruct a, distStruct b) { return a.dist < b.dist; });

		for (int i = 0; i < min(nPoints*PERCENTAGEPOINTSOULIERS, 0.9*distV.size()); i++)
		{
			double s = distV[i].dist;
			///x(0)*PointsElli(0, i)*PointsElli(0, i) + x(1)*PointsElli(0, i)*PointsElli(1, i) + x(2)*PointsElli(1, i)*PointsElli(1, i) + x(3)*PointsElli(0, i) + x(4)*PointsElli(1, i) + x(5);
			if (s < ELLIPSETHRESHOLD)
			{
				fvec(i) = s * s / 2.0;
			}
			else
			{
				fvec(i) = ELLIPSETHRESHOLD * s - ELLIPSETHRESHOLD * ELLIPSETHRESHOLD / 2.0;
			}
		}
		double disc = (x(0)*x(0) - 4.0*x(1));

		if (disc < 0.0)
		{
			fvec(nPoints) = 0.0;
		}
		else if (disc < 1.0)
		{
			fvec(nPoints) = 100000.0 + 1000.0*disc;
		}
		else
		{
			fvec(nPoints) = 100000.0 + 1000.0*disc*disc*disc;
		}
		return 0;
	}

	int nPoints;
	Eigen::Matrix2Xd PointsElli;
};



struct optimization_ellipse_functor_Regression : Functor<double>
{
	optimization_ellipse_functor_Regression(int size, std::vector<cv::Point2i> points, ellipseParams lastEllipse) : Functor<double>(5, size + 2)
	{
		nPoints = size;
		PointsElli = Eigen::Matrix2Xd(2, size);
		for (int i = 0; i < size; i++)
		{
			PointsElli(0, i) = (double)(points[i].x);
			PointsElli(1, i) = (double)(points[i].y);
		}
		
		Lellipse.a = 1.0;
		Lellipse.b = lastEllipse.b;
		Lellipse.c = lastEllipse.c;
		Lellipse.d = lastEllipse.d;
		Lellipse.e = lastEllipse.e;
		Lellipse.f = lastEllipse.f;
	}	


	int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const
	{
		fvec(0) = 0.0;
		std::vector<distStruct> distV(0);
		for (int i = 0; i < nPoints; i++)
		{
			fvec(i) = 0.0;
			if (abs(PointsElli(0, i)) < 0.00001)
			{
				fvec(i) = 0.0;
				continue;
			}
			double s = abs(PointsElli(0, i)*PointsElli(0, i) + x(0)*PointsElli(0, i)*PointsElli(1, i) + x(1)*PointsElli(1, i)*PointsElli(1, i) + x(2)*PointsElli(0, i) + x(3)*PointsElli(1, i) + x(4)) / 2.0 / sqrt((PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0)*(PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0) + (x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0)*(x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0) + 0.0001);
			distStruct ds;
			ds.ind = i;
			ds.dist = s;
			distV.push_back(ds);

		}
		std::sort(distV.begin(), distV.end(), [](distStruct a, distStruct b) { return a.dist < b.dist; });

		for (int i = 0; i < min(nPoints*PERCENTAGEPOINTSOULIERS, 0.9*distV.size()); i++)
		{
			double s = distV[i].dist;
			///x(0)*PointsElli(0, i)*PointsElli(0, i) + x(1)*PointsElli(0, i)*PointsElli(1, i) + x(2)*PointsElli(1, i)*PointsElli(1, i) + x(3)*PointsElli(0, i) + x(4)*PointsElli(1, i) + x(5);
			if (s < ELLIPSETHRESHOLD)
			{
				fvec(i) = s * s / 2.0;
			}
			else
			{
				fvec(i) = ELLIPSETHRESHOLD * s - ELLIPSETHRESHOLD * ELLIPSETHRESHOLD / 2.0;
			}
		}
		double disc = (x(0)*x(0) - 4.0*x(1));

		if (disc < 0.0)
		{
			fvec(nPoints) = 0.0;
		}
		else if (disc < 1.0)
		{
			fvec(nPoints) = 100000.0 + 1000.0*disc;
		}
		else
		{
			fvec(nPoints) = 100000.0 + 1000.0*disc*disc*disc;
		}
		
		double cEllix = (2.0*Lellipse.c*Lellipse.d - Lellipse.b*Lellipse.e) / (Lellipse.b*Lellipse.b - 4.0*Lellipse.a*Lellipse.c);
		double cElliy = (2.0*Lellipse.a*Lellipse.e - Lellipse.b*Lellipse.d) / (Lellipse.b*Lellipse.b - 4.0*Lellipse.a*Lellipse.c);

		fvec(nPoints + 1) = REGRESSIONVALUE*(abs((Lellipse.b-x(0))*cEllix*cElliy) + abs((Lellipse.c - x(1))*cElliy*cElliy) + abs((Lellipse.d - x(2))*cEllix) + abs((Lellipse.e - x(3))*cElliy) + abs(Lellipse.f - x(4)));
		return 0;
	}

	int nPoints;
	Eigen::Matrix2Xd PointsElli;
	ellipseParams Lellipse;
};


struct optimization_ellipse_functor2 : Functor<double>
{
	optimization_ellipse_functor2(int size, std::vector<cv::Point2i> points) : Functor<double>(5, size+1)
	{
		nPoints = size;
		PointsElli = Eigen::Matrix2Xd(2, size);
		for (int i = 0; i < size; i++)
		{
			PointsElli(0, i) = (double)(points[i].x);
			PointsElli(1, i) = (double)(points[i].y);
		}
	}


	int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const
	{
		fvec(0) = 0.0;

		for (int i = 0; i < nPoints; i++)
		{
			fvec(i) = 0.0;
			if (abs(PointsElli(0, i)) < 0.00001)
			{
				fvec(i) = 0.0;
			}
			else
			{
				double s = abs(PointsElli(0, i)*PointsElli(0, i) + x(0)*PointsElli(0, i)*PointsElli(1, i) + x(1)*PointsElli(1, i)*PointsElli(1, i) + x(2)*PointsElli(0, i) + x(3)*PointsElli(1, i) + x(4)) / 2.0 / sqrt((PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0)*(PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0) + (x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0)*(x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0)+1.0);
				///x(0)*PointsElli(0, i)*PointsElli(0, i) + x(1)*PointsElli(0, i)*PointsElli(1, i) + x(2)*PointsElli(1, i)*PointsElli(1, i) + x(3)*PointsElli(0, i) + x(4)*PointsElli(1, i) + x(5);
				if (s < ELLIPSETHRESHOLD)
				{
					fvec(i) = s * s / 2.0;
				}
				else
				{
					fvec(i) = ELLIPSETHRESHOLD * s - ELLIPSETHRESHOLD * ELLIPSETHRESHOLD / 2.0;
				}
				if (fvec(i) < 0.0)
				{
					fvec(i) = 0.0;
				}
			}
		}
		double disc = x(0)*x(0) - 4.0*x(1);
		if (disc < 0.0)
		{
			fvec(nPoints) = 0.0;
		}
		else if (disc < 1.0)
		{
			fvec(nPoints) = 100000.0 + 1000.0*disc;
		}
		else
		{
			fvec(nPoints) = 100000.0 + 1000.0*disc*disc*disc;
		}
		return 0;
	}

	int nPoints;
	Eigen::Matrix2Xd PointsElli;
};

struct optimization_ellipse_functorN2 : Functor<double>
{
	optimization_ellipse_functorN2(int size, std::vector<cv::Point2i> points) : Functor<double>(5, size + 1)
	{
		nPoints = size;
		PointsElli = Eigen::Matrix2Xd(2, size);
		for (int i = 0; i < size; i++)
		{
			PointsElli(0, i) = (double)(points[i].x);
			PointsElli(1, i) = (double)(points[i].y);
		}
	}


	int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const
	{
		fvec(0) = 0.0;

		for (int i = 0; i < nPoints; i++)
		{
			if (abs(PointsElli(0, i)) < 0.00001)
			{
				fvec(i) = 0.0;
			}
			else
			{
				double s = abs(PointsElli(0, i)*PointsElli(0, i) + x(0)*PointsElli(0, i)*PointsElli(1, i) + x(1)*PointsElli(1, i)*PointsElli(1, i) + x(2)*PointsElli(0, i) + x(3)*PointsElli(1, i) + x(4)) / 2.0 / sqrt((PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0)*(PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0) + (x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0)*(x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0));
				///x(0)*PointsElli(0, i)*PointsElli(0, i) + x(1)*PointsElli(0, i)*PointsElli(1, i) + x(2)*PointsElli(1, i)*PointsElli(1, i) + x(3)*PointsElli(0, i) + x(4)*PointsElli(1, i) + x(5);
				fvec(i) = s * s / 2.0;
			}
		}
		double disc = (x(0)*x(0) - 4.0*x(1));
		if (disc < 0.0)
		{
			fvec(nPoints) = 0.0;
		}
		else if (disc < 1.0)
		{
			fvec(nPoints) = 100000.0 + 1000.0*disc;
		}
		else
		{
			fvec(nPoints) = 100000.0 + 1000.0*disc*disc*disc;
		}
		return 0;
	}

	int nPoints;
	Eigen::Matrix2Xd PointsElli;
};

struct optimization_ellipse_constant_functor : Functor<double>
{
	optimization_ellipse_constant_functor(int size, std::vector<cv::Point2i> points, ellipseParams ellipse) : Functor<double>(1, size)
	{
		nPoints = size;
		PointsElli = Eigen::Matrix2Xd(2, size);
		for (int i = 0; i < size; i++)
		{
			PointsElli(0, i) = (double)(points[i].x);
			PointsElli(1, i) = (double)(points[i].y);
		}
		a = ellipse.a;
		b = ellipse.b;
		c = ellipse.c;
		d = ellipse.d;
		e = ellipse.e;
	}


	int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const
	{
		std::vector<distStruct> distV(0);
		for (int i = 0; i < nPoints; i++)
		{
			if (abs(PointsElli(0, i)) < 0.00001)
			{
				continue;
			}
			double s = abs(a*PointsElli(0, i)*PointsElli(0, i) + b*PointsElli(0, i)*PointsElli(1, i) + c*PointsElli(1, i)*PointsElli(1, i) + d*PointsElli(0, i) + e*PointsElli(1, i) + x(0)) / 2.0 / sqrt((a*PointsElli(0, i) + b*PointsElli(1, i) / 2.0 + d / 2.0)*(a*PointsElli(0, i) + b*PointsElli(1, i) / 2.0 + d / 2.0) + (b*PointsElli(0, i) / 2.0 + c*PointsElli(1, i) + e / 2.0)*(b*PointsElli(0, i) / 2.0 + c*PointsElli(1, i) + e / 2.0));
			distStruct ds;
			ds.ind = i;
			ds.dist = s;
			distV.push_back(ds);

		}
		std::sort(distV.begin(), distV.end(), [](distStruct a, distStruct b) { return a.dist < b.dist; });

		int m = (int)min(nPoints*PERCENTAGEPOINTSOULIERS, 0.9*distV.size());
		for (int i = 0; i < m; i++)
		{
			fvec(i) = distV[i].dist;
		}
		for (int i = m; i < nPoints; i++)
		{
			fvec(i) = 0.0;
		}
		return 0;
	}

	int nPoints;
	Eigen::Matrix2Xd PointsElli;
	double a, b, c, d, e;
};


// CTrackingHerniaDlg dialog
class CTrackingHerniaDlg : public CDialogEx
{
// Construction
public:
	CTrackingHerniaDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TRACKINGHERNIA_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CButton m_butProc;
	CStatic m_picImageInitial;
	CStatic m_picImageProc;

	cv::Size winSegSizeIni, winSegSizeProc;
	cv::Mat cvImgTmpIni, cvImgTmpProc;
	CImage* mfcImg;
	BITMAPINFO bitInfoIni, bitInfoProc;

	uint8_t datacross[3][3];
	cv::Mat gradMorf, segdilate, segerode, squareKernel, crossKernel;
	cv::Mat segClose;
	int segrowi, segrowsize, segcoli, segcolsize;

	int numSegm;
	std::vector<cv::Point2f> meanSegmentats;

	ellipseParams lastEllipse;

	std::vector<cv::Point> lastPoints;
	cv::Mat last_seg_image;
	cv::Mat last_src_image;

	CString imgPath;
	CString outPath;

	//Functions
	void SetInitialImage(cv::Mat & entrada);
	void SetProcImage(cv::Mat & entrada);
	void paintEllipse(ellipseParams &ellipse, cv::Mat &image, double red, double green, double blue);
	int creixementRegio(cv::Mat & imatge, cv::Mat & mascara, int x, int y, int lbl, std::vector <cv::Point2i> & points);
	int creixementRegioContorn(cv::Mat & imatge, cv::Mat & mascara, int x, int y, int lbl, std::vector <cv::Point2i> & points);
	void trobarPuntsElipse(cv::Mat &imatge, std::vector<cv::Point2i> &punts, int nPunts, int rowInici, int colInici);
	void trobarPuntsElipseHSV(cv::Mat &imatge, std::vector<cv::Point2i> &punts, int nPunts, int rowInici, int colInici);
	std::vector<cv::Point> GetPointsUsingLastEllipse(cv::Mat &image, ellipseParams & ellipse, int x0, int y0, int width, int height, int nPoints);
	std::vector<cv::Point> GetPointsUsingLastEllipse(cv::Mat &image, ellipseParams & ellipse, std::vector<cv::Point> &last_points, int x, int y, int width, int height, int nPoints);
	std::vector<cv::Point> GetPointsUsingLastEllipseHSV(cv::Mat &image, ellipseParams & ellipse, int x, int y, int width, int height, int nPoints);
	std::vector<cv::Point> GetPointsUsingLastEllipseHSV(cv::Mat &image, ellipseParams & ellipse, std::vector<cv::Point> &last_points, int x, int y, int width, int height, int nPoints);
	std::vector<cv::Point> GetPointsUsingLastEllipseCompleteHSV(cv::Mat &image, ellipseParams & ellipse, std::vector<cv::Point> &last_points, int x, int y, int width, int height, int nPoints);


	std::vector < cv::Point> GetPointsRealImageUsingLastEllipse(cv::Mat &image, ellipseParams & ellipse, std::vector<cv::Point> & last_points, int x, int y, int width, int height);
	
	std::vector<cv::Point> ReducePoints(std::vector<cv::Point> & points, ellipseParams & ellipse);
	std::vector<cv::Point> ReducePointsCNNUsingLastEllipse(cv::Mat & image, ellipseParams & ellipse, std::vector<cv::Point> &points);
	
	ellipseParams GetEllipseLMS(std::vector<cv::Point> & points);
	ellipseParams GetEllipsePCA(std::vector<cv::Point> & points);
	ellipseParams GetEllipseMixNorm(std::vector<cv::Point> & points, ellipseParams & initial);
	ellipseParams GetEllipseMixNorm2(std::vector<cv::Point> & points, ellipseParams & initial);
	ellipseParams GetEllipseMixNormN2(std::vector<cv::Point> & points, ellipseParams & initial);
	ellipseParams GetEllipseConstant(std::vector<cv::Point> & points, ellipseParams & initial);
	ellipseParams GetEllipseMixNormRegression(std::vector<cv::Point> & points, ellipseParams & initial);
	
	void SegmentacioImatge(cv::Mat &entrada, cv::Mat &sortida);
	inline cv::Mat imCompositeFilter(cv::Mat &I);
	void thinningIteration(cv::Mat& img, int iter);
	void ZhangSuen(const cv::Mat& src, cv::Mat& dst);
	void Binaritzacion(cv::Mat &entrada, cv::Mat & sortida);

	bool isToolPoint(cv::Point point, cv::Mat &image);
	bool isToolPoint(double * points, int index, int numPunts, int totalPoints);
	std::vector<cv::Point> EraseToolFromPoints(std::vector<cv::Point2i> &points, cv::Mat &image);
	std::vector<cv::Point> EraseToolFromPoints(std::vector<cv::Point2i> &points, double * clasValues, int samplesPerPoint);

	Eigen::MatrixXd elm_RBF_Kernel_eigen(Eigen::MatrixXd & train_data, double kernel_pars, Eigen::MatrixXd Xt);
	Eigen::MatrixXd elm_RBF_Kernel_eigen(Eigen::MatrixXd & train_data, double kernel_pars);
	Eigen::MatrixXd elm_training_phase_eigen(Eigen::MatrixXd & values, Eigen::MatrixXd & data, double regularization_coefficient, double kernel_pars);
	Eigen::MatrixXd elm_output_eigen(Eigen::MatrixXd & training_data, Eigen::MatrixXd & input_data, Eigen::MatrixXd & weights, double kernel_pars);
	Eigen::MatrixXd elm_create_values_eigen(Eigen::VectorXd & input, int nclasses);
	afx_msg void OnBnClickedProc();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedGetpointssynth();
	afx_msg void OnBnClickedSynthtic();
};
