/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * lusolve.cpp
 *
 * Code generation for function 'lusolve'
 *
 */

/* Include files */
#include <cmath>
#include "rt_nonfinite.h"
#include "elm_RBF_Kernel_2_params.h"
#include "elm_RBF_Kernel_3_params.h"
#include "elm_create_values.h"
#include "elm_output.h"
#include "elm_training_phase.h"
#include "lusolve.h"
#include "elm_lib_emxutil.h"
#include "colon.h"

/* Function Definitions */
void lusolve(const emxArray_real_T *A, const emxArray_real_T *B, emxArray_real_T
             *X)
{
  emxArray_real_T *b_A;
  int i8;
  int iy;
  emxArray_real_T *b_B;
  int kAcol;
  int jA;
  emxArray_int32_T *ipiv;
  int n;
  int u1;
  int j;
  int mmj;
  double smax;
  int c;
  int k;
  int ix;
  double s;
  int ijA;
  emxInit_real_T(&b_A, 2);
  i8 = b_A->size[0] * b_A->size[1];
  b_A->size[1] = A->size[0];
  b_A->size[0] = A->size[1];
  emxEnsureCapacity_real_T(b_A, i8);
  iy = A->size[1];
  for (i8 = 0; i8 < iy; i8++) {
    kAcol = A->size[0];
    for (jA = 0; jA < kAcol; jA++) {
      b_A->data[jA + b_A->size[1] * i8] = A->data[i8 + A->size[1] * jA];
    }
  }

  emxInit_real_T(&b_B, 2);
  i8 = b_B->size[0] * b_B->size[1];
  b_B->size[1] = B->size[0];
  b_B->size[0] = 3;
  emxEnsureCapacity_real_T(b_B, i8);
  for (i8 = 0; i8 < 3; i8++) {
    iy = B->size[0];
    for (jA = 0; jA < iy; jA++) {
      b_B->data[jA + b_B->size[1] * i8] = B->data[i8 + B->size[1] * jA];
    }
  }

  emxInit_int32_T(&ipiv, 2);
  n = b_A->size[0];
  iy = b_A->size[0];
  eml_signed_integer_colon(iy, ipiv);
  if (!(b_A->size[0] < 1)) {
    iy = b_A->size[0] - 1;
    u1 = b_A->size[0];
    if (iy < u1) {
      u1 = iy;
    }

    for (j = 0; j < u1; j++) {
      mmj = n - j;
      c = j * (n + 1);
      if (mmj < 1) {
        jA = -1;
      } else {
        jA = 0;
        if (mmj > 1) {
          ix = c;
          smax = std::abs(b_A->data[c]);
          for (k = 2; k <= mmj; k++) {
            ix++;
            s = std::abs(b_A->data[ix]);
            if (s > smax) {
              jA = k - 1;
              smax = s;
            }
          }
        }
      }

      if (b_A->data[c + jA] != 0.0) {
        if (jA != 0) {
          ipiv->data[j] = (j + jA) + 1;
          ix = j;
          iy = j + jA;
          for (k = 1; k <= n; k++) {
            smax = b_A->data[ix];
            b_A->data[ix] = b_A->data[iy];
            b_A->data[iy] = smax;
            ix += n;
            iy += n;
          }
        }

        i8 = c + mmj;
        for (jA = c + 1; jA < i8; jA++) {
          b_A->data[jA] /= b_A->data[c];
        }
      }

      kAcol = n - j;
      jA = (c + n) + 1;
      iy = c + n;
      for (k = 1; k < kAcol; k++) {
        smax = b_A->data[iy];
        if (b_A->data[iy] != 0.0) {
          ix = c + 1;
          i8 = mmj + jA;
          for (ijA = jA; ijA < i8 - 1; ijA++) {
            b_A->data[ijA] += b_A->data[ix] * -smax;
            ix++;
          }
        }

        iy += n;
        jA += n;
      }
    }
  }

  for (iy = 0; iy < n - 1; iy++) {
    if (ipiv->data[iy] != iy + 1) {
      kAcol = ipiv->data[iy] - 1;
      for (jA = 0; jA < 3; jA++) {
        smax = b_B->data[iy + b_B->size[1] * jA];
        b_B->data[iy + b_B->size[1] * jA] = b_B->data[kAcol + b_B->size[1] * jA];
        b_B->data[kAcol + b_B->size[1] * jA] = smax;
      }
    }
  }

  emxFree_int32_T(&ipiv);
  if (b_B->size[1] != 0) {
    for (j = 0; j < 3; j++) {
      iy = n * j - 1;
      for (k = 1; k <= n; k++) {
        kAcol = n * (k - 1);
        if (b_B->data[k + iy] != 0.0) {
          for (jA = k + 1; jA <= n; jA++) {
            b_B->data[jA + iy] -= b_B->data[k + iy] * b_A->data[(jA + kAcol) - 1];
          }
        }
      }
    }
  }

  if (b_B->size[1] != 0) {
    for (j = 0; j < 3; j++) {
      iy = n * j - 1;
      for (k = n; k > 0; k--) {
        kAcol = n * (k - 1) - 1;
        if (b_B->data[k + iy] != 0.0) {
          b_B->data[k + iy] /= b_A->data[k + kAcol];
          for (jA = 1; jA < k; jA++) {
            b_B->data[jA + iy] -= b_B->data[k + iy] * b_A->data[jA + kAcol];
          }
        }
      }
    }
  }

  emxFree_real_T(&b_A);
  i8 = X->size[0] * X->size[1];
  X->size[1] = 3;
  X->size[0] = b_B->size[1];
  emxEnsureCapacity_real_T(X, i8);
  iy = b_B->size[1];
  for (i8 = 0; i8 < iy; i8++) {
    for (jA = 0; jA < 3; jA++) {
      X->data[jA + X->size[1] * i8] = b_B->data[i8 + b_B->size[1] * jA];
    }
  }

  emxFree_real_T(&b_B);
}

/* End of code generation (lusolve.cpp) */
