#include <cuda_runtime.h>
#include <cublas.h>
#include <cublas_api.h>
#include <cublas_v2.h>
#include <stdio.h>

cudaError_t setup_gpu(int gpu_id, cublasHandle_t &handle);
void close_gpu(cublasHandle_t &handle);
cudaError_t up_to_gpu(double * input, double ** dev_output, int size);
cudaError_t down_from_gpu(double * dev_input, double * output, int size);
cudaError_t save_memory_gpu(double ** dev_memory, int size);

extern cudaError_t setup_gpu(int gpu_id, cublasHandle_t &handle)
{
	cudaError_t cudaStatus;

	cudaStatus = cudaSetDevice(gpu_id);
	cublasCreate(&handle);
	return cudaStatus;
}

extern void close_gpu(cublasHandle_t &handle)
{
	cublasDestroy(handle);
}

extern cudaError_t up_to_gpu(double * input, double ** dev_output, int size)
{
	cudaError_t cudaStatus = cudaMalloc((void**)dev_output, size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMemcpy(*dev_output, input, size * sizeof(double), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMemcpy failed!\n", __func__, __LINE__);
	}

out:
	return cudaStatus;
}

extern cudaError_t down_from_gpu(double * dev_input, double * output, int size)
{
	cudaError_t cudaStatus = cudaMemcpy(output, dev_input, size * sizeof(double), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMemcpy failed!\n", __func__, __LINE__);
	}
	return cudaStatus;
}

extern cudaError_t save_memory_gpu(double ** dev_memory, int size)
{
	cudaError_t cudaStatus = cudaMalloc((void**)dev_memory, size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
	}
	return cudaStatus;
}