#include <cuda_runtime.h>
#include "device_launch_parameters.h"
#include <cublas.h>
#include <cublas_api.h>
#include <cublas_v2.h>
#include <cusolverDn.h>
#include <stdio.h>


__global__ void elm_create_values_aux(double * input, double * output, int size)
{
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	if (i < size)
	{
		output[i] = -1.0;
		output[size + i] = -1.0;
		output[2 * size + i] = -1.0;

		int j = (int)input[i] - 1;
		output[j * size + i] = 1.0;

	}
}

__global__ void sum_square_aux(double * input, double * output, int size)
{
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	if (i < size)
	{
		output[i] = input[3 * i] * input[3 * i] + input[3 * i + 1] * input[3 * i + 1] + input[3 * i + 2] * input[3 * i + 2];
	}
}

__global__ void transpose_data_cuda(double *input, double *output, int size)
{
	int index = blockIdx.x*blockDim.x + threadIdx.x;
	int iO1 = index / size;
	int iO2 = 3 * index % 3 * size;

	int indexO = iO1 + iO2;

	if (index < 3*size)
	{
		output[indexO] = input[index];
	}
}

__global__ void exp_sum_omega_aux(double * xxh, double * xcorr, double * output, int size, double kernel_param, double reg_coeff)
{
	int col = blockIdx.y*blockDim.y + threadIdx.y;
	int row = blockIdx.x*blockDim.x + threadIdx.x;

	if ((row < size) && (col < size))
	{
		if (row == col)
		{
			output[row*size + col] = exp((2.0*xcorr[row*size + col] - xxh[row*size + col] - xxh[col*size + row]) / kernel_param) + 1.0 / reg_coeff;
		}
		else
		{
			output[row*size + col] = exp((2.0*xcorr[row*size + col] - xxh[row*size + col] - xxh[col*size + row]) / kernel_param);
		}
	}
}

__global__ void exp_sum_omega_aux_2(double * xxh1, double * xxh2, double * xcorr, double * output, int size1, int size2, double kernel_param)
{
	int row = blockIdx.y*blockDim.y + threadIdx.y;
	int col = blockIdx.x*blockDim.x + threadIdx.x;

	if ((row < size1) && (col < size2))
	{
		output[col*size1 + row] = exp((2.0*xcorr[col*size1 + row] - xxh1[col*size1 + row] - xxh2[row*size2 + col]) / kernel_param);
	}
}


cudaError_t elm_create_values(double * input, double ** output, int size);
cudaError_t elm_training_phase(cublasHandle_t &handle, double * dev_data, double *dev_values, double ** dev_output, double **dev_ones, int size, double kernel_par, double reg_coeff);
cudaError_t elm_output(cublasHandle_t &handle, double * dev_data, double *dev_values, double * dev_weights, double * dev_onesD, double ** dev_output, int sizeD, int sizeV, double kernel_par);

cudaError_t elm_RBF_Kernel_2_params(cublasHandle_t &handle, double *input, double *output, double **dev_ones, int size, double kernel_par, double reg_coeff)
{
	double *ones_d = new double[size];
	double *dev_square = 0;
	double *dev_XXh = 0;
	double *dev_trainCorr = 0;
	int threadBlock = 512;
	int blockSize = (size + threadBlock - 1) / threadBlock;
	int blockSize2 = (size*size + threadBlock - 1) / threadBlock;
	cudaError_t cudaStatus;
	double alpha = 1.0;
	double beta = 0.0;

	int threadBlock2 = 32;
	dim3 threadsPerBlock(threadBlock2, threadBlock2);
	dim3 blocksPerGrid((size + threadsPerBlock.x - 1) / threadsPerBlock.x, (size + threadsPerBlock.y - 1) / threadsPerBlock.y);

	auto tmpv = ones_d;
	for (int i = 0; i < size; i++)
	{
		*tmpv++ = 1.0;
	}

	cudaStatus = cudaMalloc((void**)&dev_square, size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMalloc((void**)&dev_XXh, size*size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMalloc((void**)&dev_trainCorr, size * size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMalloc((void**)dev_ones, size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}

	cudaStatus = cudaMemcpy(*dev_ones, ones_d, size * sizeof(double),
		cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}

	double * inputH = new double[3 * size];
	cudaStatus = cudaMemcpy(inputH, input, 3*size * sizeof(double),
		cudaMemcpyDeviceToHost);

	sum_square_aux << <blockSize, threadBlock >> > (input, dev_square, size);
	cudaStatus = cudaThreadSynchronize();
	cublasStatus_t cublasstatus  = cublasDgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, size, size, 1,
		&alpha, dev_square, 1, *dev_ones, 1, &beta, dev_XXh, size);
	cudaStatus = cudaThreadSynchronize();

	alpha = 1.0;
	beta = 0.0;
	cublasDgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, size, size, 3,
		&alpha, input, 3, input, 3, &beta, dev_trainCorr, size);
	cudaStatus = cudaThreadSynchronize();

	//exp_sum_omega_aux(dev_XXh, dev_trainCorr, output, size, kernel_par, reg_coeff);
	exp_sum_omega_aux << <blocksPerGrid, threadsPerBlock >> > (dev_XXh, dev_trainCorr, output, size, kernel_par, reg_coeff);
	cudaStatus = cudaThreadSynchronize();


out:
	if (dev_square) cudaFree(dev_square);
	if (dev_XXh) cudaFree(dev_XXh);
	if (dev_trainCorr) cudaFree(dev_trainCorr);
	delete[]ones_d;
	return cudaStatus;// == cudaSuccess;
}

cudaError_t elm_RBF_Kernel_3_params(cublasHandle_t &handle, double *data, double *values, double *weights, double *dev_onesD, double *output, int sizeD, int sizeV, double kernel_par)
{
	double *ones_dV = new double[sizeV];
	double *dev_square1 = 0;
	double *dev_square2 = 0;
	double *dev_onesV = 0;
	double *dev_XXh1 = 0;
	double *dev_XXh2 = 0;
	double *dev_trainCorr = 0;
	double *dev_omega = 0;
	int threadBlock = 512;
	int blockSizeV = (sizeV + threadBlock - 1) / threadBlock;
	int blockSizeD = (sizeD + threadBlock - 1) / threadBlock;
	int blockSize2 = (sizeV*sizeD + threadBlock - 1) / threadBlock;
	cudaError_t cudaStatus;
	double alpha = 1.0;
	double beta = 0.0;

	int threadBlock2 = 32;
	dim3 threadsPerBlock(threadBlock2, threadBlock2);
	dim3 blocksPerGrid((sizeV + threadsPerBlock.x - 1) / threadsPerBlock.x, (sizeD + threadsPerBlock.y - 1) / threadsPerBlock.y);

	auto tmpv = ones_dV;
	for (int i = 0; i < sizeV; i++)
	{
		*tmpv++ = 1.0;
	}

	cudaStatus = cudaMalloc((void**)&dev_square1, sizeD * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMalloc((void**)&dev_square2, sizeV * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}

	cudaStatus = cudaMalloc((void**)&dev_XXh1, sizeD*sizeV * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMalloc((void**)&dev_XXh2, sizeD*sizeV * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}

	cudaStatus = cudaMalloc((void**)&dev_trainCorr, sizeV * sizeD * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	
	cudaStatus = cudaMalloc((void**)&dev_onesV, sizeV * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMalloc((void**)&dev_omega, sizeD*sizeV * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}


	//Copy to GPU
	cudaStatus = cudaMemcpy(dev_onesV, ones_dV, sizeV * sizeof(double),
		cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}

	sum_square_aux << <blockSizeD, threadBlock >> > (data, dev_square1, sizeD);
	cudaStatus = cudaThreadSynchronize();

	sum_square_aux << <blockSizeV, threadBlock >> > (values, dev_square2, sizeV);
	cudaStatus = cudaThreadSynchronize();
	
	cublasStatus_t cublasstatus = cublasDgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, sizeD, sizeV, 1,
		&alpha, dev_square1, 1, dev_onesV, 1, &beta, dev_XXh1, sizeD);
	cudaStatus = cudaThreadSynchronize();

	cublasstatus = cublasDgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, sizeV, sizeD, 1,
		&alpha, dev_square2, 1, dev_onesD, 1, &beta, dev_XXh2, sizeV);
	cudaStatus = cudaThreadSynchronize();

	alpha = 1.0;
	beta = 0.0;
	cublasstatus = cublasDgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, sizeD, sizeV, 3,
		&alpha, data, 3, values, 3, &beta, dev_trainCorr, sizeD);
	cudaStatus = cudaThreadSynchronize();

	//exp_sum_omega_aux(dev_XXh, dev_trainCorr, output, size, kernel_par, reg_coeff);
	exp_sum_omega_aux_2 << <blocksPerGrid, threadsPerBlock >> > (dev_XXh1, dev_XXh2, dev_trainCorr, dev_omega, sizeD, sizeV, kernel_par);
	cudaStatus = cudaThreadSynchronize();

	cublasstatus = cublasDgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, sizeV, 3, sizeD,
		&alpha, dev_omega, sizeD, weights, sizeD, &beta, output, sizeV);
	cudaStatus = cudaThreadSynchronize();

out:
	if (dev_onesV) cudaFree(dev_onesV);
	if (dev_square1) cudaFree(dev_square1);
	if (dev_square2) cudaFree(dev_square2);
	if (dev_XXh1) cudaFree(dev_XXh1);
	if (dev_XXh2) cudaFree(dev_XXh2);
	if (dev_trainCorr) cudaFree(dev_trainCorr);
	if (dev_omega) cudaFree(dev_omega);
	delete[]ones_dV;
	return cudaStatus;// == cudaSuccess;
}

extern cudaError_t elm_create_values(double * input, double ** output, int size)
{
	double *dev_input = 0;
	//double *dev_output = 0;
	int threadBlock = 256;
	int blockSize = (size + threadBlock - 1) / threadBlock;
	cudaError_t cudaStatus;

	cudaStatus = cudaMalloc((void**)&dev_input, size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMalloc((void**)output, 3 * size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMemcpy(dev_input, input, size * sizeof(double),
		cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}

	elm_create_values_aux <<<blockSize, threadBlock>>> (dev_input, *output, size);

	/*cudaStatus = cudaMemcpy(output, dev_output, 3 * size * sizeof(double),
		cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMemcpy failed!\n", __func__, __LINE__);
		goto out;
	}*/

out:
	if (dev_input) cudaFree(dev_input);
	//if (dev_output) cudaFree(dev_output);
	return cudaStatus;// == cudaSuccess;
}

extern cudaError_t elm_training_phase(cublasHandle_t &handle, double * dev_data, double *dev_values, double ** dev_output, double ** dev_ones, int size, double kernel_par, double reg_coeff)
{
	double *dev_omega = 0;
	int threadBlock = 256;
	int blockSize = (size + threadBlock - 1) / threadBlock;
	cudaError_t cudaStatus;

	//Allocate space in the GPU for all the matrices and vectors
	cudaStatus = cudaMalloc((void**)dev_output, 3 * size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}
	cudaStatus = cudaMalloc((void**)&dev_omega, size * size * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}

	//operations
	elm_RBF_Kernel_2_params(handle,dev_data, dev_omega, dev_ones, size, kernel_par, reg_coeff);

	double * dev_solver = 0;
	int * dev_Ipiv = 0;
	int * dev_info = 0;
	int lwork = 0;

	cusolverDnHandle_t cusolverH = NULL;
	cudaStream_t stream = NULL;
	cusolverStatus_t cusolStatus;
	cusolStatus = cusolverDnCreate(&cusolverH);
	if (cusolStatus != CUSOLVER_STATUS_SUCCESS)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out2;
	}

	cudaStatus = cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking);
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out2;
	}

	cusolStatus = cusolverDnSetStream(cusolverH, stream);
	if (cusolStatus != CUSOLVER_STATUS_SUCCESS)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out2;
	}

	cusolStatus = cusolverDnDgetrf_bufferSize(cusolverH, size, size, dev_omega, size, &lwork);
	if(cusolStatus != CUSOLVER_STATUS_SUCCESS )
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out2;
	}
	cudaStatus = cudaMalloc((void**)&dev_solver, sizeof(double)*lwork);
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out2;
	}

	cusolStatus = cusolverDnDgetrf(cusolverH, size, size, dev_omega, size, dev_solver, dev_Ipiv, dev_info);
	cudaStatus = cudaDeviceSynchronize();
	if (cusolStatus != CUSOLVER_STATUS_SUCCESS)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out2;
	}
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out2;
	}

	cudaStatus = cudaMemcpy(*dev_output, dev_values, 3 * size * sizeof(double), cudaMemcpyDeviceToDevice);

	/*int threadBlock3 = 512;
	int blockGrid3 = (3 * size + threadBlock3 - 1) / threadBlock3;
	transpose_data_cuda << <blockGrid3, threadBlock3 >> > (dev_values, dev_output, size);*/

	cusolStatus = cusolverDnDgetrs(cusolverH, CUBLAS_OP_N, size, 3, dev_omega, size, dev_Ipiv, *dev_output, size, dev_info);
	cudaStatus = cudaDeviceSynchronize();

out2:
	if (cusolverH) cusolverDnDestroy(cusolverH);
	if (dev_solver) cudaFree(dev_solver);
	if (dev_Ipiv) cudaFree(dev_Ipiv);
	if (dev_info) cudaFree(dev_info);
	
	//cudaStatus = cudaMemcpy(output, output, sizeof(double)*3 * size, cudaMemcpyDeviceToHost);
out:
	if (dev_omega) cudaFree(dev_omega);
	return cudaStatus;// == cudaSuccess;
}

extern cudaError_t elm_output(cublasHandle_t &handle, double * dev_data, double *dev_values, double * dev_weights, double * dev_onesD, double ** dev_output, int sizeD, int sizeV, double kernel_par)
{
	int threadBlock = 256;
	int blockSizeD = (sizeD + threadBlock - 1) / threadBlock;
	int blockSizeV = (sizeV + threadBlock - 1) / threadBlock;
	cudaError_t cudaStatus;

	cudaStatus = cudaMalloc((void**)dev_output, 3 * sizeV * sizeof(double));
	if (cudaStatus != cudaSuccess)
	{
		printf("%s, line %d, cudaMalloc failed!\n", __func__, __LINE__);
		goto out;
	}

	//operations
	elm_RBF_Kernel_3_params(handle, dev_data, dev_values, dev_weights, dev_onesD, *dev_output, sizeD, sizeV, kernel_par);


	//cudaStatus = cudaMemcpy(output, output, sizeof(double)*3 * size, cudaMemcpyDeviceToHost);
out:
	return cudaStatus;// == cudaSuccess;
}