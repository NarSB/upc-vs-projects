
// DemoRegistreDlg.h: archivo de encabezado
//

#pragma once
#include "afxwin.h"
#include "uEyeCam.h"
#include "stdafx.h"
#include <mutex>

//EIGEN LIBRARY
#include <Eigen/Dense>
#include <Eigen/Core>
#include <unsupported/Eigen/NonLinearOptimization>
#ifndef EIGEN_DONT_ALIGN_STATICALLY
#define EIGEN_DONT_ALIGN_STATICALLY  // http://eigen.tuxfamily.org/dox-devel/group__TopicUnalignedArrayAssert.html
#endif


#define ROB_CLIENT 1
#define ROB_CLIENT_PORT 1004
#define STATE_WAITING 0
#define TOLDIST 30.0f
#define TOLDISTOMPLIR 10.0f
#define ANGMAXOMPLIRCONTORN 30
#define TOLERANCIA 0.0001
#define MAXITERLM 3
#define MINVALNORMAOPTIMITZACIO 0.04
#define MAXVALNORMAOPTIMITZACIO 0.06
#define FRAMERATEPINTAR 50.0f
#define PUNTSCONTORNIMATGE2D 100
#define NUMMEANPUNTSCONTORN 10
#define MINTAMANYZONACONTORN 10
#define DISTANCIAMEANCONTORN 10

#define KFSTATEDIM 2 //30


#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using namespace Eigen;

// Cuadro de di�logo de CDemoRegistreDlg
class CDemoRegistreDlg : public CDialogEx
{
// Construcci�n
public:
	CDemoRegistreDlg(CWnd* pParent = NULL);	// Constructor est�ndar

	COurSocket * RobotClientSock;
	CString m_strRobotIP;
	bool bRobConnected;
	CString sms;
	int puntTrajectoria, nPuntsTotals, nPuntsRob, puntRobotTrajectoria;
	int posNPoint;
	__int64 initTime;
	__int64 freq;

	struct Joints {
		double j0; double j1; double j2; double j3; double j4; double j5;
	};

	struct StauliRob {
		double x; double y; double z;
		double alpha; double beta; double gamma;
		double joint0; double joint1; double joint2; double joint3; double joint4; double joint5;
		double qw; double qx; double qy; double qz;
	};
	StauliRob last_pos;

	// Generic functor

	template<typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
	struct Functor
	{
		typedef _Scalar Scalar;
		enum {
			InputsAtCompileTime = NX,
			ValuesAtCompileTime = NY
		};
		typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
		typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
		typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

		const int m_inputs, m_values;

		Functor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
		Functor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

		int inputs() const { return m_inputs; }
		int values() const { return m_values; }

		// you should define that in the subclass :
		//  void operator() (const InputType& x, ValueType* v, JacobianType* _j=0) const;
	};

	//ICP optimization functor
	struct ICP_optimization_functor : Functor<float>
	{
		ICP_optimization_functor(int size, std::vector<cv::Point2f> * P2D, std::vector<cv::Point3f> * P3D, int numImp) : Functor<float>(7, 2 * size)
		{
			nPunts = size;
			nPuntsImp = numImp;
			Punts2D = Eigen::Matrix2Xf(2,size);
			Punts3D = Eigen::Matrix4Xf(4,size);
			for (int i = 0; i < size; i++)
			{
				Punts2D(0, i) = (*P2D)[i].x;
				Punts2D(1, i) = (*P2D)[i].y;
				Punts3D(0, i) = (*P3D)[i].x;
				Punts3D(1, i) = (*P3D)[i].y;
				Punts3D(2, i) = (*P3D)[i].z;
				Punts3D(3, i) = 1.0f;
			}
		}


		int operator()(const Eigen::VectorXf &x, Eigen::VectorXf &fvec) const
		{
			Eigen::Matrix3f Ri;
			Eigen::Matrix4f TransM;
			Eigen::Matrix4Xf trans3D;
			int count = 0;
			QuaternionToMatrix(x(0), x(1), x(2), x(3), &Ri);

			for (int i = 0; i<3; i++)
			{
				for (int j = 0; j < 3; j++)
					TransM(i, j) = Ri(i, j);
				TransM(i, 3) = x(4 + i);
				TransM(3, i) = 0.0f;
			}
			TransM(3, 3) = 1.0f;
			trans3D = TransM*Punts3D;

			Eigen::VectorXf STf1(nPunts), STf2(nPunts);

			STf1 = trans3D.row(0).cwiseQuotient(trans3D.row(2));
			STf2 = trans3D.row(1).cwiseQuotient(trans3D.row(2));
			
			for (int i = 0; i < nPuntsImp; i++)
			{
				fvec(2 * i) = 2 * (Punts2D(0, i) - STf1(i));
				fvec(2 * i + 1) = 2 * (Punts2D(1, i) - STf2(i));
			}
			for (int i = nPuntsImp; i < nPunts; i++)
			{
				fvec(2 * i) = Punts2D(0, i) - STf1(i);
				fvec(2 * i + 1) = Punts2D(1, i) - STf2(i);
			}
			return 0;
		}

		int nPuntsImp;
		int nPunts;
		Eigen::Matrix2Xf Punts2D;
		Eigen::Matrix4Xf Punts3D;
	};


	//ICP Optimizer Point-to-Plane
	//ICP optimization functor
	struct ICP_optimization_functor_point_to_plane : Functor<float>
	{
		ICP_optimization_functor_point_to_plane(int size, std::vector<cv::Point2f> * P2D, std::vector<cv::Point3f> * P3D, std::vector<cv::Point2f> * Normals, int numImp) : Functor<float>(7, size + numImp)
		{
			nPunts = size;
			nPuntsImp = numImp;
			Punts2D = Eigen::Matrix2Xf(2, size);
			Normals2D = Eigen::Matrix2Xf(2, size);
			Punts3D = Eigen::Matrix4Xf(4, size);
			for (int i = 0; i < size; i++)
			{
				Punts2D(0, i) = (*P2D)[i].x;
				Punts2D(1, i) = (*P2D)[i].y;
				Normals2D(0, i) = (*Normals)[i].x;
				Normals2D(1, i) = (*Normals)[i].y;
				Punts3D(0, i) = (*P3D)[i].x;
				Punts3D(1, i) = (*P3D)[i].y;
				Punts3D(2, i) = (*P3D)[i].z;
				Punts3D(3, i) = 1.0f;
			}
		}


		int operator()(const Eigen::VectorXf &x, Eigen::VectorXf &fvec) const
		{
			Eigen::Matrix3f Ri;
			Eigen::Matrix4f TransM;
			Eigen::Matrix4Xf trans3D;
			int count = 0;
			QuaternionToMatrix(x(0), x(1), x(2), x(3), &Ri);

			for (int i = 0; i<3; i++)
			{
				for (int j = 0; j < 3; j++)
					TransM(i, j) = Ri(i, j);
				TransM(i, 3) = x(4 + i);
				TransM(3, i) = 0.0f;
			}
			TransM(3, 3) = 1.0f;
			trans3D = TransM*Punts3D;

			Eigen::VectorXf STf1(nPunts), STf2(nPunts);

			STf1 = trans3D.row(0).cwiseQuotient(trans3D.row(2));
			STf2 = trans3D.row(1).cwiseQuotient(trans3D.row(2));

			for (int i = 0; i < nPuntsImp; i++)
			{
				fvec(i) = 2 * ((Punts2D(0, i) - STf1(i))*Normals2D(0,i) + (Punts2D(1, i) - STf2(i))*Normals2D(1, i));
				fvec(i + nPuntsImp) = 2*((Punts2D(0, i) - STf1(i))*(Punts2D(0, i) - STf1(i)) + (Punts2D(1, i) - STf2(i))*(Punts2D(1, i) - STf2(i)));
			}
			for (int i = nPuntsImp; i < nPunts; i++)
			{
				fvec(i + nPuntsImp) = ((Punts2D(0, i) - STf1(i))*Normals2D(0, i) + (Punts2D(1, i) - STf2(i))*Normals2D(1, i));
			}
			return 0;
		}

		int nPuntsImp;
		int nPunts;
		Eigen::Matrix2Xf Punts2D;
		Eigen::Matrix2Xf Normals2D;
		Eigen::Matrix4Xf Punts3D;
	};



	/////////////////////////////////////////////////////////
	//Threads
	//Thread Specific Variable and Functions 
	CWinThread * thRecording;
	bool bRecording;	//True: Thread active, False: Kill Thread

						///////////////////////////////////////////////////////////////
						//uEye camera variables and functions
						///////////////////////////////////////////////////////////////
	uEyeCam*        ueCamera;       //camera
	//CvVideoWriter*  VideoWriter;    //The video writer
	CvSize          VideoSize;      //Size of the image (video frame) to be saves
	CvSize          VideoSizeUI;    //Size of the image (video frame) to be displayed on UI
	IplImage*       VideoImage;     //Video image (frame) to be saved
	IplImage*       VideoImageUI;   //Video image (frame) to be displayed on UI
	double          VideoFrameRate; //Video frame rate (FPS)
	int ctrlVideoImageReady;
	cv::Mat lastImage;
	cv::Mat cameraMatrix, distCoef;
	float fc[2], cc[2], alpha_c;
	double kc[5];
	int iTotalFramesRecorded;


	//Segmentation
	bool bSegmenting;
	cv::Mat segImage;
	cv::Size winSegSize;
	cv::Mat cvImgTmp;
	CImage* mfcImg;
	BITMAPINFO bitInfo;
	cv::Mat gradMorf, segdilate, segerode, squareKernel, crossKernel;
	uint8_t datacross[3][3];
	uint8_t b1[3][3], b2[3][3], b3[3][3], b4[3][3];
	cv::Mat b1Kernel, b2Kernel, b3Kernel, b4Kernel;
	cv::Mat puntsSegmentats, puntsSegmentatsDes;
	std::vector<cv::Point2i> puntsSegmentatsvect;
	//std::vector<cv::Point2f> puntsSegmentatsDesvect;
	std::vector<std::vector<cv::Point2i>> ultimsSegmentats;
	int numSegm, primerSegm;
	std::vector<cv::Point2f> meanSegmentats;

	//Punts 2D
	std::vector<cv::Point2f> punts2D;
	std::mutex punts2DMutex;
	bool bPunts2D;

	//Model 3D
	std::vector<cv::Point3f> punts3D;
	std::mutex punts3DMutex;

	//Pintar
	Eigen::Matrix4Xf PuntsPintar3D;
	std::vector<Point2f> PuntsPintar2D;
	bool bPintant;
	CWinThread * thPintar;
	cv::Size pintarSize;

	//ICP
	bool bFinalICP;
	std::mutex finalICP;
	float IniICP[6];
	std::mutex iniICPMutex;
	int nPuntsIn, nPuntsOut, nDelta;
	CWinThread * thICP;


	//Kalman Filter
	cv::Mat transMatrix, measMatrix, noiseCovMatrix, errorCovPost;
	float measurement[6];
	int dadaNova[6];
	bool indKF[6];
	std::mutex indKFMutex;
	bool bFinalKF;
	std::mutex measureMutex;
	std::mutex finalKF;
	std::mutex currentStateMutex;
	float predRate;
	float currentState[6];
	CWinThread * thKalmanFilter[6];

	bool btracking;
	int globalImage;

// Datos del cuadro de di�logo
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEMOREGISTRE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//uEye Message Map
	LRESULT OnUEyeMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:

	// Client Socket Specific Functions
	void OnRecieve(int id, int nErrorCode);
	void OnClosed(int id, int nErrorCode);
	void OnConnected(int id, int nErrorCode);

	bool InitializeVideoRecording();
	void FinalizeVideoRecording();
	//void Imadjust(const Mat1b& src, Mat1b& dst, int tol = 1, Vec2i in = Vec2i(0, 255), Vec2i out = Vec2i(0, 255));
	void Imadjust(cv::Mat * entrada, cv::Mat * sortida, int lint, int hint, int lout, int hout);
	void Imadjust(const Mat1b& src, Mat1b& dst, int tol, Vec2i in, Vec2i out);
	void cannyPF(cv::Mat & image, int gaussianSize, float VMGradient, cv::Mat & edgeMap);
	void cannyPF(cv::Mat &image, int gaussianSize, float VMGradient, cv::Mat &edgeMap, cv::Mat &gradMod, cv::Mat &gradAngle);
	void SegmentacioImatge(cv::Mat * entrada, cv::Mat * sortida);
	void SegmentacioImatge(cv::Mat &entrada, cv::Mat &sortida, cv::Mat &gradMod, cv::Mat &gradAngle);
	void PosarImatgeSegmentacio(cv::Mat * entrada);
	void getSkeleton(cv::Mat &entrada, cv::Mat & sortida);
	void thinningIteration(cv::Mat& img, int iter);
	void ZhangSuen(const cv::Mat& src, cv::Mat& dst);
	void nonMaximumSuppresssion(cv::Mat &image, cv::Mat &gradMod, cv::Mat &gradPhase, cv::Mat &output);
	int creixementRegio(cv::Mat * imatge, cv::Mat * mascara, int x, int y, int lbl, std::vector <cv::Point2i> * points);
	int creixementRegio2(cv::Mat &imatge, cv::Mat &mascara, int x, int y, int lbl, std::vector <cv::Point2i> &points);
	void correspondences(std::vector <cv::Point2f> * points2D, std::vector <cv::Point2f> * points3D, std::vector <int> * corresp, std::vector <int> * indPoints2D, std::vector <int> * indPoints3D);
	void trobarPuntsContorn(cv::Mat * imatge, std::vector<cv::Point2i> * punts, int nPunts);
	cv::Point2f PuntsContornProjectats(Eigen::Matrix2Xf * punts, int nPunts, std::vector <cv::Point2f> * puntsContorn, std::vector<int> * indPuntsContorn);
	void PuntsMaxCurvatura(std::vector<cv::Point2f> Punts, int nPunts, std::vector<cv::Point2f> * PuntsOut, std::vector<int> * indPuntsOut, int * nPuntsImp, std::vector<int> * indPuntsOrdenat, std::vector<int> * indPuntsOrdImp, std::vector<int> *permutImp);
	void PuntsMaxCurvatura(std::vector<cv::Point2f> Punts, int nPunts, std::vector<cv::Point2f> * PuntsOut, std::vector<int> * indPuntsOut, int * nPuntsImp, std::vector<int> * indPuntsOrdenat, std::vector<int> * indPuntsOrdImp, std::vector<int> *permutImp, std::vector<float>* sigma);
	void static QuaternionToMatrix(float q0, float qx, float qy, float qz, Eigen::Matrix3f * resultat);
	void static QuaternionFromMatrix(float * q0, float * qx, float * qy, float * qz, Eigen::Matrix3f R);
	void desdistorcionarPunts(std::vector<cv::Point2i> *pixels, std::vector<cv::Point2f> *pixelsDesdistorcionats, int nPix);
	float ICPCurvatura2D3DIter(std::vector<cv::Point2f> * Punts2D, std::vector<cv::Point3f> * Punts3D, Eigen::Matrix4f * tIn, Eigen::Matrix4f * tOut, int nIn, int nDelta, int iterICP, float tol, int *nOut);
	void Matriu2QuatandPos(Eigen::Matrix4f * Input, Eigen::Vector4f * quad, Eigen::Vector3f * pos);
	void QuatandPos2Matriu(Eigen::Vector4f * quad, Eigen::Vector3f * pos, Eigen::Matrix4f * Output);
	std::vector<cv::Point2f> calculNormals(std::vector<cv::Point2f> Punts);
	void distorcionarPunts(std::vector < cv::Point2f> Punts, std::vector<cv::Point2i> *PuntsPixels);
	void Matriu2EulerXYZandPos(Eigen::Matrix4f Input, Eigen::Vector3f * euler1, Eigen::Vector3f * euler2, Eigen::Vector3f * pos);
	void EulerXYZFromMatrix(float angx[2], float angy[2], float angz[2], Eigen::Matrix3f R);
	void EulerXYZtoMatrix(float angx, float angy, float angz, Eigen::Matrix3f * Rot);
	void EulerXYZandPos2Matriu(Eigen::Vector3f * euler, Eigen::Vector3f * pos, Eigen::Matrix4f * Output);
	int SeguimentContorn(cv::Mat &image, cv::Mat &mask, int x1, int y1, int x2, int y2, int lbl, std::vector<cv::Point2i> &punts);
	void acabarPintar();
	void StartTracking();
	void StopTracking();
	inline cv::Mat imCompositeFilter(cv::Mat I);
	inline cv::Mat imMorphGrad(cv::Mat I, cv::Mat kernel);
	inline cv::Mat imMorphErode(cv::Mat I, cv::Mat kernel);
	inline cv::Mat imMorphDilate(cv::Mat I, cv::Mat kernel);
	float evaluarICP(int size, std::vector<cv::Point2f> * P2D, std::vector<cv::Point3f> * P3D, std::vector<cv::Point2f> * Normals, int numImp, Eigen::VectorXf x);
	cv::Point2f getMeanPunts2D(Eigen::Matrix2Xf * punts);
	virtual BOOL DestroyWindow();
	CButton m_butCamera;
	afx_msg void OnBnClickedButcamera();
	CButton m_butSegmentation;
	afx_msg void OnBnClickedButsegmen();
	CStatic m_picSegmentation;
	CEdit m_strFps;
	afx_msg void OnBnClickedButprova();
	void printRegister();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedProva2();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedProva3();
	afx_msg void OnBnClickedButtracking();
	CButton m_butTracking;
	afx_msg void OnBnClickedButprova4();
	afx_msg void OnBnClickedButprova5();
};
