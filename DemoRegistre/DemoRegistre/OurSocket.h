#ifndef __OURSOCK_H__
#define __OURSOCK_H__

#include "OurSocket.h"

/// @class COurSocket
/// @brief CAsyncSocket envelope class.
/// @author Alberto Rodriguez Garcia
/// @version 1.0
/// @date June 2005
///
/// Non blocking, stream_socket, message based envelope of CAsyncSocket.

class COurSocket : public CAsyncSocket
{
// Attributes
public:
	CDialog * mainDlg;	//back pointer to the main dialog
	int id;  //Defines the identificator of the soccket you are creating

private:
	bool Readcallback;
	bool Writecallback;
	bool Oobcallback;
	bool Acceptcallback;
	bool Connectcallback;
	bool Closecallback;

// Construction
public:
	COurSocket(int identificator);
	COurSocket(CDialog * pDlg, int identificator);
	virtual ~COurSocket();

// Overrided callbacks
public:
	virtual void OnAccept(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	virtual void OnOutOfBandData(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnSend(int nErrorCode);
	virtual void Close();
	
//Extra functionalities
	int Receive(CString * str);
	int Send(CString *str);

	int Create();
	int Create(int LocalPort);
	int Create(int LocalPort, CString LocalIP);
	int Connect(int HostPort, CString HostIP);
	int Listen();
	int CreateAndConnect(int LocalPort, CString LocalIP, int HostPort, CString HostIP);
	int CreateAndConnect(int HostPort, CString HostIP);
	int CreateAndListen(int LocalPort, CString LocalIP);
	int CreateAndListen();
	int Accept(COurSocket *Listensock);

	void SetIdentificator(int identificator);
	void SetParent(CDialog* pDlg);
	void SetOnReceive(bool set);
	void SetOnSend(bool set);
	void SetOnOutOfBandData(bool set);
	void SetOnAccept(bool set);
	void SetOnConnect(bool set);
	void SetOnClose(bool set);
	void SetCallBacks(bool accept, bool close, bool connect, bool oob, bool read, bool write);

private:
	void DefineCallbacks();
};
#endif // __OURSOCK_H__
