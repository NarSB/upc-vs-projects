#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "opencv2\highgui\highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/core/cuda.hpp"
#include "opencv2/core/cuda.inl.hpp"
#include "opencv2/core/cuda_stream_accessor.hpp"
#include "opencv2/core/cuda_types.hpp"

#include <stdio.h>

cudaError_t ZhangSuenGPU(cv::Mat& src, cv::Mat& dst);

__global__ void ZhangSuenKernel(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst, int tipus, int colsReals)
{
	int ind = blockIdx.x*blockDim.x + threadIdx.x;
	int rind = ind / (colsReals - 2);
	int cind = ind - rind*(colsReals - 2) + 1;
	rind++;
	dst.ptr(rind)[cind] = src.ptr(rind)[cind];
}

/*int main()
{
const int arraySize = 5;
const int a[arraySize] = { 1, 2, 3, 4, 5 };
const int b[arraySize] = { 10, 20, 30, 40, 50 };
int c[arraySize] = { 0 };

// Add vectors in parallel.
cudaError_t cudaStatus = addWithCuda(c, a, b, arraySize);
if (cudaStatus != cudaSuccess) {
fprintf(stderr, "addWithCuda failed!");
return 1;
}

printf("{1,2,3,4,5} + {10,20,30,40,50} = {%d,%d,%d,%d,%d}\n",
c[0], c[1], c[2], c[3], c[4]);

// cudaDeviceReset must be called before exiting in order for profiling and
// tracing tools such as Nsight and Visual Profiler to show complete traces.
cudaStatus = cudaDeviceReset();
if (cudaStatus != cudaSuccess) {
fprintf(stderr, "cudaDeviceReset failed!");
return 1;
}

return 0;
}*/

// ZhangSuenGPU
extern cudaError_t ZhangSuenGPU(cv::Mat& src, cv::Mat& dst)
{
	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}

	dst = cv::Mat::zeros(src.rows, src.cols, src.type());

	cv::cuda::GpuMat dImage(src);
	cv::cuda::GpuMat dResult(dst);

	int size = (src.rows - 2)*(src.cols - 2);
	int threadBlock = min(max(32, size), 128);
	int blockSize = (size + threadBlock - 1) / threadBlock;

	// Launch a kernel on the GPU with one thread for each element.
	ZhangSuenKernel <<<blockSize, threadBlock >>>(dImage, dResult,0, src.cols);

	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
		goto Error;
	}

	dResult.download(dst);

Error:
	return cudaStatus;
}