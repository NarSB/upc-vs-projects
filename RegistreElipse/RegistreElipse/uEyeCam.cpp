#include "stdafx.h"
#include "uEyeCam.h"

//Includes for uEye
#include <uEye.h>
#include <ueye_deprecated.h>


uEyeCam::uEyeCam()
{
	//iImage = NULL;
	m_pcImageMemory = NULL;
	m_lMemoryId = 0;
	m_hCam = 0;
	m_nRenderMode = IS_RENDER_FIT_TO_WINDOW;
	m_nPosX = 0;
	m_nPosY = 0;
	m_nFlipHor = 0;
	m_nFlipVert = 0;
}

uEyeCam::~uEyeCam()
{
}

//bool uEyeCam::InitAcquisition(WId wid)
bool uEyeCam::InitAcquisition(HWND hWnd)
{
	if (bInited)
	{
		// Enable camera event Messages
		//INI DEBUG: Removed the call with wid//
		//is_EnableMessage(m_hCam, IS_DEVICE_REMOVED, reinterpret_cast<HWND>(wid));
		//is_EnableMessage(m_hCam, IS_DEVICE_RECONNECTED, reinterpret_cast<HWND>(wid));
		//is_EnableMessage(m_hCam, IS_FRAME, reinterpret_cast<HWND>(wid));
		//END DEBUG: Removed the call with wid//

		// Enable Messages
		is_EnableMessage(m_hCam, IS_DEVICE_REMOVED, hWnd);
		is_EnableMessage(m_hCam, IS_DEVICE_RECONNECTED, hWnd);
		is_EnableMessage(m_hCam, IS_FRAME, hWnd);

		// Continuous acquisition
		is_CaptureVideo(m_hCam, IS_DONT_WAIT);

		bRunning = true;

		//Debug info qDebug()<<"UEyeCam: Init Acquisition";
	}

	return true;
}

bool uEyeCam::FinalizeAcquisition()
{
	if (bInited)
	{
		if (m_hCam != 0)
		{
			bRunning = false;

			// Disable messages
			is_EnableMessage(m_hCam, IS_FRAME, NULL);
			is_EnableMessage(m_hCam, IS_DEVICE_RECONNECTED, NULL);
			is_EnableMessage(m_hCam, IS_DEVICE_REMOVED, NULL);

			// Stop live video
			is_StopLiveVideo(m_hCam, IS_WAIT);

			//Debug info qDebug()<<"UEyeCam: Finalize Acquisition";
		}
	}

	return true;
}

//bool uEyeCam::OpenCamera(WId wid, HWND hWnd, int id, CStringA paramsfile)
bool uEyeCam::OpenCamera(HWND hWnd, int id, CStringA paramsfile)
{
	
	//Previous to Open new camera, forcing to close potential pervious opened camera.
	INT nRet = IS_NO_SUCCESS;
	ExitCamera();


	//Render mode
	m_hWndDisplay = hWnd;
	m_nRenderMode = IS_RENDER_FIT_TO_WINDOW;

	//Init id camera with params
	m_hCam = (HIDS)id;
	nRet = InitCamera(&m_hCam, m_hWndDisplay);


	if (nRet == IS_SUCCESS)
	{
		//Load params
		const char *nstringa = paramsfile;
		if (is_LoadParameters(m_hCam, nstringa) != IS_SUCCESS) {}

		// Get sensor info
		is_GetSensorInfo(m_hCam, &m_sInfo);

		GetMaxImageSize(&m_nSizeX, &m_nSizeY);

		//Init display mode
		nRet = InitMemory();

		bInited = true;

		if (nRet == IS_SUCCESS) return true;
		else return false;
	}
	else
	{
		bInited = false;

		return false;
	}
}


void uEyeCam::GetMaxImageSize(INT *pnSizeX, INT *pnSizeY)
{
	// Check if the camera supports an arbitrary AOI
	// Only the ueye xs does not support an arbitrary AOI
	INT nAOISupported = 0;
	BOOL bAOISupported = TRUE;
	if (is_ImageFormat(m_hCam,
		IMGFRMT_CMD_GET_ARBITRARY_AOI_SUPPORTED,
		(void*)&nAOISupported,
		sizeof(nAOISupported)) == IS_SUCCESS)
	{
		bAOISupported = (nAOISupported != 0);
	}

	if (bAOISupported)
	{
		// All other sensors
		// Get maximum image size
		SENSORINFO sInfo;
		is_GetSensorInfo(m_hCam, &sInfo);
		*pnSizeX = sInfo.nMaxWidth;
		*pnSizeY = sInfo.nMaxHeight;
	}
	else
	{
		// Only ueye xs
		// Get image size of the current format
		IS_SIZE_2D imageSize;
		is_AOI(m_hCam, IS_AOI_IMAGE_GET_SIZE, (void*)&imageSize, sizeof(imageSize));

		*pnSizeX = imageSize.s32Width;
		*pnSizeY = imageSize.s32Height;
	}
}

int uEyeCam::InitCamera(HIDS *hCam, HWND hWnd)
{
	INT nRet = is_InitCamera(hCam, hWnd);
	/************************************************************************************************/
	/*                                                                                              */
	/*  If the camera returns with "IS_STARTER_FW_UPLOAD_NEEDED", an upload of a new firmware       */
	/*  is necessary. This upload can take several seconds. We recommend to check the required      */
	/*  time with the function is_GetDuration().                                                    */
	/*                                                                                              */
	/*  In this case, the camera can only be opened if the flag "IS_ALLOW_STARTER_FW_UPLOAD"        */
	/*  is "OR"-ed to m_hCam. This flag allows an automatic upload of the firmware.                 */
	/*                                                                                              */
	/************************************************************************************************/
	if (nRet == IS_STARTER_FW_UPLOAD_NEEDED)
	{
		// Time for the firmware upload = 25 seconds by default
		INT nUploadTime = 25000;
		is_GetDuration(*hCam, IS_STARTER_FW_UPLOAD, &nUploadTime);

		//CString Str1, Str2, Str3;
		//Str1 = "This camera requires a new firmware. The upload will take about";
		//Str2 = "seconds. Please wait ...";
		//Str3.Format ("%s %d %s", Str1, nUploadTime / 1000, Str2);
		//AfxMessageBox (Str3, MB_ICONWARNING);

		// Set mouse to hourglass
		//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		
		// Try again to open the camera. This time we allow the automatic upload of the firmware by
		// specifying "IS_ALLOW_STARTER_FIRMWARE_UPLOAD"
		*hCam = (HIDS)(((INT)*hCam) | IS_ALLOW_STARTER_FW_UPLOAD);
		nRet = is_InitCamera(hCam, hWnd);
	}
	return nRet;
}

void uEyeCam::ExitCamera()
{
	if (bInited)
	{
		if (m_hCam != 0)
		{

			//INI DEBUG: TEST IF THIS TWO ACTIONS ARE NECESSARY
			// Disable messages
			is_EnableMessage(m_hCam, IS_FRAME, NULL);

			// Stop live video
			is_StopLiveVideo(m_hCam, IS_WAIT);
			//END DEBUG: TEST IF THIS TWO ACTIONS ARE NECESSARY
			
			// Free the allocated buffer
			if (m_pcImageMemory != NULL)
				is_FreeImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);
				
			m_pcImageMemory = NULL;
			
			// Close camera
			is_ExitCamera(m_hCam);
			m_hCam = NULL;

			//Free acquisition image
			//cvReleaseImage(&iImage);
			
		}
	}
}

double uEyeCam::GetFPS()
{
	double res;
	is_GetFramesPerSecond(m_hCam, &res);
	return res;
}

/*bool uEyeCam::CaptureFrame()
{
	if (bInited && bRunning)
	{
		//Copy image to iImage memory
		is_CopyImageMem(m_hCam, m_pcImageMemory, m_lMemoryId, iImage->imageData);
	}

	return true;
}*/

bool uEyeCam::GetImage(uchar * image)
{
	if (bInited && bRunning)
	{
		is_CopyImageMem(m_hCam, m_pcImageMemory, m_lMemoryId, reinterpret_cast<char*>(image));
		//cvResize(iImage, image, CV_INTER_LINEAR);
	}

	return true;
}

bool uEyeCam::GetStatus()
{
	return bInited;
}

CvSize uEyeCam::GetImageSize()
{
	return cvSize(m_nSizeX, m_nSizeY);
}

int uEyeCam::GetImageBPP()
{
	return m_nBitsPerPixel / 8;
}

int uEyeCam::InitMemory()
{
	INT nRet = IS_NO_SUCCESS;

	if (m_hCam == NULL) return IS_NO_SUCCESS;

	if (m_pcImageMemory != NULL) is_FreeImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);

	m_pcImageMemory = NULL;

	// Set display mode to DIB
	nRet = is_SetDisplayMode(m_hCam, IS_SET_DM_DIB);

	//if (m_sInfo.nColorMode == IS_COLORMODE_BAYER)
	if (is_SetColorMode(m_hCam, IS_GET_COLOR_MODE) == IS_COLORMODE_BAYER)
	{
		// setup the color depth to the current windows setting
		is_GetColorDepth(m_hCam, &m_nBitsPerPixel, &m_nColorMode);
	}
	//else if (m_sInfo.nColorMode == IS_COLORMODE_CBYCRY)
	else if (is_SetColorMode(m_hCam, IS_GET_COLOR_MODE) == IS_COLORMODE_CBYCRY)
	{
		// for color camera models use RGB32 mode
		m_nColorMode = IS_CM_BGRA8_PACKED;
		m_nBitsPerPixel = 32;
	}
	else
	{
		// for monochrome camera models use Y8 mode
		m_nColorMode = IS_CM_MONO8;
		m_nBitsPerPixel = 8;
	}

	//Allocate an image memory
	if (is_AllocImageMem(m_hCam, m_nSizeX, m_nSizeY, m_nBitsPerPixel, &m_pcImageMemory, &m_lMemoryId) != IS_SUCCESS)
	{
		AfxMessageBox(_T("Memory allocation failed!"), MB_ICONWARNING);
	}
	else
	{
		is_SetImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);
		//qDebug()<< sImageSize.s32Width << sImageSize.s32Height << m_nBitsPerPixel;
		//qDebug()<<"Memory allocation successfully!";
	}

	if (nRet == IS_SUCCESS)
	{
		//Set the desired color mode
		INT res = is_SetColorMode(m_hCam, m_nColorMode);

		// set the image size to capture
		IS_SIZE_2D imageSize;
		imageSize.s32Width = m_nSizeX;
		imageSize.s32Height = m_nSizeY;

		is_AOI(m_hCam, IS_AOI_IMAGE_SET_SIZE, (void*)&imageSize, sizeof(imageSize));
	}

	return nRet;
}

bool uEyeCam::GetButtonStatus()
{
	if (bInited && bRunning)
	{
		// static read the digital (trigger) input
		return is_SetExternalTrigger(m_hCam, IS_GET_TRIGGER_STATUS);
	}
	else return false;
}

int uEyeCam::GetExposureTime()
{
	if (bInited && bRunning)
	{
		double exposure = 0.0;

		//Get Exposure Time
		is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void*)&exposure, sizeof(double));

		return (double)exposure;
	}
	else return false;
}

bool uEyeCam::SwitchFlash(bool b)
{
	INT nRet = IS_SUCCESS;

	// Disable flash
	UINT nMode;

	if (!b)
	{
		// Set the flash to a constant low output
		nMode = IO_FLASH_MODE_CONSTANT_LOW;
		nRet = is_IO(m_hCam, IS_IO_CMD_FLASH_SET_MODE, (void*)&nMode, sizeof(nMode));
	}
	else
	{
		// Set the flash to a high active pulse for each image in the trigger mode
		nMode = IO_FLASH_MODE_CONSTANT_HIGH;
		nRet = is_IO(m_hCam, IS_IO_CMD_FLASH_SET_MODE, (void*)&nMode, sizeof(nMode));
	}

	return nRet;
}


/*void uEyeCam::LoadParameters()
{
	if (m_hCam == 0)
		OpenCamera();

	if (m_hCam != 0)
	{
		if (is_ParameterSet(m_hCam, IS_PARAMETERSET_CMD_LOAD_FILE, NULL, NULL) == IS_SUCCESS && m_pcImageMemory != NULL)
		{
			// determine live capture
			BOOL bWasLive = (BOOL)(is_CaptureVideo(m_hCam, IS_GET_LIVE));
			if (bWasLive)
				is_StopLiveVideo(m_hCam, IS_FORCE_VIDEO_STOP);

			// realloc image mem with actual sizes and depth.
			is_FreeImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);

			IS_SIZE_2D imageSize;
			is_AOI(m_hCam, IS_AOI_IMAGE_GET_SIZE, (void*)&imageSize, sizeof(imageSize));

			INT nAllocSizeX = 0;
			INT nAllocSizeY = 0;

			m_nSizeX = nAllocSizeX = imageSize.s32Width;
			m_nSizeY = nAllocSizeY = imageSize.s32Height;

			UINT nAbsPosX = 0;
			UINT nAbsPosY = 0;

			// absolute pos?
			is_AOI(m_hCam, IS_AOI_IMAGE_GET_POS_X_ABS, (void*)&nAbsPosX, sizeof(nAbsPosX));
			is_AOI(m_hCam, IS_AOI_IMAGE_GET_POS_Y_ABS, (void*)&nAbsPosY, sizeof(nAbsPosY));

			if (nAbsPosX)
			{
				nAllocSizeX = m_sInfo.nMaxWidth;
			}
			if (nAbsPosY)
			{
				nAllocSizeY = m_sInfo.nMaxHeight;
			}

			switch (is_SetColorMode(m_hCam, IS_GET_COLOR_MODE))
			{
			case IS_CM_RGBA12_UNPACKED:
			case IS_CM_BGRA12_UNPACKED:
				m_nBitsPerPixel = 64;
				break;

			case IS_CM_RGB12_UNPACKED:
			case IS_CM_BGR12_UNPACKED:
			case IS_CM_RGB10_UNPACKED:
			case IS_CM_BGR10_UNPACKED:
				m_nBitsPerPixel = 48;
				break;

			case IS_CM_RGBA8_PACKED:
			case IS_CM_BGRA8_PACKED:
			case IS_CM_RGB10_PACKED:
			case IS_CM_BGR10_PACKED:
			case IS_CM_RGBY8_PACKED:
			case IS_CM_BGRY8_PACKED:
				m_nBitsPerPixel = 32;
				break;

			case IS_CM_RGB8_PACKED:
			case IS_CM_BGR8_PACKED:
				m_nBitsPerPixel = 24;
				break;

			case IS_CM_BGR565_PACKED:
			case IS_CM_UYVY_PACKED:
			case IS_CM_CBYCRY_PACKED:
				m_nBitsPerPixel = 16;
				break;

			case IS_CM_BGR5_PACKED:
				m_nBitsPerPixel = 15;
				break;

			case IS_CM_MONO16:
			case IS_CM_SENSOR_RAW16:
			case IS_CM_MONO12:
			case IS_CM_SENSOR_RAW12:
			case IS_CM_MONO10:
			case IS_CM_SENSOR_RAW10:
				m_nBitsPerPixel = 16;
				break;

			case IS_CM_RGB8_PLANAR:
				m_nBitsPerPixel = 24;
				break;

			case IS_CM_MONO8:
			case IS_CM_SENSOR_RAW8:
			default:
				m_nBitsPerPixel = 8;
				break;
			}

			// memory initialization
			is_AllocImageMem(m_hCam, nAllocSizeX, nAllocSizeY, m_nBitsPerPixel, &m_pcImageMemory, &m_lMemoryId);

			// set memory active
			is_SetImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);

			// display initialization
			imageSize.s32Width = m_nSizeX;
			imageSize.s32Height = m_nSizeY;

			// Set the AOI with the correct size
			is_AOI(m_hCam, IS_AOI_IMAGE_SET_SIZE, (void*)&imageSize, sizeof(imageSize));

			// run live image again
			if (bWasLive)
				is_CaptureVideo(m_hCam, IS_DONT_WAIT);
		}
	}
}
*/