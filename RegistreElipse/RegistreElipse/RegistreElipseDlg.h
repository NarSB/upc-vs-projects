
// RegistreElipseDlg.h: archivo de encabezado
//

#pragma once
#include "afxwin.h"
#include "uEyeCam.h"
#include "stdafx.h"
#include <mutex>
#include <conio.h>
#include <Ws2tcpip.h>
#include "WinSock2.h"

//EIGEN LIBRARY
#include <Eigen/Dense>
#include <Eigen/Core>

//DEFINE CONTSTANTS
#define PUNTSCONTORNIMATGE2D 100
#define TOLERANCIA 0.0001
#define MINTAMANYZONACONTORN 50
#define MAXTAMANYZONACONTORN 800
#define DISTANCIACENTREELIPSE 50
#define DISTENTREPUNTS 10

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// Cuadro de di�logo de CRegistreElipseDlg
class CRegistreElipseDlg : public CDialogEx
{
// Construcci�n
public:
	CRegistreElipseDlg(CWnd* pParent = NULL);	// Constructor est�ndar

												/////////////////////////////////////////////////////////
												//Threads
												//Thread Specific Variable and Functions 
	CWinThread * thRecording;
	bool bRecording;	//True: Thread active, False: Kill Thread

						///////////////////////////////////////////////////////////////
						//uEye camera variables and functions
						///////////////////////////////////////////////////////////////
	uEyeCam*        ueCamera;       //camera
									//CvVideoWriter*  VideoWriter;    //The video writer
	CvSize          VideoSize;      //Size of the image (video frame) to be saves
	CvSize          VideoSizeUI;    //Size of the image (video frame) to be displayed on UI
	IplImage*       VideoImage;     //Video image (frame) to be saved
	IplImage*       VideoImageUI;   //Video image (frame) to be displayed on UI
	double          VideoFrameRate; //Video frame rate (FPS)
	int ctrlVideoImageReady;
	cv::Mat lastImage;
	cv::Mat cameraMatrix, distCoef;
	float fc[2], cc[2], alpha_c;
	double kc[5];
	int iTotalFramesRecorded;

	//Segmentation
	bool bSegmenting;
	cv::Mat segImage;
	cv::Size winSegSize;
	cv::Mat cvImgTmp;
	CImage* mfcImg;
	BITMAPINFO bitInfo;
	cv::Mat gradMorf, segdilate, segerode, squareKernel, crossKernel;
	uint8_t datacross[3][3];
	uint8_t b1[3][3], b2[3][3], b3[3][3], b4[3][3];
	cv::Mat b1Kernel, b2Kernel, b3Kernel, b4Kernel;
	cv::Mat puntsSegmentats, puntsSegmentatsDes;
	std::vector<cv::Point2i> puntsSegmentatsvect;
	std::vector<std::vector<cv::Point2i>> ultimsSegmentats;
	int numSegm, primerSegm;
	std::vector<cv::Point2f> meanSegmentats;
	int segrowi, segrowsize, segcoli, segcolsize;

	//Centre elipse i sockets
	float ex, ey, ex0, ey0;
	float valMigx[10], valMigy[10];
	std::mutex centreMutex;
	SOCKET clntdes;			/* Socket descriptor for client */
	SOCKET servdes;                 /* Socket descriptor for server */
	struct sockaddr_in ServAddr; /* Local address */
	struct sockaddr_in ClntAddr; /* Client address */
	bool clientConnectat;

// Datos del cuadro de di�logo
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_REGISTREELIPSE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//uEye Message Map
	LRESULT OnUEyeMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

public:
	int ParametresEllipseProbabilitat(std::vector<cv::Point2f>& punts, float & x0, float & y0, float & a2, float & b2, float & ang);
	int creixementRegio(cv::Mat & imatge, cv::Mat & mascara, int x, int y, int lbl, std::vector<cv::Point2i>& points);
	void trobarPuntsElipse(cv::Mat & imatge, std::vector<cv::Point2i>& punts, int nPunts, int rowInici, int colInici);
	bool InitializeVideoRecording();
	void FinalizeVideoRecording();
	void OnBnClickedButcamera();
	int BuscarEllipse(cv::Mat & image, int & x, int & y);
	cv::Mat imCompositeFilter(cv::Mat & I);
	void cannyPF(cv::Mat & image, int gaussianSize, float VMGradient, cv::Mat & edgeMap);
	void thinningIteration(cv::Mat & img, int iter);
	void ZhangSuen(const cv::Mat & src, cv::Mat & dst);
	void SegmentacioImatge(cv::Mat & entrada, cv::Mat & sortida);
	int BuscarEllipse(cv::Mat & image, int & x, int & y, cv::Mat & segm);
	void PosarImatgePictureBox(cv::Mat & entrada);
	CStatic m_picImageElipse;
	CButton m_butCamera;
	CButton m_butSegmentation;
	afx_msg void OnBnClickedButsegmen();
	afx_msg void OnDestroy();
	CEdit m_strTemps;
	afx_msg void OnBnClickedButprova1();
};
