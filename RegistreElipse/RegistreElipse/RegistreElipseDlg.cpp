
// RegistreElipseDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "RegistreElipse.h"
#include "RegistreElipseDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//Global Inlines
inline int modval(int a, int b) { return (a % b + b) % b; }

inline int evaluarMat(uint8_t * matrix, int r, int c, int ncols)
{
	if ((r < 0) || (c<0) || (c >= ncols))
		return -1;
	return matrix[r * ncols + c];
}

inline float normDistancia(float x1, float y1, float x2, float y2)
{
	return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
}

inline float normCvPoint2f(cv::Point2f P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint2i(cv::Point2i P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint3f(cv::Point3f P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline float normCvPoint3i(cv::Point3i P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline int float2int(float x)
{
	return (int)(x + TOLERANCIA);
}

inline float FuncioSigne(float x)
{
	if (std::abs(x)<TOLERANCIA)
		return 0.0;
	else if (x>0)
		return 1.0;
	else
		return -1.0;
}


//Thread Camera

UINT ThreadRecording(LPVOID pParam)
{
	CRegistreElipseDlg* pObject = (CRegistreElipseDlg*)pParam;

	_LARGE_INTEGER StartingTime, EndingTime, ElapsedMiliseconds;
	LARGE_INTEGER Frequency;
	CString fpsText;
	std::vector<cv::Point2i> auxPointsContorn = std::vector<cv::Point2i>();
	std::vector<cv::Point2f> auxPointsFloat = std::vector<cv::Point2f>();

	pObject->iTotalFramesRecorded = 0;
	int index;
	float x0, y0, a_2, b_2, angE;
	float x1, y1;
	x0 = 0.0f;
	y0 = 0.0f;


	int iFrameRate = 1000 / pObject->VideoFrameRate; //milliseconds per frame
	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&StartingTime);

	int a1, a2, nCentres;
	for (int i = 0; i < 10; i++)
	{
		pObject->valMigx[i] = 0;
		pObject->valMigy[i] = 0;
	}

	while (pObject->bRecording)
	{
		QueryPerformanceCounter(&StartingTime);
		pObject->ueCamera->GetImage((pObject->lastImage.data));

		if (pObject->bSegmenting)
		{
			
			pObject->iTotalFramesRecorded++;
			pObject->meanSegmentats[1].x = pObject->meanSegmentats[0].x;
			pObject->meanSegmentats[1].y = pObject->meanSegmentats[0].y;
			nCentres = pObject->BuscarEllipse(pObject->lastImage, a1, a2, pObject->segImage);
			pObject->SegmentacioImatge(pObject->lastImage, pObject->segImage);
			if (nCentres > -1)
			{
				pObject->meanSegmentats[0].x = (float)a1;
				pObject->meanSegmentats[0].y = (float)a2;
			}
			pObject->trobarPuntsElipse((pObject->segImage), auxPointsContorn, PUNTSCONTORNIMATGE2D, pObject->meanSegmentats[0].y, pObject->meanSegmentats[0].x);
			auxPointsFloat.clear();
			for (int i = 0; i < auxPointsContorn.size(); i++)
			{
				if ((auxPointsContorn[i].x != 0) && (auxPointsContorn[i].y != 0))
					auxPointsFloat.push_back((cv::Point2f)auxPointsContorn[i]);
			}
			/*x1 = x0;
			y1 = y0;*/
			pObject->ParametresEllipseProbabilitat(auxPointsFloat, x0, y0, a_2, b_2, angE);
			TRACE("\nNou Frame: a1: %d a2: %d, mx: %f my: %f, cx: %f cy: %f, a_2: %f b_2: %f ang: %f\n", a1, a2, pObject->meanSegmentats[0].x, pObject->meanSegmentats[0].y, x0, y0,a_2,b_2,angE);
			pObject->puntsSegmentatsvect.clear();
			std::copy(auxPointsContorn.begin(), auxPointsContorn.end(), std::back_inserter(pObject->puntsSegmentatsvect));
			pObject->PosarImatgePictureBox(pObject->lastImage);
			/*pObject->centreMutex.lock();
			pObject->ex0 = pObject->ex;
			pObject->ey0 = pObject->ey;
			pObject->ex = x0;
			pObject->ey = y0;
			if ((pObject->meanSegmentats[0].y > 0.0f) && (pObject->meanSegmentats[0].x > 0.0f))
			{
				for (int i = 0; i < 9; i++)
				{
					pObject->valMigx[i + 1] = pObject->valMigx[i];
					pObject->valMigy[i + 1] = pObject->valMigy[i];
				}
				pObject->valMigx[0] = pObject->meanSegmentats[0].x;
				pObject->valMigy[0] = pObject->meanSegmentats[0].y;
			}
			pObject->centreMutex.unlock();*/

			

		}
		else
		{
			pObject->puntsSegmentatsvect.clear();
			pObject->PosarImatgePictureBox(pObject->lastImage);
		}
		QueryPerformanceCounter(&EndingTime);
		ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;

		// We now have the elapsed number of ticks, along with the number of ticks-per-second. We use these values
		// to convert to the number of elapsed milliseconds.  To guard against loss-of-precision, we convert
		// to milliseconds *before* dividing by ticks-per-second.
		ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
		ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
		fpsText.Format("%d", ElapsedMiliseconds);
		pObject->m_strTemps.SetWindowTextA(fpsText);
	}
	if (pObject->mfcImg)
	{
		pObject->mfcImg->ReleaseDC();
		delete pObject->mfcImg; pObject->mfcImg = nullptr;
	}

	return 0;
}





// Cuadro de di�logo de CRegistreElipseDlg



CRegistreElipseDlg::CRegistreElipseDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_REGISTREELIPSE_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRegistreElipseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DISPLAY, m_picImageElipse);
	DDX_Control(pDX, IDC_BUTCAMERA, m_butCamera);
	DDX_Control(pDX, IDC_BUTSEGMEN, m_butSegmentation);
	DDX_Control(pDX, IDC_EDIT1, m_strTemps);
}

BEGIN_MESSAGE_MAP(CRegistreElipseDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTCAMERA, &CRegistreElipseDlg::OnBnClickedButcamera)
	ON_BN_CLICKED(IDC_BUTSEGMEN, &CRegistreElipseDlg::OnBnClickedButsegmen)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTProva1, &CRegistreElipseDlg::OnBnClickedButprova1)
END_MESSAGE_MAP()


// Controladores de mensaje de CRegistreElipseDlg

BOOL CRegistreElipseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Establecer el icono para este cuadro de di�logo.  El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// TODO: agregar aqu� inicializaci�n adicional

	//App Control variables
	bRecording = false;
	ctrlVideoImageReady = 0;
	//Initialitation camera
	ueCamera = new uEyeCam();
	// get handle to display window
	ueCamera->m_hWndDisplay = GetDlgItem(IDC_DISPLAY)->m_hWnd;
	//Set configuration camera file [.ini]
	CString CamConfigFile("C:\\Users\\UPC-ESAII\\Google Drive\\Files\\ESAII\\ModelsOrgans\\Fotos Cor\\configuracioCameraCaptura_10fps.ini");
	//CString CamConfigFile("C:\\Users\\UPC-ESAII\\Google Drive\\Files\\ESAII\\ModelsOrgans\\Fotos Cor\\configuracioCameraCaptura_30fps.ini");
	//Open and setup uEye camera
	if (!ueCamera->OpenCamera(ueCamera->m_hWndDisplay, 0, CamConfigFile))
	{
		AfxMessageBox(_T("Error: Camera not initialized"), IDOK);
	}
	lastImage = cv::Mat(ueCamera->GetImageSize(), CV_8UC1);
	m_butCamera.SetWindowTextA(_T("Start Camera"));

	//Variables segmentacio
	bSegmenting = false;
	m_butSegmentation.EnableWindow(FALSE);
	m_butSegmentation.SetWindowTextA(_T("Start Segmentation"));
	squareKernel = getStructuringElement(MORPH_RECT, Size(3, 3), Point(-1, -1));
	datacross[0][0] = 1; datacross[0][1] = 0; datacross[0][2] = 1; datacross[1][0] = 0; datacross[1][1] = 1; datacross[1][2] = 0; datacross[2][0] = 1; datacross[2][1] = 0; datacross[2][2] = 1;
	crossKernel = cv::Mat(3, 3, CV_8U, datacross);
	segerode = getStructuringElement(MORPH_CROSS, Size(3, 3), Point(-1, -1));
	segdilate = getStructuringElement(MORPH_RECT, Size(5, 5), Point(-1, -1));

	RECT r;
	m_picImageElipse.GetClientRect(&r);
	winSegSize = cv::Size(r.right, r.bottom);
	if (winSegSize.width % 4 != 0)
	{
		winSegSize.width = 4 * (winSegSize.width / 4);
	}
	cvImgTmp = cv::Mat(winSegSize, CV_8UC3);
	bitInfo.bmiHeader.biBitCount = 24;
	bitInfo.bmiHeader.biWidth = winSegSize.width;
	bitInfo.bmiHeader.biHeight = winSegSize.height;
	bitInfo.bmiHeader.biPlanes = 1;
	bitInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfo.bmiHeader.biCompression = BI_RGB;
	bitInfo.bmiHeader.biClrImportant = 0;
	bitInfo.bmiHeader.biClrUsed = 0;
	bitInfo.bmiHeader.biSizeImage = 0;
	bitInfo.bmiHeader.biXPelsPerMeter = 0;
	bitInfo.bmiHeader.biYPelsPerMeter = 0;

	CvSize cameraSize = ueCamera->GetImageSize();
	segrowi = cameraSize.height / 4;
	segcoli = cameraSize.width / 4;
	segrowsize = cameraSize.height / 2;
	segcolsize = cameraSize.width / 2;

	m_strTemps.SetWindowTextA("");

	//Inicialitzar CUDA
	cudaError_t cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
	}

	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono.  Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CRegistreElipseDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CRegistreElipseDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CRegistreElipseDlg::PosarImatgePictureBox(cv::Mat &entrada)
{
	cv::Mat auxiliar;
	cv::cvtColor(entrada, auxiliar, cv::COLOR_GRAY2BGR);

	////PER BORRAR
	uint8_t *imData = auxiliar.data;
	int nrows = auxiliar.rows;
	int ncols = auxiliar.cols;
	int deltax, deltay;

	for (int i = 0; i < puntsSegmentatsvect.size(); ++i)
	{
		for (int a = -2; a < 3; a++)
		{
			for (int b = -2; b < 3; b++)
			{
				if ((puntsSegmentatsvect[i].x == 0) || (puntsSegmentatsvect[i].y == 0) || (puntsSegmentatsvect[i].x + b >= auxiliar.cols - 2) || (puntsSegmentatsvect[i].y + a >= auxiliar.rows - 2) || (puntsSegmentatsvect[i].x + b < 2) || (puntsSegmentatsvect[i].y + a < 2))
				{
					continue;
				}

				imData[((puntsSegmentatsvect[i].y + a) * auxiliar.cols + puntsSegmentatsvect[i].x + b) * 3] = 0;
				imData[((puntsSegmentatsvect[i].y + a) * auxiliar.cols + puntsSegmentatsvect[i].x + b) * 3 + 1] = 255;
				imData[((puntsSegmentatsvect[i].y + a) * auxiliar.cols + puntsSegmentatsvect[i].x + b) * 3 + 2] = 255;
			}
		}
	}

	if (meanSegmentats.size() > 0)
	{
		for (int a = -2; a < 3; a++)
		{
			for (int b = -2; b < 3; b++)
			{
				if ((meanSegmentats[0].x == 0) || (meanSegmentats[0].y == 0) || (meanSegmentats[0].x + b >= auxiliar.cols - 2) || (meanSegmentats[0].y + a >= auxiliar.rows - 2) || (meanSegmentats[0].x + b < 2) || (meanSegmentats[0].y + a < 2))
				{
					continue;
				}
				if ((isnan(meanSegmentats[0].x)) || (isnan(meanSegmentats[0].y)))
				{
					continue;
				}
				imData[(((int)meanSegmentats[0].y + a) * auxiliar.cols + (int)meanSegmentats[0].x + b) * 3] = 255;
				imData[(((int)meanSegmentats[0].y + a) * auxiliar.cols + (int)meanSegmentats[0].x + b) * 3 + 1] = 0;
				imData[(((int)meanSegmentats[0].y + a) * auxiliar.cols + (int)meanSegmentats[0].x + b) * 3 + 2] = 0;
			}
		}
	}
	////


	//If size does not match
	if (auxiliar.size() != winSegSize)
	{
		cv::resize(auxiliar, cvImgTmp, winSegSize);
	}
	else
	{
		cvImgTmp = auxiliar.clone();
	}


	//Rotate image
	cv::flip(cvImgTmp, cvImgTmp, 0);


	//Create MFC image
	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}

	mfcImg = new CImage();
	mfcImg->Create(winSegSize.width, winSegSize.height, 24);


	//Add header and OpenCV image to mfcImg
	StretchDIBits(mfcImg->GetDC(), 0, 0,
		winSegSize.width, winSegSize.height, 0, 0,
		winSegSize.width, winSegSize.height,
		cvImgTmp.data, &bitInfo, DIB_RGB_COLORS, SRCCOPY
	);

	//Display mfcImg in MFC window
	mfcImg->BitBlt(::GetDC(m_picImageElipse.m_hWnd), 0, 0);
}

int CRegistreElipseDlg::BuscarEllipse(cv::Mat &image, int &x, int &y)
{
	cv::Mat auxiliar = image.clone();
	auxiliar = Mat(auxiliar, cv::Rect(auxiliar.cols / 4, auxiliar.rows / 4, auxiliar.cols / 2, auxiliar.rows / 2));
	cv::Mat auxiliar2;

	double otsuvalue = cv::threshold(auxiliar, auxiliar2, 0, 255, CV_THRESH_OTSU);
	cv::threshold(auxiliar, auxiliar2, (int)otsuvalue + 10, 255, CV_THRESH_BINARY);
	u_char * auxData = auxiliar2.data;

	int maxValue = 0;
	int jBona = -1;
	int valueAux, valueAux2;
	int jAux, jini;
	bool bsc, bscn;
	bool finalImage;

	std::vector<std::vector<cv::Point3i>> PosX(0);
	std::vector<bool> possible(0);

	for (int i = 0; i < auxiliar2.rows; i = i + 5)
	{
		valueAux = 0;
		jAux = 0;
		while (jAux < auxiliar2.cols)
		{
			valueAux2 = 0;
			if (evaluarMat(auxData, i, jAux, auxiliar2.cols) > 0)
			{
				bscn = true;
				bsc = false;
				finalImage = false;

				while (bscn)
				{
					jAux = jAux + 5;
					if (jAux >= auxiliar2.cols)
					{
						bscn = false;
						bsc = false;
					}
					if (evaluarMat(auxData, i, jAux, auxiliar2.cols) == 0)
					{
						jini = jAux;
						bscn = false;
						bsc = true;
					}
				}
				if (bsc)
				{
					while (bsc)
					{
						jAux = jAux + 5;
						if (jAux >= auxiliar2.cols)
						{
							valueAux2 = jAux - jini;
							bsc = false;
							finalImage = true;
						}
						else if (evaluarMat(auxData, i, jAux, auxiliar2.cols) > 0)
						{
							bsc = false;
							valueAux2 = jAux - jini;
						}
					}
					if (valueAux2 > 15)
					{
						if (PosX.size() == 0)
						{
							std::vector<cv::Point3i> auxv(1);
							auxv[0].x = i;
							auxv[0].y = jini;
							auxv[0].z = jAux;
							PosX.push_back(auxv);
							if (finalImage)
							{
								possible.push_back(false);
							}
							else
							{
								possible.push_back(true);
							}
						}
						else
						{
							bool afegit = false;
							for (int jj = 0; jj < PosX.size(); jj++)
							{
								if ((i - PosX[jj][PosX[jj].size() - 1].x < 15) && (abs(PosX[jj][PosX[jj].size() - 1].y - jini) < 10))
								{
									PosX[jj].push_back(cv::Point3i(i, jini, jAux));
									if (finalImage)
									{
										possible[jj] = false;
									}
									afegit = true;
								}
								else if ((i - PosX[jj][PosX[jj].size() - 1].x < 15) && (abs(PosX[jj][PosX[jj].size() - 1].z - jAux) < 10))
								{
									PosX[jj].push_back(cv::Point3i(i, jini, jAux));
									if (finalImage)
									{
										possible[jj] = false;
									}
									afegit = true;
								}
							}
							if (!afegit)
							{
								std::vector<cv::Point3i> auxv(1);
								auxv[0].x = i;
								auxv[0].y = jini;
								auxv[0].z = jAux;
								PosX.push_back(auxv);
								if (finalImage)
								{
									possible.push_back(false);
								}
								else
								{
									possible.push_back(true);
								}
							}
						}
					}
					//jAux += 5;
				}
			}
			else
			{
				jAux += 5;
				bscn = false;
				bsc = false;
			}
		}
	}
	for (int i = 0; i < PosX.size(); i++)
	{
		if (PosX[i].size() < 6)
		{
			possible[i] = false;
			continue;
		}
		if ((auxiliar2.rows - PosX[i][PosX[i].size() - 1].x) < 10)
		{
			possible[i] = false;
			continue;
		}
		if (PosX[i][0].x < 10)
		{
			possible[i] = false;
			continue;
		}
		if (evaluarMat(auxData, PosX[i][0].x - 5, PosX[i][0].y, auxiliar2.cols) == 0)
		{
			bool finalE = false;
			bool blanc = false;
			int jaux = PosX[i][0].y - 5;
			while (!finalE)
			{
				if (evaluarMat(auxData, PosX[i][0].x - 5, jaux, auxiliar2.cols) == 0)
				{
					if (PosX[i][0].y - jaux > 20)
					{
						finalE = true;
						blanc = false;
					}
					jaux -= 5;
				}
				else
				{
					finalE = true;
					blanc = true;
				}
			}
			if (!blanc)
			{
				possible[i] = false;
			}
		}
		if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, PosX[i][PosX[i].size() - 1].y, auxiliar2.cols) == 0)
		{
			bool finalE = false;
			bool blanc = false;
			int jaux = PosX[i][PosX[i].size() - 1].y - 5;
			while (!finalE)
			{
				if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, jaux, auxiliar2.cols) == 0)
				{
					if (PosX[i][PosX[i].size() - 1].y - jaux > 20)
					{
						finalE = true;
						blanc = false;
					}
					jaux -= 5;
				}
				else
				{
					finalE = true;
					blanc = true;
				}
			}
			if (!blanc)
			{
				possible[i] = false;
			}
		}
		if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, PosX[i][PosX[i].size() - 1].z, auxiliar2.cols) == 0)
		{
			bool finalE = false;
			bool blanc = false;
			int jaux = PosX[i][PosX[i].size() - 1].z + 5;
			while (!finalE)
			{
				if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, jaux, auxiliar2.cols) == 0)
				{
					if (jaux - PosX[i][PosX[i].size() - 1].z > 20)
					{
						finalE = true;
						blanc = false;
					}
					jaux += 5;
				}
				else
				{
					finalE = true;
					blanc = true;
				}
			}
			if (!blanc)
			{
				possible[i] = false;
			}
		}
	}
	std::vector<int> iPoss(0);
	int maxA = 0;
	int posM = -1;
	int maxAux;
	for (int i = 0; i < possible.size(); i++)
	{
		if (possible[i])
		{
			iPoss.push_back(i);
		}
	}
	if (iPoss.size() == 0)
	{
		return -1;
	}
	if (iPoss.size() == 1)
	{
		y = (PosX[iPoss[0]][0].x + PosX[iPoss[0]][PosX[iPoss[0]].size() - 1].x) / 2 + image.rows / 4;
		x = (PosX[iPoss[0]][PosX[iPoss[0]].size() / 2].y + PosX[iPoss[0]][PosX[iPoss[0]].size() / 2].z) / 2 + image.cols / 4;
		return 0;
	}
	else
	{
		for (int i = 0; i < iPoss.size(); i++)
		{
			maxAux = 0;
			for (int j = 0; j < PosX[iPoss[i]].size(); j++)
			{
				maxAux = maxAux + PosX[iPoss[i]][j].z - PosX[iPoss[i]][j].y;
			}
			if (maxAux > maxA)
			{
				maxA = maxAux;
				posM = i;
			}
		}
		y = (PosX[iPoss[posM]][0].x + PosX[iPoss[posM]][PosX[iPoss[posM]].size() - 1].x) / 2 + image.rows / 4;
		x = (PosX[iPoss[posM]][PosX[iPoss[posM]].size() / 2].y + PosX[iPoss[posM]][PosX[iPoss[posM]].size() / 2].z) / 2 + image.cols / 4;
		return 1;
	}
}

inline cv::Mat CRegistreElipseDlg::imCompositeFilter(cv::Mat &I)
{
	cv::Mat Aux1, Aux2;
	cv::morphologyEx(I, Aux1, MORPH_OPEN, crossKernel);
	cv::morphologyEx(Aux1, Aux1, MORPH_CLOSE, squareKernel);
	cv::morphologyEx(I, Aux2, MORPH_CLOSE, crossKernel);
	cv::morphologyEx(Aux2, Aux2, MORPH_OPEN, squareKernel);
	return (Aux1 / 2 + Aux2 / 2);
}

//CannyPF from the paper CannyLines: A parameter-free line segment detector (Lu et al ICIP2015)
void CRegistreElipseDlg::cannyPF(cv::Mat &image, int gaussianSize, float VMGradient, cv::Mat &edgeMap)
{
	int i, j, m, n;
	int grayLevels = 255;
	float gNoise = 1.3333;  //1.3333 
	float thGradientLow = gNoise;

	int cols = image.cols;
	int rows = image.rows;
	int cols_1 = cols - 1;
	int rows_1 = rows - 1;

	//meaningful Length
	int thMeaningfulLength = int(2.0*log((float)rows*cols) / log(8.0) + 0.5);
	float thAngle = 2 * atan(2.0 / float(thMeaningfulLength));

	//gaussian filter
	cv::Mat filteredImage;
	cv::GaussianBlur(image, filteredImage, cv::Size(gaussianSize, gaussianSize), 1.0);
	//grayImage.release();

	//get gradient map and orientation map
	cv::Mat gradientMap = cv::Mat::zeros(filteredImage.rows, filteredImage.cols, CV_32FC1);
	cv::Mat dx(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));
	cv::Mat dy(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));

	int apertureSize = 3;
	cv::Sobel(filteredImage, dx, CV_16S, 1, 0, apertureSize, 1, 0, cv::BORDER_REPLICATE);
	cv::Sobel(filteredImage, dy, CV_16S, 0, 1, apertureSize, 1, 0, cv::BORDER_REPLICATE);

	//calculate gradient and orientation
	int totalNum = 0;
	int times = 8;
	std::vector<int> histogram(times*grayLevels, 0);
	for (i = 0; i<rows; ++i)
	{
		float *ptrG = gradientMap.ptr<float>(i);
		short *ptrX = dx.ptr<short>(i);
		short *ptrY = dy.ptr<short>(i);
		for (j = 0; j<cols; ++j)
		{
			float gx = ptrX[j];
			float gy = ptrY[j];

			ptrG[j] = sqrt(gx*gx + gy*gy);// abs(gx) + abs(gy);
			if (ptrG[j]>thGradientLow)
			{
				histogram[int(ptrG[j] + 0.5)]++;
				totalNum++;
			}
			else
				ptrG[j] = 0.0;
		}
	}

	//gradient statistic
	float N2 = 0;
	for (i = 0; i<histogram.size(); ++i)
	{
		if (histogram[i])
			N2 += (float)histogram[i] * (histogram[i] - 1);
	}
	float pMax = 1.0 / exp((log(N2) / thMeaningfulLength));
	float pMin = 1.0 / exp((log(N2) / sqrt((float)cols*rows)));

	std::vector<float> greaterThan(times*grayLevels, 0);
	int count = 0;
	for (i = times*grayLevels - 1; i >= 0; --i)
	{
		count += histogram[i];
		float probabilityGreater = float(count) / float(totalNum);
		greaterThan[i] = probabilityGreater;
	}
	count = 0;

	//get two gradient thresholds
	int thGradientHigh = 0;
	for (i = times*grayLevels - 1; i >= 0; --i)
	{
		if (greaterThan[i]>pMax)
		{
			thGradientHigh = i;
			break;
		}
	}
	for (i = times*grayLevels - 1; i >= 0; --i)
	{
		if (greaterThan[i]>pMin)
		{
			thGradientLow = i;
			break;
		}
	}
	if (thGradientLow<gNoise) thGradientLow = gNoise;

	//convert probabilistic meaningful to visual meaningful
	thGradientHigh = sqrt(thGradientHigh*VMGradient);

	//canny
	cv::Canny(filteredImage, edgeMap, thGradientLow, thGradientHigh, apertureSize, true);
}

void CRegistreElipseDlg::thinningIteration(cv::Mat& img, int iter)
{
	CV_Assert(img.channels() == 1);
	CV_Assert(img.depth() != sizeof(uchar));
	CV_Assert(img.rows > 3 && img.cols > 3);

	cv::Mat marker = cv::Mat::zeros(img.size(), CV_8UC1);

	int nRows = img.rows;
	int nCols = img.cols;

	if (img.isContinuous()) {
		nCols *= nRows;
		nRows = 1;
	}

	int x, y;
	uchar *pAbove;
	uchar *pCurr;
	uchar *pBelow;
	uchar *nw, *no, *ne;    // north (pAbove)
	uchar *we, *me, *ea;
	uchar *sw, *so, *se;    // south (pBelow)

	uchar *pDst;

	// initialize row pointers
	pAbove = NULL;
	pCurr = img.ptr<uchar>(0);
	pBelow = img.ptr<uchar>(1);

	for (y = 1; y < img.rows - 1; ++y) {
		// shift the rows up by one
		pAbove = pCurr;
		pCurr = pBelow;
		pBelow = img.ptr<uchar>(y + 1);

		pDst = marker.ptr<uchar>(y);

		// initialize col pointers
		no = &(pAbove[0]);
		ne = &(pAbove[1]);
		me = &(pCurr[0]);
		ea = &(pCurr[1]);
		so = &(pBelow[0]);
		se = &(pBelow[1]);

		for (x = 1; x < img.cols - 1; ++x) {
			// shift col pointers left by one (scan left to right)
			nw = no;
			no = ne;
			ne = &(pAbove[x + 1]);
			we = me;
			me = ea;
			ea = &(pCurr[x + 1]);
			sw = so;
			so = se;
			se = &(pBelow[x + 1]);

			int A = (*no == 0 && *ne == 1) + (*ne == 0 && *ea == 1) +
				(*ea == 0 && *se == 1) + (*se == 0 && *so == 1) +
				(*so == 0 && *sw == 1) + (*sw == 0 && *we == 1) +
				(*we == 0 && *nw == 1) + (*nw == 0 && *no == 1);
			int B = *no + *ne + *ea + *se + *so + *sw + *we + *nw;
			int m1 = iter == 0 ? (*no * *ea * *so) : (*no * *ea * *we);
			int m2 = iter == 0 ? (*ea * *so * *we) : (*no * *so * *we);

			if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
				pDst[x] = 1;
		}
	}

	img &= ~marker;
}

void CRegistreElipseDlg::ZhangSuen(const cv::Mat& src, cv::Mat& dst)
{
	dst = src.clone();
	dst /= 255;         // convert to binary image

	cv::Mat prev = cv::Mat::zeros(dst.size(), CV_8UC1);
	cv::Mat diff;

	int times = 0;

	do {
		times++;
		thinningIteration(dst, 0);
		thinningIteration(dst, 1);
		cv::absdiff(dst, prev, diff);
		dst.copyTo(prev);
	} while ((cv::countNonZero(diff) > 0)&&(times<3));

	dst *= 255;
}

void CRegistreElipseDlg::SegmentacioImatge(cv::Mat &entrada, cv::Mat &sortida)
{

	cv::Mat auxiliar, auxiliar2;


	auxiliar = entrada.clone();
	auxiliar = auxiliar(cv::Rect(segcoli, segrowi, segcolsize, segrowsize));
	cv::medianBlur(auxiliar, auxiliar2, 5); //Median Filter of window 5x5

	auxiliar = imCompositeFilter(auxiliar);

	//cannyPF(auxiliar, 5, 130.0, auxiliar2);
	cv::Mat filteredImage;
	cv::GaussianBlur(auxiliar, filteredImage, cv::Size(5, 5), 2.0);
	//grayImage.release();

	//get gradient map and orientation map
	cv::Mat gradientMap = cv::Mat::zeros(filteredImage.rows, filteredImage.cols, CV_32FC1);
	cv::Mat dx(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));
	cv::Mat dy(filteredImage.rows, filteredImage.cols, CV_16S, Scalar(0));

	int apertureSize = 3;
	cv::Sobel(filteredImage, dx, CV_16S, 1, 0, apertureSize, 1, 0, cv::BORDER_REPLICATE);
	cv::Sobel(filteredImage, dy, CV_16S, 0, 1, apertureSize, 1, 0, cv::BORDER_REPLICATE);
	//calculate gradient and orientation
	int totalNum = 0;
	int times = 8;
	float thGradientLow = 2.0;// 1.3333;
	std::vector<int> histogram(200, 0);
	float maxValue = 0.0;
	for (int i = 0; i<dx.rows; ++i)
	{
		float *ptrG = gradientMap.ptr<float>(i);
		short *ptrX = dx.ptr<short>(i);
		short *ptrY = dy.ptr<short>(i);
		for (int j = 0; j<dx.cols; ++j)
		{
			float gx = ptrX[j];
			float gy = ptrY[j];

			ptrG[j] = sqrt(gx*gx + gy*gy);
			if (ptrG[j]<thGradientLow)
				ptrG[j] = 0.0;
			else
			{
				if (ptrG[j] > maxValue)
					maxValue = ptrG[j];
			}
		}
	}

	//CString nameImage;
	//CString pathD = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\RegistreElipse\\RegistreElipse\\Imatges\\Process\\";
	//nameImage.Format("Imatge_Canny.png");
	//nameImage = pathD + nameImage;
	//imwrite(nameImage.GetString(), gradientMap);

	int totalHist = 0;
	for (int i = 0; i<dx.rows; ++i)
	{
		float *ptrG = gradientMap.ptr<float>(i);
		for (int j = 0; j<dx.cols; ++j)
		{
			if (fabs(ptrG[j] - maxValue) < 0.001*maxValue)
			{
				histogram[199]++;
				totalHist++;
			}
			else if (ptrG[j] > 0.0)
			{
				histogram[(int)(ptrG[j] / maxValue * 200.0)]++;
				totalHist++;
			}
		}
	}

	int sumi = 0;
	float wb = 0.0;
	float wf;
	int sumb = 0;
	float between, maxBetween = 0.0;
	float mf;
	for (int i = 0; i < 200; i++)
	{
		sumi += i*histogram[i];
	}

	float levelOtsu = 0.0f;
	for (int i = 0; i < 200; i++)
	{
		wb += histogram[i];
		wf = totalHist - wb;
		if (wb == 0 || wf == 0)
			continue;
		sumb += i*histogram[i];
		mf = (float)(sumi - sumb) / (float)wf;
		between = wb*wf*((float)sumb / (float)wb - mf)*((float)sumb / (float)wb - mf);
		if (between >= maxBetween)
		{
			levelOtsu = i;
			maxBetween = between;
		}
	}

	double thres_val = 2.5 * levelOtsu / 200.0f * maxValue;//cv::threshold(gradientMap, auxiliar2, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	Canny(dx, dy, auxiliar2, 1.0/12.0*thres_val, thres_val, true); //0.05/6.0*thres_val, thres_val, true);

	cv::morphologyEx(auxiliar2, auxiliar2, MORPH_CLOSE, squareKernel, cv::Point(-1, -1), 2);
	cv::dilate(auxiliar2, auxiliar, segdilate);
	cv::erode(auxiliar, auxiliar2, segerode);

	

	//Filter small blobs
	cv::filterSpeckles(auxiliar2, 0, 100, 0);

	ZhangSuen(auxiliar2, auxiliar2);

	sortida = cv::Mat::zeros(entrada.rows, entrada.cols, auxiliar2.type());
	auxiliar2.copyTo(sortida(cv::Rect(segcoli,segrowi,segcolsize,segrowsize)));
}

int CRegistreElipseDlg::BuscarEllipse(cv::Mat &image, int &x, int &y, cv::Mat &segm)
{
	cv::Mat auxiliar = image.clone();
	auxiliar = Mat(auxiliar, cv::Rect(auxiliar.cols / 4, auxiliar.rows / 4, auxiliar.cols / 2, auxiliar.rows / 2));
	cv::Mat auxiliar2;

	double otsuvalue = cv::threshold(auxiliar, auxiliar2, 0, 255, CV_THRESH_OTSU);
	if (otsuvalue < 150.0)
		otsuvalue = 150.0;
	cv::threshold(auxiliar, auxiliar2, (int)otsuvalue + 10, 255, CV_THRESH_BINARY);
	u_char * auxData = auxiliar2.data;

	segm = cv::Mat::zeros(cv::Size(image.cols,image.rows), image.type());
	auxiliar2.copyTo(segm(cv::Rect(segm.cols / 4, segm.rows / 4, segm.cols / 2, segm.rows / 2)));

	int maxValue = 0;
	int jBona = -1;
	int valueAux, valueAux2;
	int jAux, jini;
	bool bsc, bscn;
	bool finalImage;

	std::vector<std::vector<cv::Point3i>> PosX(0);
	std::vector<bool> possible(0);

	for (int i = 0; i < auxiliar2.rows; i = i + 5)
	{
		valueAux = 0;
		jAux = 0;
		while (jAux < auxiliar2.cols)
		{
			valueAux2 = 0;
			if (evaluarMat(auxData, i, jAux, auxiliar2.cols) > 0)
			{
				bscn = true;
				bsc = false;
				finalImage = false;

				while (bscn)
				{
					jAux = jAux + 5;
					if (jAux >= auxiliar2.cols)
					{
						bscn = false;
						bsc = false;
					}
					if (evaluarMat(auxData, i, jAux, auxiliar2.cols) == 0)
					{
						jini = jAux;
						bscn = false;
						bsc = true;
					}
				}
				if (bsc)
				{
					while (bsc)
					{
						jAux = jAux + 5;
						if (jAux >= auxiliar2.cols)
						{
							valueAux2 = jAux - jini;
							bsc = false;
							finalImage = true;
						}
						else if (evaluarMat(auxData, i, jAux, auxiliar2.cols) > 0)
						{
							bsc = false;
							valueAux2 = jAux - jini;
						}
					}
					if (valueAux2 > 15)
					{
						if (PosX.size() == 0)
						{
							std::vector<cv::Point3i> auxv(1);
							auxv[0].x = i;
							auxv[0].y = jini;
							auxv[0].z = jAux;
							PosX.push_back(auxv);
							if (finalImage)
							{
								possible.push_back(false);
							}
							else
							{
								possible.push_back(true);
							}
						}
						else
						{
							bool afegit = false;
							for (int jj = 0; jj < PosX.size(); jj++)
							{
								if ((i - PosX[jj][PosX[jj].size() - 1].x < 15) && (abs(PosX[jj][PosX[jj].size() - 1].y - jini) < 10))
								{
									PosX[jj].push_back(cv::Point3i(i, jini, jAux));
									if (finalImage)
									{
										possible[jj] = false;
									}
									afegit = true;
								}
								else if ((i - PosX[jj][PosX[jj].size() - 1].x < 15) && (abs(PosX[jj][PosX[jj].size() - 1].z - jAux) < 10))
								{
									PosX[jj].push_back(cv::Point3i(i, jini, jAux));
									if (finalImage)
									{
										possible[jj] = false;
									}
									afegit = true;
								}
							}
							if (!afegit)
							{
								std::vector<cv::Point3i> auxv(1);
								auxv[0].x = i;
								auxv[0].y = jini;
								auxv[0].z = jAux;
								PosX.push_back(auxv);
								if (finalImage)
								{
									possible.push_back(false);
								}
								else
								{
									possible.push_back(true);
								}
							}
						}
					}
				}
			}
			else
			{
				jAux += 5;
				bscn = false;
				bsc = false;
			}
		}
	}
	for (int i = 0; i < PosX.size(); i++)
	{
		if (PosX[i].size() < 6)
		{
			possible[i] = false;
			continue;
		}
		if ((auxiliar2.rows - PosX[i][PosX[i].size() - 1].x) < 10)
		{
			possible[i] = false;
			continue;
		}
		if (PosX[i][0].x < 10)
		{
			possible[i] = false;
			continue;
		}
		if (evaluarMat(auxData, PosX[i][0].x - 5, PosX[i][0].y, auxiliar2.cols) == 0)
		{
			bool finalE = false;
			bool blanc = false;
			int jaux = PosX[i][0].y - 5;
			while (!finalE)
			{
				if (evaluarMat(auxData, PosX[i][0].x - 5, jaux, auxiliar2.cols) == 0)
				{
					if (PosX[i][0].y - jaux > 20)
					{
						finalE = true;
						blanc = false;
					}
					jaux -= 5;
				}
				else
				{
					finalE = true;
					blanc = true;
				}
			}
			if (!blanc)
			{
				possible[i] = false;
			}
		}
		if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, PosX[i][PosX[i].size() - 1].y, auxiliar2.cols) == 0)
		{
			bool finalE = false;
			bool blanc = false;
			int jaux = PosX[i][PosX[i].size() - 1].y - 5;
			while (!finalE)
			{
				if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, jaux, auxiliar2.cols) == 0)
				{
					if (PosX[i][PosX[i].size() - 1].y - jaux > 20)
					{
						finalE = true;
						blanc = false;
					}
					jaux -= 5;
				}
				else
				{
					finalE = true;
					blanc = true;
				}
			}
			if (!blanc)
			{
				possible[i] = false;
			}
		}
		if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, PosX[i][PosX[i].size() - 1].z, auxiliar2.cols) == 0)
		{
			bool finalE = false;
			bool blanc = false;
			int jaux = PosX[i][PosX[i].size() - 1].z + 5;
			while (!finalE)
			{
				if (evaluarMat(auxData, PosX[i][PosX[i].size() - 1].x - 5, jaux, auxiliar2.cols) == 0)
				{
					if (jaux - PosX[i][PosX[i].size() - 1].z > 20)
					{
						finalE = true;
						blanc = false;
					}
					jaux += 5;
				}
				else
				{
					finalE = true;
					blanc = true;
				}
			}
			if (!blanc)
			{
				possible[i] = false;
			}
		}
	}
	std::vector<int> iPoss(0);
	int maxA = 0;
	int posM = -1;
	int maxAux;
	for (int i = 0; i < possible.size(); i++)
	{
		if (possible[i])
		{
			iPoss.push_back(i);
		}
	}
	if (iPoss.size() == 0)
	{
		return -1;
	}
	if (iPoss.size() == 1)
	{
		y = (PosX[iPoss[0]][0].x + PosX[iPoss[0]][PosX[iPoss[0]].size() - 1].x) / 2 + image.rows / 4;
		x = (PosX[iPoss[0]][PosX[iPoss[0]].size() / 2].y + PosX[iPoss[0]][PosX[iPoss[0]].size() / 2].z) / 2 + image.cols / 4;
		return 0;
	}
	else
	{
		for (int i = 0; i < iPoss.size(); i++)
		{
			maxAux = 0;
			for (int j = 0; j < PosX[iPoss[i]].size(); j++)
			{
				maxAux = maxAux + PosX[iPoss[i]][j].z - PosX[iPoss[i]][j].y;
			}
			if (maxAux > maxA)
			{
				maxA = maxAux;
				posM = i;
			}
		}
		y = (PosX[iPoss[posM]][0].x + PosX[iPoss[posM]][PosX[iPoss[posM]].size() - 1].x) / 2 + image.rows / 4;
		x = (PosX[iPoss[posM]][PosX[iPoss[posM]].size() / 2].y + PosX[iPoss[posM]][PosX[iPoss[posM]].size() / 2].z) / 2 + image.cols / 4;
		return 1;
	}
}

int CRegistreElipseDlg::ParametresEllipseProbabilitat(std::vector <cv::Point2f> &punts, float &x0, float &y0, float &a2, float &b2, float &ang)
{
	int numTirades = 2000;
	srand(time(NULL));
	int millor;
	int millorAux;
	float distancia, distanciaMillor = 100000000.0f, distanciaTotal;
	float distBona = 0.0002f;
	int tirada;
	Eigen::VectorXf paramBons(5);

	std::vector <cv::Point2f> puntsBons;
	std::vector <int> samples;
	puntsBons.clear();
	Eigen::Matrix3f conicaAux;

	for (int i = 0; i < punts.size(); i++)
	{
		if ((punts[i].x == 0) || (punts[i].y == 0))
			continue;
		puntsBons.push_back(punts[i]);
	}

	if (puntsBons.size() < PUNTSCONTORNIMATGE2D / 4)
		return -1;

	bool trobat = false;
	int sizeMatrix = 10;
	Eigen::MatrixXf A(sizeMatrix, 5);
	Eigen::VectorXf B(sizeMatrix);
	Eigen::VectorXf param;
	float deltaAux, JAux, IAux;
	millor = 0;
	std::vector<float> distanciesActual;
	for (int i = 0; i < numTirades; i++)
	{
		samples.clear();
		while (samples.size() < sizeMatrix)
		{
			tirada = rand() % puntsBons.size();
			trobat = false;
			for (int j = 0; j < samples.size(); j++)
			{
				if (samples[j] == tirada)
				{
					trobat = true;
					break;
				}
			}
			if (!trobat)
			{
				samples.push_back(tirada);
			}
		}
		for (int j = 0; j < samples.size(); j++)
		{
			A(j, 0) = puntsBons[samples[j]].x * puntsBons[samples[j]].x;
			A(j, 1) = puntsBons[samples[j]].x * puntsBons[samples[j]].y;
			A(j, 2) = puntsBons[samples[j]].y * puntsBons[samples[j]].y;
			A(j, 3) = puntsBons[samples[j]].x;
			A(j, 4) = puntsBons[samples[j]].y;
			B(j) = -1.0f;
		}
		param = (A.transpose()*A).inverse()*A.transpose()*B;
		millorAux = 0;
		distanciaTotal = 0.0f;


		conicaAux(0, 0) = param(0) / param(0);
		conicaAux(0, 1) = param(1) / 2.0f / param(0);
		conicaAux(1, 0) = param(1) / 2.0f / param(0);
		conicaAux(1, 1) = param(2) / param(0);
		conicaAux(0, 2) = param(3) / 2.0f / param(0);
		conicaAux(2, 0) = param(3) / 2.0f / param(0);
		conicaAux(1, 2) = param(4) / 2.0f / param(0);
		conicaAux(2, 1) = param(4) / 2.0f / param(0);
		conicaAux(2, 2) = 1.0f / param(0);
		deltaAux = conicaAux.determinant();
		JAux = conicaAux(0, 0)*conicaAux(1, 1) - conicaAux(0, 1)*conicaAux(1, 0);
		IAux = conicaAux(0, 0) + conicaAux(1, 1);
		if ((fabs(deltaAux) > 0.00000001f) && (JAux > 0.0f) && (deltaAux / IAux < 0.0f))
		{
			distanciesActual.clear();
			for (int j = 0; j < puntsBons.size(); j++)
			{
				distancia = fabs(puntsBons[j].x* puntsBons[j].x*param(0) + puntsBons[j].x * puntsBons[j].y*param(1) + puntsBons[j].y*puntsBons[j].y*param(2) + puntsBons[j].x*param(3) + puntsBons[j].y*param(4) + 1.0f);
				/*if (distancia < distBona)
				{
					millorAux++;
					distanciaTotal += distancia;
				}*/
				distanciesActual.push_back(distancia);
			}
			std::sort(distanciesActual.begin(),distanciesActual.end());
			for (int j = 0; j < distanciesActual.size() / 2; j++)
				distanciaTotal += distanciesActual[j];
			/*if (millorAux > millor)
			{
				millor = millorAux;
				distanciaMillor = distanciaTotal;
				for (int j = 0; j < 5; j++)
					paramBons(j) = param(j);
			}
			else if ((millorAux == millor) && (distanciaTotal < distanciaMillor))
			{
				distanciaMillor = distanciaTotal;
				for (int j = 0; j < 5; j++)
					paramBons(j) = param(j);
			}*/
			if (distanciaTotal < distanciaMillor)
			{
				distanciaMillor = distanciaTotal;
				for (int j = 0; j < 5; j++)
					paramBons(j) = param(j);
			}
		}
	}
	/*if (millor < 10)
		return 0;*/
	Eigen::Matrix3f conica;
	conica(0, 0) = paramBons(0) / paramBons(0);
	conica(0, 1) = paramBons(1) / 2.0f / paramBons(0);
	conica(1, 0) = paramBons(1) / 2.0f / paramBons(0);
	conica(1, 1) = paramBons(2) / paramBons(0);
	conica(0, 2) = paramBons(3) / 2.0f / paramBons(0);
	conica(2, 0) = paramBons(3) / 2.0f / paramBons(0);
	conica(1, 2) = paramBons(4) / 2.0f / paramBons(0);
	conica(2, 1) = paramBons(4) / 2.0f / paramBons(0);
	conica(2, 2) = 1.0f / paramBons(0);
	float delta = conica.determinant();
	float J = conica(0, 0)*conica(1, 1) - conica(0, 1)*conica(1, 0);
	float I = conica(0, 0) + conica(1, 1);
	float termeEixos;
	if (fabs(delta)> 0.00000000001f)
	{
		if (J > 0.0f)
		{
			if (delta / I < 0.0f)
			{
				x0 = -(conica(1, 1)*conica(0, 2) - conica(0, 1)*conica(1, 2)) / J;
				y0 = -(conica(0, 0)*conica(1, 2) - conica(0, 1)*conica(0, 2)) / J;
				termeEixos = -2.0f*(conica(0, 0)*conica(1, 2)*conica(1, 2) + conica(1, 1)*conica(0, 2)*conica(0, 2) + conica(2, 2)*conica(0, 1)*conica(0, 1) - 2.0f*conica(0, 1)*conica(0, 2)*conica(1, 2) - conica(0, 0)*conica(1, 1)*conica(2, 2)) / J;
				a2 = sqrtf(termeEixos / (sqrtf((conica(0, 0) - conica(1, 1))*(conica(0, 0) - conica(1, 1)) + 4.0f*conica(0, 1)*conica(0, 1)) - conica(0, 0) - conica(1, 1)));
				b2 = sqrtf(termeEixos / (-sqrtf((conica(0, 0) - conica(1, 1))*(conica(0, 0) - conica(1, 1)) + 4.0f*conica(0, 1)*conica(0, 1)) - conica(0, 0) - conica(1, 1)));

				if ((fabs(conica(0, 1)) < 0.000000001f) && (conica(0, 0)<conica(1, 1)))
				{
					ang = 0.0f;
				}
				else if (fabs(conica(0, 1)) < 0.000000001f)
				{
					ang = M_PI / 2.0f;
				}
				else if (conica(0, 0) < conica(1, 1))
				{
					ang = 1.0f / 2.0f * atanf(1 / ((conica(0, 0) - conica(1, 1)) / 2.0f / conica(0, 1)));
				}
				else
				{
					ang = M_PI / 2.0f + 1.0f / 2.0f * atanf(1 / ((conica(0, 0) - conica(1, 1)) / 2.0f / conica(0, 1)));
				}

				return 1;
			}
			return -2;
		}
		return -3;
	}
	return -4;
}

int CRegistreElipseDlg::creixementRegio(cv::Mat & imatge, cv::Mat & mascara, int x, int y, int lbl, std::vector <cv::Point2i> & points)
{
	std::vector<cv::Point2i> cua;
	cv::Point2i pt = cv::Point2i(x, y);
	points.clear();
	points.push_back(pt);
	cua.push_back(pt);
	uint8_t *imData = imatge.data;
	uint8_t *masData = mascara.data;
	int valInicial = evaluarMat(imData, y, x, imatge.cols);
	//int DD[2][4] = { { -1, 0, 1, 0 },{ 0, -1, 0, 1 } };
	int DD[2][8] = { { -1, -1, -1, 0, 1, 1, 1, 0 },{ -1, 0, 1, 1, 1, 0, -1, -1 } };
	int nPunts = 1;
	int trobatlbl, trobatblanc;
	masData[y * mascara.cols + x] = lbl;
	while (cua.size() > 0)
	{
		pt = cua.back();
		cua.pop_back();
		for (int i = 0; i < 8; ++i)
		{
			if ((pt.x + DD[0][i] > -1) && (pt.y + DD[1][i] > -1) && (pt.x + DD[0][i] < mascara.cols) && (pt.y + DD[1][i] < mascara.rows))
			{
				if ((evaluarMat(imData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) == valInicial) && (evaluarMat(masData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) == 0))
				{
					if(((evaluarMat(imData, (pt.y + DD[1][modval(i+1,8)]), (pt.x + DD[0][modval(i + 1, 8)]), imatge.cols) == valInicial)&& (evaluarMat(imData, (pt.y + DD[1][modval(i - 1, 8)]), (pt.x + DD[0][modval(i - 1, 8)]), imatge.cols) != valInicial))|| ((evaluarMat(imData, (pt.y + DD[1][modval(i + 1, 8)]), (pt.x + DD[0][modval(i + 1, 8)]), imatge.cols) != valInicial) && (evaluarMat(imData, (pt.y + DD[1][modval(i - 1, 8)]), (pt.x + DD[0][modval(i - 1, 8)]), imatge.cols) == valInicial)))
					{
						cv::Point2i ptr = cv::Point2i(pt.x + DD[0][i], pt.y + DD[1][i]);
						cua.push_back(ptr);
						masData[(pt.y + DD[1][i]) * imatge.cols + (pt.x + DD[0][i])] = lbl;
						points.push_back(ptr);
						nPunts++;
					}
					else
					{
						trobatlbl = 0;
						trobatblanc = 0;
						for (int j = 0; j < 8; j++)
						{
							if (evaluarMat(masData, (pt.y + DD[1][i] + DD[1][j]), (pt.x + DD[0][i] + DD[0][j]), imatge.cols) == lbl)
							{
								trobatlbl++;
							}
							else if (evaluarMat(imData, (pt.y + DD[1][i] + DD[1][j]), (pt.x + DD[0][i] + DD[0][j]), imatge.cols) != valInicial)
							{
								trobatblanc++;
							}
							if ((trobatblanc > 0) && (trobatlbl > 1))
								break;
						}
						if ((trobatblanc > 0) && (trobatlbl > 1))
						{
							cv::Point2i ptr = cv::Point2i(pt.x + DD[0][i], pt.y + DD[1][i]);
							cua.push_back(ptr);
							masData[(pt.y + DD[1][i]) * imatge.cols + (pt.x + DD[0][i])] = lbl;
							points.push_back(ptr);
							nPunts++;
						}
					}
				}
			}
		}
	}
	return nPunts;
}

void CRegistreElipseDlg::trobarPuntsElipse(cv::Mat &imatge, std::vector<cv::Point2i> &punts, int nPunts, int rowInici, int colInici)
{
	cv::Mat imatgeAux = imatge.clone();
	uint8_t *imData = imatgeAux.data;
	cv::Mat mascara = cv::Mat::zeros(imatgeAux.rows, imatgeAux.cols, CV_8UC1);
	uint8_t *masData = mascara.data;
	std::vector<cv::Point2i> points = std::vector<cv::Point2i>();
	std::vector<cv::Point2i> pointsaux = std::vector<cv::Point2i>();
	int rows = imatgeAux.rows;
	int cols = imatgeAux.cols;
	int val, vmas;

	int rowC = rowInici;
	int colC = colInici;
	if ((rowC < segrowi) || (rowC > segrowi + segrowsize))
	{
		rowC = imatgeAux.rows / 2;
	}
	if ((colC < segcoli) || (colC > segcoli + segcolsize))
	{
		colC = imatgeAux.cols / 2;
	}
	
	int valInicial = evaluarMat(imData, rowC, colC, cols);
	int valBuscant;
	bool buscant;
	int indBusc;
	int nSeg;

	//Buscarem en la direccions (1,0)
	buscant = true;
	indBusc = colC;
	while (buscant)
	{
		indBusc++;
		valBuscant = evaluarMat(imData, rowC, indBusc, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegio(imatgeAux, mascara, indBusc - 1, rowC, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
			}
		}
		if ((indBusc - colC > 80)||(indBusc + 1 >= cols))
		{
			buscant = false;
		}

	}
	//Buscarem en la direccions (-1,0)
	buscant = true;
	indBusc = colC;
	while (buscant)
	{
		indBusc--;
		valBuscant = evaluarMat(imData, rowC, indBusc, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegio(imatgeAux, mascara, indBusc + 1, rowC, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
			}
		}
		if ((colC - indBusc > 80) || (indBusc < 1))
		{
			buscant = false;
		}

	}
	//Buscarem en la direccions (0,1)
	buscant = true;
	indBusc = rowC;
	while (buscant)
	{
		indBusc++;
		valBuscant = evaluarMat(imData, indBusc, colC, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegio(imatgeAux, mascara, colC, indBusc - 1, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
			}
		}
		if ((indBusc - rowC > 80) || (indBusc + 1 >= rows))
		{
			buscant = false;
		}

	}
	//Buscarem en la direccions (0, -1)
	buscant = true;
	indBusc = rowC;
	while (buscant)
	{
		indBusc--;
		valBuscant = evaluarMat(imData, indBusc, colC, cols);
		if (valBuscant != valInicial)
		{
			nSeg = creixementRegio(imatgeAux, mascara, colC, indBusc + 1, 1, pointsaux);
			if ((nSeg > MINTAMANYZONACONTORN) && (nSeg < MAXTAMANYZONACONTORN))
			{
				std::copy(pointsaux.begin(), pointsaux.end(), std::back_inserter(points));
			}
		}
		if ((rowC - indBusc > 80) || (indBusc < 1))
		{
			buscant = false;
		}

	}

	//Part de seccions
	cv::Point2f meanVal(0.0f, 0.0f);
	punts = std::vector<cv::Point2i>(nPunts);
	std::vector<std::vector<cv::Point2i>> puntsCirc(nPunts);
	std::vector<float> rmaxP(nPunts);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPunts; i++)
	{
		rmaxP[i] = 100.0f*(cols + rows) * (cols + rows);
	}

	meanVal.x = (float)colC;
	meanVal.y = (float)rowC;

	/*for (int i = 0; i < points.size(); ++i)
	{
		meanVal.x += (float)points[i].x;
		meanVal.y += (float)points[i].y;
	}
	meanVal /= (float)points.size();*/

	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2f) points[i] - meanVal;
		raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPunts);
		if (indEx >= nPunts)
			indEx = 0;
		puntsCirc[indEx].push_back(points[i]);
		if (rmaxP[indEx] > raux)
		{
			rmaxP[indEx] = raux;
			(punts)[indEx].x = points[i].x;
			(punts)[indEx].y = points[i].y;
		}
	}
	cv::Point2f centerElipse(0.0f, 0.0f);
	int esborrats = 0;
	for (int i = 0; i < punts.size(); i++)
	{
		if ((punts[i].x == 0) || (punts[i].y == 0))
		{
			esborrats++;
		}
		else if ((normCvPoint2i(punts[i] - punts[modval(i + 1, punts.size())]) > DISTENTREPUNTS) && (normCvPoint2i(punts[i] - punts[modval(i - 1, punts.size())]) > DISTENTREPUNTS))
		{
			esborrats++;
		}
		else
		{
			centerElipse.x = centerElipse.x + punts[i].x;
			centerElipse.y = centerElipse.y + punts[i].y;
		}
	}
	centerElipse /= (float)(punts.size() - esborrats);

	//Zona mean contorn
	if (numSegm < 1)
	{
		if (numSegm == 0)
		{
			meanSegmentats[0] = centerElipse;
		}
		numSegm++;

	}
	else
	{
		if (normCvPoint2f(meanSegmentats[0] - centerElipse) < DISTANCIACENTREELIPSE)
		{
			meanSegmentats[0] = centerElipse;
		}
	}
}

bool CRegistreElipseDlg::InitializeVideoRecording()
{
	//Setup Video Size (Activate desired size or define a new one)
	VideoSize = ueCamera->GetImageSize();   //Resolution from ueCamera

											//VideoSize = cvSize(1920,1080);          //Resolution for 1080p (Full HD)
											//VideoSize = cvSize(1440,1080);          //Resolution for 1080i (HDV)

											//Setup Video frame rate (FPS) (Activate desired FPS or define a new one)
											//VideoFrameRate = ueCamera->GetFPS();    //FPS from ueCamera
	VideoFrameRate = 30.0;
	//VideoFrameRate = 60.0;

	//Video Image initialization (image that will be used to display and record from camera
	VideoImage = cvCreateImage(VideoSize, 8, 1);	//Create original image header and allocate the image data. Function parameters (CvSize size, int depth, int channels)
													//VideoWriter = cvCreateVideoWriter("record.avi", -1, VideoFrameRate, VideoSize);
													//CV_FOURCC('M', 'J', 'P', 'G')
													//Initialize and setup camera acquisition
													//Initialize camera acquisition
	bool ret = ueCamera->InitAcquisition(GetSafeHwnd());
	return ret;
}

void CRegistreElipseDlg::FinalizeVideoRecording()
{
	//Finalize camera acquisition
	ueCamera->FinalizeAcquisition();

	//Finalize and close videos and images
	cvReleaseImage(&VideoImage);
	cvReleaseImage(&VideoImageUI);
	//cvReleaseVideoWriter(&VideoWriter);
}

///////////////////////////////////////////////////////////////////////////////
//
// METHOD CIdsSimpleLiveDlg::OnUEyeMessage() 
//
// DESCRIPTION: - handles the messages from the uEye camera
//				- messages must be enabled using is_EnableMessage()
//
///////////////////////////////////////////////////////////////////////////////
LRESULT CRegistreElipseDlg::OnUEyeMessage(WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
	case IS_DEVICE_REMOVED:
		Beep(400, 50);
		break;
	case IS_DEVICE_RECONNECTED:
		Beep(400, 50);
		break;
	case IS_FRAME:
	{
		break;
	}
	}
	return 0;
}

void CRegistreElipseDlg::OnBnClickedButcamera()
{
	if (bRecording)
	{
		bRecording = false;
		if (bSegmenting)
		{
			bSegmenting = false;
		}
		FinalizeVideoRecording();
		Sleep(100);
		m_butCamera.SetWindowTextA(_T("Start Camera"));
		m_butSegmentation.EnableWindow(FALSE);
		m_butSegmentation.SetWindowTextA(_T("Start Segmentation"));
	}
	else
	{
		bRecording = true;
		InitializeVideoRecording();
		Sleep(100);
		m_butCamera.SetWindowTextA(_T("Stop Camera"));
		m_butSegmentation.EnableWindow(TRUE);
		m_butSegmentation.SetWindowTextA(_T("Start Segmentation"));
		thRecording = AfxBeginThread(ThreadRecording, this, THREAD_PRIORITY_HIGHEST, 0, 0, NULL);	//Initialize thread uEyeCamera
	} 


	////INI DEBUG: MIRAR SEGMENTACIO

	//vector<String> filenames;
	//CString path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\RegistreElipse\\RegistreElipse\\Imatges\\Raw\\";
	//CString nameImage;
	//int nImatges = 100;
	//cv::Mat imageProva = cv::Mat(ueCamera->GetImageSize(), CV_8UC1);
	//std::vector<cv::Mat> imageProva2(nImatges);
	//for (int i = 0; i < nImatges; i++)
	//{
	//	imageProva2[i] = cv::Mat(ueCamera->GetImageSize(), CV_8UC1);
	//}
	//InitializeVideoRecording();
	//Sleep(1000);

	//for (size_t i = 0; i < nImatges; ++i)
	//{

	//	ueCamera->GetImage(imageProva.data);
	//	Sleep(10);
	//	imageProva2[i] = imageProva.clone();
	//	Sleep(10);

	//}

	//for (int i = 0; i < nImatges; i++)
	//{

	//	if (i < 9)
	//		nameImage.Format("Imatge_00%d.png", i + 1);
	//	else if (i < 99)
	//		nameImage.Format("Imatge_0%d.png", i + 1);
	//	else
	//		nameImage.Format("Imatge_%d.png", i + 1);
	//	nameImage = path + nameImage;
	//	imwrite(nameImage.GetString(), imageProva2[i]);
	//}

	//FinalizeVideoRecording();
	//Sleep(100);




	//CString path = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\RegistreElipse\\RegistreElipse\\Imatges\\Raw\\";
	//vector<String> filenames;
	//CString nameImage;
	//CString pathD = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\RegistreElipse\\RegistreElipse\\Imatges\\Process\\";
	//int a1, a2;
	//glob(path.GetString(), filenames);
	//std::vector<cv::Point2i> auxPointsContorn;

	////mean Segmentacio
	//numSegm = 0;
	//meanSegmentats.clear();
	//meanSegmentats.resize(PUNTSCONTORNIMATGE2D);
	//for (int i = 0; i < PUNTSCONTORNIMATGE2D; i++)
	//{
	//	meanSegmentats[i].x = 0.0f;
	//	meanSegmentats[i].y = 0.0f;
	//}

	//for (size_t i = 0; i < filenames.size(); ++i)
	//{
	//	cv::Mat src = imread(filenames[i], CV_LOAD_IMAGE_GRAYSCALE);
	//	cv::Mat src2;
	//	uint8_t *imData; 

	//	BuscarEllipse(src, a1, a2, src2);
	//	SegmentacioImatge(src, src2);
	//	
	//	trobarPuntsElipse(src2, auxPointsContorn, PUNTSCONTORNIMATGE2D, a2, a1);
	//	
	//	cv::cvtColor(src2, src2, cv::COLOR_GRAY2BGR);

	//	imData = src2.data;
	//	for (int ii = -2; ii < 3; ii++)
	//		for (int jj = -2; jj < 3; jj++)
	//		{
	//			imData[((a2 + jj) * src2.cols + a1 + ii) * 3] = 0;
	//			imData[((a2 + jj) * src2.cols + a1 + ii) * 3 + 1] = 0;
	//			imData[((a2 + jj) * src2.cols + a1 + ii) * 3 + 2] = 255;
	//		}

		//
		//for (int kk = 0; kk < auxPointsContorn.size(); kk++)
		//{
		//	if ((auxPointsContorn[kk].x == 0) || (auxPointsContorn[kk].y == 0))
		//		continue;
		//	for (int ii = -1; ii < 2; ii++)
		//		for (int jj = -1; jj < 2; jj++)
		//		{
		//			imData[((auxPointsContorn[kk].y + jj) * src2.cols + auxPointsContorn[kk].x + ii) * 3] = 255;
		//			imData[((auxPointsContorn[kk].y + jj) * src2.cols + auxPointsContorn[kk].x + ii) * 3 + 1] = 0;
		//			imData[((auxPointsContorn[kk].y + jj) * src2.cols + auxPointsContorn[kk].x + ii) * 3 + 2] = 0;
		//		}
		//}

		//if (i < 9)
		//	nameImage.Format("Imatge_00%d.png", i + 1);
		//else if (i < 99)
		//	nameImage.Format("Imatge_0%d.png", i + 1);
		//else
		//	nameImage.Format("Imatge_%d.png", i + 1);
		//nameImage = pathD + nameImage;
		//imwrite(nameImage.GetString(), src2);

	//}

	////END DEBU: MIRAR SEGMENTACIO


}


void CRegistreElipseDlg::OnBnClickedButsegmen()
{
	if (bSegmenting)
	{
		bSegmenting = false;
		m_butSegmentation.SetWindowTextA(_T("Start Segmentation"));

		//mean Segmentaci�
		numSegm = 0;
	}
	else
	{
		//mean Segmentacio
		numSegm = 0;
		meanSegmentats.clear();
		meanSegmentats.resize(PUNTSCONTORNIMATGE2D);
		for (int i = 0; i < PUNTSCONTORNIMATGE2D; i++)
		{
			meanSegmentats[i].x = 0.0f;
			meanSegmentats[i].y = 0.0f;
		}

		bSegmenting = true;
		m_butSegmentation.SetWindowTextA(_T("Stop Segmentation"));
	}
}


void CRegistreElipseDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	if (bRecording)
	{
		ueCamera->FinalizeAcquisition();
		bRecording = false;
		//Whait until thread is dead
		Sleep(30);
		//Finalize and close video
		cvReleaseImage(&VideoImage);
	}

	if (bSegmenting)
	{
		bSegmenting = false;
		Sleep(100);
	}

	//Close and Delete uEyeCam

	ueCamera->FinalizeAcquisition();
	ueCamera->ExitCamera();
	delete(ueCamera);
}


void CRegistreElipseDlg::OnBnClickedButprova1()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	

}
