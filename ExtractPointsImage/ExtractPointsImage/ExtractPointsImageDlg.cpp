
// ExtractPointsImageDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "ExtractPointsImage.h"
#include "ExtractPointsImageDlg.h"
#include "afxdialogex.h"
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExtractPointsImageDlg dialog



CExtractPointsImageDlg::CExtractPointsImageDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_EXTRACTPOINTSIMAGE_DIALOG, pParent)
	, m_nsamples(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CExtractPointsImageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_INFO, m_stext_info);
	DDX_Text(pDX, IDC_EDIT1, m_nsamples);
	DDX_Control(pDX, IDC_IMG, m_picImage);
}

BEGIN_MESSAGE_MAP(CExtractPointsImageDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CExtractPointsImageDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CExtractPointsImageDlg::OnBnClickedButton2)
	ON_WM_LBUTTONUP()
//	ON_STN_CLICKED(IDC_IMG, &CExtractPointsImageDlg::OnClickedImg)
ON_BN_CLICKED(IDC_BUTTON5, &CExtractPointsImageDlg::OnBnClickedButton5)
END_MESSAGE_MAP()

// CExtractPointsImageDlg message handlers

BOOL CExtractPointsImageDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	RECT r;
	m_picImage.GetClientRect(&r);
	winSegSizeProc = cv::Size(r.right, r.bottom);
	if (winSegSizeProc.width % 4 != 0)
	{
		winSegSizeProc.width = 4 * (winSegSizeProc.width / 4);
	}
	cvImgTmpProc = cv::Mat(winSegSizeProc, CV_8UC3);
	bitInfoProc.bmiHeader.biBitCount = 24;
	bitInfoProc.bmiHeader.biWidth = winSegSizeProc.width;
	bitInfoProc.bmiHeader.biHeight = winSegSizeProc.height;
	bitInfoProc.bmiHeader.biPlanes = 1;
	bitInfoProc.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfoProc.bmiHeader.biCompression = BI_RGB;
	bitInfoProc.bmiHeader.biClrImportant = 0;
	bitInfoProc.bmiHeader.biClrUsed = 0;
	bitInfoProc.bmiHeader.biSizeImage = 0;
	bitInfoProc.bmiHeader.biXPelsPerMeter = 0;
	bitInfoProc.bmiHeader.biYPelsPerMeter = 0;

	m_picImage.GetClientRect(&r);
	winSegSizeIni = cv::Size(r.right, r.bottom);
	if (winSegSizeIni.width % 4 != 0)
	{
		winSegSizeIni.width = 4 * (winSegSizeIni.width / 4);
	}
	cvImgTmpIni = cv::Mat(winSegSizeIni, CV_8UC3);
	bitInfoIni.bmiHeader.biBitCount = 24;
	bitInfoIni.bmiHeader.biWidth = winSegSizeIni.width;
	bitInfoIni.bmiHeader.biHeight = winSegSizeIni.height;
	bitInfoIni.bmiHeader.biPlanes = 1;
	bitInfoIni.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfoIni.bmiHeader.biCompression = BI_RGB;
	bitInfoIni.bmiHeader.biClrImportant = 0;
	bitInfoIni.bmiHeader.biClrUsed = 0;
	bitInfoIni.bmiHeader.biSizeImage = 0;
	bitInfoIni.bmiHeader.biXPelsPerMeter = 0;
	bitInfoIni.bmiHeader.biYPelsPerMeter = 0;

	m_nsamples = 20;
	bgetPoints = false;

	UpdateData(false);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CExtractPointsImageDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CExtractPointsImageDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CExtractPointsImageDlg::SetImage(cv::Mat & entrada)
{
	cv::Mat auxiliar;
	entrada.copyTo(auxiliar);

	//If size does not match
	if (auxiliar.size() != winSegSizeIni)
	{
		cv::resize(auxiliar, cvImgTmpIni, winSegSizeIni);
	}
	else
	{
		cvImgTmpIni = auxiliar.clone();
	}


	//Rotate image
	cv::flip(cvImgTmpIni, cvImgTmpIni, 0);


	//Create MFC image
	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}

	mfcImg = new CImage();
	mfcImg->Create(winSegSizeIni.width, winSegSizeIni.height, 24);


	//Add header and OpenCV image to mfcImg
	StretchDIBits(mfcImg->GetDC(), 0, 0,
		winSegSizeIni.width, winSegSizeIni.height, 0, 0,
		winSegSizeIni.width, winSegSizeIni.height,
		cvImgTmpIni.data, &bitInfoIni, DIB_RGB_COLORS, SRCCOPY
	);

	//Display mfcImg in MFC window
	mfcImg->BitBlt(::GetDC(m_picImage.m_hWnd), 0, 0);
}



void CExtractPointsImageDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	CString path_image;
	CFileDialog dlg(TRUE);
	/*dlg.m_ofn.nMaxFile = 511;

	dlg.m_ofn.lpstrFilter = _T("PNG Files (*.png)\0*.png\0All Files (*.*)\0*.*\0\0");

	dlg.m_ofn.lpstrTitle = _T("Open image File");*/
	if (dlg.DoModal() == IDOK)
		path_image = dlg.GetPathName();


	std::string STDStr(CW2A(path_image.GetString()));
	image = cv::imread(STDStr, cv::IMREAD_COLOR);
	image.copyTo(imageIni);

	SetImage(image);
	imageName = path_image;
	imageName.Delete(imageName.GetLength() - 3, 3);
	imageName += CString(_T("txt"));
}


void CExtractPointsImageDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
	if (image.empty())
	{
		return;
	}

	UpdateData(true);

	char strFilter[] = { "Text Files (*.txt)|*.txt|" };
	CFileDialog FileDlg(FALSE, CString(".txt"), imageName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, CString(strFilter));
	

	if (FileDlg.DoModal() == IDOK)
	{
		path_file = FileDlg.GetPathName();
	}
	else
	{
		return;
	}

	

	
	CString strTextPoint;
	strText.resize(0);
	strTextPoint.Format(_T("Number of samples: %d\nInside:\nRow\tColumn\tRed\tGreen\tBlue\n"), m_nsamples);
	strText.push_back(strTextPoint);
	//filePoints.WriteString(strText);
	bgetPoints = true;
	nIn = 0;
	nContorn = 0;
	nOut = 0;
	nTool = 0;
	points.resize(0);

	CString message;
	message.Format(_T("Get an Inside Point!!!\n\nInside Points taken: %d"), nIn);
	m_stext_info.SetWindowTextW(message);

	UpdateData(false);

}


void CExtractPointsImageDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (bgetPoints)
	{
		int x = point.x - 11;
		int y = point.y - 11;
		

		RECT r;
		m_picImage.GetClientRect(&r);
		winSegSizeProc = cv::Size(r.right, r.bottom);


		if (x < 0)
		{
			return;
		}
		if (y < 0)
		{
			return;
		}
		if (x >= r.right)
		{
			return;
		}
		if (y >= r.bottom)
		{
			return;
		}

		int ncols = image.cols;
		int nrows = image.rows;

		int px = x * ncols / r.right;
		int py = y * nrows / r.bottom;
		points.push_back(cv::Point(px,py));

		uint8_t *imDataIni = imageIni.data;
		uint8_t *imData = image.data;
		int red, green, blue;


		blue = imDataIni[(py * image.cols + px) * 3];
		green = imDataIni[(py * image.cols + px) * 3 + 1];
		red = imDataIni[(py * image.cols + px) * 3 + 2];


		CString strTextPoint;
		strTextPoint.Format(_T("%d\t%d\t%d\t%d\t%d\n"), py, px, red, green, blue);
		strText.push_back(strTextPoint);

		//filePoints.WriteString(strText);

		if (nIn < m_nsamples)
		{
			for (int ii = -4; ii < 5; ii++)
			{
				for (int jj = -4; jj < 5; jj++)
				{
					if ((px + jj >= image.cols) || (py + ii >= image.rows) || (px + jj < 0) || (py + ii < 0))
					{
						continue;
					}
					imData[((py + jj) * image.cols + (px + ii)) * 3] = 255;
					imData[((py + jj) * image.cols + (px + ii)) * 3 + 1] = 255;
					imData[((py + jj) * image.cols + (px + ii)) * 3 + 1] = 0;
				}
			}
			nIn++;
			if (nIn < m_nsamples)
			{
				CString message;
				message.Format(_T("Get an Inside Point!!!\n\nInside Points taken: %d"), nIn);
				m_stext_info.SetWindowTextW(message);
			}
			else
			{
				CString message;
				message.Format(_T("Get an Contour Point!!!\n\nInside Points taken: %d\n\nContour Points taken: %d"), nIn, nContorn);
				m_stext_info.SetWindowTextW(message);

				strTextPoint.Format(_T("Contour:\nRow\tColumn\tRed\tGreen\tBlue\n"), m_nsamples);
				strText.push_back(strTextPoint);
				//filePoints.WriteString(strText);
			}
		}
		else if (nContorn < m_nsamples)
		{
			for (int ii = -4; ii < 5; ii++)
			{
				for (int jj = -4; jj < 5; jj++)
				{
					if ((px + jj >= image.cols) || (py + ii >= image.rows) || (px + jj < 0) || (py + ii < 0))
					{
						continue;
					}
					imData[((py + jj) * image.cols + (px + ii)) * 3] = 0;
					imData[((py + jj) * image.cols + (px + ii)) * 3 + 1] = 0;
					imData[((py + jj) * image.cols + (px + ii)) * 3 + 1] = 255;
				}
			}
			nContorn++;
			if (nContorn < m_nsamples)
			{
				CString message;
				message.Format(_T("Get an Contour Point!!!\n\nInside Points taken: %d\nContour Points taken: %d"), nIn, nContorn);
				m_stext_info.SetWindowTextW(message);
			}
			else
			{
				CString message;
				message.Format(_T("Get a Outside Point!!!\n\nInside Points taken: %d\n\nContour Points taken: %d\n\nOutside Points taken: %d"), nIn, nContorn, nOut);
				m_stext_info.SetWindowTextW(message);

				strTextPoint.Format(_T("Outside:\nRow\tColumn\tRed\tGreen\tBlue\n"), m_nsamples);
				strText.push_back(strTextPoint);
				//strText.Format(_T("Tool:\nRed\tGreen\tBlue\n"), m_nsamples);
				//filePoints.WriteString(strText);
			}
		}
		else if (nOut < m_nsamples)
		{
			for (int ii = -4; ii < 5; ii++)
			{
				for (int jj = -4; jj < 5; jj++)
				{
					if ((px + jj >= image.cols) || (py + ii >= image.rows) || (px + jj < 0) || (py + ii < 0))
					{
						continue;
					}
					imData[((py + jj) * image.cols + (px + ii)) * 3] = 255;
					imData[((py + jj) * image.cols + (px + ii)) * 3 + 1] = 255;
					imData[((py + jj) * image.cols + (px + ii)) * 3 + 1] = 255;
				}
			}
			nOut++;
			if (nOut < m_nsamples)
			{
				CString message;
				message.Format(_T("Get an Outside Point!!!\n\nInside Points taken: %d\n\nContour Points taken: %d\n\nOutside Points taken: %d"), nIn, nContorn, nOut);
				m_stext_info.SetWindowTextW(message);
			}
			else
			{
				CString message;
				message.Format(_T("Get a Tool Point!!!\n\nInside Points taken: %d\n\nContour Points taken: %d\n\nOutside Points taken: %d\n\nTool Points taken: %d"), nIn, nContorn, nOut, nTool);
				m_stext_info.SetWindowTextW(message);

				strTextPoint.Format(_T("Tool:\nRow\tColumn\tRed\tGreen\tBlue\n"), m_nsamples);
				strText.push_back(strTextPoint);
				//strText.Format(_T("Tool:\nRed\tGreen\tBlue\n"), m_nsamples);
				//filePoints.WriteString(strText);
			}
		}
		else if (nTool < m_nsamples)
		{
			for (int ii = -4; ii < 5; ii++)
			{
				for (int jj = -4; jj < 5; jj++)
				{
					if ((px + jj >= image.cols) || (py + ii >= image.rows) || (px + jj < 0) || (py + ii < 0))
					{
						continue;
					}
					imData[((py + jj) * image.cols + (px + ii)) * 3] = 0;
					imData[((py + jj) * image.cols + (px + ii)) * 3 + 1] = 165;
					imData[((py + jj) * image.cols + (px + ii)) * 3 + 1] = 255;
				}
			}
			nTool++;
			if (nTool < m_nsamples)
			{
				CString message;
				message.Format(_T("Get a Tool Point!!!\n\nInside Points taken: %d\n\nContour Points taken: %d\n\nOutside Points taken: %d\n\nTool Points taken: %d"), nIn, nContorn, nOut, nTool);
				m_stext_info.SetWindowTextW(message);
			}
			else
			{
				CString message;
				message.Format(_T("Got all points!!!\n\nInside Points taken: %d\n\nContour Points taken: %d\n\nOutside Points taken: %d\n\nTool Points taken: %d"), nIn, nContorn, nOut, nTool);
				m_stext_info.SetWindowTextW(message);
				
				filePoints.Open(path_file, CFile::modeCreate | CFile::modeWrite);
				for (int i = 0; i < strText.size(); i++)
				{
					filePoints.WriteString(strText[i]);
				}
				filePoints.Close();
				bgetPoints = false;
			}
		}
		else
		{
			bgetPoints = false;
		}

		SetImage(image);
		UpdateData(false);
	}


	CDialogEx::OnLButtonUp(nFlags, point);
}


//void CExtractPointsImageDlg::OnClickedImg()
//{
//	// TODO: Add your control notification handler code here
//}


void CExtractPointsImageDlg::OnBnClickedButton5()
{
	// TODO: Add your control notification handler code here
	if ((bgetPoints))
	{
		if (nTool > 0)
		{
			strText.pop_back();
			points.pop_back();
			nTool--;
			CString message;
			message.Format(_T("Get a Tool Point!!!\n\nInside Points taken: %d\n\nContour Points taken: %d\n\nOutside Points taken: %d\n\nTool Points taken: %d"), nIn, nContorn, nOut, nTool);
			m_stext_info.SetWindowTextW(message);

			imageIni.copyTo(image);
			uint8_t *imData = image.data;
			for (int i = 0; i < points.size(); i++)
			{
				if (i < m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
						}
					}
				}
				else if (i < 2*m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
				else if (i < 3*m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
				else if (i < 4 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 165;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
			}
			SetImage(image);
		}
		else if (nOut > 0)
		{
			if (nOut >= m_nsamples)
			{
				strText.pop_back();
			}
			strText.pop_back();
			points.pop_back();
			nOut--;
			CString message;
			message.Format(_T("Get an Outside Point!!!\n\nInside Points taken: %d\n\nContour Points taken: %d\n\nOutside Points taken: %d\n\n"), nIn, nContorn, nOut);
			m_stext_info.SetWindowTextW(message);

			imageIni.copyTo(image);
			uint8_t *imData = image.data;
			for (int i = 0; i < points.size(); i++)
			{
				if (i < m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
						}
					}
				}
				else if (i < 2 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
				else if (i < 3 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
				else if (i < 4 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
			}
			SetImage(image);
		}
		else if (nContorn > 0)
		{
			if (nContorn >= m_nsamples)
			{
				strText.pop_back();
			}
			strText.pop_back();
			points.pop_back();
			nContorn--;
			CString message;
			message.Format(_T("Get an Contour Point!!!\n\nInside Points taken: %d\n\nOutside Points taken: %d"), nIn, nContorn);
			m_stext_info.SetWindowTextW(message);

			imageIni.copyTo(image);
			uint8_t *imData = image.data;
			for (int i = 0; i < points.size(); i++)
			{
				if (i < m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
						}
					}
				}
				else if (i < 2 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
				else if (i < 3 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
				else if (i < 4 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
			}
			SetImage(image);
		}
		else if (nIn > 0)
		{
			if (nIn >= m_nsamples)
			{
				strText.pop_back();
			}
			strText.pop_back();
			points.pop_back();
			nIn--;
			CString message;
			message.Format(_T("Get an Inside Point!!!\n\nInside Points taken: %d\n\n"), nIn, nOut, nTool);
			m_stext_info.SetWindowTextW(message);

			imageIni.copyTo(image);
			uint8_t *imData = image.data;
			for (int i = 0; i < points.size(); i++)
			{
				if (i < m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
						}
					}
				}
				else if (i < 2 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
				else if (i < 3 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
				else if (i < 4 * m_nsamples)
				{
					for (int ii = -4; ii < 5; ii++)
					{
						for (int jj = -4; jj < 5; jj++)
						{
							if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
							{
								continue;
							}
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
							imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						}
					}
				}
			}
			SetImage(image);
		}
	}
	else if (nTool > 0)
	{
		bgetPoints = true;
		strText.pop_back();
		points.pop_back();
		nTool--;
		CString message;
		message.Format(_T("Get a Tool Point!!!\n\nInside Points taken: %d\n\nContour Points taken: %d\n\nOutside Points taken: %d\n\nTool Points taken: %d"), nIn, nContorn, nOut, nTool);
		m_stext_info.SetWindowTextW(message);

		imageIni.copyTo(image);
		uint8_t *imData = image.data;
		for (int i = 0; i < points.size(); i++)
		{
			if (i < m_nsamples)
			{
				for (int ii = -4; ii < 5; ii++)
				{
					for (int jj = -4; jj < 5; jj++)
					{
						if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
						{
							continue;
						}
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
					}
				}
			}
			else if (i < 2 * m_nsamples)
			{
				for (int ii = -4; ii < 5; ii++)
				{
					for (int jj = -4; jj < 5; jj++)
					{
						if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
						{
							continue;
						}
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 0;
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
					}
				}
			}
			else if (i < 3 * m_nsamples)
			{
				for (int ii = -4; ii < 5; ii++)
				{
					for (int jj = -4; jj < 5; jj++)
					{
						if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
						{
							continue;
						}
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
					}
				}
			}
			else if (i < 4 * m_nsamples)
			{
				for (int ii = -4; ii < 5; ii++)
				{
					for (int jj = -4; jj < 5; jj++)
					{
						if ((points[i].x + jj >= image.cols) || (points[i].y + ii >= image.rows) || (points[i].x + jj < 0) || (points[i].y + ii < 0))
						{
							continue;
						}
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3] = 255;
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 0;
						imData[((points[i].y + jj) * image.cols + (points[i].x + ii)) * 3 + 1] = 255;
					}
				}
			}
		}
		SetImage(image);
	}

}
