
// ExtractPointsImageDlg.h : header file
//

#pragma once


// CExtractPointsImageDlg dialog
class CExtractPointsImageDlg : public CDialogEx
{
// Construction
public:
	CExtractPointsImageDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EXTRACTPOINTSIMAGE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CButton m_stext_info;
	int m_nsamples;
	CStatic m_picImage;
	bool bgetPoints;
	int nIn, nOut, nTool, nContorn;

	cv::Size winSegSizeIni, winSegSizeProc;
	cv::Mat cvImgTmpIni, cvImgTmpProc;
	CImage* mfcImg;
	BITMAPINFO bitInfoIni, bitInfoProc;

	cv::Mat imageIni, image;

	CStdioFile filePoints;
	CString path_file;
	std::vector<CString> strText;
	std::vector<cv::Point> points;

	CString imageName;

	void SetImage(cv::Mat & entrada);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
//	afx_msg void OnClickedImg();
	afx_msg void OnBnClickedButton5();
};
