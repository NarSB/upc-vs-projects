
// TrackingHerniaNoCudaDlg.cpp: archivo de implementación
//

#include "pch.h"
#include "framework.h"
#include "TrackingHerniaNoCuda.h"
#include "TrackingHerniaNoCudaDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//Global Inlines
inline int modval(int a, int b) { return (a % b + b) % b; }

inline int evaluarMat(uint8_t * matrix, int r, int c, int ncols)
{
	if ((r < 0) || (c < 0) || (c >= ncols))
		return -1;
	return matrix[r * ncols + c];
}

inline float normDistancia(float x1, float y1, float x2, float y2)
{
	return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
}

inline float normCvPoint2f(cv::Point2f P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint2i(cv::Point2i P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint3f(cv::Point3f P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline float normCvPoint3i(cv::Point3i P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline int float2int(float x)
{
	return (int)(x + TOLERANCIA);
}

inline float FuncioSigne(float x)
{
	if (std::abs(x) < TOLERANCIA)
		return 0.0;
	else if (x > 0)
		return 1.0;
	else
		return -1.0;
}

inline void GetElapsedMiliseconds(_LARGE_INTEGER &ElapsedMiliseconds, _LARGE_INTEGER StartingTime, _LARGE_INTEGER Frequency)
{
	_LARGE_INTEGER Time;
	QueryPerformanceCounter(&Time);
	ElapsedMiliseconds.QuadPart = Time.QuadPart - StartingTime.QuadPart;
	ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
	ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
}

inline double EvaluateEllipse(ellipseParams & ellipse, cv::Point & point)
{
	return ellipse.a*point.x*point.x + ellipse.b*point.x*point.y + ellipse.c*point.y*point.y + ellipse.d*point.x + ellipse.e*point.y + ellipse.f;
}

inline double EvaluateEllipse(ellipseParams & ellipse, cv::Point2d & point)
{
	return ellipse.a*point.x*point.x + ellipse.b*point.x*point.y + ellipse.c*point.y*point.y + ellipse.d*point.x + ellipse.e*point.y + ellipse.f;
}

inline double SampsonDistance(ellipseParams & ellipse, cv::Point & point)
{
	return abs((double)ellipse.a*point.x*point.x + ellipse.b*point.x*point.y + ellipse.c*point.y*point.y + ellipse.d*point.x + ellipse.e*point.y + ellipse.f) / 2.0 / sqrt((ellipse.a*point.x + ellipse.b*point.y / 2.0 + ellipse.d / 2.0)*(ellipse.a*point.x + ellipse.b*point.y / 2.0 + ellipse.d / 2.0) + (ellipse.b*point.x / 2.0 + ellipse.c*point.y + ellipse.e / 2.0)*(ellipse.b*point.x / 2.0 + ellipse.c*point.y + ellipse.e / 2.0));
}

inline double SampsonDistance(ellipseParams & ellipse, cv::Point2d & point)
{
	return abs(ellipse.a*point.x*point.x + ellipse.b*point.x*point.y + ellipse.c*point.y*point.y + ellipse.d*point.x + ellipse.e*point.y + ellipse.f) / 2.0 / sqrt((ellipse.a*point.x + ellipse.b*point.y / 2.0 + ellipse.d / 2.0)*(ellipse.a*point.x + ellipse.b*point.y / 2.0 + ellipse.d / 2.0) + (ellipse.b*point.x / 2.0 + ellipse.c*point.y + ellipse.e / 2.0)*(ellipse.b*point.x / 2.0 + ellipse.c*point.y + ellipse.e / 2.0));
}

inline cv::Point2d GetCenterEllipse(ellipseParams & ellipse)
{
	cv::Point2d res;
	res.x = (2.0*ellipse.c*ellipse.d - ellipse.b*ellipse.e) / (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);
	res.y = (2.0*ellipse.a*ellipse.e - ellipse.b*ellipse.d) / (ellipse.b*ellipse.b - 4.0*ellipse.a*ellipse.c);
	return res;
}

inline cv::Point2d GetSemiAxesEllipse(ellipseParams & ellipseP)
{
	cv::Point2d res;
	res.x = -sqrt(2.0*(ellipseP.a*ellipseP.e*ellipseP.e + ellipseP.c*ellipseP.d*ellipseP.d - ellipseP.b*ellipseP.d*ellipseP.e + (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c)*ellipseP.f)*((ellipseP.a + ellipseP.c) + sqrt((ellipseP.a - ellipseP.c)*(ellipseP.a - ellipseP.c) + ellipseP.b*ellipseP.b)));
	res.x /= (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c);
	res.y = -sqrt(2.0*(ellipseP.a*ellipseP.e*ellipseP.e + ellipseP.c*ellipseP.d*ellipseP.d - ellipseP.b*ellipseP.d*ellipseP.e + (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c)*ellipseP.f)*((ellipseP.a + ellipseP.c) - sqrt((ellipseP.a - ellipseP.c)*(ellipseP.a - ellipseP.c) + ellipseP.b*ellipseP.b)));
	res.y /= (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c);
	return res;
}

inline double GetAreaEllipse(ellipseParams & ellipse)
{
	cv::Point2d axis = GetSemiAxesEllipse(ellipse);
	return M_PI * axis.x*axis.y;
}

double test(Eigen::VectorXd &x, Eigen::VectorXd &fvec, int nPoints, std::vector<cv::Point2i> &points, std::vector<int> &indOut)
{
	Eigen::Matrix2Xd PointsElli = Eigen::Matrix2Xd(2, nPoints);
	for (int i = 0; i < nPoints; i++)
	{
		PointsElli(0, i) = (double)(points[i].x);
		PointsElli(1, i) = (double)(points[i].y);
	}
	fvec(0) = 0.0;
	std::vector<distStruct> distV(0);
	for (int i = 0; i < nPoints; i++)
	{
		if (abs(PointsElli(0, i)) < 0.00001)
		{
			continue;
		}
		double s = abs(PointsElli(0, i)*PointsElli(0, i) + x(0)*PointsElli(0, i)*PointsElli(1, i) + x(1)*PointsElli(1, i)*PointsElli(1, i) + x(2)*PointsElli(0, i) + x(3)*PointsElli(1, i) + x(4)) / 2.0 / sqrt((PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0)*(PointsElli(0, i) + x(0)*PointsElli(1, i) / 2.0 + x(2) / 2.0) + (x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0)*(x(0)*PointsElli(0, i) / 2.0 + x(1)*PointsElli(1, i) + x(3) / 2.0));
		distStruct ds;
		ds.ind = i;
		ds.dist = s;
		distV.push_back(ds);

	}
	std::sort(distV.begin(), distV.end(), [](distStruct a, distStruct b) { return a.dist < b.dist; });
	int indL = min(nPoints*PERCENTAGEPOINTSOULIERS, 0.9*distV.size());
	for (int i = 0; i < indL; i++)
	{
		double s = distV[i].dist;
		///x(0)*PointsElli(0, i)*PointsElli(0, i) + x(1)*PointsElli(0, i)*PointsElli(1, i) + x(2)*PointsElli(1, i)*PointsElli(1, i) + x(3)*PointsElli(0, i) + x(4)*PointsElli(1, i) + x(5);
		if (s < ELLIPSETHRESHOLD)
		{
			fvec(0) += s * s / 2.0;
		}
		else
		{
			fvec(0) += ELLIPSETHRESHOLD * s - ELLIPSETHRESHOLD * ELLIPSETHRESHOLD / 2.0;
		}
	}
	indOut.resize(0);
	for (int i = indL; i < distV.size(); i++)
	{
		indOut.push_back(distV[i].ind);
	}
	double disc = (x(0)*x(0) - 4.0*x(1));
	for (int i = 1; i < nPoints; i++)
	{
		if (disc < 0.0)
		{
			fvec(i) = 0.0;
		}
		else if (disc < 1.0)
		{
			fvec(i) = 100000.0 + 1000.0*disc;
		}
		else
		{
			fvec(i) = 100000.0 + 1000.0*disc*disc*disc;
		}
	}
	return fvec(0);
}


// Cuadro de diálogo de CTrackingHerniaNoCudaDlg



CTrackingHerniaNoCudaDlg::CTrackingHerniaNoCudaDlg(CWnd* pParent /*= nullptr*/)
	: CDialog(IDD_TRACKINGHERNIANOCUDA_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTrackingHerniaNoCudaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMGINITIAL, m_picImageInitial);
	DDX_Control(pDX, IDC_IMGPROC, m_picImageProc);
	DDX_Control(pDX, IDC_PROC, m_butProc);
}

BEGIN_MESSAGE_MAP(CTrackingHerniaNoCudaDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON2, &CTrackingHerniaNoCudaDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDOK, &CTrackingHerniaNoCudaDlg::OnBnClickedOk)
	ON_BN_CLICKED(BTN_GetPointsSynth, &CTrackingHerniaNoCudaDlg::OnBnClickedGetpointssynth)
	ON_BN_CLICKED(BN_Synthtic, &CTrackingHerniaNoCudaDlg::OnBnClickedSynthtic)
	ON_BN_CLICKED(IDC_PROC, &CTrackingHerniaNoCudaDlg::OnBnClickedProc)
END_MESSAGE_MAP()


// Controladores de mensajes de CTrackingHerniaNoCudaDlg

BOOL CTrackingHerniaNoCudaDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Establecer el icono para este cuadro de diálogo.  El marco de trabajo realiza esta operación
	//  automáticamente cuando la ventana principal de la aplicación no es un cuadro de diálogo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono pequeño

	// TODO: agregar aquí inicialización adicional

	RECT r;
	m_picImageProc.GetClientRect(&r);
	winSegSizeProc = cv::Size(r.right, r.bottom);
	if (winSegSizeProc.width % 4 != 0)
	{
		winSegSizeProc.width = 4 * (winSegSizeProc.width / 4);
	}
	cvImgTmpProc = cv::Mat(winSegSizeProc, CV_8UC3);
	bitInfoProc.bmiHeader.biBitCount = 24;
	bitInfoProc.bmiHeader.biWidth = winSegSizeProc.width;
	bitInfoProc.bmiHeader.biHeight = winSegSizeProc.height;
	bitInfoProc.bmiHeader.biPlanes = 1;
	bitInfoProc.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfoProc.bmiHeader.biCompression = BI_RGB;
	bitInfoProc.bmiHeader.biClrImportant = 0;
	bitInfoProc.bmiHeader.biClrUsed = 0;
	bitInfoProc.bmiHeader.biSizeImage = 0;
	bitInfoProc.bmiHeader.biXPelsPerMeter = 0;
	bitInfoProc.bmiHeader.biYPelsPerMeter = 0;

	m_picImageInitial.GetClientRect(&r);
	winSegSizeIni = cv::Size(r.right, r.bottom);
	if (winSegSizeIni.width % 4 != 0)
	{
		winSegSizeIni.width = 4 * (winSegSizeIni.width / 4);
	}
	cvImgTmpIni = cv::Mat(winSegSizeIni, CV_8UC3);
	bitInfoIni.bmiHeader.biBitCount = 24;
	bitInfoIni.bmiHeader.biWidth = winSegSizeIni.width;
	bitInfoIni.bmiHeader.biHeight = winSegSizeIni.height;
	bitInfoIni.bmiHeader.biPlanes = 1;
	bitInfoIni.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfoIni.bmiHeader.biCompression = BI_RGB;
	bitInfoIni.bmiHeader.biClrImportant = 0;
	bitInfoIni.bmiHeader.biClrUsed = 0;
	bitInfoIni.bmiHeader.biSizeImage = 0;
	bitInfoIni.bmiHeader.biXPelsPerMeter = 0;
	bitInfoIni.bmiHeader.biYPelsPerMeter = 0;

	squareKernel = getStructuringElement(MORPH_RECT, Size(3, 3), Point(-1, -1));
	datacross[0][0] = 1; datacross[0][1] = 0; datacross[0][2] = 1; datacross[1][0] = 0; datacross[1][1] = 1; datacross[1][2] = 0; datacross[2][0] = 1; datacross[2][1] = 0; datacross[2][2] = 1;
	crossKernel = cv::Mat(3, 3, CV_8U, datacross);
	segerode = getStructuringElement(MORPH_CROSS, Size(3, 3), Point(-1, -1));
	segdilate = getStructuringElement(MORPH_RECT, Size(5, 5), Point(-1, -1));
	segClose = getStructuringElement(MORPH_ELLIPSE, Size(19, 19), Point(-1, -1));

	/*imgPath.Format(_T("G:\\Mi unidad\\Videos Fetal\\videos hernia\\hernia 11\\"));
	outPath.Format(_T("G:\\Mi unidad\\NACompartida\\Fetal\\Tracking\\Hernia\\ImgResults\\hernia 11\\"));*/

	imgPath.Format(_T("G:\\Mi unidad\\Videos Fetal\\videos hernia\\hernia 11\\"));
	//imgPath.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Videos\\SHEEPFRSB170401_2017-12-12_054209_VID001\\"));
	outPath.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Results\\hernia 11\\"));
	//outPath.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Results\\SHEEPFRSB170401_2017-12-12_054209_VID001\\"));


	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un botón Minimizar al cuadro de diálogo, necesitará el siguiente código
//  para dibujar el icono.  Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operación la realiza automáticamente el marco de trabajo.

void CTrackingHerniaNoCudaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rectángulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// El sistema llama a esta función para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CTrackingHerniaNoCudaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CTrackingHerniaNoCudaDlg::SetProcImage(cv::Mat & entrada)
{
	cv::Mat auxiliar;
	//cv::cvtColor(entrada, auxiliar, cv::COLOR_GRAY2BGR);
	auxiliar = entrada.clone();


	//If size does not match
	if (auxiliar.size() != winSegSizeProc)
	{
		cv::resize(auxiliar, cvImgTmpProc, winSegSizeProc);
	}
	else
	{
		cvImgTmpProc = auxiliar.clone();
	}


	//Rotate image
	cv::flip(cvImgTmpProc, cvImgTmpProc, 0);


	//Create MFC image
	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}

	mfcImg = new CImage();
	mfcImg->Create(winSegSizeProc.width, winSegSizeProc.height, 24);


	//Add header and OpenCV image to mfcImg
	StretchDIBits(mfcImg->GetDC(), 0, 0,
		winSegSizeProc.width, winSegSizeProc.height, 0, 0,
		winSegSizeProc.width, winSegSizeProc.height,
		cvImgTmpProc.data, &bitInfoProc, DIB_RGB_COLORS, SRCCOPY
	);

	//Display mfcImg in MFC window
	mfcImg->BitBlt(::GetDC(m_picImageProc.m_hWnd), 0, 0);
}


std::vector<cv::Point> CTrackingHerniaNoCudaDlg::GetPointsRealImageUsingLastEllipse(cv::Mat &image, ellipseParams & ellipse, std::vector<cv::Point> & last_points, int x, int y, int width, int height)
{
	std::vector<cv::Point> res;

	//FALTA FER CAS HI HAGI FORÇA TRASLACIÓ ENTRE FRAMES. MOURE LA ANTINGA ELLIPSE VIA CORRELACIÓ ENTRE IMG ACTUAL I ANTERIOR

	//

	for (int i = 0; i < last_points.size(); i++)
	{
		double d = SampsonDistance(ellipse, last_points[i]);
		if (d < 0.75*ELLIPSETHRESHOLD)
		{
			cv::Point p;
			p.x = last_points[i].x;
			p.y = last_points[i].y;
			res.push_back(p);
		}
	}

	return res;
}

std::vector<cv::Point> CTrackingHerniaNoCudaDlg::ReducePoints(std::vector<cv::Point> & points, ellipseParams & ellipse)
{
	int nPoints = 100;

	//Part de seccions
	std::vector<cv::Point> res(nPoints);
	cv::Point2d meanVal = GetCenterEllipse(ellipse);
	std::vector<float> dmaxP(nPoints);
	cv::Point2f auxP;
	float raux, angaux;
	int indEx;

	for (int i = 0; i < nPoints; i++)
	{
		dmaxP[i] = 10000000.0f;// 100.0f*(cols + rows) * (cols + rows);
	}


	for (int i = 0; i < points.size(); ++i)
	{
		auxP = (cv::Point2d) points[i] - meanVal;
		//raux = auxP.x * auxP.x + auxP.y * auxP.y;
		angaux = atan2(auxP.y, auxP.x);
		indEx = floor((angaux + M_PI) / 2 / M_PI * nPoints);
		if (indEx >= nPoints)
			indEx = 0;

		double dist = abs(lastEllipse.a*points[i].x*points[i].x + lastEllipse.b*points[i].x*points[i].y + lastEllipse.c*points[i].y*points[i].y + lastEllipse.d*points[i].x + lastEllipse.e*points[i].y + lastEllipse.f) / 2.0 / sqrt((lastEllipse.a*points[i].x + lastEllipse.b*points[i].y / 2.0 + lastEllipse.d / 2.0)*(lastEllipse.a*points[i].x + lastEllipse.b*points[i].y / 2.0 + lastEllipse.d / 2.0) + (lastEllipse.b*points[i].x / 2.0 + lastEllipse.c*points[i].y + lastEllipse.e / 2.0)*(lastEllipse.b*points[i].x / 2.0 + lastEllipse.c*points[i].y + lastEllipse.e / 2.0) + 1.0);

		if (dmaxP[indEx] > dist)//if (rmaxP[indEx] < raux)
		{
			//rmaxP[indEx] = raux;
			dmaxP[indEx] = dist;
			(res)[indEx].x = points[i].x;
			(res)[indEx].y = points[i].y;
		}
	}

	return res;
}

ellipseParams CTrackingHerniaNoCudaDlg::GetEllipsePCA(std::vector<cv::Point> & points)
{
	int n = points.size();

	int dolentes = 0;

	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			dolentes++;
		}
	}

	int nbons = n - dolentes;

	double cx, cy;
	cx = 0.0;
	cy = 0.0;
	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			continue;
		}
		cx += (double)points[i].x;
		cy += (double)points[i].y;
	}
	cx /= (double)nbons;
	cy /= (double)nbons;


	Eigen::MatrixXd A = Eigen::MatrixXd::Zero(2, nbons);

	int ii = 0;
	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			continue;
		}

		A(0, ii) = points[i].x - cx;
		A(1, ii) = points[i].y - cy;
		ii++;
	}
	Eigen::Matrix2d B = A * A.transpose();

	Eigen::EigenSolver<Eigen::Matrix2d> eigenSol;
	eigenSol.compute(B);

	double l1 = eigenSol.eigenvalues().x().real();
	double l2 = eigenSol.eigenvalues().y().real();

	double a = sqrt(l1 / M_PI) / 4.0;
	double b = sqrt(l2 / M_PI) / 4.0;

	double ux = eigenSol.eigenvectors().col(0).x().real();
	double uy = eigenSol.eigenvectors().col(0).y().real();

	double vx = eigenSol.eigenvectors().col(1).x().real();
	double vy = eigenSol.eigenvectors().col(1).y().real();

	double alpha = acos(ux / (ux*ux + uy * uy));

	ellipseParams res;

	res.a = cos(alpha)*cos(alpha) / a / a + sin(alpha)*sin(alpha) / b / b;
	res.b = 2.0*cos(alpha)*sin(alpha)*(1 / a / a - 1 / b / b);
	res.c = sin(alpha)*sin(alpha) / a / a + cos(alpha)*cos(alpha) / b / b;
	res.d = 2.0*(-cx * cos(alpha)*cos(alpha) / a / a - cy * cos(alpha)*sin(alpha) / a / a - cx * sin(alpha)*sin(alpha) / b / b + cy * cos(alpha)*sin(alpha) / b / b);
	res.e = 2.0*(-cy * sin(alpha)*sin(alpha) / a / a - cx * cos(alpha)*sin(alpha) / a / a - cy * cos(alpha)*cos(alpha) / b / b + cx * cos(alpha)*sin(alpha) / b / b);
	res.f = 1 / a / a * (cx*cx*cos(alpha)*cos(alpha) + cy * cy*sin(alpha)*sin(alpha) + 2.0*cx*cy*cos(alpha)*sin(alpha)) + 1 / b / b * (cx*cx*sin(alpha)*sin(alpha) + cy * cy*cos(alpha)*cos(alpha) - 2.0*cx*cy*cos(alpha)*sin(alpha)) - 1.0;

	res.b /= res.a;
	res.c /= res.a;
	res.d /= res.a;
	res.e /= res.a;
	res.f /= res.a;
	res.a /= res.a;

	return res;


	//https://math.stackexchange.com/questions/937259/whats-the-parametric-equation-for-the-general-form-of-an-ellipse-rotated-by-any
	//https://www.maa.org/external_archive/joma/Volume8/Kalman/General.html
}

ellipseParams CTrackingHerniaNoCudaDlg::GetEllipseLMS(std::vector<cv::Point> & points)
{
	int n = points.size();
	int dolentes = 0;

	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			dolentes++;
		}
	}

	Eigen::MatrixXd A = Eigen::MatrixXd::Zero(n - dolentes, 5);
	Eigen::VectorXd b = Eigen::VectorXd::Zero(n - dolentes);

	int ii = 0;
	for (int i = 0; i < n; i++)
	{
		if (points[i].x == 0 || points[i].y == 0)
		{
			continue;
		}
		A(ii, 0) = points[i].x*points[i].y;
		A(ii, 1) = points[i].y*points[i].y;
		A(ii, 2) = points[i].x;
		A(ii, 3) = points[i].y;
		A(ii, 4) = 1.0;
		b(ii) = points[i].x*points[i].x;
		ii++;
	}

	Eigen::VectorXd param = (A.transpose()*A).inverse()*A.transpose()*b;

	ellipseParams res;
	res.a = 1.0;
	res.b = -param(0);
	res.c = -param(1);
	res.d = -param(2);
	res.e = -param(3);
	res.f = -param(4);
	return res;
}

ellipseParams CTrackingHerniaNoCudaDlg::GetEllipseMixNorm(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(5);
	x << initial.b, initial.c, initial.d, initial.e, initial.f;
	optimization_ellipse_functor_opt2 functor(points.size(), points);
	Eigen::NumericalDiff<optimization_ellipse_functor_opt2> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor_opt2>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
	TRACE("\n inici calcul Nolineal\n");
	info = lm.minimizeInit(x);
	for (int i = 0; i < NITERATION; i++)
	{
		info = lm.minimizeOneStep(x);
		vNormaOptimit = lm.fnorm;
		TRACE("FINAL OPTIMITZACIÓ: Iteració LM %d, ERROR OPTIMITZACIÓ: %f\n", i, vNormaOptimit);
		if (info != Eigen::LevenbergMarquardtSpace::Running)
			break;
	}
	TRACE("final calcul Nolineal\n");
	TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0), x(1), x(2), x(3), x(4));
	ellipseParams res;
	res.a = 1.0;
	res.b = x(0);
	res.c = x(1);
	res.d = x(2);
	res.e = x(3);
	res.f = x(4);
	return res;
}

ellipseParams CTrackingHerniaNoCudaDlg::GetEllipseMixNormRegression(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(5);
	x << initial.b, initial.c, initial.d, initial.e, initial.f;
	optimization_ellipse_functor_Regression functor(points.size(), points, initial);
	Eigen::NumericalDiff<optimization_ellipse_functor_Regression> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor_Regression>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
	TRACE("\n inici calcul Nolineal\n");
	info = lm.minimizeInit(x);
	for (int i = 0; i < NITERATION; i++)
	{
		info = lm.minimizeOneStep(x);
		vNormaOptimit = lm.fnorm;
		TRACE("FINAL OPTIMITZACIÓ: Iteració LM %d, ERROR OPTIMITZACIÓ: %f\n", i, vNormaOptimit);
		if (info != Eigen::LevenbergMarquardtSpace::Running)
			break;
	}
	TRACE("final calcul Nolineal\n");
	TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0), x(1), x(2), x(3), x(4));
	ellipseParams res;
	res.a = 1.0;
	res.b = x(0);
	res.c = x(1);
	res.d = x(2);
	res.e = x(3);
	res.f = x(4);
	return res;
}

ellipseParams CTrackingHerniaNoCudaDlg::GetEllipseMixNorm2(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(5);
	x << initial.b, initial.c, initial.d, initial.e, initial.f;
	optimization_ellipse_functor2 functor(points.size(), points);
	Eigen::NumericalDiff<optimization_ellipse_functor2> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor2>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
	TRACE("\n inici calcul Nolineal\n");
	info = lm.minimizeInit(x);
	for (int i = 0; i < NITERATION; i++)
	{
		info = lm.minimizeOneStep(x);
		vNormaOptimit = lm.fnorm;
		TRACE("FINAL OPTIMITZACIÓ: Iteració LM %d, ERROR OPTIMITZACIÓ: %f\n", i, vNormaOptimit);
		if (info != Eigen::LevenbergMarquardtSpace::Running)
			break;
	}
	TRACE("final calcul Nolineal\n");
	TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0), x(1), x(2), x(3), x(4));
	ellipseParams res;
	res.a = 1.0;
	res.b = x(0);
	res.c = x(1);
	res.d = x(2);
	res.e = x(3);
	res.f = x(4);
	return res;
}

ellipseParams CTrackingHerniaNoCudaDlg::GetEllipseMixNormN2(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(5);
	x << initial.b, initial.c, initial.d, initial.e, initial.f;
	optimization_ellipse_functorN2 functor(points.size(), points);
	Eigen::NumericalDiff<optimization_ellipse_functorN2> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functorN2>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
	TRACE("\n inici calcul Nolineal\n");
	info = lm.minimizeInit(x);
	for (int i = 0; i < NITERATION; i++)
	{
		info = lm.minimizeOneStep(x);
		vNormaOptimit = lm.fnorm;
		TRACE("FINAL OPTIMITZACIÓ: Iteració LM %d, ERROR OPTIMITZACIÓ: %f\n", i, vNormaOptimit);
		if (info != Eigen::LevenbergMarquardtSpace::Running)
			break;
	}
	TRACE("final calcul Nolineal\n");
	TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0), x(1), x(2), x(3), x(4));
	ellipseParams res;
	res.a = 1.0;
	res.b = x(0);
	res.c = x(1);
	res.d = x(2);
	res.e = x(3);
	res.f = x(4);
	return res;
}

ellipseParams CTrackingHerniaNoCudaDlg::GetEllipseConstant(std::vector<cv::Point> & points, ellipseParams & initial)
{
	Eigen::VectorXd x = Eigen::VectorXd(1);
	x << initial.f;
	optimization_ellipse_constant_functor functor(points.size(), points, initial);
	Eigen::NumericalDiff<optimization_ellipse_constant_functor> numDiff(functor);

	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_constant_functor>, double> lm(numDiff);
	//Eigen::LevenbergMarquardt<Eigen::NumericalDiff<optimization_ellipse_functor>, float>::JacobianType::RealScalar oing = lm.lm_param();
	//lm.resetParameters();
	//lm.parameters.ftol=lm.parameters.ftol*10.0;
	//lm.parameters.xtol=lm.parameters.xtol*10.0;
	//lm.parameters.maxfev=10;
	//lm.parameters.epsfcn=0.1;
	//lm.resetParameters();

	int info;
	double vNormaOptimit;
	TRACE("\n inici calcul Nolineal\n");
	info = lm.minimizeInit(x);
	for (int i = 0; i < NITERATION; i++)
	{
		info = lm.minimizeOneStep(x);
		vNormaOptimit = lm.fnorm;
		TRACE("FINAL OPTIMITZACIÓ: Iteració LM %d, ERROR OPTIMITZACIÓ: %f\n", i, vNormaOptimit);
		if (info != Eigen::LevenbergMarquardtSpace::Running)
			break;
	}
	TRACE("final calcul Nolineal\n");
	TRACE("ellipse: 1.00 %.2lf %.2lf %.2lf %.2lf %.2lf\n", x(0));
	ellipseParams res;
	res.a = initial.a;
	res.b = initial.b;
	res.c = initial.c;
	res.d = initial.d;
	res.e = initial.e;
	res.f = x(0);
	return res;
}

void CTrackingHerniaNoCudaDlg::paintEllipse(ellipseParams &ellipseP, cv::Mat &image, double red, double green, double blue)
{
	uint8_t *imData = image.data;

	double a = -sqrt(2.0*(ellipseP.a*ellipseP.e*ellipseP.e + ellipseP.c*ellipseP.d*ellipseP.d - ellipseP.b*ellipseP.d*ellipseP.e + (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c)*ellipseP.f)*((ellipseP.a + ellipseP.c) + sqrt((ellipseP.a - ellipseP.c)*(ellipseP.a - ellipseP.c) + ellipseP.b*ellipseP.b)));
	a /= (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c);
	double b = -sqrt(2.0*(ellipseP.a*ellipseP.e*ellipseP.e + ellipseP.c*ellipseP.d*ellipseP.d - ellipseP.b*ellipseP.d*ellipseP.e + (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c)*ellipseP.f)*((ellipseP.a + ellipseP.c) - sqrt((ellipseP.a - ellipseP.c)*(ellipseP.a - ellipseP.c) + ellipseP.b*ellipseP.b)));
	b /= (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c);
	int r = (int)max(a, b);
	if (r <= 0)
	{
		return;
	}
	if (r > min(image.cols, image.rows))
	{
		r = min(image.cols, image.rows);
	}
	int x = (int)((2.0*ellipseP.c*ellipseP.d - ellipseP.b*ellipseP.e) / (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c));
	int y = (int)((2.0*ellipseP.a*ellipseP.e - ellipseP.b*ellipseP.d) / (ellipseP.b*ellipseP.b - 4.0*ellipseP.a*ellipseP.c));


	for (int i = y - r - 5; i <= y + r + 6; i++)
	{
		for (int j = x - r - 5; j <= x + r + 6; j++)
		{
			if ((j >= image.cols - 2) || (i >= image.rows - 2) || (j < 2) || (i < 2))
			{
				continue;
			}
			cv::Point2d p;
			p.x = (double)j;
			p.y = (double)i;
			double val = SampsonDistance(ellipseP, p);//EvaluateEllipse(ellipseP, p);
			if (abs(val) < 2.0)
			{
				imData[(i * image.cols + j) * 3] = blue;
				imData[(i * image.cols + j) * 3 + 1] = green;
				imData[(i * image.cols + j) * 3 + 2] = red;
			}
		}
	}
}



void CTrackingHerniaNoCudaDlg::OnBnClickedButton2()
{
	// TODO: Agregue aquí su código de controlador de notificación de control

	CString strText, strTextTimes;
	CStdioFile fileTime;
	CStdioFile fileEllipseData, fileEllipseTimes;
	CStdioFile filePoints;

	filePoints.Open(_T("./PointsRel_d.txt"), CFile::modeCreate | CFile::modeWrite);

	fileEllipseData.Open(_T("./ellipsedata_Seq4.txt"), CFile::modeCreate | CFile::modeWrite);

	fileEllipseTimes.Open(_T("./ellipsedata_Times.txt"), CFile::modeCreate | CFile::modeWrite);

	strText.Format(_T("Image\tEDa\tEDb\tEDc\tEDd\tEDe\tEDf\tEDcx\tEDcy\tEDsa\tEDsb\tEDAr\tELSMa\tELSMb\tELSMc\tELSMd\tELSMe\tELSMf\tELSMcx\tELSMcy\tELSMsa\tELSMsb\tELSMAr\tEPCAa\tEPCAb\tEPCAc\tEPCAd\tEPCAe\tEPCAf\tEPCAcx\tEPCAcy\tEPCAsa\tEPCAsb\tEPCAAr\n"));
	fileEllipseData.WriteString(strText);

	//Read list points
	CString path_listPoints;
	//path_listPoints.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Videos\\SubSeq2\\"));
	path_listPoints.Format(_T("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Tracking\\Hernia\\Videos\\SubSeq4\\"));
	CString txt_seedName, img_seedName, img_seedOutName;
	txt_seedName.Format(_T("PointsImage_"));
	img_seedName.Format(_T("Image_"));
	img_seedOutName.Format(_T("OutEllipseImage_Red_"));

	int iniFile = 9700;
	int endFile = 10255;


	CStdioFile listPointsFile;
	CString numTxt, numImg;
	CString elmtxt;
	std::vector<cv::Point> listPoints;

	CString zerStr;

	int numImage = 0;

	int lfin = (int)ceil(log10(endFile + 1));

	_LARGE_INTEGER StartingTime, StepTime, EndingTime, ElapsedMiliseconds1, ElapsedMiliseconds2;
	LARGE_INTEGER Frequency;
	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&StartingTime);

	for (int ii = iniFile; ii < endFile + 1; ii++)
	{
		if (ii == 8245)
			continue;
		listPoints.resize(0);
		int lImg = (int)ceil(log10(ii + 1));
		if (lImg < lfin)
		{
			numTxt.Format(_T("0%d.txt"), ii);
			numImg.Format(_T("0%d.png"), ii);
		}
		else
		{
			numTxt.Format(_T("%d.txt"), ii);
			numImg.Format(_T("%d.png"), ii);
		}

		listPointsFile.Open(path_listPoints + txt_seedName + numTxt, CFile::modeRead);
		listPointsFile.ReadString(elmtxt);
		while (listPointsFile.ReadString(elmtxt))
		{
			int curPos = 0;
			CString strToken = elmtxt.Tokenize(_T(" "), curPos);
			cv::Point p;
			p.y = _ttoi(strToken);
			for (int j = 1; j < 2; j++)
			{
				strToken = elmtxt.Tokenize(_T(" "), curPos);
				p.x = _ttoi(strToken);
			}
			listPoints.push_back(p);
		}
		listPointsFile.Close();

		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiString(path_listPoints + img_seedName + numImg);

		// construct a std::string using the LPCSTR input
		std::string strStd(pszConvertedAnsiString);


		cv::Mat srcColor = cv::imread(strStd, IMREAD_COLOR);


		std::vector<cv::Point> points, pointsRed;

		GetElapsedMiliseconds(ElapsedMiliseconds1, StartingTime, Frequency);
		strTextTimes.Format(_T("Inici Temps Ellipse:\t%d\t%d\n"), ElapsedMiliseconds1.QuadPart, ElapsedMiliseconds1.QuadPart);
		fileEllipseTimes.WriteString(strTextTimes);

		if (numImage == 0)
		{
			std::copy(listPoints.begin(), listPoints.end(), std::back_inserter(points));
			std::copy(listPoints.begin(), listPoints.end(), std::back_inserter(pointsRed));
			numImage = 1;
		}
		else
		{
			cv::Point2d p = GetCenterEllipse(lastEllipse);
			double a = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) + sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			a /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			double b = -sqrt(2.0*(lastEllipse.a*lastEllipse.e*lastEllipse.e + lastEllipse.c*lastEllipse.d*lastEllipse.d - lastEllipse.b*lastEllipse.d*lastEllipse.e + (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c)*lastEllipse.f)*((lastEllipse.a + lastEllipse.c) - sqrt((lastEllipse.a - lastEllipse.c)*(lastEllipse.a - lastEllipse.c) + lastEllipse.b*lastEllipse.b)));
			b /= (lastEllipse.b*lastEllipse.b - 4.0*lastEllipse.a*lastEllipse.c);
			int r = (int)min(max(abs(a), abs(b)), srcColor.rows / 5.0);
			points = GetPointsRealImageUsingLastEllipse(srcColor, lastEllipse, listPoints, (int)p.x - r - ELLIPSETHRESHOLD, (int)p.y - r - ELLIPSETHRESHOLD, 2 * (r + ELLIPSETHRESHOLD), 2 * (r + ELLIPSETHRESHOLD));
			pointsRed = ReducePoints(points, lastEllipse);
			numImage = 2;
		}



		uint8_t *imData = srcColor.data;
		for (int k = 0; k < pointsRed.size(); k++)
		{
			for (int ii = -3; ii < 4; ii++)
			{
				for (int jj = -3; jj < 4; jj++)
				{
					if ((pointsRed[k].x == 0) || (pointsRed[k].y == 0) || (pointsRed[k].x + jj >= srcColor.cols - 2) || (pointsRed[k].y + ii >= srcColor.rows - 2) || (pointsRed[k].x + jj < 2) || (pointsRed[k].y + ii < 2))
					{
						continue;
					}
					if (ii*ii + jj * jj <= 3)
					{
						imData[((pointsRed[k].y + ii) * srcColor.cols + pointsRed[k].x + jj) * 3] = 0;
						imData[((pointsRed[k].y + ii) * srcColor.cols + pointsRed[k].x + jj) * 3 + 1] = 0;
						imData[((pointsRed[k].y + ii) * srcColor.cols + pointsRed[k].x + jj) * 3 + 2] = 0;
					}
				}
			}
		}

		ellipseParams elliN1, elliN12, elliN22, elliLMS, elliPCA;

		std::vector<cv::Point> pointsClean(0), pointsCleanRed(0);

		strText.Format(_T("%d "), ii);

		for (int i = 0; i < points.size(); i++)
		{
			if (points[i].x == 0 || points[i].y == 0)
			{
				continue;
			}
			cv::Point pp;
			pp.x = points[i].x;
			pp.y = points[i].y;
			pointsClean.push_back(pp);

			CString auxPoint;
			auxPoint.Format(_T("%d %d "), pp.x, pp.y);
			strText += auxPoint;

		}

		for (int i = 0; i < pointsRed.size(); i++)
		{
			if (pointsRed[i].x == 0 || pointsRed[i].y == 0)
			{
				continue;
			}
			cv::Point pp;
			pp.x = pointsRed[i].x;
			pp.y = pointsRed[i].y;
			pointsCleanRed.push_back(pp);
		}

		strText += _T("\n");
		filePoints.WriteString(strText);

		elliLMS = GetEllipseLMS(pointsCleanRed);
		elliPCA = GetEllipsePCA(pointsClean);
		if (numImage == 1)
		{
			elliN1 = GetEllipseMixNorm(pointsCleanRed, elliLMS);
		}
		else
		{
			elliN1 = GetEllipseMixNorm(pointsCleanRed, lastEllipse);
		}

		GetElapsedMiliseconds(ElapsedMiliseconds1, StartingTime, Frequency);
		strTextTimes.Format(_T("Final Temps Ellipse:\t%d\t%d\n"), ElapsedMiliseconds1.QuadPart, ElapsedMiliseconds1.QuadPart);
		fileEllipseTimes.WriteString(strTextTimes);


		lastEllipse = elliN1;
		lastPoints.resize(0);
		std::copy(points.begin(), points.end(), back_inserter(lastPoints));
		paintEllipse(elliN1, srcColor, 0, 0, 255);
		paintEllipse(elliLMS, srcColor, 255, 0, 0);
		//paintEllipse(elliPCA, srcColor, 0, 255, 0);

		cv::Point2d cN1 = GetCenterEllipse(elliN1);
		cv::Point2d cLMS = GetCenterEllipse(elliLMS);
		cv::Point2d cPCA = GetCenterEllipse(elliPCA);

		cv::Point2d axN1 = GetSemiAxesEllipse(elliN1);
		cv::Point2d axLMS = GetSemiAxesEllipse(elliLMS);
		cv::Point2d axPCA = GetSemiAxesEllipse(elliPCA);

		double ArN1 = GetAreaEllipse(elliN1);
		double ArLMS = GetAreaEllipse(elliLMS);
		double ArPCA = GetAreaEllipse(elliPCA);

		strText.Format(_T("%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n"), ii, elliN1.a, elliN1.b, elliN1.c, elliN1.d, elliN1.e, elliN1.f, cN1.x, cN1.y, axN1.x, axN1.y, ArN1, elliLMS.a, elliLMS.b, elliLMS.c, elliLMS.d, elliLMS.e, elliLMS.f, cLMS.x, cLMS.y, axLMS.x, axLMS.y, ArLMS, elliPCA.a, elliPCA.b, elliPCA.c, elliPCA.d, elliPCA.e, elliPCA.f, cPCA.x, cPCA.y, axPCA.x, axPCA.y, ArPCA);
		fileEllipseData.WriteString(strText);


		Eigen::VectorXd x = Eigen::VectorXd(5);
		x << lastEllipse.b, lastEllipse.c, lastEllipse.d, lastEllipse.e, lastEllipse.f;
		Eigen::VectorXd fvec = Eigen::VectorXd(points.size());
		std::vector<int> indOut;
		test(x, fvec, points.size(), points, indOut);



		// Convert a TCHAR string to a LPCSTR
		CT2CA pszConvertedAnsiStringOut(path_listPoints + img_seedOutName + numImg);

		// construct a std::string using the LPCSTR input
		std::string strStdOut(pszConvertedAnsiStringOut);

		cv::imwrite(strStdOut, srcColor);
		SetProcImage(srcColor);

	}
	fileEllipseData.Close();
	fileEllipseTimes.Close();
}


void CTrackingHerniaNoCudaDlg::OnBnClickedOk()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	CDialog::OnOK();
}


void CTrackingHerniaNoCudaDlg::OnBnClickedGetpointssynth()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
}


void CTrackingHerniaNoCudaDlg::OnBnClickedSynthtic()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
}


void CTrackingHerniaNoCudaDlg::OnBnClickedProc()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
}
