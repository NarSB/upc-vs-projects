
// DemoFetalDlg.h: archivo de encabezado
//

#pragma once
#include "afxwin.h"

#define TOLERANCIA 0.001
#define SIZETAQUES 10
#define SIZEBUSCARULTIMESTAQUES 4
#define SIZECONTORN 16
#define XSIZEIMAGE 600

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

struct tacaStruct {
	cv::Point2f mean;
	double distCercle;
	int size;
	int minX;
	int maxX;
	int minY;
	int maxY;
	std::vector<cv::Point2i> points;
	std::vector<cv::Point2i> contorn;
};

struct indexosTaca {
	bool trobat;
	int nNuls;
	int igualNTaques;
};


// Cuadro de di�logo de CDemoFetalDlg
class CDemoFetalDlg : public CDialogEx
{
// Construcci�n
public:
	CDemoFetalDlg(CWnd* pParent = NULL);	// Constructor est�ndar

	CWinThread * thRecording;
	bool bRecording;	//True: Thread active, False: Kill Thread

	double          VideoFrameRate; //Video frame rate (FPS)
	int ctrlVideoImageReady;
	cv::Mat lastImage;
	cv::Mat cameraMatrix, distCoef;
	float fc[2], cc[2], alpha_c;
	double kc[5];
	int iTotalFramesRecorded;

	VideoCapture cam;
	VideoWriter video;
	bool bvideo;

	cv::Size winSegSize;
	cv::Mat cvImgTmp;
	CImage* mfcImg;
	BITMAPINFO bitInfo;

	//Segmentacio
	int indSegmentacio;
	cv::Mat masCercle;
	double paramCercle[3];

	std::vector<std::vector<std::vector<tacaStruct>>> ultTaques;
	std::vector<indexosTaca> indUltTaques;
	
	int indlastTaques;
	std::vector<tacaStruct> lastTaques[SIZETAQUES];

// Datos del cuadro de di�logo
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEMOFETALJULIOL2018_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_picImage;
	CButton m_butCamera;
	afx_msg void OnBnClickedButton1();
	void PosarImatgePictureBox(cv::Mat & entrada);
	void Segmentacio(cv::Mat & entrada, cv::Mat & sortida);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButvideo();
	void BuscarTaques(cv::Mat & image, std::vector<tacaStruct>& taques);
	bool InterseccioTaques(tacaStruct & A, tacaStruct & B);
	void CalcularCadenaTaques(std::vector<tacaStruct>& novaTaca);
	tacaStruct creixementRegio(cv::Mat & imatge, cv::Mat & mascara, int x, int y, int lbl);
	CButton m_butVideo;
	afx_msg void OnBnClickedButprova();
};
