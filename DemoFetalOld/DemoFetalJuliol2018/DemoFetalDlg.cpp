
// DemoFetalDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "DemoFetalJuliol2018.h"
#include "DemoFetalDlg.h"
#include "afxdialogex.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//Global Inlines
inline int modval(int a, int b) { return (a % b + b) % b; }

inline int evaluarMat(uint8_t * matrix, int r, int c, int ncols)
{
	if ((r < 0) || (c<0) || (c >= ncols))
		return -1;
	return matrix[r * ncols + c];
}

inline float normDistancia(float x1, float y1, float x2, float y2)
{
	return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
}

inline float normCvPoint2f(cv::Point2f P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint2i(cv::Point2i P)
{
	return sqrt(P.x*P.x + P.y*P.y);
}

inline float normCvPoint3f(cv::Point3f P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline float normCvPoint3i(cv::Point3i P)
{
	return sqrt(P.x*P.x + P.y*P.y + P.z*P.z);
}

inline int float2int(float x)
{
	return (int)(x + TOLERANCIA);
}

inline float FuncioSigne(float x)
{
	if (std::abs(x)<TOLERANCIA)
		return 0.0;
	else if (x>0)
		return 1.0;
	else
		return -1.0;
}

inline bool InterseccioSegments(cv::Point Pi1, cv::Point Pf1, cv::Point Pi2, cv::Point Pf2)
{
	Eigen::Matrix2d A;
	Eigen::Vector2d b;

	if ((Pi1.x == Pi2.x) && (Pi1.y == Pi2.y))
		return true;
	if ((Pi1.x == Pf2.x) && (Pi1.y == Pf2.y))
		return true;
	if ((Pf1.x == Pi2.x) && (Pf1.y == Pi2.y))
		return true;
	if ((Pf1.x == Pf2.x) && (Pf1.y == Pf2.y))
		return true;

	A(0, 0) = (double)(Pf1.x - Pi1.x);
	A(0, 1) = (double)(Pi2.x - Pf2.x);
	A(1, 0) = (double)(Pf1.y - Pi1.y);
	A(1, 1) = (double)(Pi2.y - Pf2.y);

	b(0) = (double)(Pi2.x - Pi1.x);
	b(0) = (double)(Pi2.x - Pi1.x);

	if (A.determinant() < 0.000000001)
	{
		return false;
	}

	Eigen::Vector2d X = A.inverse()*b;
	if ((X(0) > 0) && (X(0) < 1) && (X(1) > 0) && (X(1) < 1))
	{
		return true;
	}
	return false;
}

inline cv::Point2f CalcularMeanCadenaTaca(std::vector<std::vector<tacaStruct>> & cadena)
{
	cv::Point2f res(0.0, 0.0);
	float totSize = 0.0;
	for (int i = 0; i < cadena.size(); i++)
	{
		for (int j = 0; j < cadena[i].size(); j++)
		{
			res += ((float)cadena[i][j].size)*cadena[i][j].mean;
			totSize += (float)cadena[i][j].size;
		}
	}
	return res / totSize;
}


//Thread Camera

UINT ThreadRecording(LPVOID pParam)
{
	CDemoFetalDlg* pObject = (CDemoFetalDlg*)pParam;

	_LARGE_INTEGER StartingTime, EndingTime, ElapsedMiliseconds;
	LARGE_INTEGER Frequency;
	CString fpsText;
	std::vector<cv::Point2i> auxPointsContorn = std::vector<cv::Point2i>();
	std::vector<cv::Point2f> auxPointsFloat = std::vector<cv::Point2f>();

	pObject->iTotalFramesRecorded = 0;
	int index;
	float x0, y0, a_2, b_2, angE;
	float x1, y1;
	x0 = 0.0f;
	y0 = 0.0f;


	int iFrameRate = 1000 / pObject->VideoFrameRate; //milliseconds per frame
	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&StartingTime);

	
	while (pObject->bRecording)
	{
		QueryPerformanceCounter(&StartingTime);
		pObject->cam.read(pObject->lastImage);

		if (pObject->bvideo)
		{
			pObject->video << pObject->lastImage;
		}


		pObject->PosarImatgePictureBox(pObject->lastImage);
		QueryPerformanceCounter(&EndingTime);
		ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;

		// We now have the elapsed number of ticks, along with the number of ticks-per-second. We use these values
		// to convert to the number of elapsed milliseconds.  To guard against loss-of-precision, we convert
		// to milliseconds *before* dividing by ticks-per-second.
		ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
		ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
	}

	if (pObject->mfcImg)
	{
		pObject->mfcImg->ReleaseDC();
		delete pObject->mfcImg; pObject->mfcImg = nullptr;
	}
	return 0;
}





// Cuadro de di�logo de CDemoFetalDlg



CDemoFetalDlg::CDemoFetalDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DEMOFETALJULIOL2018_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDemoFetalDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DISPLAY, m_picImage);
	DDX_Control(pDX, IDC_BUTTON1, m_butCamera);
	DDX_Control(pDX, IDC_BUTVideo, m_butVideo);
}

BEGIN_MESSAGE_MAP(CDemoFetalDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CDemoFetalDlg::OnBnClickedButton1)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTVideo, &CDemoFetalDlg::OnBnClickedButvideo)
	ON_BN_CLICKED(IDC_BUTProva, &CDemoFetalDlg::OnBnClickedButprova)
END_MESSAGE_MAP()


// Controladores de mensaje de CDemoFetalDlg

BOOL CDemoFetalDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Establecer el icono para este cuadro de di�logo.  El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// TODO: agregar aqu� inicializaci�n adicional

	bRecording = false;
	bvideo = false;
	/*cam = VideoCapture(0);
	if (!cam.isOpened())  // check if we succeeded
	{
		AfxMessageBox("No camera");
		exit(-1);
	}
		
	//int codec = CV_FOURCC('M', 'J', 'P', 'G');
	//cam.set(CV_CAP_PROP_FOURCC, codec);
	cam.set(CV_CAP_PROP_FRAME_WIDTH, 800);
	cam.set(CV_CAP_PROP_FRAME_HEIGHT, 600);
	cam.set(CV_CAP_PROP_FPS, 30.0);

	//int ex = (int)cam.get(CV_CAP_PROP_FOURCC);
	int fourcc = CV_FOURCC_DEFAULT;
	std::string videoname = "./video/video001.avi";
	video.open(videoname, fourcc, cam.get(CV_CAP_PROP_FPS), cv::Size((int)cam.get(CV_CAP_PROP_FRAME_WIDTH), (int)cam.get(CV_CAP_PROP_FRAME_HEIGHT)), true);
	if (!video.isOpened())
	{
		AfxMessageBox("VideoWriter Error");
		exit(-1);
	}

	lastImage = cv::Mat(cam.get(CV_CAP_PROP_FRAME_WIDTH), cam.get(CV_CAP_PROP_FRAME_HEIGHT), CV_8UC3);*/

	m_butCamera.SetWindowTextA(_T("Start Camera"));
	m_butVideo.EnableWindow(FALSE);
	m_butVideo.SetWindowTextA(_T("Start Video"));

	RECT r;
	m_picImage.GetClientRect(&r);
	winSegSize = cv::Size(r.right, r.bottom);
	if (winSegSize.width % 4 != 0)
	{
		winSegSize.width = 4 * (winSegSize.width / 4);
	}
	cvImgTmp = cv::Mat(winSegSize, CV_8UC3);
	bitInfo.bmiHeader.biBitCount = 24;
	bitInfo.bmiHeader.biWidth = winSegSize.width;
	bitInfo.bmiHeader.biHeight = winSegSize.height;
	bitInfo.bmiHeader.biPlanes = 1;
	bitInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfo.bmiHeader.biCompression = BI_RGB;
	bitInfo.bmiHeader.biClrImportant = 0;
	bitInfo.bmiHeader.biClrUsed = 0;
	bitInfo.bmiHeader.biSizeImage = 0;
	bitInfo.bmiHeader.biXPelsPerMeter = 0;
	bitInfo.bmiHeader.biYPelsPerMeter = 0;

	indSegmentacio = -1;
	indlastTaques = 0;

	//Cercle Parametres
	CString filename = "paramCercle.txt";
	CStdioFile file;
	CString filestr;
	file.Open(filename, CFile::modeRead);
	file.SeekToBegin();
	file.ReadString(filestr);
	sscanf_s(filestr, "%lf  %lf  %lf", &paramCercle[0], &paramCercle[1], &paramCercle[2]);
	file.Close();

	masCercle = cv::imread("mascara.png", CV_LOAD_IMAGE_GRAYSCALE);
	cv::threshold(masCercle, masCercle, 128, 255, CV_THRESH_BINARY);

	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono.  Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CDemoFetalDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

void CDemoFetalDlg::PosarImatgePictureBox(cv::Mat &entrada)
{
	cv::Mat auxiliar = entrada.clone();
	//cv::cvtColor(entrada, auxiliar, cv::COLOR_GRAY2BGR);

	////PER BORRAR
	//uint8_t *imData = auxiliar.data;
	//int nrows = auxiliar.rows;
	//int ncols = auxiliar.cols;
	//int deltax, deltay;

	/*for (int i = 0; i < puntsSegmentatsvect.size(); ++i)
	{
		for (int a = -2; a < 3; a++)
		{
			for (int b = -2; b < 3; b++)
			{
				if ((puntsSegmentatsvect[i].x == 0) || (puntsSegmentatsvect[i].y == 0) || (puntsSegmentatsvect[i].x + b >= auxiliar.cols - 2) || (puntsSegmentatsvect[i].y + a >= auxiliar.rows - 2) || (puntsSegmentatsvect[i].x + b < 2) || (puntsSegmentatsvect[i].y + a < 2))
				{
					continue;
				}

				imData[((puntsSegmentatsvect[i].y + a) * auxiliar.cols + puntsSegmentatsvect[i].x + b) * 3] = 0;
				imData[((puntsSegmentatsvect[i].y + a) * auxiliar.cols + puntsSegmentatsvect[i].x + b) * 3 + 1] = 255;
				imData[((puntsSegmentatsvect[i].y + a) * auxiliar.cols + puntsSegmentatsvect[i].x + b) * 3 + 2] = 255;
			}
		}
	}

	if (meanSegmentats.size() > 0)
	{
		for (int a = -2; a < 3; a++)
		{
			for (int b = -2; b < 3; b++)
			{
				if ((meanSegmentats[0].x == 0) || (meanSegmentats[0].y == 0) || (meanSegmentats[0].x + b >= auxiliar.cols - 2) || (meanSegmentats[0].y + a >= auxiliar.rows - 2) || (meanSegmentats[0].x + b < 2) || (meanSegmentats[0].y + a < 2))
				{
					continue;
				}
				if ((isnan(meanSegmentats[0].x)) || (isnan(meanSegmentats[0].y)))
				{
					continue;
				}
				imData[(((int)meanSegmentats[0].y + a) * auxiliar.cols + (int)meanSegmentats[0].x + b) * 3] = 255;
				imData[(((int)meanSegmentats[0].y + a) * auxiliar.cols + (int)meanSegmentats[0].x + b) * 3 + 1] = 0;
				imData[(((int)meanSegmentats[0].y + a) * auxiliar.cols + (int)meanSegmentats[0].x + b) * 3 + 2] = 0;
			}
		}
	}*/
	////


	//If size does not match
	if (auxiliar.size() != winSegSize)
	{
		cv::resize(auxiliar, cvImgTmp, winSegSize);
	}
	else
	{
		cvImgTmp = auxiliar.clone();
	}


	//Rotate image
	cv::flip(cvImgTmp, cvImgTmp, 0);

	//Create MFC image
	if (mfcImg)
	{
		mfcImg->ReleaseDC();
		delete mfcImg; mfcImg = nullptr;
	}

	mfcImg = new CImage();
	mfcImg->Create(winSegSize.width, winSegSize.height, 24);


	//Add header and OpenCV image to mfcImg
	StretchDIBits(mfcImg->GetDC(), 0, 0,
		winSegSize.width, winSegSize.height, 0, 0,
		winSegSize.width, winSegSize.height,
		cvImgTmp.data, &bitInfo, DIB_RGB_COLORS, SRCCOPY
	);

	//Display mfcImg in MFC window
	mfcImg->BitBlt(::GetDC(m_picImage.m_hWnd), 0, 0);
}

void CDemoFetalDlg::Segmentacio(cv::Mat & entrada, cv::Mat & sortida)
{
	cv::Mat auxiliar;
	cv::cvtColor(entrada, auxiliar, CV_RGB2YCrCb);
	//cv::medianBlur(auxiliar, auxiliar, 3);
	cv::GaussianBlur(auxiliar, auxiliar, cv::Size(3, 3),3);
	cv::extractChannel(auxiliar, auxiliar, 2);
	float rang[] = { 0,256 };
	const float* range[] = { rang };
	cv::Mat hist;
	int histsize[] = { 256 };
	int channel[] = { 0 };
	cv::calcHist(&auxiliar, 1, channel, Mat(), hist, 1, histsize, range, true, false);

	std::vector<int> histAc(256);

	histAc[255] = (int)hist.at<float>(255);

	float * hisData = (float*) hist.data;
	for (int i = 254; i > -1; i--)
	{
		histAc[i] = hisData[i] + histAc[i + 1];//(int)hist.at<float>(i) + histAc[i + 1];
	}

	double minPendent = 100000.0;
	int indMinPendent = -1;
	double pendent;

	for (int i = 150; i < 210; i++)
	{
		if ((histAc[i] < histAc[0] / 10)&&(histAc[i] > histAc[0]/100))
		{
			pendent = 0;
			for (int j = -5; j < 6; j++)
			{
				pendent += abs(-3.0*histAc[i + j + 4] + 32.0*histAc[i + j + 3] - 168.0*histAc[i + j + 2] + 672.0* histAc[i + j + 1] - 672.0*histAc[i + j - 1] + 168.0*histAc[i + j - 2] - 32.0*histAc[i + j - 3] + 3.0*histAc[i + j - 4]) / 840.0;//abs((- histAc[i + 2] + 8 * histAc[i + 1] - 8 * histAc[i - 1] + histAc[i - 2]) / 12.0);
			}
			if (pendent < minPendent)
			{
				minPendent = pendent;
				indMinPendent = i;
			}
		}
	}

	//double maxVal = 0;
	//minMaxLoc(hist, 0, &maxVal, 0, 0);
	//cv::Mat histImg = Mat::zeros(500, 256 * 3, CV_8UC3);
	//for (int h = 0; h < 256; h++)
	//{
	//	float binVal = histAc[h];
	//	int intensity = cvRound(binVal * histImg.rows / histAc[0]);
	//	rectangle(histImg, Point(h*3, histImg.rows - 1 - intensity),
	//		Point((h + 1)*3 - 1, histImg.rows - 1),
	//		Scalar::all(255),
	//		-1);
	//	if (h == indMinPendent)
	//		rectangle(histImg, Point(h * 3, histImg.rows - 1 - intensity),
	//			Point((h + 1) * 3 - 1, histImg.rows - 1),
	//			Scalar::all(64),
	//			-1);
	//}


	if (indSegmentacio == -1)
	{
		indSegmentacio = indMinPendent;
	}
	else if (indMinPendent == -1)
	{
		indSegmentacio = indSegmentacio;
	}
	else if (indSegmentacio > indMinPendent)
	{
		indSegmentacio = max(indSegmentacio - 3, indMinPendent);
	}
	else
	{
		indSegmentacio = min(indSegmentacio + 3, indMinPendent);
	}

	//float binVal = histAc[indSegmentacio];
	//int intensity = cvRound(binVal * histImg.rows / histAc[0]);
	//rectangle(histImg, Point(indSegmentacio * 3, histImg.rows - 1 - intensity),
	//	Point((indSegmentacio + 1) * 3 - 1, histImg.rows - 1),
	//	Scalar::all(128),
	//	-1);

	//cv::namedWindow("H-S Histogram", 1);
	//cv::imshow("H-S Histogram", histImg);
	//cv::waitKey(5);
	////indMinPendent = 190;

	cv::threshold(auxiliar, auxiliar, indSegmentacio, 255, CV_THRESH_BINARY);
	cv::Mat squareKernel = getStructuringElement(MORPH_RECT, Size(3, 3), Point(-1, -1));
	cv::Mat crossKernel = getStructuringElement(MORPH_CROSS, Size(3, 3), Point(-1, -1));
	cv::morphologyEx(auxiliar, auxiliar, MORPH_DILATE, squareKernel);
	cv::morphologyEx(auxiliar, auxiliar, MORPH_ERODE, crossKernel);
	cv::morphologyEx(auxiliar, auxiliar, MORPH_CLOSE, squareKernel);
	cv::filterSpeckles(auxiliar, 0, 100, 0);


	sortida = auxiliar.clone();

}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CDemoFetalDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CDemoFetalDlg::OnBnClickedButton1()
{
	if (bRecording)
	{
		bRecording = false;
		m_butCamera.SetWindowTextA("Start Camera");
		m_butVideo.EnableWindow(FALSE);
		m_butVideo.SetWindowTextA("Start Video");
	}
	else
	{
		bRecording = true;
		m_butCamera.SetWindowTextA("Stop Camera");
		m_butVideo.EnableWindow(TRUE);
		m_butVideo.SetWindowTextA("Start Video");
		thRecording = AfxBeginThread(ThreadRecording, this, THREAD_PRIORITY_HIGHEST, 0, 0, NULL);	//Initialize thread uEyeCamera
	}
}


void CDemoFetalDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	if (bRecording)
	{
		bRecording = false;
		//Whait until thread is dead
		Sleep(300);
	}

}


void CDemoFetalDlg::OnBnClickedButvideo()
{
	if (bvideo)
	{
		bvideo = false;
		m_butVideo.SetWindowTextA("Start Video");
	}
	else
	{
		bvideo = true;
		m_butVideo.SetWindowTextA("Stop Video");
	}
}

void CDemoFetalDlg::BuscarTaques(cv::Mat & image, std::vector<tacaStruct> & taques)
{
	taques = std::vector<tacaStruct>(0);
	cv::Mat auxiliar = image.clone();
	cv::Mat mask = cv::Mat::zeros(auxiliar.rows, auxiliar.cols, auxiliar.type());
	uint8_t *imData;
	uint8_t *mskData;

	int label = 1;
	int indexTaca = 0;
	bool trobat;

	for (int i = 20; i < image.rows - 20; i = i + 10)
	{
		imData = auxiliar.ptr(i);
		mskData = mask.ptr(i);
		for (int j = 20; j < auxiliar.cols - 20; j = j + 10)
		{
			if ((imData[j] > 0) && (mskData[j] == 0))
			{
				tacaStruct novaTaca = creixementRegio(image, mask, j, i, label);
				label++;
				if (novaTaca.size > 50)
				{
					indexTaca = 0;
					trobat = false;
					for (int k = 0; k < taques.size(); k++)
					{
						indexTaca = k;
						if (taques[indexTaca].distCercle < novaTaca.distCercle)
						{
							trobat = true;
							break;
						}
					}
					if (trobat)
						taques.insert(taques.begin() + indexTaca, novaTaca);
					else
					{
						taques.push_back(novaTaca);
					}
				}
			}
		}
	}

	//Ajuntar taques petites juntes
	int esborrades = 0;
	int sizeInicial = taques.size();
	for (int i = 0; i < sizeInicial - 1 - esborrades; i++)
	{
		for (int j = i + 1; j < sizeInicial - esborrades; j++)
		{
			if (normCvPoint2f(taques[i].mean - taques[j].mean) < 30)
			{
				taques[i].distCercle = min(taques[i].distCercle, taques[j].distCercle);
				std::copy(taques[j].points.begin(), taques[j].points.end(), std::back_inserter(taques[i].points));
				taques[i].mean = (taques[i].size * taques[i].mean + taques[j].size*taques[j].mean) / (double)(taques[i].size + taques[j].size);
				taques[i].size += taques[j].size;
				taques.erase(taques.begin() + j);
				esborrades++;
				j = i;
			}
		}
	}



}
double sqrt111;
double sqrt222;
bool CDemoFetalDlg::InterseccioTaques(tacaStruct & A, tacaStruct &B)
{
	if (A.maxX < B.minX)
		return false;
	if (A.minX > B.maxX)
		return false;
	if (A.minY > B.maxY)
		return false;
	if (A.maxY < B.minY)
		return false;
	
	sqrt222 = (double)((A.mean.x - B.mean.x)*(A.mean.x - B.mean.x) + (A.mean.y - B.mean.y)*(A.mean.y - B.mean.y));
	sqrt111 = (double)sqrt(sqrt222);
	
	CStdioFile file;

	file.Open("./expedienteX.txt", CFile::modeCreate | CFile::modeWrite);
	CString strText;
	strText.Format("%lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
	file.WriteString(strText);
	file.Close();
	if ((A.size > 300) && (B.size > 300))
	{
		if (sqrt222 < 2500.0)//normCvPoint2f(A.mean - B.mean) < 50)
		{
			file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
			CString strText2;
			strText2.Format("hei1 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
			file.WriteString(strText);
			file.Close();
			return true;
		}
		else
		{
			if (A.size > B.size)
			{
				cv::Point2f aux(B.mean.x, XSIZEIMAGE);
				int count = 0;
				for (int i = 0; i < A.contorn.size(); i++)
				{
					if (InterseccioSegments(A.contorn[i], A.contorn[modval(i + 1, A.contorn.size())], B.mean, aux))
					{
						count++;
					}
				}
				if (count % 2 > 0)
				{
					file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
					CString strText2;
					strText2.Format("hei2 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
					file.WriteString(strText);
					file.Close();
					return true;
				}
			}
			else
			{
				cv::Point2f aux(A.mean.x, XSIZEIMAGE);
				int count = 0;
				for (int i = 0; i < B.contorn.size(); i++)
				{
					if (InterseccioSegments(B.contorn[i], B.contorn[modval(i + 1, B.contorn.size())], A.mean, aux))
					{
						count++;
					}
				}
				if (count % 2 > 0)
				{
					file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
					CString strText2;
					strText2.Format("hei3 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
					file.WriteString(strText);
					file.Close();
					return true;
				}
			}
			for (int i = 0; i < A.contorn.size(); i++)
			{
				for (int j = 0; j < B.contorn.size(); j++)
				{
					if (InterseccioSegments(A.contorn[i], A.contorn[modval(i + 1, A.contorn.size())], B.contorn[j], B.contorn[modval(j + 1, B.contorn.size())]))
					{
						file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
						CString strText2;
						strText2.Format("hei4 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
						file.WriteString(strText);
						file.Close();
						return true;
					}
				}
			}
			file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
			CString strText2;
			strText2.Format("hei5 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
			file.WriteString(strText);
			file.Close();
			return false;
		}
	}
	else if ((A.size <= 300) && (B.size > 300))
	{
		if (normCvPoint2f(A.mean - B.mean) < sqrt(B.size) / 5.0)
		{
			file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
			CString strText2;
			strText2.Format("hei6 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
			file.WriteString(strText);
			file.Close();
			return true;
		}
		else
		{
			file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
			CString strText2;
			strText2.Format("hei7 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
			file.WriteString(strText);
			file.Close();
			return false;
		}
	}
	else if ((A.size > 300) && (B.size <= 300))
	{
		if (normCvPoint2f(A.mean - B.mean) < sqrt(A.size) / 5.0)
		{
			file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
			CString strText2;
			strText2.Format("hei8 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
			file.WriteString(strText);
			file.Close();
			return true;
		}
		else
		{
			file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
			CString strText2;
			strText2.Format("hei9 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
			file.WriteString(strText);
			file.Close();
			return false;
		}
	}
	else
	{
		if (normCvPoint2f(A.mean - B.mean) < sqrt(A.size / 3.0) + sqrt(B.size / 3.0))
		{
			file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
			CString strText2;
			strText2.Format("hei10 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
			file.WriteString(strText);
			file.Close();
			return true;
		}
		else
		{
			file.Open("./expedienteX2.txt", CFile::modeCreate | CFile::modeWrite);
			CString strText2;
			strText2.Format("hei11 %lf %lf %f %f %f %f\n", sqrt222, sqrt111, A.mean.x, A.mean.y, B.mean.x, B.mean.y);
			file.WriteString(strText);
			file.Close();
			return false;
		}			
	}
}

void CDemoFetalDlg::CalcularCadenaTaques(std::vector<tacaStruct> & novaTaca)
{
	bool trobat = false;

	for (int i = 0; i < indUltTaques.size(); i++)
	{
		indUltTaques[i].trobat = false;
		std::vector<tacaStruct> auxTaca = std::vector<tacaStruct>(0);
		ultTaques[i].push_back(auxTaca);
	}
	for (int ll = 0; ll < novaTaca.size(); ll++)
	{
		trobat = false;
		for (int i = 0; i < ultTaques.size(); i++)
		{
			for (int j = (int)ultTaques[i].size() - 2; j >= max((int)ultTaques[i].size() - 1 - SIZEBUSCARULTIMESTAQUES,0); j--)
			{
				for (int k = 0; k < ultTaques[i][j].size(); k++)
				{
					if (InterseccioTaques(ultTaques[i][j][k], novaTaca[ll]))
					{
						trobat = true;
						indUltTaques[i].trobat = true;
						ultTaques[i][ultTaques[i].size() - 1].push_back(novaTaca[ll]);
						break;
					}
				}
				if (trobat)
					break;
			}
			if (trobat)
				break;
		}
		if (!trobat)
		{
			std::vector<std::vector<tacaStruct>> auxTaca = std::vector<std::vector<tacaStruct>>(0);
			std::vector<tacaStruct> auxTaca2 = std::vector<tacaStruct>(0);
			auxTaca2.push_back(novaTaca[ll]);
			auxTaca.push_back(auxTaca2);
			ultTaques.push_back(auxTaca);
			indexosTaca nTaca;
			nTaca.nNuls = 0;
			nTaca.trobat = true;
			nTaca.igualNTaques = 0;
			indUltTaques.push_back(nTaca);
		}
	}

	int esborrats = 0;
	int sizeInicial = indUltTaques.size();
	for (int i = 0; i < sizeInicial - esborrats; i++)
	{
		if (indUltTaques[i].trobat)
		{
			indUltTaques[i].nNuls = 0;
		}
		else
		{
			indUltTaques[i].nNuls++;
			if (indUltTaques[i].nNuls > SIZEBUSCARULTIMESTAQUES)
			{
				indUltTaques.erase(indUltTaques.begin() + i);
				ultTaques.erase(ultTaques.begin() + i);
				esborrats++;
				i--;
				continue;
			}
		}
		if (ultTaques[i].size() > SIZETAQUES)
		{
			ultTaques[i].erase(ultTaques[i].begin());
		}
	}

	int afegits = 0;
	sizeInicial = ultTaques.size();
	for (int i = 0; i < sizeInicial + afegits; i++)
	{
		if (ultTaques[i].size() < 2)
		{
			continue;
		}
		if (ultTaques[i][ultTaques[i].size() - 1].size() == ultTaques[i][ultTaques[i].size() - 2].size())
		{
			if (ultTaques[i][ultTaques[i].size() - 1].size() == 1)
			{
				indUltTaques[i].igualNTaques = 0;
				continue;
			}
			indUltTaques[i].igualNTaques++;
			if (indUltTaques[i].igualNTaques >= SIZETAQUES)
			{
				int afegitsAux = ultTaques[i][SIZETAQUES - 1].size();
				for (int j = 0; j < afegitsAux; j++)
				{
					std::vector<std::vector<tacaStruct>> auxTaca = std::vector<std::vector<tacaStruct>>(0);
					for (int k = 0; k < SIZETAQUES; k++)
					{
						std::vector<tacaStruct> auxTaca2 = std::vector<tacaStruct>(0);
						auxTaca2.push_back(ultTaques[i][k][j]);
						auxTaca.push_back(auxTaca2);
					}
					ultTaques.insert(ultTaques.begin() + i + j + 1, auxTaca);
					
					indexosTaca nTaca;
					nTaca.nNuls = 0;
					nTaca.trobat = false;
					nTaca.igualNTaques = 0;
					indUltTaques.insert(indUltTaques.begin() + i + j + 1, nTaca);
				}
				indUltTaques.erase(indUltTaques.begin() + i);
				ultTaques.erase(ultTaques.begin() + i);
				afegits += afegitsAux - 1;
			}
		}
		else
		{
			indUltTaques[i].igualNTaques = 0;
		}
	}

	lastTaques[indlastTaques] = novaTaca;
	indlastTaques++;
	if (indlastTaques >= SIZETAQUES)
	{
		indlastTaques = 0;
	}
}

tacaStruct CDemoFetalDlg::creixementRegio(cv::Mat & imatge, cv::Mat & mascara, int x, int y, int lbl)
{
	tacaStruct resultat;
	resultat.points = std::vector<cv::Point2i>(0);
	resultat.mean = cv::Point2f(0.0, 0.0);
	resultat.distCercle = 1000000000000.0;
	std::vector<cv::Point2i> cua;
	cv::Point2i pt = cv::Point2i(x, y);
	resultat.points.push_back(pt);
	cua.push_back(pt);
	uint8_t *imData = imatge.data;
	uint8_t *masData = mascara.data;
	int valInicial = evaluarMat(imData, y, x, imatge.cols);
	//int DD[2][4] = { { -1, 0, 1, 0 },{ 0, -1, 0, 1 } };
	int DD[2][8] = { { -1, -1, -1, 0, 1, 1, 1, 0 },{ -1, 0, 1, 1, 1, 0, -1, -1 } };
	int nPunts = 1;
	int trobatlbl, trobatblanc;
	masData[y * mascara.cols + x] = lbl;
	double dist;
	int xvals[2] = { 1000000,-1 };
	int yvals[2] = { 1000000,-1 };
	while (cua.size() > 0)
	{
		pt = cua.back();
		cua.pop_back();
		for (int i = 0; i < 8; ++i)
		{
			if ((pt.x + DD[0][i] > -1) && (pt.y + DD[1][i] > -1) && (pt.x + DD[0][i] < mascara.cols) && (pt.y + DD[1][i] < mascara.rows))
			{
				if ((evaluarMat(imData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) > 0) && (evaluarMat(masData, (pt.y + DD[1][i]), (pt.x + DD[0][i]), imatge.cols) == 0))
				{
					cv::Point2i ptr = cv::Point2i(pt.x + DD[0][i], pt.y + DD[1][i]);
					cua.push_back(ptr);
					masData[(pt.y + DD[1][i]) * imatge.cols + (pt.x + DD[0][i])] = lbl;
					resultat.points.push_back(ptr);
					dist = -sqrt((pt.x - paramCercle[0])*(pt.x - paramCercle[0]) + (pt.y - paramCercle[1])*(pt.y - paramCercle[1])) + paramCercle[2];
					if (dist < resultat.distCercle)
						resultat.distCercle = dist;
					if (pt.x > xvals[1])
						xvals[1] = pt.x;
					if (pt.x < xvals[0])
						xvals[0] = pt.x;
					if (pt.y > yvals[1])
						yvals[1] = pt.y;
					if (pt.y < yvals[0])
						yvals[0] = pt.y;
					nPunts++;
				}
					
			}
		}
	}

	for (int i = 0; i < resultat.points.size(); i++)
	{
		resultat.mean.x += (float)resultat.points[i].x;
		resultat.mean.y += (float)resultat.points[i].y;
	}
	resultat.mean /= (float)resultat.points.size();
	resultat.size = resultat.points.size();
	resultat.minX = xvals[0];
	resultat.maxX = xvals[1];
	resultat.minY = yvals[0];
	resultat.maxY = yvals[1];

	resultat.contorn = std::vector<cv::Point2i>(SIZECONTORN);
	for (int i = 0; i < SIZECONTORN; i++)
	{
		resultat.contorn[i].x = floor(resultat.mean.x);
		resultat.contorn[i].y = floor(resultat.mean.y);
	}
	double radaux, angleaux;
	int indAux;
	double radis[SIZECONTORN];
	for (int i = 0; i < SIZECONTORN; i++)
	{
		radis[i] = 0;
	}

	cv::Point2f ptAux;
	for (int i = 0; i < resultat.points.size(); i++)
	{
		ptAux = (cv::Point2f)resultat.points[i] - resultat.mean;
		radaux = sqrt(ptAux.x*ptAux.x + ptAux.y*ptAux.y);
		angleaux = atan2(ptAux.y, ptAux.x);
		indAux = modval(floor(SIZECONTORN*(angleaux + M_PI) / 2.0 / M_PI), SIZECONTORN);
		if (radis[indAux] < radaux)
		{
			radis[indAux] = radaux;
			resultat.contorn[indAux] = resultat.points[i];
		}
	}
	return resultat;
}


void CDemoFetalDlg::OnBnClickedButprova()
{
	////INI DEBUG: MIRAR SEGMENTACIO

	_LARGE_INTEGER StartingTime, EndingTime, ElapsedMiliseconds;
	LARGE_INTEGER Frequency;
	QueryPerformanceFrequency(&Frequency);
	CString strText;

	CStdioFile file;

	file.Open("./timeframe.txt", CFile::modeCreate | CFile::modeWrite);

	//CString path = _T("C:\\Users\\UPC-ESAII\\Mega\\Projectes\\DemoFetal\\DemoFetalJuliol2018\\video\\imatge_video004_006.png");
	CString path = _T("C:\\Users\\UPC-ESAII\\Desktop\\provaNula\\merdatotal.avi");

	CString pathD = "C:\\Users\\UPC-ESAII\\Desktop\\provaNula\\merdatotal\\";
	CString nameImage;

	VideoCapture cap;

	cap.open(path.GetString());

	if (!cap.isOpened())
	{
		AfxMessageBox("Error Opening Video");
		exit(-1);
	}

	indSegmentacio = -1;

	

	//cv::Mat src = imread(path.GetString(), CV_LOAD_IMAGE_COLOR);
	cv::Mat src2;
	cv::Mat src;
	cv::Mat src3;
	int iframe = 0;
	while (true)
	{

		QueryPerformanceCounter(&StartingTime);
		cap >> src;

		if (src.empty()) break; // Ran out of film


		//Segmentacio(src, src2);

		//std::vector<tacaStruct> keypoints;
		//BuscarTaques(src2, keypoints);
		//
		//CalcularCadenaTaques(keypoints);

		////DEGUG
		//iframe++;
		//if (iframe < 827)
		//{
		//	continue;
		//}
		//iframe--;
		////END DEBUG

		//QueryPerformanceCounter(&EndingTime);
		//ElapsedMiliseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
		//ElapsedMiliseconds.QuadPart *= 1000;// 1000000;
		//ElapsedMiliseconds.QuadPart /= Frequency.QuadPart;
		//strText.Format("%d %d\n", iframe, ElapsedMiliseconds.QuadPart);
		//file.WriteString(strText);

		//for (int i = 0; i < keypoints.size(); i++)
		//{
		//	if (keypoints[i].points.size() > 100.0)
		//	{
		//		uint8_t *imData = src.data;
		//		for (int ii = -2; ii < 3; ii++)
		//			for (int jj = -2; jj < 3; jj++)
		//			{
		//				imData[(((int)keypoints[i].mean.y + jj) * src.cols + (int)keypoints[i].mean.x + ii) * 3] = 0;
		//				imData[(((int)keypoints[i].mean.y + jj) * src.cols + (int)keypoints[i].mean.x + ii) * 3 + 1] = 0;
		//				imData[(((int)keypoints[i].mean.y + jj) * src.cols + (int)keypoints[i].mean.x + ii) * 3 + 2] = 0;
		//			}
		//	}
		//}

		//cv::Point2f auxmean;
		//for (int i = 0; i < ultTaques.size(); i++)
		//{
		//	auxmean = CalcularMeanCadenaTaca(ultTaques[i]);
		//	uint8_t *imData = src.data;
		//	for (int ii = -2; ii < 3; ii++)
		//		for (int jj = -2; jj < 3; jj++)
		//		{
		//			imData[(((int)auxmean.y + jj) * src.cols + (int)auxmean.x + ii) * 3] = 255;
		//			imData[(((int)auxmean.y + jj) * src.cols + (int)auxmean.x + ii) * 3 + 1] = 255;
		//			imData[(((int)auxmean.y + jj) * src.cols + (int)auxmean.x + ii) * 3 + 2] = 255;
		//		}

		//}

		if (iframe < 9)
			nameImage.Format("Imatge_0000%d.png", iframe + 1);
		else if (iframe < 99)
			nameImage.Format("Imatge_000%d.png", iframe + 1);
		else if (iframe < 999)
			nameImage.Format("Imatge_00%d.png", iframe + 1);
		else if (iframe < 9999)
			nameImage.Format("Imatge_0%d.png", iframe + 1);
		else
			nameImage.Format("Imatge_%d.png", iframe + 1);
		nameImage = pathD + nameImage;
		cv::imwrite(nameImage.GetString(), src);




		/*cv::cvtColor(src2, src3, CV_GRAY2RGB);

		for (int i = 0; i < keypoints.size(); i++)
		{
			if (keypoints[i].points.size() > 100.0)
			{
				uint8_t *imData = src3.data;
				for (int ii = -2; ii < 3; ii++)
					for (int jj = -2; jj < 3; jj++)
					{
						imData[(((int)keypoints[i].mean.y + jj) * src3.cols + (int)keypoints[i].mean.x + ii) * 3] = 0;
						imData[(((int)keypoints[i].mean.y + jj) * src3.cols + (int)keypoints[i].mean.x + ii) * 3 + 1] = 0;
						imData[(((int)keypoints[i].mean.y + jj) * src3.cols + (int)keypoints[i].mean.x + ii) * 3 + 2] = 255;
					}
			}
		}

		for (int i = 0; i < ultTaques.size(); i++)
		{
			auxmean = CalcularMeanCadenaTaca(ultTaques[i]);
			uint8_t *imData = src3.data;
			for (int ii = -2; ii < 3; ii++)
				for (int jj = -2; jj < 3; jj++)
				{
					imData[(((int)auxmean.y + jj) * src3.cols + (int)auxmean.x + ii) * 3] = 255;
					imData[(((int)auxmean.y + jj) * src3.cols + (int)auxmean.x + ii) * 3 + 1] = 255;
					imData[(((int)auxmean.y + jj) * src3.cols + (int)auxmean.x + ii) * 3 + 2] = 255;
				}

		}

		if (iframe < 9)
			nameImage.Format("ImatgeSeg_000%d.png", iframe + 1);
		else if (iframe < 99)
			nameImage.Format("ImatgeSeg_00%d.png", iframe + 1);
		else if (iframe < 999)
			nameImage.Format("ImatgeSeg_0%d.png", iframe + 1);
		else
			nameImage.Format("ImatgeSeg_%d.png", iframe + 1);
		nameImage = pathD + nameImage;
		cv::imwrite(nameImage.GetString(), src3);*/
		iframe++;


		/*cv::namedWindow("Display window2", WINDOW_AUTOSIZE);
		cv::imshow("Display window2", src3);
		cv::waitKey(5);*/
	}
	file.Close();

	cv::namedWindow("Display window1", WINDOW_AUTOSIZE);
	//cv::imshow("Display window1", src);
	cv::waitKey(1);
	////END DEBU: MIRAR SEGMENTACIO
}
