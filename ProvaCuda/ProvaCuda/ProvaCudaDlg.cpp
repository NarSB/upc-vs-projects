
// ProvaCudaDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "ProvaCuda.h"
#include "ProvaCudaDlg.h"
#include "afxdialogex.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cuadro de di�logo de CProvaCudaDlg

const int arraySize = 1 << 20;
int vecA[arraySize];
int vecB[arraySize];
int vecC[arraySize];

CProvaCudaDlg::CProvaCudaDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PROVACUDA_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CProvaCudaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CProvaCudaDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CProvaCudaDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// Controladores de mensaje de CProvaCudaDlg

BOOL CProvaCudaDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Establecer el icono para este cuadro de di�logo.  El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// TODO: agregar aqu� inicializaci�n adicional

	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono.  Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CProvaCudaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CProvaCudaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


extern cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);

extern cudaError_t ZhangSuenGPU(cv::Mat& src, cv::Mat& dst);

void CProvaCudaDlg::OnBnClickedButton1()
{
	CString filename = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\RegistreElipse\\RegistreElipse\\Imatges\\Raw\\Imatge_011.png";
	cv::Mat src = imread(filename.GetString(), CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat src2;
	cudaError_t cudaStatus = ZhangSuenGPU(src, src2);

	CString nameImage = "C:\\Users\\UPC-ESAII\\Mega\\Projectes\\RegistreElipse\\RegistreElipse\\Imatges\\Process\\ImatgeGPU.png";
	imwrite(nameImage.GetString(), src2);

	/*for (int i = 0; i < arraySize; i++)
	{
		vecA[i] = 1;
		vecB[i] = 2;
	}

	// Add vectors in parallel.
	cudaError_t cudaStatus = addWithCuda(vecC, vecA, vecB, arraySize);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addWithCuda failed!");
	}
	int sumV = 0;
	for (int i = 0; i < arraySize; i++)
	{
		sumV += (vecC[i] - 3);
	}

	TRACE("Valor de suma: %d\n",sumV);

	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
	}*/
	
}
