
// Video2FramesDlg.h : header file
//

#pragma once
#include <string>
#include <tchar.h>


// CVideo2FramesDlg dialog
class CVideo2FramesDlg : public CDialogEx
{
// Construction
public:
	CVideo2FramesDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VIDEO2FRAMES_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString m_str_tpath;
	afx_msg void OnBnClickedButbrowse();
	afx_msg void OnBnClickedButv2f();
};
