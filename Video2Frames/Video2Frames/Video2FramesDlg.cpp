
// Video2FramesDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "Video2Frames.h"
#include "Video2FramesDlg.h"
#include "afxdialogex.h"
#include <opencv2/opencv.hpp> 
#include <opencv2/core/core.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CVideo2FramesDlg dialog



CVideo2FramesDlg::CVideo2FramesDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_VIDEO2FRAMES_DIALOG, pParent)
	, m_str_tpath(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVideo2FramesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TEXTPATH, m_str_tpath);
}

BEGIN_MESSAGE_MAP(CVideo2FramesDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTBrowse, &CVideo2FramesDlg::OnBnClickedButbrowse)
	ON_BN_CLICKED(IDC_BUTV2F, &CVideo2FramesDlg::OnBnClickedButv2f)
END_MESSAGE_MAP()


// CVideo2FramesDlg message handlers

BOOL CVideo2FramesDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CVideo2FramesDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CVideo2FramesDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CVideo2FramesDlg::OnBnClickedButbrowse()
{
	// TODO: Add your control notification handler code here
	CFileDialog dlg(TRUE);
	/*dlg.m_ofn.nMaxFile = 511;

	dlg.m_ofn.lpstrFilter = _T("MP4 Files (*.mp4)\0*.mp4\0All Files (*.*)\0*.*\0\0");

	dlg.m_ofn.lpstrTitle = _T("Open video File");*/

	if (dlg.DoModal() == IDOK)
		m_str_tpath = dlg.GetPathName();

	
	UpdateData(false);
}


void CVideo2FramesDlg::OnBnClickedButv2f()
{
	// TODO: Add your control notification handler code here
	CFolderPickerDialog dlgFolder;
	if (m_str_tpath.GetLength() < 2)
	{
		return;
	}

	CString path;

	if (dlgFolder.DoModal() == IDOK)
		path = dlgFolder.GetFolderPath();

	std::string STDStr(CW2A(m_str_tpath.GetString()));
	//cv::VideoCapture cap("D:\\Dropbox\\Dropbox (Robotics)\\Albert\\Fetal\\Surgery Videos\\TTTS\\cirurgia ttts 200306.mp4");
	cv::VideoCapture cap(STDStr);
	if (!cap.isOpened())
	{
		std::cout << "Cannot open the video file" << std::endl;
		return;
	}
	double fps = cap.get(CV_CAP_PROP_FPS); //get the frames per seconds of the video

	std::cout << "Frame per seconds : " << fps << std::endl;

	int i = 1;
	std::string filename;
	while (true)
	{
		cv::Mat frame;
		bool bSuccess = cap.read(frame);
		if (!bSuccess)
		{
			break;
		}
		CString fname;
		if (i < 10)
		{
			fname.Format(_T("\\image_00000%d.png"), i);
		}
		else if (i < 100)
		{
			fname.Format(_T("\\image_0000%d.png"), i);
		}
		else if (i < 1000)
		{
			fname.Format(_T("\\image_000%d.png"), i);
		}
		else if (i < 10000)
		{
			fname.Format(_T("\\image_00%d.png"), i);
		}
		else if (i < 100000)
		{
			fname.Format(_T("\\image_0%d.png"), i);
		}
		else
		{
			fname.Format(_T("\\image_%d.png"), i);
		}
		fname = path + fname;
		std::string filename(CW2A(fname.GetString()));
		cv::imwrite(filename, frame);
		i++;		
	}
}
