
// Video2Frames.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CVideo2FramesApp:
// See Video2Frames.cpp for the implementation of this class
//

class CVideo2FramesApp : public CWinApp
{
public:
	CVideo2FramesApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CVideo2FramesApp theApp;
